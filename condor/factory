#!/bin/csh -f
#
# GENERAL SCRIPT FOR CONDOR MULTIPLE VISIT SUBMISSION
#       FOR THE IMAGE SIMULATION FACTORY
#
#    Written by John R Peterson (Purdue)
#

# requires output directory in trunk


# argument #1 is the factory orders
# argument #2 is the shipping location
# argument #3 is the kind of condor Universe

echo ' '
echo 'Image Simulation Factory'
echo ' '


set conerr=`condor_q | grep running | wc -l`

if ($conerr == 0) then 

    echo 'Condor Read Error'
    exit

endif

set output = ./output
set fpcondorscript = ./full_focalplane_condor_recheck
set filesys = `echo $cwd | grep lustreA | wc -w | awk '{if($1==0) print "scratch9"; else print "lustreA"}'`
set nchips = 378
set minsize0 = `echo "(370.8+102.0*$nchips)/1024" |bc -l | awk '{print int($1+0.5)}'` #in GB
set minsize1 = `echo "102.0*$nchips/1024" |bc -l | awk '{print int($1+0.5)}'`
set minfile0 = `echo "1989+4*$nchips+21+3*(21+4*$nchips)+20*$nchips" |bc -l`
set minfile1 = `echo "3*(21+4*$nchips)+20*$nchips" |bc -l`
 
set universe = $3

#set hname=`hostname | sed 's/\./ /' | sed 's/fe/ /' | awk '{print $2}'`
#set crun=`/usr/local/bin/condor_q $user | grep running | awk '{print $5}'`
#echo $crun > run$hname
#set cra=`cat run00`
#set crb=`cat run01`
#set crc=`cat run02`
#set crd=`cat run03`
#set tproc=`echo "$cra + $crb + $crc + $crd" | bc`
set tproc = `/usr/bin/condor_q $user | grep running | awk '{print $5}'`
#set tproc = `condor_status -run | grep $user |wc -l`
echo 'Active Processors:     '$tproc

myquota > tmp.tmp
set spaceused = `grep $filesys tmp.tmp | awk '{print $3, $4}' | awk 'BEGIN{FS="GB"}{print int($1)}'` # in GB
set fileused = `grep $filesys tmp.tmp | awk '{print $6}' | awk '{ gsub(/','/, ""); print int($1/1000)}'`
rm tmp.tmp
echo 'Open files:            '$fileused'k'
echo 'Space used:            '$spaceused'G'

set nvisits=`/bin/ls *dagman.out | wc -l`
echo 'Open visits:           '$nvisits

set prod=`find $output/*.tar -cmin -60 | wc -l`
set rate=`echo "$prod * 24 / $nchips" | bc`
echo 'Production rate:       '$rate' visits/day'
echo ' '

if (! -e flock) then 

    echo ' ' > flock

	#--------------------------------
	#FIRST SETUP SCRIPT IF FIRST TIME
	#--------------------------------

	rm -f new$1
	foreach f ( `cat $1` )
            set a=`echo $f | grep notsubmitted`
	    set b=`echo $f | grep running`
            set c=`echo $f | grep finished`
	    if ($a == '' && $b == '' && $c == '') then
                echo $f':notsubmitted' >> new$1
	    else
		echo $f >> new$1
	    endif
	end
	mv new$1 $1
	rm -f new$1

	#-------------------
	#LOOP THROUGH VISITS
	#-------------------

	rm -f new$1
        foreach f ( `cat $1` ) 

	    set q=`echo $f | sed 's/:notsubmitted//' | sed 's/:running//' | sed 's/:finished//'`
	    set a=`echo $f | grep notsubmitted`
	    set b=`echo $f | grep running`
	    set c=`echo $f | grep finished`
            
	    set instcat=`echo $q | cut -d ':' -f 1`
	    set commfile=`echo $q | cut -d ':' -f 2`
	    set obshistid=`cat $instcat default_instcat | grep Opsim_obshistid | head -n1 | awk '{print $2}'`
	    if ($commfile != '' && $commfile != 'none' ) then 
	        set extraid=`cat $commfile | grep extraid | awk '{print $2}'`
	        if ($extraid != '') then 
	            set obshistid = $obshistid$extraid
	        endif
	    endif

            if ($c == '') then 
            
                if ($a != '') then 
                    echo $q' is not submitted yet.'
                    echo $q':notsubmitted' >> new$1
                endif
                
                if ($b != '') then 
                    set compla=`sh -c "/bin/ls $output/*$obshistid*.fin 2>/dev/null" | wc -l`
                    set complb=`sh -c "/bin/ls $output/*$obshistid*.tar 2>/dev/null" | wc -l`
                    set percom=`echo "($compla + $complb) * 100 / $nchips" | bc`
                    echo $q' is running and '$percom'% complete.'
        
                    if (-e dag_$obshistid.dag.dagman.out) then 

                        #EXITING STATUS  
                        set fstatus = `grep 'STATUS ' dag_$obshistid.dag.dagman.out | head -n1 | awk '{print $11}'`
                    	
                        #STILL RUNNING
                        if ($fstatus == '') then
                            echo $q':running' >> new$1
                    		
                            #KILL ZOMBIE PROCESSES THAT HAVENT DONE ANYTHING FOR A WHILE OR ERRORS
                            set zombie = `grep 'since last' dag_$obshistid.dag.dagman.out | tail -1 | awk '{print $3}'`
                            
                            set nerr = `grep 'ERROR' dag_$obshistid.dag.dagman.out | wc -l`
                            
                            if ( $zombie > 18000 || $nerr > 200 ) then
                            	set prid=`grep exec dag_$obshistid.dag.dagman.out | head -1 | sed -e "s/_exec./ /" | awk '{print $5}'`
                            	set prrn=`/usr/bin/condor_q $user | grep dagman | awk '{print $1}' | grep $prid | wc -l`
                            	if ($prrn == 1) then 
                            		echo 'Killing process '$prid' with '$nerr' errors and '$zombie' seconds since last event.'
                            		/usr/bin/condor_rm $prid
                            	endif
                            endif
                    	
                        #FINISHED AND SUCCESS
                        else if ($fstatus == '0') then
                            #./movefiles $obshistid $output $2 &
                            ./cleanup $obshistid
                            echo $q':finished' >> new$1
                    	
                        #FINISHED BUT ERROR
                        else if (($fstatus == '1') || ($fstatus == '2')) then
                            if ($commfile != '' && $commfile != 'none' ) then 
                            	$fpcondorscript $instcat $output $universe $commfile
                            else
                            	$fpcondorscript $instcat $output $universe
                            endif
                            echo $q':running' >> new$1
                        endif
                    
                    else
                
                        #THINKS IT IS RUNNING BUT MYSTERIOUSLY IS NOT
                        #PROBABLY DUE TO SUBMISSION FAILURE?
                        if ($commfile != '' && $commfile != 'none' ) then 
                        	$fpcondorscript $instcat $output $universe $commfile
                        else
                        	$fpcondorscript $instcat $output $universe
                        endif
                        echo $q':running' >> new$1
                
                    endif
        
                endif
        
            else
                rm -f $output/*$obshistid*.fin >& /dev/null
                echo $q' is finished.'
                echo $q':finished' >> new$1
            endif

        end
	mv new$1 $1
	rm -f new$1


#------------------
#SUBMIT A NEW VISIT
#------------------

	rm -f new$1
        @ nsub = 0
        @ full = 0
	foreach f ( `cat $1` ) 
			
				
		set q=`echo $f | sed 's/:notsubmitted//' | sed 's/:running//' | sed 's/:finished//'`
		set a=`echo $f | grep notsubmitted`


		if ($a != '') then

		    #set fileused=`quota -Q | grep scratch95 | awk '{print $5}'`
		    #set spaceused=`quota -Q | grep scratch95 | awk '{print $2}'`
		    #set spacecalc=`echo "$spaceused / 1000000 + $fileused/2000" | bc`
                    if ( $full == 0 ) then 
                        myquota > tmp.tmp
                        set availsize = `grep $filesys tmp.tmp | awk '{print $3, $4}' | awk 'BEGIN{FS="GB"}{print int($2*1000-$1)}'` # in GB
                        set availfile = `grep $filesys tmp.tmp | awk '{print $6, $7}' | awk '{ gsub(/','/, ""); print $2-$1}'`
                        rm tmp.tmp
                        @ minsize = $minsize1 * $nsub + $minsize0
                        @ minfile = $minfile1 * $nsub + $minfile0
                    endif
				
		    #if (($fileused < 88000) && ($spacecalc < 250)) then 
		    if (($availsize > $minsize ) && ($availfile > $minfile ) && ($full == 0)) then 


			set instcat=`echo $q | cut -d ':' -f 1`
				
			set commfile=`echo $q | cut -d ':' -f 2`
			set obshistid=`cat $instcat default_instcat | grep Opsim_obshistid | head -n1 | awk '{print $2}'`
		
			if ($commfile != '' && $commfile != 'none' ) then 
			    set extraid=`cat $commfile | grep extraid | awk '{print $2}'`
			    if ($extraid != '') then 
				    set obshistid=$obshistid$extraid
			    endif
			endif
		
			if ($commfile != '' && $commfile != 'none' ) then 
				$fpcondorscript $instcat $output $universe $commfile
			else
				$fpcondorscript $instcat $output $universe
			endif
			@ full = 1
			echo $q':running' >> new$1
                        @ nsub++ 

		    else
			echo $f >> new$1
                        @ full = 1
		    endif
					
		else
		    echo $f >> new$1
		endif

	end
	mv new$1 $1
	rm -f new$1
	echo ' '
	rm -f flock

else

    echo 'Lock file exists'

endif

