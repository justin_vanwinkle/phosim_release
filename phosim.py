#!/usr/bin/env python3
##
# @package phosim.py
# @file phosim.py
# @brief python methods called by phosim script
##
# @brief Created by:
# @author John R. Peterson (Purdue)
##
# @brief Modified by:
# @author Emily Grace (Purdue)
# @author Nathan Todd (Purdue)
# @author En-Hsin Peng (Purdue)
# @author Glenn Sembroski (Purdue)
# @author Jim Chiang (SLAC)
# @author Jeff Gardner (Google)
# @author Colin Burke (Purdue)
##
# @warning This code is not fully validated
# and not ready for full release.  Please
# treat results with caution.
##
# The intention is that script is the only file necessary to run phosim for
# any purpose-- ranging from a single star on your laptop to full fields
# on large-scale computing clusters.  There is no physics in this
# script.  Its only purpose is to move files to the correct place.
# This file is called by the phosim script

from os.path import exists
from os.path import join
from os.path import abspath
from os.path import dirname
from os.path import isabs
from os.path import getsize
from os.path import split
from os import remove
from os import environ
from os import pathsep
from os import chdir
from os import linesep
from os import getenv

import subprocess
import sys
import glob
import shutil
import multiprocessing
import math


_version = 'PHOSIM a.b.c'


def pars_fn(name, number):
    return f'{name}_{number}.pars'


def jobChip(observationID,
            cid,
            eid,
            filt,
            nframes,
            nskip,
            ngroups,
            outputDir,
            binDir,
            instrDir,
            instrument='lsst',
            run_e2adc=True,
            run_ds9=False):
    "jobChip is a function that run an individual chip for a single exposure"
    fid = observationID + '_' + cid + '_' + eid
    segfile = join(instrDir, 'segmentation.txt')
    runProgram("raytrace < raytrace_" + fid + ".pars", binDir)
    removeFile(pars_fn('raytrace', fid))
    nsamples = nframes + nskip
    # try to find first image before we run e2adc program
    if (nsamples > 1 or ngroups > 1):  # sequence mode
        nstop = ngroups + 1  # include reference frame as "group 0"
        eImage = instrument + '_e_' + observationID + \
            '_f' + filt + '_' + cid + '_' + eid + '_R000.fits.gz'
    else:  # frame mode
        nstop = 1
        eImage = (instrument + '_e_' + observationID + '_f' + filt + '_' +
                  cid + '_' + eid + '.fits.gz')
    if exists(eImage):
        if run_e2adc:
            runProgram("e2adc < e2adc_" + fid + ".pars", binDir)
            removeFile(pars_fn('e2adc', fid))
            # move amplifier images to output directory
            for line in open(segfile):
                aid = line.split()[0]
                if cid in line and aid != cid:
                    for ngroup in range(nstop):
                        if (nstop == 1):
                            rawImage = (
                                instrument +
                                '_a_' +
                                observationID +
                                '_f' +
                                filt +
                                '_' +
                                aid +
                                '_' +
                                eid +
                                '.fits.gz')
                        else:
                            rawImage = (
                                instrument +
                                '_a_' +
                                observationID +
                                '_f' +
                                filt +
                                '_' +
                                aid +
                                '_' +
                                eid +
                                '_G' +
                                str(ngroup).zfill(3) +
                                '.fits.gz')
                        if exists(rawImage):
                            shutil.move(rawImage, join(outputDir, rawImage))
        # move electron images to output directory
        nframei = 0
        for nframe in range(ngroups * nsamples - nskip):
            if (nstop != 1):
                eImage = (
                    instrument +
                    '_e_' +
                    observationID +
                    '_f' +
                    filt +
                    '_' +
                    cid +
                    '_' +
                    eid +
                    '_R' +
                    str(nframe).zfill(3) +
                    '.fits.gz')
            if exists(eImage):
                shutil.move(eImage, join(outputDir, eImage))
            # do not simulate frame skips
            if nframei % nframes == 0:
                nframe += nskip
                framei = 0
        if run_ds9:
            fullpath = '/Applications:' + environ["PATH"]
            for path in fullpath.split(pathsep):
                if exists(join(path, "ds9")):
                    commandName = join(
                        path,
                        'ds9 -scale log ' + join(outputDir, eImage) + ' &')
                    runProgram(commandName)


def runProgram(command, binDir=None, argstring=None):
    """
    calls each of the phosim programs using subprocess.call
       it raises an exception and aborts if the return code is non-zero.
    """
    myCommand = command
    if binDir is not None:
        myCommand = join(binDir, command)
    if argstring is not None:
        myCommand += argstring

    print('** RUN:', myCommand)
    if subprocess.call(myCommand, shell=True) != 0:
        sys.exit(1)


def removeFile(filename):
    "delete file ignoring errors"
    print('** DELETE:', filename)
    try:
        remove(filename)
    except OSError:
        print('** NOTFOUND:', filename)


def assignPath(path, phosimDir):
    "figures out the whole path"
    if path == 'none':
        return 'none'
    full_path = join(phosimDir, path)
    if exists(full_path):
        return full_path
    elif exists(path):
        return abspath(path)
    raise RuntimeError('%s does not exist.' % path)


def checkPaths(opt, phosimDir):
    "checkPaths makes sure the paths make sense"
    for x in ['outputDir',
              'workDir',
              'binDir',
              'dataDir',
              'sedDir',
              'imageDir',
              'extraCommands']:
        exec('opt.%s=assignPath(opt.%s,phosimDir)' % (x, x))

    # Now find the instrument directory
    instrDir = join(opt.dataDir, opt.instrument)
    if not exists(instrDir):
        raise RuntimeError(
            'The instrument directory %s does not exist.' % instrDir)
    opt.instrDir = instrDir
    opt.instrument = instrDir.split("/")[-1]
    if len(opt.instrument) == 0:
        opt.instrument = instrDir.split("/")[-2]


class PhosimFocalplane(object):
    """
    PhosimFocalplane is a class for handling phosim files and directories
       for one focalplane.

    In order to perform all of the preprocessing steps, use doPreproc().  The
       raytrace steps can then be scheduled (exactly how depends on the 'grid'
       option) using scheduleRaytrace().  Once the computation is finished,
       intermediate files can be removed using cleanup().  Thus, an example
       workflow would be as follows:

       focalplane = PhosimFocalplane(phosimDir, outputDir, workDir, binDir,
                                     dataDir, instrDir, opt.grid, grid_opts)
       focalplane.doPreproc(instanceCatalog, extraCommands, sensor)
       focalplane.scheduleRaytrace(instrument, run_e2adc, keep_screens)
       focalplane.cleanup(keep_screens)

    Constructor for PhosimFocalplane
     grid:      'no', 'condor', 'cluster'
     grid_opts: A dictionary to supply grid options.  Exactly which options
     depends on the value of 'grid':
     'no':      'numproc' = Number of threads used to execute raytrace.
     'condor':  'universe' = Condor universe ('vanilla', 'standard', etc)
     'cluster': 'script_writer' = callback to generate raytrace batch scripts
                'submitter' = optional callback to submit the job"""

    def __init__(self, phosimDir, opt, grid_opts={}, visualize=False):
        self.phosimDir = phosimDir
        self.outputDir = opt.outputDir
        self.workDir = opt.workDir
        self.binDir = opt.binDir
        self.dataDir = opt.dataDir
        self.instrDir = opt.instrDir
        self.sedDir = opt.sedDir
        self.imageDir = opt.imageDir
        self.numthread = opt.numthread
        self.flatdir = False
        self.tarfile = False
        self.extraCommands = None
        self.instanceCatalog = None
        self.userCatalog = None
        self.chipID = None
        self.runFlag = None
        self.devmaterial = None
        self.devtype = None
        self.devvalue = None
        self.devmode = None
        self.grid = opt.grid
        self.grid_opts = grid_opts
        self.execEnvironmentInitialized = False
        self.eventfile = visualize
        if self.grid == 'condor':
            assert 'universe' in self.grid_opts
            self.flatdir = False
            if self.grid_opts['universe'] == 'vanilla':
                self.flatdir = True
        elif self.grid == 'diagrid':
            self.flatdir = True
            self.tarfile = True

    def doPreproc(self, instanceCatalog, extraCommands, sensor):
        """run all of the non-chip steps."""
        self.loadInstanceCatalog(instanceCatalog, extraCommands)
        chdir(self.workDir)
        self.writeInputParamsAndCatalogs()
        self.generateAtmosphere()
        self.generateInstrumentConfig()
        self.trimObjects(sensor)

    def loadInstanceCatalog(self, instanceCatalog, extraCommands):
        """parses the instance catalog"""
        self.instanceCatalog = instanceCatalog
        self.extraCommands = extraCommands
        self.nframes = 1
        self.nskip = 0
        defaultCatalog = open(join(
            self.phosimDir, 'default_instcat')).readlines()
        self.userCatalog = open(instanceCatalog).readlines()
        moveString = ''
        self.catgen = 0  # flag whether to generate some object catalogs
        for line in defaultCatalog + self.userCatalog:
            lstr = line.split()
            if "obshistid" in line:
                self.observationID = lstr[1]
            elif "moonra" in line:
                self.moonra = lstr[1]
            elif "moondec" in line:
                self.moondec = lstr[1]
            elif "sunalt" in line:
                self.solaralt = lstr[1]
            elif "moonalt" in line:
                self.moonalt = lstr[1]
            elif "dist2moon" in line:
                self.moondist = lstr[1]
            elif "moonphase" in line:
                self.phaseang = lstr[1]
            elif "mjd" in line:
                self.tai = lstr[1]
            elif "seeing" in line:
                self.constrainseeing = lstr[1]
            elif "airglow" in line:
                self.constrainairglow = lstr[1]
            elif "clouds" in line:
                self.constrainclouds = lstr[1]
            elif "rottelpos" in line:
                self.spiderangle = lstr[1]
            elif "Azimuth" in line or "azimuth" in line:
                self.azimuth = lstr[1]
            elif "Altitude" in line or "altitude" in line:
                self.altitude = lstr[1]
            elif "rotskypos" in line:
                self.rotationangle = lstr[1]
            elif "Unrefracted_RA" in line or "rightascension" in line:
                self.pointingra = lstr[1]
            elif "Unrefracted_Dec" in line or "declination" in line:
                self.pointingdec = lstr[1]
            elif "SEED" in line or "seed" in line:
                self.obsseed = lstr[1]
            elif "date" in line:
                self.monthnum = lstr[1].split('/')[1]
            elif "filter" in line:
                self.filt = lstr[1]
            elif "VISTIME" in line or "vistime" in line:
                self.vistime = float(lstr[1])
            elif "NSNAP" in line or "nsnap" in line:
                self.nsnap = int(float(lstr[1]))
            elif "NFRAMES" in line or "nframes" in line:
                self.nframes = int(float(lstr[1]))
            elif "NSKIP" in line or "nskip" in line:
                self.nskip = int(float(lstr[1]))
            elif "MINSOURCE" in line or "minsource" in line:
                self.minNumSources = int(float(lstr[1]))
            elif "CAMCONFIG" in line or "camconfig" in line:
                self.camconfig = int(float(lstr[1]))
            elif "DOMEINT" in line or "domeint" in line:
                self.domeint = float(lstr[1])
            elif "DOMEWAV" in line or "domewav" in line:
                self.domewav = float(lstr[1])
            elif "TELCONFIG" in line or "telconfig" in line:
                self.telconfig = int(float(lstr[1]))
            elif "TEMPERATURE" in line or "temperature" in line:
                self.temperature = lstr[1]
            elif "TEMPVAR" in line or "tempvar" in line:
                self.tempvar = lstr[1]
            elif "PRESSURE" in line or "pressure" in line:
                self.pressure = lstr[1]
            elif "PRESSVAR" in line or "pressvar" in line:
                self.pressvar = lstr[1]
            elif "OVERDEPBIAS" in line or "overdepbias" in line:
                self.overdepbias = lstr[1]
            elif "ALTVAR" in line or "altvar" in line:
                self.altvar = lstr[1]
            elif "CONTROL" in line or "control" in line:
                self.control = lstr[1]
            elif "move" in line:
                moveString += line.strip().split("move")[1]

            # check for any catalog generation commands
            elif "stars" in line:
                self.catgenStars = line.strip().split("stars")[1]
                self.catgen = 1
            elif "stargrid" in line:
                self.catgenStarGrid = line.strip().split("stargrid")[1]
                self.catgen = 1
            elif "galaxies" in line:
                self.catgenGalaxies = line.strip().split("galaxies")[1]
                self.catgen = 1
        self.actuator = moveString + '\n'
        self.throughputfile = 0
        self.centroidfile = 0
        self.opdfile = 0
        if extraCommands != 'none':
            # look for relevant physics commands
            for line in open(extraCommands):
                lstr = line.split()
                if "extraid" in line:
                    self.extraid = lstr[1]
                    self.observationID = self.observationID + self.extraid
                if "eventfile" in line:
                    self.eventfile = int(float(lstr[1]))
                if "throughputfile" in line:
                    self.throughputfile = int(float(lstr[1]))
                if "centroidfile" in line:
                    self.centroidfile = int(float(lstr[1]))
                if "opd" in line:
                    self.opdfile = int(float(lstr[1]))
            # catch problem with appending pars file without endline
            #   character in command file
            if line[-1] != linesep:
                print('Error: No endline character found in command file.')
                sys.exit()

    def writeInputParamsAndCatalogs(self):
        """encapsulate the two instance catalog processing functions"""
        self.writeInputParams()
        self.writeCatalogList()

    def writeInputParams(self):
        """
        takes some of the parsed input parameters out of the
          instance catalog and puts them in a single file.  We mainly have to
          read this in and write out the same file because the instance catalog
          format cannot change rapidly due to compatibility with opSim and
          catSim."""
        self.inputParams = pars_fn('obs', self.observationID)
        pfile = open(self.inputParams, 'w')
        pfile.write("obshistid %s\n" % self.observationID)
        if hasattr(self, 'moonra'):
            pfile.write("moonra %s\n" % self.moonra)
        if hasattr(self, 'moondec'):
            pfile.write("moondec %s\n" % self.moondec)
        if hasattr(self, 'solaralt'):
            pfile.write("solaralt %s\n" % self.solaralt)
        if hasattr(self, 'moonalt'):
            pfile.write("moonalt %s\n" % self.moonalt)
        if hasattr(self, 'moondist'):
            pfile.write("moondist %s\n" % self.moondist)
        if hasattr(self, 'phaseang'):
            pfile.write("phaseang %s\n" % self.phaseang)
        if hasattr(self, 'tai'):
            pfile.write("tai %s\n" % self.tai)
        if hasattr(self, 'azimuth'):
            pfile.write("azimuth %s\n" % self.azimuth)
        if hasattr(self, 'altitude'):
            pfile.write("altitude %s\n" % self.altitude)
        if hasattr(self, 'pointingra'):
            pfile.write("pointingra %s\n" % self.pointingra)
        if hasattr(self, 'pointingdec'):
            pfile.write("pointingdec %s\n" % self.pointingdec)
        if hasattr(self, 'monthnum'):
            pfile.write("monthnum %s\n" % self.monthnum)
        if hasattr(self, 'spiderangle'):
            pfile.write("spiderangle %s\n" % self.spiderangle)
        if hasattr(self, 'rotationangle'):
            pfile.write("rotationangle %s\n" % self.rotationangle)
        if hasattr(self, 'filt'):
            pfile.write("filter %s\n" % self.filt)
        if hasattr(self, 'catgenStars'):
            pfile.write("stars %s\n" % self.catgenStars)
        if hasattr(self, 'catgenStarGrid'):
            pfile.write("stargrid %s\n" % self.catgenStarGrid)
        if hasattr(self, 'catgenGalaxies'):
            pfile.write("galaxies %s\n" % self.catgenGalaxies)

        pfile.write("constrainseeing %s\n" % self.constrainseeing)
        pfile.write("constrainairglow %s\n" % self.constrainairglow)
        pfile.write("constrainclouds %s\n" % self.constrainclouds)
        pfile.write("altvar %s\n" % self.altvar)
        pfile.write("temperature %s\n" % self.temperature)
        pfile.write("tempvar %s\n" % self.tempvar)
        pfile.write("pressure %s\n" % self.pressure)
        pfile.write("pressvar %s\n" % self.pressvar)
        pfile.write("overdepbias %s\n" % self.overdepbias)
        pfile.write("move %s" % self.actuator)
        pfile.write("control %s\n" % self.control)
        pfile.write("obsseed %s\n" % self.obsseed)
        pfile.write("vistime %g\n" % self.vistime)
        pfile.write("camconfig %s\n" % self.camconfig)
        pfile.write("outputdir %s\n" % self.outputDir)
        pfile.write("seddir %s\n" % self.sedDir)
        pfile.write("imagedir %s\n" % self.imageDir)
        pfile.write("datadir %s\n" % self.dataDir)
        pfile.write("instrdir %s\n" % self.instrDir)
        pfile.write("bindir %s\n" % self.binDir)
        pfile.write("thread %d\n" % self.numthread)
        pfile.write("telconfig %d\n" % self.telconfig)
        pfile.write("minsource %d\n" % self.minNumSources)
        pfile.write("domelight %g\n" % self.domeint)
        pfile.write("domewave %g\n" % self.domewav)
        if self.flatdir:
            pfile.write("flatdir 1\n")
        if self.tarfile:
            pfile.write("tarfile 1\n")
        if self.eventfile:
            pfile.write("eventfile 1\n")
        pfile.close()

    def writeCatalogList(self):
        """
        simply makes a list of possible sub-catalogs (using the includeobj
        option) or lists of objects put in the instance catalog.  The former
        is useful for 1000s of objects, whereas the latter is useful for entire
        focalplanes (millions).  Hence we support both of these options.
        """
        assert self.instanceCatalog
        assert self.userCatalog
        has_object = False
        objectCatalog = open(pars_fn('objectcatalog', self.observationID), 'w')
        for line in self.userCatalog:
            if "object" in line:
                objectCatalog.write(line)
                has_object = True
        objectCatalog.close()

        has_opd = False
        opdCatalog = open(pars_fn('opdcatalog', self.observationID), 'w')
        for line in self.userCatalog:
            if "opd" in line:
                opdCatalog.write(line)
                has_opd = True
        opdCatalog.close()
        ncat = 0
        catalogList = open(pars_fn('catlist', self.observationID), 'w')
        if has_object:
            catalogList.write("catalog %d objectcatalog_%s.pars\n" %
                              (ncat, self.observationID))
            ncat = 1
        else:
            removeFile(pars_fn('objectcatalog', self.observationID))

        if has_opd:
            catalogList.write("catalogopd opdcatalog_%s.pars\n" %
                              (self.observationID))
            ncat += 1
        else:
            removeFile(pars_fn('opdcatalog', self.observationID))
        catDir = dirname(self.instanceCatalog)
        for line in self.userCatalog:
            if "includeobj" in line:
                path = join(catDir, line.split()[1])
                if not isabs(catDir):
                    path = join("..", path)
                catalogList.write("catalog %d %s\n" % (ncat, path))
                ncat += 1
        catalogList.close()

    def generateAtmosphere(self):
        """runs the atmosphere program"""
        assert self.inputParams
        assert self.extraCommands
        inputParams = pars_fn('obsExtra', self.observationID)
        pfile = open(inputParams, 'w')
        pfile.write(open(self.inputParams).read())
        if self.extraCommands != 'none':
            pfile.write(open(self.extraCommands).read())
        pfile.close()
        runProgram("atmosphere < " + inputParams, self.binDir)
        removeFile(inputParams)
        obsFile = pars_fn('obs', self.observationID)
        pfile = open(obsFile, 'r')
        for line in pfile:
            lstr = line.split()
            if "filter" in line:
                self.filt = lstr[1]

    def generateInstrumentConfig(self):
        """runs the instrument program"""
        assert self.inputParams
        assert self.extraCommands
        inputParams = pars_fn('obsExtra', self.observationID)
        pfile = open(inputParams, 'w')
        pfile.write(open(self.inputParams).read())
        if self.extraCommands != 'none':
            pfile.write(open(self.extraCommands).read())
        pfile.close()
        runProgram("instrument < " + inputParams, self.binDir)
        removeFile(inputParams)

    def generateSimulatedCatalogs(self):
        """
        Run a program that will generate user defined simulated catalogs.
          This can include random stars, and/or  a grid of stars and/or random
          galaxies. Options for this are specified in the input command file
        """

        # Have we been requested to make simulated catalogs?
        if self.catgen != 0:
            # All commnad and info need to make catalogs are contained
            # in the obs_*.pars file.  Now run it
            obsFile = pars_fn('obs', self.observationID)
            # print("about to run phosimcatgen")
            runProgram("phosimcatgen < " + obsFile, self.binDir)

            # We shoud now have a catalog file. Append it to the end of the
            # catlist file. First generate the ncat number of this
            # additional (or perhps only)  catalog
            catgenFile = 'catgen_' + self.observationID + '.cat'

            # We now need to make sure the catlist file exists (it should
            # I think,  but it may be empty
            catListFile = pars_fn('catlist', self.observationID)
            if exists(catListFile):
                if getsize(catListFile) > 0:
                    catalogList = open(catListFile)
                    lineList = catalogList.readlines()
                    if "catalog" == lineList[-1].split()[0]:
                        ncat = int(lineList[-1].split()[1])
                        ncat += 1
                    catalogList.close()
                else:
                    ncat = 0
            else:
                ncat = 0  # This is just in case

            # Append new catalog to existing catlist file or to a newly
            # created one
            catalogList = open(catListFile, 'a+')
            catalogList.write("catalog %d %s\n" % (ncat, catgenFile))
            catalogList.close()
            # And we are done.

    def trimObjects(self, sensors):
        """
        runs the trim program
          Note this is overly complicated because we want to allow the trimming
            on grid computing to be done in groups to reduce the I/O of sending
            the entire instance catalog for every chip.  This complex looping
            isn't necessary for desktops.
        """
        self.initExecutionEnvironment()

        camstr = "%03d" % int(float(bin(self.camconfig).split('b')[1]))
        if self.camconfig == 0:
            camstr = '111'
        fp = open(join(self.instrDir, "focalplanelayout.txt")).readlines()
        chipID = []
        runFlag = []
        devmaterial = []
        devtype = []
        devmode = []
        devvalue = []

        # Go through the focalplanelayout.txt filling up the arrays
        for line in fp:
            lstr = line.split()
            addFlag = 0
            if "Group0" in line and camstr[2] == '1':
                addFlag = 1
            elif "Group1" in line and camstr[1] == '1':
                addFlag = 1
            elif "Group2" in line and camstr[0] == '1':
                addFlag = 1
            if addFlag == 1:
                chipID.append(lstr[0])
                runFlag.append(1)
                devmaterial.append(lstr[6])
                devtype.append(lstr[7])
                devmode.append(lstr[8])
                devvalue.append(float(lstr[9]))

        # OPD
        chipID.append('opd')
        runFlag.append(1)
        devmaterial.append('silicon')
        devtype.append('CCD')
        devmode.append('frame')
        devvalue.append(0)

        # See if we limit ourselves to a specific set of chipID
        #   (seperated by "|").
        if sensors != 'all':
            lstr = sensors.split('|')
            for i in range(len(chipID)):
                runFlag[i] = 0
            for j in range(len(lstr)):
                for i in range(len(chipID)):
                    if lstr[j] == chipID[i]:
                        runFlag[i] = 1
                        break

        # OPD
        secondlastchip = chipID[-2]
        lastchip = chipID[-1]
        chipcounter1 = 0
        chipcounter2 = 0
        tc = 0
        i = 0
        trimJobID = []
        for cid in chipID:
            if chipcounter1 == 0:
                jobName = 'trim_' + self.observationID + '_' + str(tc)
                inputParams = jobName + '.pars'
                pfile = open(inputParams, 'w')

            pfile.write('chipid %d %s\n' % (chipcounter1, cid))
            chipcounter1 += 1
            if runFlag[i] == 1:
                chipcounter2 += 1
        # OPD
            # Do groups of 9 to reduce grid computing I/O
            if chipcounter1 == 9 or cid == lastchip or cid == secondlastchip:
                trimJobID.append('none')
                pfile.write(open(pars_fn('obs', self.observationID)).read())
                if self.flatdir:
                    for line in open(pars_fn('catlist', self.observationID)):
                        lstr = line.split()
                        pfile.write('%s %s %s\n' %
                                    (lstr[0], lstr[1], lstr[2].split('/')[-1]))
                else:
                    pfile.write(
                        open(pars_fn('catlist', self.observationID)).read())
                pfile.close()
                if chipcounter2 > 0:
                    if self.grid in ['no', 'cluster']:
                        runProgram("trim < " + inputParams, self.binDir)
                    elif self.grid == 'condor':
                        nexp = self.nsnap if devtype[i] == 'CCD' else int(
                            self.vistime / devvalue[i])
                        condor.writeTrimDag(self, jobName, tc, nexp)
                    elif self.grid == 'diagrid':
                        nexp = self.nsnap if devtype[i] == 'CCD' else int(
                            self.vistime / devvalue[i])
                        trimJobID[tc] = diagrid.writeTrimDag(
                            self, jobName, tc, nexp)
                    else:
                        sys.stderr.write('Unknown grid type: %s' % self.grid)
                        sys.exit(-1)
                if ((self.grid in ['no', 'cluster'] or
                     (self.grid in ['condor', 'diagrid'] and
                      chipcounter2 == 0))):
                    removeFile(inputParams)
                chipcounter1 = 0
                chipcounter2 = 0
                tc += 1
            i = i + 1
        self.chipID = chipID
        self.runFlag = runFlag
        self.devmaterial = devmaterial
        self.devtype = devtype
        self.devvalue = devvalue
        self.devmode = devmode
        self.trimJobID = trimJobID

    def scheduleRaytrace(self,
                         instrument='lsst',
                         run_e2adc=True,
                         keep_screens=False,
                         run_ds9=False,
                         run_phosimvisualizer=False):
        """sets up the raytrace & e2adc jobs and also figures out the
             numbers of exposures to perform."""

        assert self.extraCommands
        chipcounter1 = 0
        tc = 0
        counter = 0
        jobs = []
        rImageArg = ''  # list of eventfile paths for phosim visualizer
        i = 0

        observationID = self.observationID

        for cid in self.chipID:
            if self.runFlag[i] == 1:
                numSources = self.minNumSources
                if self.grid in ['no', 'cluster']:
                    numSources = len(
                        open('trimcatalog_' +
                             observationID +
                             '_' +
                             cid +
                             '.pars').readlines())
                    numSources = numSources - 2
                if numSources >= self.minNumSources:
                    nexp = self.nsnap
                    if ((self.devtype[i] == 'CMOS' and
                         self.devmode[i] == 'frame')):
                        nexp = int(self.vistime / self.devvalue[i])
                    ex = 0
                    while ex < nexp:
                        eid = "E%03d" % (ex)
                        fid = observationID + '_' + cid + '_' + eid
                        pfile = open(pars_fn('image', fid), 'w')
                        pfile.write("chipid %s\n" % cid)
                        pfile.write("exposureid %d\n" % ex)
                        pfile.write("nsnap %d\n" % nexp)
                        pfile.write("nframes %d\n" % self.nframes)
                        pfile.write("nskip %d\n" % self.nskip)
                        if self.nframes == 1 and self.nskip == 0:  # override
                            self.devmode[i] = 'frame'
                        if self.devmode[i] == 'frame':
                            self.ngroups = 1
                            self.nframes = 1
                            self.skip = 0
                            nsamples = self.nframes + self.nskip
                        else:
                            nsamples = self.nframes + self.nskip
                            self.ngroups = int(math.ceil(
                                (self.vistime + self.devvalue[i] * self.nskip)
                                /
                                (self.devvalue[i] * nsamples)))
                        pfile.close()

                        # PHOTON RAYTRACE
                        pfile = open(pars_fn('raytrace', fid), 'w')
                        pfile.write(open(pars_fn('obs', observationID)).read())
                        atmosphere_fn = pars_fn('atmosphere', observationID)
                        if exists(atmosphere_fn):
                            pfile.write(open(atmosphere_fn).read())
                        pfile.write(
                            open(pars_fn('optics', observationID)).read())
                        if (cid != 'opd'):
                            pfile.write(
                                open('chip_' +
                                     observationID +
                                     '_' +
                                     cid +
                                     '.pars').read())
                        pfile.write(open(pars_fn('image', fid)).read())
                        if self.extraCommands != 'none':
                            pfile.write(open(self.extraCommands).read())
                        if self.grid in ['no', 'cluster']:
                            pfile.write(
                                open('trimcatalog_' +
                                     observationID +
                                     '_' +
                                     cid +
                                     '.pars').read())
                        pfile.close()

                        # ELECTRONS TO ADC CONVERTER
                        if run_e2adc:
                            pfile = open(pars_fn('e2adc', fid), 'w')
                            pfile.write(
                                open(pars_fn('obs', observationID)).read())
                            if (cid != 'opd'):
                                pfile.write(
                                    open('readout_' +
                                         observationID +
                                         '_' +
                                         cid +
                                         '.pars').read())
                            if self.extraCommands != 'none':
                                pfile.write(open(self.extraCommands).read())
                            pfile.write(open(pars_fn('image', fid)).read())
                            pfile.close()

                        if self.grid == 'no':
                            p = multiprocessing.Process(
                                target=jobChip,
                                args=(observationID,
                                      cid,
                                      eid,
                                      self.filt,
                                      self.nframes,
                                      self.nskip,
                                      self.ngroups,
                                      self.outputDir,
                                      self.binDir,
                                      self.instrDir),
                                kwargs={'instrument': instrument,
                                        'run_e2adc': run_e2adc,
                                        'run_ds9': run_ds9})
                            jobs.append(p)
                            p.start()
                            counter += 1
                            if counter == self.grid_opts.get('numproc', 1):
                                for p in jobs:
                                    p.join()
                                counter = 0
                                jobs = []
                        elif self.grid == 'cluster':
                            if self.grid_opts.get('script_writer', None):
                                self.grid_opts['script_writer'](
                                    observationID,
                                    cid,
                                    eid,
                                    self.filt,
                                    self.outputDir,
                                    self.binDir,
                                    self.dataDir)
                            else:
                                sys.stderr.write(
                                    ('WARNING: No script_writer callback '
                                     'in grid_opts for grid "cluster".\n'))
                            if self.grid_opts.get('submitter', None):
                                self.grid_opts['submitter'](
                                    observationID, cid, eid)
                            else:
                                sys.stdout.write(
                                    ('No submitter callback in self.grid_opts '
                                     'for grid "cluster".\n'))
                        elif self.grid == 'condor':
                            condor.writeRaytraceDag(
                                self, cid, eid, tc, run_e2adc)
                        elif self.grid == 'diagrid':
                            diagrid.writeRaytraceDag(
                                self, cid, eid, tc, run_e2adc)

                        if run_phosimvisualizer:
                            rImage = join(
                                self.outputDir,
                                instrument +
                                '_r_' +
                                observationID + '_f' + self.filt +
                                '_' + cid + '_' + eid + '.fits')
                            rImageArg += rImage + ' '

                        removeFile(pars_fn('image', fid))
                        ex += 1

            chipcounter1 += 1
            if chipcounter1 == 9:
                tc += 1
                chipcounter1 = 0

            if self.grid in ['no', 'cluster']:
                if exists('trimcatalog_' + observationID +
                          '_' + cid + '.pars'):
                    removeFile(
                        'trimcatalog_' +
                        observationID +
                        '_' +
                        cid +
                        '.pars')
            removeFile('readout_' + observationID + '_' + cid + '.pars')
            removeFile('chip_' + observationID + '_' + cid + '.pars')
            i += 1

        removeFile(pars_fn('obs', observationID))
        if not keep_screens:
            removeFile(pars_fn('atmosphere', observationID))
        removeFile(pars_fn('optics', observationID))
        removeFile(pars_fn('catlist', observationID))

        if self.grid == 'no':
            for p in jobs:
                p.join()
        elif self.grid == 'condor':
            condor.submitDag(self)
        elif self.grid == 'diagrid':
            diagrid.submitDax(self)
        chdir(self.phosimDir)
        return rImageArg

    def initExecutionEnvironment(self):
        """for handling execution environment"""
        if self.execEnvironmentInitialized:
            return
        if self.grid == 'condor':
            self.initCondorEnvironment()
        elif self.grid == 'diagrid':
            self.initDiagridEnvironment()
        elif self.grid == 'cluster':
            self.initClusterEnvironment()
        self.execEnvironmentInitialized = True

    def cleanup(self, keep_screens, rImageArg):
        """method to delete files at end"""
        if self.grid in ['no', 'cluster']:
            chdir(self.workDir)
            removeFile(pars_fn('objectcatalog', self.observationID))
            removeFile(pars_fn('tracking', self.observationID))
            if not keep_screens:
                removeFile(pars_fn('airglowscreen', self.observationID))
                for f in glob.glob(
                        'atmospherescreen_' + self.observationID + '_*'):
                    removeFile(f)
                for f in glob.glob('cloudscreen_' + self.observationID + '_*'):
                    removeFile(f)
            else:
                f = pars_fn('atmosphere', self.observationID)
                shutil.move(f, join(self.outputDir, f))
                f = 'airglowscreen_' + self.observationID + '.fits.gz'
                shutil.move(f, join(self.outputDir, f))
                for f in glob.glob(
                        'atmospherescreen_' + self.observationID + '_*'):
                    shutil.move(f, join(self.outputDir, f))
                for f in glob.glob('cloudscreen_' + self.observationID + '_*'):
                    shutil.move(f, join(self.outputDir, f))
            if self.eventfile == 1:
                for f in glob.glob('*_r_' + self.observationID + '_*'):
                    shutil.move(f, join(self.outputDir, f))
            if self.throughputfile == 1:
                for f in glob.glob('throughput_*' + self.observationID + '_*'):
                    shutil.move(f, join(self.outputDir, f))
            if self.centroidfile == 1:
                for f in glob.glob('centroid_*' + self.observationID + '_*'):
                    shutil.move(f, join(self.outputDir, f))
            for f in glob.glob('opd*' + self.observationID + '_*'):
                shutil.move(f, join(self.outputDir, f))
            chdir(self.phosimDir)

            if rImageArg != '':
                commandName = (
                    'python ' +
                    join(
                        self.phosimDir,
                        'tools/phosim_visualizer/phosim_visualizer.py') +
                    ' ' + self.instrDir + '/ ' + rImageArg)
                runProgram(commandName)

    def initCondorEnvironment(self):
        """Condor method to setup directories"""
        sys.path.append(self.phosimDir + '/condor')
        global condor
        import condor
        condor.initEnvironment(self)

    def initDiagridEnvironment(self):
        sys.path.append(self.phosimDir + '/diagrid')
        global diagrid
        import diagrid
        diagrid.initEnvironment(self)

    def initClusterEnvironment(self):
        pass


def main():
    import argparse

    phosimDir = split(abspath(__file__))[0]
    defaultOutputDir = getenv('PHOSIM_OUTPUT_DIR', join(phosimDir, 'output'))
    defaultWorkDir = getenv('PHOSIM_WORK_DIR', join(phosimDir, 'work'))
    defaultBinDir = getenv('PHOSIM_BIN_DIR', join(phosimDir, 'bin'))
    defaultDataDir = getenv('PHOSIM_DATA_DIR', join(phosimDir, 'data'))
    defaultSedDir = getenv('PHOSIM_SED_DIR', join(phosimDir, 'data', 'SEDs'))
    defaultImageDir = getenv('PHOSIM_IMAGE_DIR',
                             join(phosimDir, 'data', 'images'))

    cpuCount = 1
    try:
        cpuCount = multiprocessing.cpu_count()
    except NotImplementedError:
        pass

    parser = argparse.ArgumentParser(description='Simulate Photons!')
    parser.add_argument('catalog',
                        help='catalog file')
    parser.add_argument('-c', '--command',
                        dest="extraCommands",
                        default="none",
                        help='command file to modify the default physics')
    parser.add_argument('-p', '--proc',
                        dest="numproc",
                        default=1,
                        type=int,
                        help='number of processors')
    parser.add_argument('-t', '--thread',
                        dest="numthread",
                        default=cpuCount,
                        type=int,
                        help='number of threads')
    parser.add_argument('-o', '--output',
                        dest="outputDir",
                        default=defaultOutputDir,
                        help='output image directory')
    parser.add_argument('-w', '--work',
                        dest="workDir",
                        default=defaultWorkDir,
                        help='temporary work directory')
    parser.add_argument('-b', '--bin',
                        dest="binDir",
                        default=defaultBinDir,
                        help='binary file directory')
    parser.add_argument('-d', '--data',
                        dest="dataDir",
                        default=defaultDataDir,
                        help='data directory')
    parser.add_argument('--sed',
                        dest="sedDir",
                        default=defaultSedDir,
                        help='SED file directory')
    parser.add_argument('--image',
                        dest="imageDir",
                        default=defaultImageDir,
                        help='truth image directory')
    parser.add_argument('-s', '--sensor',
                        dest="sensor",
                        default="all",
                        help=('sensor chip specification '
                              '(e.g., all, R22_S11, "R22_S11|R22_S12")'))
    parser.add_argument('-i', '--instrument',
                        dest="instrument",
                        default="lsst",
                        help='instrument site directory')
    parser.add_argument('-g', '--grid',
                        dest="grid",
                        default="no",
                        help='execute remotely (no, condor, cluster, diagrid)')
    parser.add_argument('-u', '--universe',
                        dest="universe",
                        default="standard",
                        help='condor universe (standard, vanilla)')
    parser.add_argument('-e', '--e2adc',
                        dest="e2adc",
                        default=1,
                        type=int,
                        help=('whether to generate amplifier images '
                              '(1 = true, 0 = false)'))
    parser.add_argument('--keepscreens',
                        dest="keepscreens",
                        default=0,
                        type=int,
                        help=('whether to keep atmospheric phase screens '
                              '(0 = false, 1 = true)'))
    parser.add_argument('--checkpoint',
                        dest="checkpoint",
                        default=12,
                        type=int,
                        help='number of checkpoints (condor only)')
    parser.add_argument('--ds9',
                        dest="ds9",
                        action="store_true",
                        help='whether to launch ds9 upon completion')
    parser.add_argument('--visualize',
                        dest="visualize",
                        action="store_true",
                        help=('whether to launch phosim visualizer '
                              'upon completion'))
    parser.add_argument('-v', '--version',
                        action='version',
                        version=_version,
                        help='prints the version')

    args = parser.parse_args()

    instanceCatalog = args.catalog

    checkPaths(args, phosimDir)

    grid_opts = {'numproc': args.numproc}
    if args.grid == 'condor':
        grid_opts = {'universe': args.universe, 'checkpoint': args.checkpoint}
    elif args.grid == 'diagrid':
        grid_opts = {'checkpoint': args.checkpoint}
    elif args.grid == 'cluster':
        grid_opts = {'script_writer': jobChip}

    # the entire phosim workflow follows:
    focalplane = PhosimFocalplane(
        phosimDir, args, grid_opts, args.visualize)
    focalplane.loadInstanceCatalog(instanceCatalog, args.extraCommands)
    chdir(args.workDir)
    focalplane.writeInputParamsAndCatalogs()
    focalplane.generateAtmosphere()
    focalplane.generateInstrumentConfig()
    focalplane.generateSimulatedCatalogs()
    focalplane.trimObjects(args.sensor)
    rImageArg = focalplane.scheduleRaytrace(args.instrument,
                                            bool(args.e2adc),
                                            bool(args.keepscreens),
                                            args.ds9,
                                            args.visualize)
    focalplane.cleanup(bool(args.keepscreens), rImageArg)


if __name__ == "__main__":
    main()
