# approximate data for optimizations for each filter configuration
#
# rows are each filter configuration
# columns are:
# (1): optics file filter configuration number (x in optics_x.txt)
# (2): name
# (3): minimum wavelength of each band (microns)
# (4): maximum wavelength of each band (microns)
# (5): nominal wavelength of each band (microns)
# (6): approximate plate scale (microns per degree)
#
#(1)(2)	(3)   (4)   (5)   (6)
0   u	0.30  1.20  0.36  180000.0
1   g	0.30  1.20  0.48  180000.0
2   r	0.30  1.20  0.62  180000.0
3   i	0.30  1.20  0.75  180000.0
4   z	0.30  1.20  0.87  180000.0
5   y	0.30  1.20  0.97  180000.0
