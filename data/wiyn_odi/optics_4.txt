# Column 0: Name
# Column 1: Type
# Column 2: Curvature R (mm)
# Column 3: Thickness dz (mm)
# Column 4: Outer Radius (mm)
# Column 5: Inner Radius (mm)
# Column 6: Conic Constant Kappa
# Column 7 - 14: Aspheric Coefficient a_3 - a_10 (a_n r^n in meters)
# Column 15: Coating file
# Column 16: Medium file
# (0)   	(1)     (2)             (3)     	(4)	    	(5)     (6)	    (7)  (8)  (9)  (10) (11) (12) (13) (14)(15) (16)     
primary  	mirror	12253.5		0.0		1750.016136328	0.0	-1.0708	    0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
secondary  	mirror	5332.5 		4203.735455319 606.5175192075	0.0	-3.74	    0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
tertiary  	mirror	0.0		-3728.335455319	577.5253210944	0.0	0.0	    0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
FCL1      	lens	-1412.838581522 0.0		253.5428	0.0	0.0         0.0  8.279908770168e-14  0.0  -1.771527190009e-19  0.0  1.983015591251e-24  0.0  -1.247769972800e-29 none HPFS_7980.txt
FCL2      	lens	-1181.324794648 0.0		253.5428	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
ADC11		lens	0.0		0.0		285.1631648913 	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none HPFS_7980.txt
ADC12  		lens 	0.0		0.0		284.1246174164 	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none PBL6Y_SCS2610.txt
ADC13		lens	0.0		0.0		283.5840941602 	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
ADC21  		lens 	0.0		0.0		280.8823934117 	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none HPFS_7980.txt
ADC22		lens	0.0		0.0		279.8345288695	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none PBL6Y_SCS2610.txt
ADC23		lens	0.0		0.0		279.2841712751	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
F1		filter	0.0		0.0		273.1145792683	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 odi_z.txt HPFS_7980.txt
F2		filter	0.0		0.0		272.6403590736	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none air
L21		lens	-811.7776969767 0.0		268.0656266598	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none HPFS_7980.txt
L22		lens	-2213.111848022	0.0		272.8776352605	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none vacuum
FPA		det	0.0		0.0		277.3257069075	0.0	0.0         0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 none vacuum