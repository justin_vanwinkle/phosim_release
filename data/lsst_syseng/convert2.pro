pro convert2,filename


readcol,filename,a,b

newfilename=filename+'new'
openw,1,newfilename


for angle=14.2,23.6+0.2,0.2 do begin
for i=0L,N_elements(a)-1 do begin
   nw=a(i)*cos((angle*!Pi/180.)/1.7)/cos((18.9*!Pi/180.)/1.7)
   g=where(abs(a-nw) eq min([abs(a-nw)]))
   printf,1,angle,a(i)/1000.0,b(g(0)),1.0-b(g(0))
endfor
endfor

close,1

end
