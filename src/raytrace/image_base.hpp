#ifndef PHOSIM_IMAGE_BASE_HPP
#define PHOSIM_IMAGE_BASE_HPP

#include "air.hpp"
#include "chip.hpp"
#include "coating.hpp"
#include "contamination.hpp"
#include "dust.hpp"
#include "fea.hpp"
#include "galaxy.hpp"
#include "grating.hpp"
#include "lock.hpp"
#include "medium.hpp"
#include "observation.hpp"
#include "obstruction.hpp"
#include "perturbation.hpp"
#include "photon.hpp"
#include "random.hpp"
#include "raytrace.hpp"
#include "screen.hpp"
#include "silicon.hpp"
#include "state.hpp"
#include "surface.hpp"

#include <fftw3.h>
#include <fitsio.h>
#include <fitsio2.h>
#include <vector>

using namespace fea;

class Image : public Observation {

  public:
    // objects and structures
    Galaxy galaxy;
    Dust dust;
    Surface surface;
    Coating coating;
    Silicon silicon;
    Air air;
    Perturbation perturbation;
    Screen screen;
    Medium medium;
    Obstruction obstruction;
    Contamination contamination;
    Chip chip;
    Grating *pGrating;
    State state;
    std::vector<Random> random;

    // setup and loop methods
    int sourceLoop();
    void photonLoop(long ssource, long thread, int finish);

    // thread and optimization
    int dynamicTransmissionOptimization(long k,
                                        long *lastSurface,
                                        long *preGhost,
                                        long waveSurfaceIndex,
                                        long straylightcurrent,
                                        Photon *aph);
    static void *threadFunction(void *voidArgs);
    Lock lock;
    long remain;
    long openthreads;
    int *openthread;

    // physics methods
    int getWavelengthTime(Photon *aph, long source);
    int domeSeeing(Vector *angle, Photon *aph);
    int tracking(Vector *angle, double time);
    int atmosphericDispersion(Vector *angle, Photon *aph, long layer);
    int largeAngleScattering(Vector *largeAngle, Photon *aph);
    int secondKick(Vector *largeAngle, Photon *aph);
    int diffraction(Vector *position,
                    Vector angle,
                    Vector *largeAngle,
                    Photon *aph);
    int samplePupil(Vector *position, long long ray, Photon *aph);
    int transmissionCheck(double transmission,
                          long surfaceIndex,
                          long waveSurfaceIndex,
                          Photon *aph);
    int
    transmissionPreCheck(long surfaceIndex, long waveSurfaceIndex, Photon *aph);
    int chooseSurface(long *newSurface, long *oldSurface, Photon *aph);
    int findSurface(Vector angle,
                    Vector position,
                    double *distance,
                    long surfaceIndex,
                    Photon *aph);
    int goldenBisectSurface(double a,
                            double b,
                            double c,
                            double *z,
                            Vector angle,
                            Vector position,
                            double *distance,
                            long surfaceIndex,
                            Photon *aph);
    int
    getIntercept(double x, double y, double *z, long surfaceIndex, Photon *aph);
    int getDeltaIntercept(
        double x, double y, double *zv, long surfaceIndex, Photon *aph);
    int bloom(int saturatedFlag, Photon *aph);
    void saturate(long source,
                  Vector *largeAngle,
                  Photon *aph,
                  double shiftX,
                  double shiftY);
    int photonSiliconPropagate(Vector *angle,
                               Vector *position,
                               double lambda,
                               Vector normal,
                               double dh,
                               long waveSurfaceIndex,
                               Photon *aph);
    int electronSiliconPropagate(Vector *angle, Vector *position, Photon *aph);
    int contaminationSurfaceCheck(Vector position,
                                  Vector *angle,
                                  long surfaceIndex,
                                  Photon *aph);
    double airIndexRefraction(Photon *aph, long layer);
    double surfaceCoating(double wavelength,
                          Vector angle,
                          Vector normal,
                          long surfaceIndex,
                          double *reflection,
                          Photon *aph);
    double cloudOpacity(long layer, Photon *aph);
    double cloudOpacityMoon(long layer, Photon *aph);
    double atmosphereOpacity(Vector angle, long layer, Photon *aph);
    double atmosphereOpacityMoon(Vector angle, long layer, Photon *aph);
    double fringing(Vector angle,
                    Vector normal,
                    double wavelength,
                    double nSi,
                    double thickness,
                    double meanFreePath,
                    int polarization);
    void getAngle(Vector *angle, double time, long source);
    void getDeltaAngle(Vector *angle,
                       Vector *position,
                       long source,
                       double *shiftX,
                       double *shiftY,
                       int thread,
                       int *initGal,
                       Photon *aph);
    void newRefractionIndex(long surfaceIndex, Photon *aph);
    void atmospherePropagate(
        Vector *position, Vector angle, long layer, int mode, Photon *aph);
    void atmosphereIntercept(Vector *position, long layer, Photon *aph);
    void atmosphereRefraction(Vector *angle, long layer, int mode, Photon *aph);
    void atmosphereDiffraction(Vector *angle, Photon *aph);
    void transform(Vector *angle, Vector *position, long surfaceIndex);
    void transformInverse(Vector *angle, Vector *position, long surfaceIndex);
    void
    interceptDerivatives(Vector *normal, Vector position, long surfaceIndex);
    void cosmicRays(long long *raynumber);

    // output
    void writeImageFile() {

        int status = 0;
        char tempstring[4096], tempstring2[4096], line[4096];
        fitsfile *fptr = nullptr;
        FILE *indafile;

        std::string filename =
            "!" + workdir + "/" + outputfilename + ".fits.gz";
        fitsCreateImage(&fptr, filename.c_str());

        fitsWriteKey(fptr, "CTYPE1", "RA---TAN", "");
        fitsWriteKey(fptr, "CRPIX1", -centerx / pixsize + pixelsx / 2, "");
        fitsWriteKey(fptr, "CRVAL1", pra / DEGREE, "");
        fitsWriteKey(fptr, "CTYPE2", "DEC--TAN", "");
        fitsWriteKey(fptr, "CRPIX2", -centery / pixsize + pixelsy / 2, "");
        fitsWriteKey(fptr, "CRVAL2", pdec / DEGREE, "");
        fitsWriteKey(fptr, "CD1_1", pixsize / platescale * cos(rotatez), "");
        fitsWriteKey(fptr, "CD1_2", pixsize / platescale * sin(rotatez), "");
        fitsWriteKey(fptr, "CD2_1", -pixsize / platescale * sin(rotatez), "");
        fitsWriteKey(fptr, "CD2_2", pixsize / platescale * cos(rotatez), "");
        fitsWriteKey(fptr, "RADESYS", "ICRS", "");
        fitsWriteKey(fptr, "EQUINOX", 2000.0, "");
        header(fptr);
        fitsWriteKey(fptr, "CREATOR", "PHOSIM", "");

        sprintf(tempstring, "%s/version", bindir.c_str());
        indafile = fopen(tempstring, "r");
        fgets(line, 4096, indafile);
        sscanf(line, "%s %s", tempstring, tempstring2);

        fitsWriteKey(fptr, "VERSION", tempstring2, "");

        fgets(line, 4096, indafile);
        sscanf(line, "%s", tempstring2);
        fclose(indafile);

        fitsWriteKey(fptr, "BRANCH", tempstring2, "");

        if (date)
            fits_write_date(fptr, &status);

        for (long i = 0; i < chip.nampx; i++) {
            for (long j = 0; j < chip.nampy; j++) {
                *(state.focal_plane_fl + chip.nampx * j + i) =
                    static_cast<float>(
                        *(state.focal_plane + chip.nampx * j + i));
            }
        }
        fitsWriteImage(fptr, chip.nampx, chip.nampy, state.focal_plane_fl);
    }

    void writeOPD() {

        int status;
        char tempstring[4096];
        long naxesa[2];
        fitsfile *faptr;

        status = 0;
        double chiefRayOpl = 0.0;
        long idx;
        long jdx;

        for (long k = 0; k < nsource; k++) {
            jdx = OPD_SCREEN_SIZE * OPD_SCREEN_SIZE * k;
            idx = jdx + (OPD_SCREEN_SIZE / 2) * OPD_SCREEN_SIZE +
                  (OPD_SCREEN_SIZE / 2);
            chiefRayOpl = state.opd[idx] / state.opdcount[idx];

            for (long i = 0; i < OPD_SCREEN_SIZE; i++) {
                for (long j = 0; j < OPD_SCREEN_SIZE; j++) {
                    idx = jdx + i * OPD_SCREEN_SIZE + j;
                    if (state.opdcount[idx] > 0) {
                        state.opd[idx] = (state.opd[idx] / state.opdcount[idx] -
                                          chiefRayOpl) *
                                         1000.0;
                    } else {
                        state.opd[idx] = 0.0;
                    }
                }
            }

            sprintf(tempstring,
                    "!%s/opd_%s_%ld.fits.gz",
                    workdir.c_str(),
                    obshistid.c_str(),
                    k);
            fits_create_file(&faptr, tempstring, &status);
            naxesa[0] = 1;
            naxesa[1] = 1;
            fits_create_img(faptr, DOUBLE_IMG, 2, naxesa, &status);
            naxesa[0] = OPD_SCREEN_SIZE;
            naxesa[1] = OPD_SCREEN_SIZE;
            fits_update_key(
                faptr, TLONG, (char *)"NAXIS1", &naxesa[0], nullptr, &status);
            fits_update_key(
                faptr, TLONG, (char *)"NAXIS2", &naxesa[1], nullptr, &status);
            fitsWriteKey(faptr, "UNITS", "MICRONS", "");
            fits_write_img(faptr,
                           TDOUBLE,
                           1,
                           OPD_SCREEN_SIZE * OPD_SCREEN_SIZE,
                           state.opd + jdx,
                           &status);
            fits_close_file(faptr, &status);
        }
    }

    void writeCheckpoint(int checkpointcount) {

        fitsfile *faptr;
        long naxes[2];
        int status;
        double *tempDynamicTransmission;
        Uint32 z = 0, w = 0, zg = 0, wg = 0;
        char tempstring[4096];

        tempDynamicTransmission = static_cast<double *>(
            calloc((natmospherefile * 2 + nsurf * 2 + 2) *
                       (maxwavelength - minwavelength + 1),
                   sizeof(double)));
        for (int i = 0; i < maxwavelength - minwavelength + 1; i++) {
            for (int j = 0; j < (natmospherefile * 2 + nsurf * 2 + 2); j++) {
                tempDynamicTransmission[i * (natmospherefile * 2 + nsurf * 2 +
                                             2) +
                                        j] =
                    state.dynamicTransmission[i * (natmospherefile * 2 +
                                                   nsurf * 2 + 2) +
                                              j];
            }
        }

        status = 0;
        std::ostringstream filename;
        filename << "!" << workdir << "/" << outputfilename << "_ckptdt_"
                 << checkpointcount << ".fits.gz";
        fits_create_file(&faptr, filename.str().c_str(), &status);
        naxes[0] = 1;
        naxes[1] = 1;
        fits_create_img(faptr, DOUBLE_IMG, 2, naxes, &status);
        naxes[0] = maxwavelength - minwavelength + 1;
        naxes[1] = natmospherefile * 2 + nsurf * 2 + 2;
        fits_update_key(
            faptr, TLONG, (char *)"NAXIS1", &naxes[0], nullptr, &status);
        fits_update_key(
            faptr, TLONG, (char *)"NAXIS2", &naxes[1], nullptr, &status);
        for (int i = 0; i < numthread; i++) {
            random[i].getSeed(&z, &w);
            galaxy.random[i].getSeed(&zg, &wg);
            sprintf(tempstring, "M_Z%4d", i);
            fits_update_key(faptr, TUINT, tempstring, &z, nullptr, &status);
            sprintf(tempstring, "M_W%4d", i);
            fits_update_key(faptr, TUINT, tempstring, &w, nullptr, &status);
            sprintf(tempstring, "G_Z%4d", i);
            fits_update_key(faptr, TUINT, tempstring, &zg, nullptr, &status);
            sprintf(tempstring, "G_W%4d", i);
            fits_update_key(faptr, TUINT, tempstring, &wg, nullptr, &status);
        }
        fits_write_img(faptr,
                       TDOUBLE,
                       1,
                       (natmospherefile * 2 + nsurf * 2 + 2) *
                           (maxwavelength - minwavelength + 1),
                       tempDynamicTransmission,
                       &status);
        fits_close_file(faptr, &status);

        free(tempDynamicTransmission);

        filename.str("");
        filename << "!" << workdir << "/" << outputfilename << "_ckptfp_"
                 << checkpointcount << ".fits.gz";
        fits_create_file(&faptr, filename.str().c_str(), &status);
        naxes[0] = 1;
        naxes[1] = 1;
        fits_create_img(faptr, FLOAT_IMG, 2, naxes, &status);
        naxes[0] = chip.nampx;
        naxes[1] = chip.nampy;
        fits_update_key(
            faptr, TLONG, (char *)"NAXIS1", &naxes[0], nullptr, &status);
        fits_update_key(
            faptr, TLONG, (char *)"NAXIS2", &naxes[1], nullptr, &status);
        fits_write_img(faptr,
                       TFLOAT,
                       1,
                       chip.nampx * chip.nampy,
                       state.focal_plane,
                       &status);
        fits_close_file(faptr, &status);
    }

    void readCheckpoint(int checkpointcount) {

        fitsfile *faptr;
        long naxes[2];
        int nfound;
        int anynull;
        float nullval;
        int status;
        double *tempDynamicTransmission;
        Uint32 z = 0, w = 0, zg = 0, wg = 0;
        char tempstring[4096];

        tempDynamicTransmission = static_cast<double *>(
            calloc((natmospherefile * 2 + nsurf * 2 + 2) *
                       (maxwavelength - minwavelength + 1),
                   sizeof(double)));

        std::ostringstream filename;
        filename << workdir << "/" << outputfilename << "_ckptdt_"
                 << checkpointcount - 1 << ".fits.gz";
        status = 0;
        if (fits_open_file(&faptr, filename.str().c_str(), READONLY, &status)) {
            printf("Error opening %s\n", filename.str().c_str());
            exit(1);
        }
        fits_read_keys_lng(
            faptr, (char *)"NAXIS", 1, 2, naxes, &nfound, &status);
        for (int i = 0; i < numthread; i++) {
            sprintf(tempstring, "M_Z%4d", i);
            fits_read_key(faptr, TUINT, tempstring, &z, nullptr, &status);
            sprintf(tempstring, "M_W%4d", i);
            fits_read_key(faptr, TUINT, tempstring, &w, nullptr, &status);
            sprintf(tempstring, "G_Z%4d", i);
            fits_read_key(faptr, TUINT, tempstring, &zg, nullptr, &status);
            sprintf(tempstring, "G_W%4d", i);
            fits_read_key(faptr, TUINT, tempstring, &wg, nullptr, &status);
            random[i].setSeed(z, w);
            galaxy.random[i].setSeed(zg, wg);
        }
        fits_read_img(faptr,
                      TDOUBLE,
                      1,
                      naxes[0] * naxes[1],
                      &nullval,
                      tempDynamicTransmission,
                      &anynull,
                      &status);
        fits_close_file(faptr, &status);

        for (int i = 0; i < maxwavelength - minwavelength + 1; i++) {
            for (int j = 0; j < (natmospherefile * 2 + nsurf * 2 + 2); j++) {
                state.dynamicTransmission[i * (natmospherefile * 2 + nsurf * 2 +
                                               2) +
                                          j] = tempDynamicTransmission
                    [i * (natmospherefile * 2 + nsurf * 2 + 2) + j];
            }
        }

        free(tempDynamicTransmission);

        filename.str("");
        filename << workdir << "/" << outputfilename << "_ckptfp_"
                 << checkpointcount - 1 << ".fits.gz";
        status = 0;
        if (fits_open_file(&faptr, filename.str().c_str(), READONLY, &status)) {
            printf("Error opening %s\n", filename.str().c_str());
            exit(1);
        }
        fits_read_keys_lng(
            faptr, (char *)"NAXIS", 1, 2, naxes, &nfound, &status);
        fits_read_img(faptr,
                      TFLOAT,
                      1,
                      naxes[0] * naxes[1],
                      &nullval,
                      state.focal_plane,
                      &anynull,
                      &status);
        fits_close_file(faptr, &status);
    }

    void cleanup() {
    }

    int atmSetup() {

        char tempstring[4096];
        std::string dir;

        if (flatdir == 0)
            dir = datadir + "/atmosphere";
        else if (flatdir == 1)
            dir = ".";

        sources.sedfilename.clear();
        sources.spatialname.clear();
        sources.dustname.clear();
        sources.dustnamez.clear();

        ranseed = obsseed + pairid;

        for (int i = 0; i < numthread; i++) {
            random.emplace_back(Random());
            galaxy.random.emplace_back(Random());
        }
        random.resize(numthread);
        galaxy.random.resize(numthread);
        if (obsseed == -1) {
            for (int i = 0; i < numthread; i++) {
                random[i].setSeedFromTime();
                galaxy.random[i].setSeedFromTime();
            }
        } else {
            for (int i = 0; i < numthread; i++) {
                random[i].setSeed32(ranseed + i * 100);
                galaxy.random[i].setSeed32(ranseed + 9999 + i * 100);
            }
        }

        for (int i = 0; i < numthread; i++) {
            random[i].unwind(100);
            galaxy.random[i].unwind(100);
        }

        if (devtype == "CMOS") {
            exptime = vistime / nsnap;
            timeoffset = 0.5 * exptime + pairid * (exptime)-0.5 * vistime;
        } else {
            exptime = (vistime - (nsnap - 1) * devvalue) / nsnap;
            timeoffset =
                0.5 * exptime + pairid * (devvalue + exptime) - 0.5 * vistime;
        }

        exptime = exptime + shuttererror * random[0].normal();

        // SKY SETUP
        char sersicdata[4096];
        if (flatdir == 0)
            sprintf(sersicdata, "%s/sky/sersic_const.txt", datadir.c_str());
        else if (flatdir == 1)
            sprintf(sersicdata, "sersic_const.txt");
        galaxy.sampleSersic(sersicdata);
        galaxy.sampleSersic2d(sersicdata, numthread);
        dust.setup(maxwavelength);
        filterTruncateSources();
        totalnorm = 0.0;
        for (long i = 0; i < nsource; i++) {
            totalnorm += sources.norm[i];
        }

        // ATMOSPHERIC SETUP
        for (long i = 0; i < natmospherefile; i++) {
            seefactor[i] = pow(1 / cos(zenith), 0.6) * seefactor[i];
        }

        screen.large_sizeperpixel = 5120.0;
        screen.coarse_sizeperpixel = 640.0;
        screen.medium_sizeperpixel = 80.0;
        screen.fine_sizeperpixel = 10.0;

        screen.wavelengthfactor_nom = pow(0.5, -0.2);

        // Air Setup
        if (atmospheremode) {
            fprintf(stdout, "Creating Air.\n");
            air.opacitySetup(zenith,
                             moonalt,
                             height,
                             groundlevel,
                             raynorm,
                             o2norm,
                             h2onorm,
                             o3norm,
                             aerosoltau,
                             aerosolindex,
                             natmospherefile,
                             dir,
                             &airmass);
        }

        // Diffraction Screens
        screen.phasescreen = new double[SCREEN_SIZE * SCREEN_SIZE]();
        screen.focalscreen = new double[SCREEN_SIZE * SCREEN_SIZE]();
        screen.tfocalscreen = new double[SCREEN_SIZE * SCREEN_SIZE]();
        screen.outscreen = (fftw_complex *)fftw_malloc(
            sizeof(fftw_complex) * SCREEN_SIZE * SCREEN_SIZE);
        screen.inscreen = (fftw_complex *)fftw_malloc(
            sizeof(fftw_complex) * SCREEN_SIZE * SCREEN_SIZE);
        screen.pupil_values = new float[SCREEN_SIZE * SCREEN_SIZE]();
        screen.focalscreencum = new double[SCREEN_SIZE * SCREEN_SIZE]();

        // Atmospheric Turbulence & Clouds
        if (atmospheremode) {
            fprintf(stdout, "Generating Turbulence.\n");
            screen.turbulenceLargeX =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.turbulenceLargeY =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.turbulenceCoarseX =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.turbulenceCoarseY =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.turbulenceMediumX =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.turbulenceMediumY =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();

            screen.phaseLarge =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.phaseCoarse =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.phaseMedium =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.phaseFine =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();

            screen.phaseMediumH =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();
            screen.phaseFineH =
                new float[natmospherefile * SCREEN_SIZE * SCREEN_SIZE]();

            screen.see_norm = new float[natmospherefile]();
            screen.phase_norm = new float[natmospherefile]();

            for (long layer = 0; layer < natmospherefile; layer++) {

                if (cloudvary[layer] != 0 || cloudmean[layer] != 0) {
                    screen.cloud[layer] = static_cast<float *>(
                        calloc(SCREEN_SIZE * SCREEN_SIZE, sizeof(float)));
                    sprintf(tempstring, "%s.fits.gz", cloudfile[layer].c_str());
                    screen.readScreen(-1, screen.cloud[layer], tempstring);
                }

                sprintf(tempstring,
                        "%s_largep.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseLarge + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_coarsep.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseCoarse + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_mediump.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseMedium + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_finep.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseFine + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);

                sprintf(tempstring,
                        "%s_mediumh.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseMediumH + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_fineh.fits.gz",
                        atmospherefile[layer].c_str());
                screen.phase_norm[layer] = screen.readScreen(
                    9,
                    screen.phaseFineH + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);

                sprintf(tempstring,
                        "%s_largex.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] = screen.readScreen(
                    9,
                    screen.turbulenceLargeX + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_largey.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] = screen.readScreen(
                    9,
                    screen.turbulenceLargeY + layer * SCREEN_SIZE * SCREEN_SIZE,
                    tempstring);
                sprintf(tempstring,
                        "%s_coarsex.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] =
                    screen.readScreen(9,
                                      screen.turbulenceCoarseX +
                                          layer * SCREEN_SIZE * SCREEN_SIZE,
                                      tempstring);
                sprintf(tempstring,
                        "%s_coarsey.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] =
                    screen.readScreen(9,
                                      screen.turbulenceCoarseY +
                                          layer * SCREEN_SIZE * SCREEN_SIZE,
                                      tempstring);
                sprintf(tempstring,
                        "%s_mediumx.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] =
                    screen.readScreen(9,
                                      screen.turbulenceMediumX +
                                          layer * SCREEN_SIZE * SCREEN_SIZE,
                                      tempstring);
                sprintf(tempstring,
                        "%s_mediumy.fits.gz",
                        atmospherefile[layer].c_str());
                screen.see_norm[layer] =
                    screen.readScreen(9,
                                      screen.turbulenceMediumY +
                                          layer * SCREEN_SIZE * SCREEN_SIZE,
                                      tempstring);

                for (long i = 0; i < SCREEN_SIZE; i++) {
                    for (long j = 0; j < SCREEN_SIZE; j++) {
                        *(screen.turbulenceLargeX +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceLargeX +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                        *(screen.turbulenceLargeY +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceLargeY +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                        *(screen.turbulenceCoarseX +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceCoarseX +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                        *(screen.turbulenceCoarseY +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceCoarseY +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                        *(screen.turbulenceMediumX +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceMediumX +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                        *(screen.turbulenceMediumY +
                          layer * SCREEN_SIZE * SCREEN_SIZE + SCREEN_SIZE * i +
                          j) = (float)(*(screen.turbulenceMediumY +
                                         layer * SCREEN_SIZE * SCREEN_SIZE +
                                         SCREEN_SIZE * i + j) *
                                       seefactor[layer]);
                    }
                }
            }
        }

        double sigma = 1.0 / centralwavelength;
        double temp = temperature + 273.15;
        double ps = pressure / 760.00 * 1013.25;
        double pw = waterPressure / 760.00 * 1013.25;
        double dw =
            (1 + pw * (1 + 3.7e-4 * pw) *
                     (-2.37321e-3 + 2.23366 / temp - 710.792 / temp / temp +
                      7.75141e4 / temp / temp / temp)) *
            pw / temp;
        double ds =
            (1 + ps * (57.90e-8 - 9.325e-4 / temp + 0.25844 / temp / temp)) *
            ps / temp;
        double n = (2371.34 + 683939.7 / (130.0 - pow(sigma, 2)) +
                    4547.3 / (38.9 - pow(sigma, 2))) *
                   ds;
        n = n + (6478.31 - 58.058 * pow(sigma, 2) - 0.71150 * pow(sigma, 4) +
                 0.08851 * pow(sigma, 6)) *
                    dw;
        air.air_refraction_adc = 1e-8 * n;

        // air.air_refraction_adc = 64.328 + 29498.1/(146 -
        // 1/centralwavelength/centralwavelength) + 255.4/(41 -
        // 1/centralwavelength/centralwavelength); air.air_refraction_adc =
        // air.air_refraction_adc*pressure*(1 + (1.049 -
        // 0.0157*temperature)*1e-6*pressure)/720.883/(1 +
        // 0.003661*temperature); air.air_refraction_adc =
        // air.air_refraction_adc - ((0.0624 -
        // 0.000680/centralwavelength/centralwavelength)/
        //                                                    (1 +
        //                                                    0.003661*temperature)*waterPressure);
        // air.air_refraction_adc = air.air_refraction_adc/1e6;

        return (0);
    }

    int telSetup() {

        double runningz;

        long seedchip = 0;
        for (size_t m = 0; m < chipid.size(); m++) {
            seedchip += static_cast<long>(
                (static_cast<int>(chipid.c_str()[m] % 10)) * pow(10, m));
        }

        chip.nampx = (maxx - minx + 1);
        chip.nampy = (maxy - miny + 1);
        chip.buffer = 400;
        chip.midpoint = pixelsx / 2;
        if (saturation == 1) {

            state.satupmap = static_cast<std::atomic<int> *>(
                calloc(chip.nampx * chip.nampy, sizeof(int)));
            state.satdownmap = static_cast<std::atomic<int> *>(
                calloc(chip.nampx * chip.nampy, sizeof(int)));
            for (long i = minx; i <= maxx; i++) {
                for (long j = miny; j <= maxy; j++) {
                    *(state.satdownmap + chip.nampx * (j - miny) + (i - minx)) =
                        i - 1;
                    *(state.satupmap + chip.nampx * (j - miny) + (i - minx)) =
                        i + 1;
                }
            }
        }
        state.focal_plane = static_cast<std::atomic<unsigned long> *>(calloc(
            chip.nampx * chip.nampy, sizeof(std::atomic<unsigned long>)));
        state.focal_plane_fl = static_cast<float *>(
            calloc(chip.nampx * chip.nampy, sizeof(float)));
        if (opdfile) {
            state.opd = static_cast<double *>(calloc(
                nsource * OPD_SCREEN_SIZE * OPD_SCREEN_SIZE, sizeof(double)));
            state.opdcount = static_cast<double *>(calloc(
                nsource * OPD_SCREEN_SIZE * OPD_SCREEN_SIZE, sizeof(double)));
            state.cx = static_cast<double *>(calloc(nsource, sizeof(double)));
            state.cy = static_cast<double *>(calloc(nsource, sizeof(double)));
            state.cz = static_cast<double *>(calloc(nsource, sizeof(double)));
            state.r0 = static_cast<double *>(calloc(nsource, sizeof(double)));
            state.epR = static_cast<double *>(calloc(nsource, sizeof(double)));
        }

        // OPTICS AND COATINGS
        fprintf(stdout, "Building Optics.\n");
        std::ostringstream opticsFile;
        opticsFile << instrdir << "/optics_" << filter << ".txt";
        readText opticsPars(opticsFile.str());
        long totSurf =
            opticsPars.getSize() + 1; // detector, focalplane, exit pupil

        surface.setup(totSurf, SURFACE_POINTS);
        coating.setup(totSurf);

        nsurf = 0;
        runningz = 0.0;
        nmirror = 0;
        for (long t = 0; t < totSurf - 1; t++) {
            std::istringstream iss(opticsPars[t]);
            std::string surfaceName, surfacetype, coatingFile, mediumFile;
            iss >> surfaceName >> surfacetype;

            if (surfacetype == "mirror")
                surface.surfacetype[nsurf] = MIRROR;
            else if (surfacetype == "lens")
                surface.surfacetype[nsurf] = LENS;
            else if (surfacetype == "filter")
                surface.surfacetype[nsurf] = FILTER;
            else if (surfacetype == "det")
                surface.surfacetype[nsurf] = DETECTOR;
            else if (surfacetype == "grating")
                surface.surfacetype[nsurf] = GRATING;
            else if (surfacetype == "exitpupil") {
                nsurf++;
                surface.surfacetype[nsurf] = EXITPUPIL;
            }
            if (surfacetype == "mirror")
                nmirror++;

            iss >> surface.radiusCurvature[nsurf];
            double dz;
            iss >> dz;
            runningz += dz;
            surface.height[nsurf] = runningz;
            iss >> surface.outerRadius[nsurf];
            iss >> surface.innerRadius[nsurf];
            iss >> surface.conic[nsurf];
            iss >> surface.three[nsurf];
            iss >> surface.four[nsurf];
            iss >> surface.five[nsurf];
            iss >> surface.six[nsurf];
            iss >> surface.seven[nsurf];
            iss >> surface.eight[nsurf];
            iss >> surface.nine[nsurf];
            iss >> surface.ten[nsurf];
            iss >> coatingFile;
            iss >> mediumFile;
            surface.centerx[nsurf] = 0.0;
            surface.centery[nsurf] = 0.0;
            surface.rmax[nsurf] = surface.outerRadius[nsurf];
            surface.innerRadius0[nsurf] = surface.innerRadius[nsurf];
            if (opdfile)
                surface.innerRadius[nsurf] = 0.0;
            if (surfacetype != "none") {
                surface.asphere(nsurf, SURFACE_POINTS);
                if (surface.surfacetype[nsurf] == MIRROR ||
                    surface.surfacetype[nsurf] == DETECTOR ||
                    surface.surfacetype[nsurf] == LENS ||
                    surface.surfacetype[nsurf] == FILTER) {
                    perturbation.zernikeflag.push_back(zernikemode);
                } else if (surface.surfacetype[nsurf] == EXITPUPIL) {
                    perturbation.zernikeflag.push_back(0);
                    perturbation.zernikeflag.push_back(0);
                } else {
                    perturbation.zernikeflag.push_back(0);
                }

                // COATINGS
                surface.surfacecoating[nsurf] = 0;
                if (coatingFile != "none") {
                    readText coatingPars(instrdir + "/" + coatingFile);
                    size_t nline = coatingPars.getSize();
                    coating.allocate(nsurf, nline);
                    long j = 0;
                    long i = 0;
                    double angle, angle0 = 0.0;
                    for (size_t tt(0); tt < nline; tt++) {
                        std::istringstream isst(coatingPars[tt]);
                        isst >> angle;
                        if (i == 0 && j == 0)
                            angle0 = angle;
                        if (angle > angle0) {
                            i++;
                            coating.wavelengthNumber[nsurf] = j;
                            if (j != coating.wavelengthNumber[nsurf] && i > 1) {
                                std::cout << "Error in format of "
                                          << coatingFile << std::endl;
                                exit(1);
                            }
                            j = 0;
                        }
                        *(coating.angle[nsurf] + i) = angle;
                        isst >> *(coating.wavelength[nsurf] + j);
                        isst >> *(coating.transmission[nsurf] + tt);
                        isst >> *(coating.reflection[nsurf] + tt);
                        j++;
                        angle0 = angle;
                    }
                    i++;
                    coating.wavelengthNumber[nsurf] = j;
                    coating.angleNumber[nsurf] = i;
                    coating.wavelength[nsurf] = static_cast<double *>(realloc(
                        coating.wavelength[nsurf],
                        coating.wavelengthNumber[nsurf] * sizeof(double)));
                    coating.angle[nsurf] = static_cast<double *>(
                        realloc(coating.angle[nsurf],
                                coating.angleNumber[nsurf] * sizeof(double)));
                    surface.surfacecoating[nsurf] = 1;
                }

                // MEDIUM
                surface.surfacemed[nsurf] = 0;
                if (mediumFile == "air")
                    surface.surfacemed[nsurf] = 2;
                if (mediumFile != "vacuum" && mediumFile != "air") {
                    medium.setup(nsurf, instrdir + "/" + mediumFile);
                    surface.surfacemed[nsurf] = 1;
                }
                nsurf++;
            }
        }
        // if (opdfile) nsurf -= 2; //exit pupil and focalplane

        if (telescopeMode == 1) {
            maxr = surface.outerRadius[0];
            minr = surface.innerRadius0[0];
        }

        // OBSTRUCTIONS
        obstruction.nspid = 0;
        if (diffractionMode >= 1) {
            std::string sss;
            sss = instrdir + "/spider.txt";
            std::ifstream inStream(sss.c_str());
            if (inStream) {
                readText spiderPars(instrdir + "/spider.txt");
                std::cout << "Placing Obstructions." << std::endl;
                for (size_t t(0); t < spiderPars.getSize(); t++) {
                    std::istringstream iss(spiderPars[t]);
                    std::string spiderType;
                    iss >> spiderType;
                    if (spiderType == "pupilscreen") {
                        pupilscreenMode = 1;
                    } else {
                        iss >> obstruction.height[obstruction.nspid];
                        iss >> obstruction.width[obstruction.nspid];
                        iss >> obstruction.center[obstruction.nspid];
                        double tempf4;
                        iss >> tempf4;
                        obstruction.depth[obstruction.nspid] = tempf4;
                        obstruction.angle[obstruction.nspid] = tempf4;
                        obstruction.reference[obstruction.nspid] = tempf4;
                        if (spiderType == "crossx")
                            obstruction.type[obstruction.nspid] = 1;
                        if (spiderType == "crossy")
                            obstruction.type[obstruction.nspid] = 2;
                        obstruction.nspid++;
                    }
                }
            }
        }

        // PERTURBATIONS
        fprintf(stdout, "Perturbing Design.\n");
        perturbation.zernike_coeff =
            static_cast<double *>(calloc(NTERM * nsurf, sizeof(double)));
        // chuck's old model: 0.82312e-3*pow(zv, -1.2447)

        for (long i = 0; i < nsurf; i++) {
            for (long j = 0; j < NTERM; j++) {
                *(perturbation.zernike_coeff + i * NTERM + j) = izernike[i][j];
            }
        }

        perturbation.eulerPhi.reserve(totSurf);
        perturbation.eulerPsi.reserve(totSurf);
        perturbation.eulerTheta.reserve(totSurf);
        perturbation.decenterX.reserve(totSurf);
        perturbation.decenterY.reserve(totSurf);
        perturbation.defocus.reserve(totSurf);
        perturbation.rmax.reserve(totSurf);

        double *zernikeR, *zernikePhi, *zernikeNormalR, *zernikeNormalPhi;
        double *chebyshev, *chebyshevR, *chebyshevPhi;
        zernikeR = static_cast<double *>(
            calloc(NZERN * PERTURBATION_POINTS, sizeof(double)));
        zernikePhi = static_cast<double *>(
            calloc(NZERN * PERTURBATION_POINTS, sizeof(double)));
        zernikeNormalR = static_cast<double *>(
            calloc(NZERN * PERTURBATION_POINTS, sizeof(double)));
        zernikeNormalPhi = static_cast<double *>(
            calloc(NZERN * PERTURBATION_POINTS, sizeof(double)));
        chebyshev = static_cast<double *>(calloc(
            NCHEB * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));
        chebyshevR = static_cast<double *>(calloc(
            NCHEB * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));
        chebyshevPhi = static_cast<double *>(calloc(
            NCHEB * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));
        perturbation.zernike_r_grid =
            static_cast<double *>(calloc(PERTURBATION_POINTS, sizeof(double)));
        perturbation.zernike_phi_grid =
            static_cast<double *>(calloc(PERTURBATION_POINTS, sizeof(double)));
        perturbation.zernike_summed = static_cast<double *>(calloc(
            nsurf * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));
        perturbation.zernike_summed_nr_p = static_cast<double *>(calloc(
            nsurf * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));
        perturbation.zernike_summed_np_r = static_cast<double *>(calloc(
            nsurf * PERTURBATION_POINTS * PERTURBATION_POINTS, sizeof(double)));

        zernikes(zernikeR,
                 zernikePhi,
                 perturbation.zernike_r_grid,
                 perturbation.zernike_phi_grid,
                 zernikeNormalR,
                 zernikeNormalPhi,
                 PERTURBATION_POINTS,
                 NZERN);
        chebyshevs(perturbation.zernike_r_grid,
                   perturbation.zernike_phi_grid,
                   chebyshev,
                   chebyshevR,
                   chebyshevPhi,
                   PERTURBATION_POINTS,
                   NCHEB);

        for (long k = 0; k < nsurf; k++) {
            if (pertType[k] == "zern") {
                for (long j = 0; j < PERTURBATION_POINTS; j++) {
                    for (long l = 0; l < PERTURBATION_POINTS; l++) {
                        *(perturbation.zernike_summed +
                          k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                          l * PERTURBATION_POINTS + j) = 0;
                        *(perturbation.zernike_summed_nr_p +
                          k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                          l * PERTURBATION_POINTS + j) = 0;
                        *(perturbation.zernike_summed_np_r +
                          k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                          l * PERTURBATION_POINTS + j) = 0;
                        for (long m = 0; m < NZERN; m++) {
                            *(perturbation.zernike_summed +
                              k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                              l * PERTURBATION_POINTS + j) +=
                                *(perturbation.zernike_coeff + k * NTERM + m) *
                                (*(zernikeR + m * PERTURBATION_POINTS + j)) *
                                (*(zernikePhi + m * PERTURBATION_POINTS + l));
                            *(perturbation.zernike_summed_nr_p +
                              k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                              l * PERTURBATION_POINTS + j) +=
                                *(perturbation.zernike_coeff + k * NTERM + m) *
                                (*(zernikeNormalR + m * PERTURBATION_POINTS +
                                   j)) *
                                (*(zernikePhi + m * PERTURBATION_POINTS + l));

                            *(perturbation.zernike_summed_np_r +
                              k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                              l * PERTURBATION_POINTS + j) +=
                                *(perturbation.zernike_coeff + k * NTERM + m) *
                                (*(zernikeR + m * PERTURBATION_POINTS + j)) *
                                (*(zernikeNormalPhi + m * PERTURBATION_POINTS +
                                   l));
                        }
                    }
                }
            } else if (pertType[k] == "chebyshev") {
                for (long j = 0; j < PERTURBATION_POINTS; j++) {
                    for (long l = 0; l < PERTURBATION_POINTS; l++) {
                        long idx =
                            k * PERTURBATION_POINTS * PERTURBATION_POINTS +
                            l * PERTURBATION_POINTS + j;
                        perturbation.zernike_summed[idx] = 0;
                        perturbation.zernike_summed_nr_p[idx] = 0;
                        perturbation.zernike_summed_np_r[idx] = 0;
                        for (long m = 0; m < NCHEB; m++) {
                            long idxi =
                                m * PERTURBATION_POINTS * PERTURBATION_POINTS +
                                l * PERTURBATION_POINTS + j;
                            perturbation.zernike_summed[idx] +=
                                perturbation.zernike_coeff[k * NTERM + m] *
                                chebyshev[idxi];
                            perturbation.zernike_summed_nr_p[idx] +=
                                perturbation.zernike_coeff[k * NTERM + m] *
                                chebyshevR[idxi];
                            perturbation.zernike_summed_np_r[idx] +=
                                perturbation.zernike_coeff[k * NTERM + m] *
                                chebyshevPhi[idxi];
                        }
                    }
                }
            }
        }

        perturbation.rotationmatrix =
            static_cast<double *>(calloc(totSurf * 3 * 3, sizeof(double)));
        perturbation.inverserotationmatrix =
            static_cast<double *>(calloc(totSurf * 3 * 3, sizeof(double)));
        for (long i = 0; i < nsurf + 1; i++) {
            perturbation.eulerPhi[i] = body[i][0];
            perturbation.eulerPsi[i] = body[i][1];
            perturbation.eulerTheta[i] = body[i][2];
            perturbation.decenterX[i] = body[i][3];
            perturbation.decenterY[i] = body[i][4];
            perturbation.defocus[i] = body[i][5];
        }
        satbuffer += static_cast<long>(
            2.0 * fabs(perturbation.defocus[nsurf] / (pixsize * 1e-3)));
        perturbation.decenterX[nsurf] += centerx * 1e-3;
        perturbation.decenterY[nsurf] += centery * 1e-3;

        surface.centerx[nsurf] = 0.0;
        surface.centery[nsurf] = 0.0;
        surface.rmax[nsurf] = sqrt(pixelsx * pixelsx + pixelsy * pixelsy) *
                              sqrt(2.0) / 2.0 * pixsize * 1e-3;
        surface.height[nsurf] = surface.height[nsurf - 1];
        for (long i = 0; i < totSurf; i++) {
            *(perturbation.rotationmatrix + 9 * i + 0 * 3 + 0) =
                cos(perturbation.eulerPsi[i]) * cos(perturbation.eulerPhi[i]) -
                cos(perturbation.eulerTheta[i]) *
                    sin(perturbation.eulerPhi[i]) *
                    sin(perturbation.eulerPsi[i]);
            *(perturbation.rotationmatrix + 9 * i + 0 * 3 + 1) =
                cos(perturbation.eulerPsi[i]) * sin(perturbation.eulerPhi[i]) +
                cos(perturbation.eulerTheta[i]) *
                    cos(perturbation.eulerPhi[i]) *
                    sin(perturbation.eulerPsi[i]);
            *(perturbation.rotationmatrix + 9 * i + 0 * 3 + 2) =
                sin(perturbation.eulerPsi[i]) * sin(perturbation.eulerTheta[i]);
            *(perturbation.rotationmatrix + 9 * i + 1 * 3 + 0) =
                -sin(perturbation.eulerPsi[i]) * cos(perturbation.eulerPhi[i]) -
                cos(perturbation.eulerTheta[i]) *
                    sin(perturbation.eulerPhi[i]) *
                    cos(perturbation.eulerPsi[i]);
            *(perturbation.rotationmatrix + 9 * i + 1 * 3 + 1) =
                -sin(perturbation.eulerPsi[i]) * sin(perturbation.eulerPhi[i]) +
                cos(perturbation.eulerTheta[i]) *
                    cos(perturbation.eulerPhi[i]) *
                    cos(perturbation.eulerPsi[i]);
            *(perturbation.rotationmatrix + 9 * i + 1 * 3 + 2) =
                cos(perturbation.eulerPsi[i]) * sin(perturbation.eulerTheta[i]);
            *(perturbation.rotationmatrix + 9 * i + 2 * 3 + 0) =
                sin(perturbation.eulerTheta[i]) * sin(perturbation.eulerPhi[i]);
            *(perturbation.rotationmatrix + 9 * i + 2 * 3 + 1) =
                -sin(perturbation.eulerTheta[i]) *
                cos(perturbation.eulerPhi[i]);
            *(perturbation.rotationmatrix + 9 * i + 2 * 3 + 2) =
                cos(perturbation.eulerTheta[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 0 * 3 + 0) =
                cos(perturbation.eulerPsi[i]) * cos(perturbation.eulerPhi[i]) -
                cos(perturbation.eulerTheta[i]) *
                    sin(perturbation.eulerPhi[i]) *
                    sin(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 0 * 3 + 1) =
                -sin(perturbation.eulerPsi[i]) * cos(perturbation.eulerPhi[i]) -
                cos(perturbation.eulerTheta[i]) *
                    sin(perturbation.eulerPhi[i]) *
                    cos(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 0 * 3 + 2) =
                sin(perturbation.eulerTheta[i]) * sin(perturbation.eulerPhi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 1 * 3 + 0) =
                cos(perturbation.eulerPsi[i]) * sin(perturbation.eulerPhi[i]) +
                cos(perturbation.eulerTheta[i]) *
                    cos(perturbation.eulerPhi[i]) *
                    sin(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 1 * 3 + 1) =
                -sin(perturbation.eulerPsi[i]) * sin(perturbation.eulerPhi[i]) +
                cos(perturbation.eulerTheta[i]) *
                    cos(perturbation.eulerPhi[i]) *
                    cos(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 1 * 3 + 2) =
                -sin(perturbation.eulerTheta[i]) *
                cos(perturbation.eulerPhi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 2 * 3 + 0) =
                sin(perturbation.eulerTheta[i]) * sin(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 2 * 3 + 1) =
                sin(perturbation.eulerTheta[i]) * cos(perturbation.eulerPsi[i]);
            *(perturbation.inverserotationmatrix + 9 * i + 2 * 3 + 2) =
                cos(perturbation.eulerTheta[i]);
        }
        for (long i = 0; i < totSurf; i++) {
            perturbation.rmax[i] = surface.rmax[surfaceLink[i]];
        }

        /* Update perturbation model from FEA file */
        size_t nNear = 4;
        // int degree=1;
        int leafSize = 10;
        for (long k = 0; k < nsurf; k++) {
            for (long m = 0; m < feaflag[k]; m++) {
                std::cout << "loading " << feafile[k * MAX_SURF * 2 + m]
                          << std::endl;
                std::vector<double> ix(PERTURBATION_POINTS *
                                       PERTURBATION_POINTS),
                    iy(PERTURBATION_POINTS * PERTURBATION_POINTS);
                for (long j = 0; j < PERTURBATION_POINTS; j++) {
                    for (long l = 0; l < PERTURBATION_POINTS; l++) {
                        ix[l * PERTURBATION_POINTS + j] =
                            perturbation.rmax[k] *
                            perturbation.zernike_r_grid[j] *
                            cos(perturbation.zernike_phi_grid[l]);
                        iy[l * PERTURBATION_POINTS + j] =
                            perturbation.rmax[k] *
                            perturbation.zernike_r_grid[j] *
                            sin(perturbation.zernike_phi_grid[l]);
                    }
                }
                Fea feaTree(feafile[k * MAX_SURF * 2 + m],
                            leafSize,
                            surface,
                            perturbation,
                            k,
                            feascaling[k * MAX_SURF * 2 + m]);
                feaTree.knnQueryFitDegree1(
                    ix,
                    iy,
                    &perturbation.zernike_summed[k * PERTURBATION_POINTS *
                                                 PERTURBATION_POINTS],
                    &perturbation.zernike_summed_nr_p[k * PERTURBATION_POINTS *
                                                      PERTURBATION_POINTS],
                    &perturbation.zernike_summed_np_r[k * PERTURBATION_POINTS *
                                                      PERTURBATION_POINTS],
                    nNear);
                for (long ii = 0; ii < PERTURBATION_POINTS; ii++) {
                    for (long jj = 0; jj < PERTURBATION_POINTS; jj++) {
                        if (perturbation
                                    .zernike_summed[k * PERTURBATION_POINTS *
                                                        PERTURBATION_POINTS +
                                                    ii * PERTURBATION_POINTS +
                                                    jj] < 10e-3 &&
                            perturbation
                                    .zernike_summed[k * PERTURBATION_POINTS *
                                                        PERTURBATION_POINTS +
                                                    ii * PERTURBATION_POINTS +
                                                    jj] > -10e-3) {
                        }
                    }
                }
            }
        }

        /// Large Angle Scattering
        if (laScatterProb > 0) {
            perturbation.miescatter =
                static_cast<double *>(calloc(10000, sizeof(double)));
            for (long j = 0; j < 10000; j++) {
                *(perturbation.miescatter + j) =
                    1.0 /
                    (1.0 + pow(((static_cast<float>(j)) / 10000.0 * 0.1 *
                                PH_PI / 180.0) /
                                   (1.0 * PH_PI / 180.0 / 3600.0),
                               3.5)) *
                    2.0 * PH_PI * (static_cast<float>(j));
            }
            double tempf1 = 0.0;
            for (long j = 0; j < 10000; j++) {
                tempf1 = tempf1 + *(perturbation.miescatter + j);
            }
            for (long j = 0; j < 10000; j++) {
                *(perturbation.miescatter + j) =
                    *(perturbation.miescatter + j) / tempf1;
            }
            for (long j = 1; j < 10000; j++) {
                *(perturbation.miescatter + j) =
                    *(perturbation.miescatter + j) +
                    *(perturbation.miescatter + j - 1);
            }
        }

        // Tracking
        double angle = PH_PI / 2 - zenith;
        if (fabs(angle) < 0.5 * PH_PI / 180.0)
            angle = 0.5 * PH_PI / 180.0;
        rotationrate = 15.04 * cos(latitude) * cos(azimuth) / cos(angle);
        if (trackingfile == ".")
            trackingMode = 0;

        std::ifstream inStream(trackingfile.c_str());
        if (inStream) {
            readText trackingPars(trackingfile);
            size_t j = trackingPars.getSize();
            perturbation.jitterrot =
                static_cast<double *>(calloc(j, sizeof(double)));
            perturbation.jitterele =
                static_cast<double *>(calloc(j, sizeof(double)));
            perturbation.jitterazi =
                static_cast<double *>(calloc(j, sizeof(double)));
            screen.jitterwind =
                static_cast<double *>(calloc(j, sizeof(double)));
            perturbation.jittertime =
                static_cast<double *>(calloc(j, sizeof(double)));
            trackinglines = j;
            for (size_t t(0); t < j; t++) {
                std::istringstream iss(trackingPars[t]);
                iss >> perturbation.jittertime[t];
                double f1, f2, f3, f4;
                iss >> f1 >> f2 >> f3 >> f4;
                perturbation.jitterele[t] = f1 * elevationjitter;
                perturbation.jitterazi[t] = f2 * azimuthjitter;
                perturbation.jitterrot[t] = f3 * rotationjitter;
                if (trackingMode == 0) {
                    perturbation.jitterele[t] = 0.0;
                    perturbation.jitterazi[t] = 0.0;
                    perturbation.jitterrot[t] = 0.0;
                }
                screen.jitterwind[t] = f4 * windjitter;
            }
        }

        /// Setup chip material
        fprintf(stdout, "Electrifying Devices.\n");
        if (detectorMode != 0) {
            silicon.setup(devmaterial,
                          sensorTempNominal + sensorTempDelta,
                          nbulk,
                          nf,
                          nb,
                          sf,
                          sb,
                          sensorthickness,
                          overdepbias,
                          instrdir,
                          chip.nampx,
                          chip.nampy,
                          pixsize,
                          seedchip,
                          &impurityX,
                          &impurityY,
                          impurityvariation,
                          minwavelength,
                          maxwavelength);
        }

        if (contaminationmode == 1) {
            fprintf(stdout, "Contaminating Surfaces.\n");
            contamination.setup(surface.innerRadius,
                                surface.outerRadius,
                                nsurf - 1,
                                PERTURBATION_POINTS,
                                pixsize,
                                chip.nampx,
                                chip.nampy,
                                qevariation,
                                seedchip);
        }

        fprintf(stdout, "Diffracting.\n");
        // 2nd kick
        // {

        //     Photon photon;
        //     Vector position;
        //     Vector angle;
        //     double shiftedAngle;
        //     double r,  phi;
        //     double rindex;
        //     long index;
        //     double radius;
        //     long atmtempdebug = 0;
        //     double bestvalue;
        //     double bestscale = 1.0;
        //     Vector largeAngle;
        //     double stdx = 0.0, stdy = 0.0;
        //     double radx = 0.0, rady = 0.0;
        //     double ncount = 0.0;
        //     screen.hffunc = static_cast<double*>(calloc(10000,
        //     sizeof(double))); screen.hffunc_n =
        //     static_cast<double*>(calloc(10000, sizeof(double)));

        //     atmtempdebug = atmdebug;

        //     for (long l = 0; l <= atmtempdebug; l++) {
        //         atmdebug = l;

        //         for (long zz = 0 ; zz < 100; zz++) {

        //         for (long i = 0; i < SCREEN_SIZE; i++) {
        //             for (long j = 0; j < SCREEN_SIZE; j++) {
        //                 screen.tfocalscreen[i*SCREEN_SIZE + j] = 0;
        //             }
        //         }

        //         photon.prtime = -1.0;

        //         for (long k = 0; k < 10; k++) {

        //             photon.wavelength = 0.3 +
        //             static_cast<double>(zz)/100.0*0.9;
        //             photon.wavelengthFactor = pow(photon.wavelength,
        //             -0.2)/screen.wavelengthfactor_nom; photon.time =
        //             random.uniform()*exptime; photon.absoluteTime =
        //             photon.time - exptime/2.0 + timeoffset; shiftedAngle =
        //             spiderangle + photon.time*rotationrate*ARCSEC; r =
        //             sqrt(random.uniform()*(maxr*maxr - minr*minr) +
        //             minr*minr); phi = random.uniform()*2*PH_PI; position.x =
        //             r*cos(phi); position.y = r*sin(phi); index =
        //             find_linear(const_cast<double *>(&surface.radius[0]),
        //             SURFACE_POINTS, r, &rindex); position.z =
        //             interpolate_linear(const_cast<double
        //             *>(&surface.profile[0]), index, rindex); photon.xp =
        //             position.x; photon.yp = position.y; angle.x = 0.0;
        //             angle.y = 0.0;
        //             angle.z = -1.0;

        //             atmospherePropagate(&position, angle, -1, 2, &photon);
        //             for (long layer = 0; layer < natmospherefile; layer++) {
        //                 atmospherePropagate(&position, angle, layer, 2,
        //                 &photon); atmosphereIntercept(&position, layer,
        //                 &photon); atmosphereRefraction(&angle, layer, 2,
        //                 &photon);
        //             }
        //             atmosphereDiffraction(&angle, &photon);
        //             for (long i = 0; i < SCREEN_SIZE; i++) {
        //                 for (long j = 0; j < SCREEN_SIZE; j++) {
        //                     screen.tfocalscreen[i*SCREEN_SIZE + j] +=
        //                     screen.focalscreen[i*SCREEN_SIZE + j];
        //                 }
        //             }

        //         }

        //         for (long i = 0; i < 10000; i++) {
        //             *(screen.hffunc + i) = 0.0;
        //         }
        //         for (long i = 0; i < 10000; i++) {
        //             *(screen.hffunc_n + i) = 0.0;
        //         }
        //         for (long i = 0; i < SCREEN_SIZE; i++) {
        //             for (long j = 0; j < SCREEN_SIZE; j++) {
        //                 radius = sqrt(pow((i - (SCREEN_SIZE/2 + 1)), 2) +
        //                 pow((j - (SCREEN_SIZE/2 + 1)), 2));
        //                 *(screen.hffunc + ((long)(radius))) +=
        //                 screen.tfocalscreen[i*SCREEN_SIZE + j];
        //                 *(screen.hffunc_n + ((long)(radius))) += 1;
        //             }
        //         }

        //         for (long j = 0; j < 10000; j++) {
        //             if (screen.hffunc_n[j] < 1) {
        //                 *(screen.hffunc + j) = 0;
        //             } else {
        //                 *(screen.hffunc + j) = *(screen.hffunc +
        //                 j)/(screen.hffunc_n[j])*((double)(j));
        //             }
        //         }
        //         double tempf1 = 0.0;
        //         for (long j = 0; j < 10000; j++) {
        //             tempf1 = tempf1 + *(screen.hffunc + j);
        //         }
        //         for (long j = 0; j < 10000; j++) {
        //             *(screen.hffunc + j) = *(screen.hffunc + j)/tempf1;
        //         }
        //         for (long j = 1; j < 10000; j++) {
        //             *(screen.hffunc + j) = *(screen.hffunc + j) +
        //             *(screen.hffunc + j - 1);
        //         }
        //         for (long j = 0; j < 10000; j++) {
        //             printf("xx %ld %ld %lf\n",zz,j,*(screen.hffunc + j));
        //         }

        //         }
        //     }
        // }

        // 2nd kick
        {

            Photon photon;
            Vector position;
            Vector angle;
            // double shiftedAngle;
            double r, phi;
            double rindex;
            long index;
            double radius;
            long atmtempdebug = 0;
            double bestvalue;
            double bestscale = 1.0;
            Vector largeAngle;
            double stdx = 0.0, stdy = 0.0;
            double radx = 0.0, rady = 0.0;
            double ncount = 0.0;
            screen.hffunc =
                static_cast<double *>(calloc(10000, sizeof(double)));
            screen.hffunc_n =
                static_cast<double *>(calloc(10000, sizeof(double)));
            photon.thread = 0;
            atmtempdebug = atmdebug;

            for (long l = 0; l <= atmtempdebug; l++) {
                atmdebug = l;

                for (long i = 0; i < SCREEN_SIZE; i++) {
                    for (long j = 0; j < SCREEN_SIZE; j++) {
                        screen.tfocalscreen[i * SCREEN_SIZE + j] = 0;
                    }
                }

                photon.prtime = -1.0;

                for (long k = 0; k < 10; k++) {

                    photon.wavelength = 0.5;
                    photon.wavelengthFactor = pow(photon.wavelength, -0.2) /
                                              screen.wavelengthfactor_nom;
                    photon.time = random[0].uniform() * exptime;
                    photon.absoluteTime =
                        photon.time - exptime / 2.0 + timeoffset;
                    // shiftedAngle =
                    //    spiderangle + photon.time * rotationrate * ARCSEC;
                    r = sqrt(random[0].uniform() * (maxr * maxr - minr * minr) +
                             minr * minr);
                    phi = random[0].uniform() * 2 * PH_PI;
                    position.x = r * cos(phi);
                    position.y = r * sin(phi);
                    index =
                        find_linear(const_cast<double *>(&surface.radius[0]),
                                    SURFACE_POINTS,
                                    r,
                                    &rindex);
                    position.z = interpolate_linear(
                        const_cast<double *>(&surface.profile[0]),
                        index,
                        rindex);
                    photon.xp = position.x;
                    photon.yp = position.y;
                    angle.x = 0.0;
                    angle.y = 0.0;
                    angle.z = -1.0;

                    atmospherePropagate(&position, angle, -1, 2, &photon);
                    for (long layer = 0; layer < natmospherefile; layer++) {
                        atmospherePropagate(
                            &position, angle, layer, 2, &photon);
                        atmosphereIntercept(&position, layer, &photon);
                        atmosphereRefraction(&angle, layer, 3, &photon);
                    }
                    atmosphereDiffraction(&angle, &photon);
                    for (long i = 0; i < SCREEN_SIZE; i++) {
                        for (long j = 0; j < SCREEN_SIZE; j++) {
                            screen.tfocalscreen[i * SCREEN_SIZE + j] +=
                                screen.focalscreen[i * SCREEN_SIZE + j];
                        }
                    }
                }

                for (long i = 0; i < 10000; i++) {
                    *(screen.hffunc + i) = 0.0;
                }
                for (long i = 0; i < 10000; i++) {
                    *(screen.hffunc_n + i) = 0.0;
                }
                for (long i = 0; i < SCREEN_SIZE; i++) {
                    for (long j = 0; j < SCREEN_SIZE; j++) {
                        radius = sqrt(pow((i - (SCREEN_SIZE / 2 + 1)), 2) +
                                      pow((j - (SCREEN_SIZE / 2 + 1)), 2));
                        *(screen.hffunc + ((long)(radius))) +=
                            screen.tfocalscreen[i * SCREEN_SIZE + j];
                        *(screen.hffunc_n + ((long)(radius))) += 1;
                    }
                }

                for (long j = 0; j < 10000; j++) {
                    if (screen.hffunc_n[j] < 1) {
                        *(screen.hffunc + j) = 0;
                    } else {
                        *(screen.hffunc + j) = *(screen.hffunc + j) /
                                               (screen.hffunc_n[j]) *
                                               ((double)(j));
                    }
                }
                double tempf1 = 0.0;
                for (long j = 0; j < 10000; j++) {
                    tempf1 = tempf1 + *(screen.hffunc + j);
                }
                for (long j = 0; j < 10000; j++) {
                    *(screen.hffunc + j) = *(screen.hffunc + j) / tempf1;
                }
                for (long j = 1; j < 10000; j++) {
                    *(screen.hffunc + j) =
                        *(screen.hffunc + j) + *(screen.hffunc + j - 1);
                }

                if (l == 0) {

                    bestvalue = 1e10;
                    bestscale = 1.0;
                    for (double scales = 0.0; scales <= 2.01; scales += 0.01) {
                        stdx = 0.0;
                        stdy = 0.0;
                        ncount = 0.0;

                        for (long k = 0; k < 10000; k++) {

                            screen.secondKickSize = scales;
                            photon.wavelength = 0.5;
                            photon.wavelengthFactor =
                                pow(photon.wavelength, -0.2) /
                                screen.wavelengthfactor_nom;
                            photon.time = random[0].uniform() * 1000.0;
                            photon.absoluteTime =
                                photon.time - 1000.0 / 2.0 + timeoffset;
                            //                            shiftedAngle =
                            //                            spiderangle +
                            //                                           photon.time
                            //                                           *
                            //                                           rotationrate
                            //                                           *
                            //                                           ARCSEC;
                            r = sqrt(random[0].uniform() *
                                         (maxr * maxr - minr * minr) +
                                     minr * minr);
                            phi = random[0].uniform() * 2 * PH_PI;
                            position.x = r * cos(phi);
                            position.y = r * sin(phi);
                            index = find_linear(
                                const_cast<double *>(&surface.radius[0]),
                                SURFACE_POINTS,
                                r,
                                &rindex);
                            position.z = interpolate_linear(
                                const_cast<double *>(&surface.profile[0]),
                                index,
                                rindex);
                            photon.xp = position.x;
                            photon.yp = position.y;
                            angle.x = 0.0;
                            angle.y = 0.0;
                            angle.z = -1.0;
                            largeAngle.x = 0;
                            largeAngle.y = 0;

                            secondKick(&largeAngle, &photon);
                            atmospherePropagate(
                                &position, angle, -1, 1, &photon);
                            for (long layer = 0; layer < natmospherefile;
                                 layer++) {
                                atmospherePropagate(
                                    &position, angle, layer, 1, &photon);
                                atmosphereIntercept(&position, layer, &photon);
                                atmosphereRefraction(&angle, layer, 1, &photon);
                            }
                            angle.x = angle.x + largeAngle.x;
                            angle.y = angle.y + largeAngle.y;

                            radx =
                                angle.x / ((totalseeing + 1e-6) / 2.35482 *
                                           ARCSEC * pow(1 / cos(zenith), 0.6) *
                                           photon.wavelengthFactor);
                            rady =
                                angle.y / ((totalseeing + 1e-6) / 2.35482 *
                                           ARCSEC * pow(1 / cos(zenith), 0.6) *
                                           photon.wavelengthFactor);
                            stdx +=
                                radx * radx *
                                exp(-(radx * radx + rady * rady) / 2.0 / 1.0);
                            stdy +=
                                rady * rady *
                                exp(-(radx * radx + rady * rady) / 2.0 / 1.0);
                            ncount +=
                                exp(-(radx * radx + rady * rady) / 2.0 / 1.0);
                        }

                        stdx /= ncount;
                        stdy /= ncount;
                        screen.secondKickSize = sqrt(stdx + stdy);
                        if (fabs(screen.secondKickSize - 1.0) < bestvalue) {
                            bestvalue = fabs(screen.secondKickSize - 1.0);
                            bestscale = scales;
                        }
                    }
                    screen.secondKickSize = bestscale;
                }
            }

            atmdebug = atmtempdebug;
        }

        if (areaExposureOverride == 0) {
            nphot = static_cast<long long>(
                PH_PI *
                (maxr / 10.0 * maxr / 10.0 - minr / 10.0 * minr / 10.0) *
                exptime * (totalnorm / H_CGS));
        } else {
            nphot = static_cast<long long>(1e6 * (totalnorm / H_CGS));
        }
        if (opdfile == 1) {
            nphot = totalnorm;
        }

        settings();

        fprintf(stdout, "Number of Sources: %ld\n", nsource);
        fprintf(stdout,
                "Photons: %6.2e  Photons/cm^2/s: %6.2e\n",
                static_cast<double>(nphot),
                totalnorm / H_CGS);

        fprintf(stdout,
                "------------------------------------------------------"
                "------------------------------------\n");
        fprintf(stdout, "Photon Raytrace\n");
        readText versionPars(bindir + "/version");
        fprintf(stdout, "%s\n", versionPars[0].c_str());
        fprintf(stdout,
                "------------------------------------------------------"
                "------------------------------------\n");

        return (0);
    }
};

struct thread_args {
    Image *instance;
    long runthread;
    long ssource[4096];
    long thread;
};

#endif
