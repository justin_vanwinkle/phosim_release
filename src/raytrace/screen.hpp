#ifndef PHOSIM_SCREEN_HPP
#define PHOSIM_SCREEN_HPP

#include "parameters.hpp"

#include <fftw3.h>
#include <fitsio.h>
#include <fitsio2.h>

class Screen {

  public:
    double large_sizeperpixel;
    double coarse_sizeperpixel;
    double medium_sizeperpixel;
    double fine_sizeperpixel;
    double *hffunc;
    double *hffunc_n;
    float *turbulenceCoarseX;
    float *turbulenceCoarseY;
    float *turbulenceLargeX;
    float *turbulenceLargeY;
    float *turbulenceMediumX;
    float *turbulenceMediumY;
    float *phaseLarge;
    float *phaseCoarse;
    float *phaseMedium;
    float *phaseFine;
    float *phaseMediumH;
    float *phaseFineH;
    float *cloud[MAX_LAYER];
    float *see_norm, *phase_norm;
    float secondKickSize;
    double *phasescreen;
    double *focalscreen;
    double *tfocalscreen;
    fftw_complex *inscreen;
    fftw_complex *outscreen;
    double wavelengthfactor_nom;
    double *jitterwind;
    double *focalscreencum;
    float *pupil_values;
    double pupilscreenscale;
    double paddingfactor;

    double readScreen(int keynum, float *array, char *tempstring) {

        char *ffptr;
        fitsfile *faptr;
        long naxes[2];
        int nfound;
        int anynull;
        float nullval;
        char keyname[4096];
        char comment[4096];
        char value[4096];
        int status;

        ffptr = tempstring;
        status = 0;
        if (fits_open_file(&faptr, ffptr, READONLY, &status)) {
            printf("Error opening %s\n", ffptr);
            exit(1);
        }
        fits_read_keys_lng(
            faptr, (char *)"NAXIS", 1, 2, naxes, &nfound, &status);
        if (keynum >= 0)
            fits_read_keyn(faptr, keynum, keyname, value, comment, &status);
        fits_read_img(faptr,
                      TFLOAT,
                      1,
                      naxes[0] * naxes[1],
                      &nullval,
                      array,
                      &anynull,
                      &status);
        fits_close_file(faptr, &status);

        return (atof(value));
    }
};

#endif
