#ifndef PHOSIM_STATE_HPP
#define PHOSIM_STATE_HPP

#include "counter.hpp"
#include "event.hpp"

#include <atomic>

struct State {

    std::atomic<int> *satupmap;
    std::atomic<int> *satdownmap;
    double *opd;
    double *opdcount;
    std::atomic<double> *dynamicTransmission;
    std::atomic<unsigned long> *focal_plane;
    float *focal_plane_fl;
    double *cx;
    double *cy;
    double *cz;
    double *r0;
    double *epR;

    EventFile *pEventLogging;
    Clog counterLog;
    Tlog throughputLog;
    Clog globalLog;
};

#endif
