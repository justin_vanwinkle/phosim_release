#ifndef PHOSIM_PERTURBATION_HPP
#define PHOSIM_PERTURBATION_HPP

class Perturbation {

  public:
    std::vector<double> eulerPhi;
    std::vector<double> eulerPsi;
    std::vector<double> eulerTheta;
    std::vector<double> decenterX;
    std::vector<double> decenterY;
    std::vector<double> defocus;
    double *zernike_r_grid;
    double *zernike_phi_grid;
    double *zernike_coeff;
    double *zernike_summed;
    double *zernike_summed_nr_p;
    double *zernike_summed_np_r;
    double *miescatter;
    double *rotationmatrix;
    double *inverserotationmatrix;
    double *jitterrot;
    double *jitterele;
    double *jitterazi;
    double *jittertime;
    std::vector<int> zernikeflag;
    std::vector<double> rmax;
};

#endif
