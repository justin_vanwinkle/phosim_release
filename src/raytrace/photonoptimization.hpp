#ifndef PHOSIM_PHOTONOPTIMIZATION_HPP
#define PHOSIM_PHOTONOPTIMIZATION_HPP
///
#include "image.hpp"

int Image::dynamicTransmissionOptimization(long k,
                                           long *lastSurface,
                                           long *preGhost,
                                           long waveSurfaceIndex,
                                           long straylightcurrent,
                                           Photon *aph) {

    long newSurf;

redoghlp:;
    newSurf = -1;
    aph->direction = 1;
    aph->counter = -1;
    *preGhost = 0;

    for (int i = -1; i < natmospherefile; i++) {

        if (i >= 0) {
            if (aph->counter >= (MAX_BOUNCE - 1))
                goto maxbounce;
            if (transmissionPreCheck(i * 2 + 1, waveSurfaceIndex, aph)) {
                if (k > 0)
                    goto redoghlp;
                return (1);
            }
        }

        if (aph->counter >= (MAX_BOUNCE - 1))
            goto maxbounce;
        if (transmissionPreCheck(i * 2 + 2, waveSurfaceIndex, aph)) {
            if (k > 0)
                goto redoghlp;
            return (1);
        }
    }
    while (1) {
        if (aph->direction == 1) {
            newSurf++;
        } else {
            newSurf--;
        }
        if (newSurf == -1) {
            if (k > 0)
                goto redoghlp;
            return (1);
        }

        if (throughputfile == 1 && aph->direction == 1) {
            *lastSurface = newSurf;
        }
        if (aph->counter >= (MAX_BOUNCE - 1))
            goto maxbounce;
        if (transmissionPreCheck(
                natmospherefile * 2 + 1 + 2 * newSurf, waveSurfaceIndex, aph)) {
            if (straylightcurrent == 1) {
                if (aph->counter >= (MAX_BOUNCE - 1))
                    goto maxbounce;
                if (transmissionPreCheck(natmospherefile * 2 + 1 + 2 * newSurf +
                                             1,
                                         waveSurfaceIndex,
                                         aph)) {
                    if (k > 0)
                        goto redoghlp;
                    return (1);
                } else {
                    aph->direction = -aph->direction;
                    (*preGhost)++;
                }
            } else {
                if (k > 0)
                    goto redoghlp;
                return (1);
            }
        }
        if (aph->direction == 1 && newSurf == nsurf - 1) {
            if (aph->counter >= (MAX_BOUNCE - 1))
                goto maxbounce;
            if (transmissionPreCheck(natmospherefile * 2 + 1 + 2 * nsurf,
                                     waveSurfaceIndex,
                                     aph)) {
                if (k > 0)
                    goto redoghlp;
                return (1);
            }
            goto maxbounce;
        }

        if (aph->direction == -1 && newSurf == 0) {
            if (k > 0)
                goto redoghlp;
            return (1);
        }
    }
maxbounce:;
    aph->maxcounter = aph->counter;

    return (0);
}

#endif
