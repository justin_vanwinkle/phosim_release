#ifndef PHOSIM_COATING_HPP
#define PHOSIM_COATING_HPP

class Coating {

  public:
    double **reflection;
    double **transmission;
    double **wavelength;
    double **angle;

    long *wavelengthNumber;
    long *angleNumber;

    void setup(long totalSurfaces) {

        wavelengthNumber = new long[totalSurfaces]();
        angleNumber = new long[totalSurfaces]();

        reflection = new double *[totalSurfaces]();
        transmission = new double *[totalSurfaces]();
        wavelength = new double *[totalSurfaces]();
        angle = new double *[totalSurfaces]();
    }

    void allocate(long surfaceIndex, long lines) {

        transmission[surfaceIndex] = new double[lines]();
        reflection[surfaceIndex] = new double[lines]();
        wavelength[surfaceIndex] = new double[lines]();
        angle[surfaceIndex] = new double[lines]();
    }
};

#endif
