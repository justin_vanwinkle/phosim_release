#ifndef PHOSIM_OBSTRUCTION_HPP
#define PHOSIM_OBSTRUCTION_HPP

#include "parameters.hpp"

class Obstruction {

  public:
    double par[MAX_SURF];
    double center[MAX_SURF];
    double width[MAX_SURF];
    double depth[MAX_SURF];
    double angle[MAX_SURF];
    double reference[MAX_SURF];
    double height[MAX_SURF];
    int type[MAX_SURF];
    int nspid;
    int pupil;
};

#endif
