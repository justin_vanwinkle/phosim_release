#ifndef PHOSIM_OBSERVATION_HPP
#define PHOSIM_OBSERVATION_HPP

#include "constants.hpp"
#include "coordinate.hpp"
#include "fits.hpp"
#include "helpers.hpp"
#include "parameters.hpp"
#include "raytrace.hpp"
#include "readtext.hpp"
#include "source.hpp"
#include "vector_type.hpp"

#include <cmath>
#include <cstring>
#include <fitsio.h>
#include <fitsio2.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <zlib.h>

class Observation {

  public:
    double rotationjitter;
    double zenith;
    double azimuth;
    double rotationrate;
    double elevationjitter;
    double azimuthjitter;
    double windjitter;
    double groundlevel;
    double xtelloc;
    double ytelloc;
    double latitude;
    double longitude;
    std::vector<double> seefactor;
    std::vector<double> wind;
    std::vector<double> winddir;
    std::vector<double> outerscale;
    std::vector<double> height;
    std::vector<double> cloudmean;
    std::vector<double> cloudvary;
    std::vector<std::string> extraCommandString;
    double *dtau;
    double pressure;
    double waterPressure;
    double temperature;
    double raynorm;
    double o2norm;
    double o3norm;
    double h2onorm;
    double raygradient;
    double o2gradient;
    double o3gradient;
    double h2ogradient;
    double aerosolgradient;
    double rayAngle;
    double o2Angle;
    double o3Angle;
    double h2oAngle;
    double aerosolAngle;
    double aerosoltau;
    double aerosolindex;
    std::vector<std::string> pertType;
    int NTERM;
    std::vector<std::vector<double>> izernike;
    std::vector<int> surfaceLink;
    std::vector<std::vector<double>> body;
    double minr;
    double shuttererror;
    double zodiacalLightMagnitude;
    double overrideZodiacalLightMagnitude;
    double transtol, backAlpha, backBeta, backRadius, backGamma, backDelta,
        backBuffer, backEpsilon, np, finiteDistance;
    long satbuffer;
    long activeBuffer;
    double screentol;
    double maxr;
    double domeseeing;
    double toypsf;
    double pixsize;
    double pra;
    double pdec;
    double spiderangle;
    double platescale;
    double centerx;
    double centery;
    double tai;
    double day;
    double laScatterProb;
    double exptime;
    double vistime;
    double timeoffset;
    double rotatex;
    double rotatey;
    double rotatez;
    double *sedCorr;
    double *sedDwdp;
    double *sedW;
    double *sedC;
    double largeScale;
    double coarseScale;
    double mediumScale;
    double fineScale;
    double totalnorm;
    double totalseeing;
    double moonalt;
    double moondist;
    double moonra;
    double moondec;
    double phaseang;
    double solarzen;
    double airglowcintensity;
    double airglowpintensity;
    double watervar;
    double minwavelength;
    double maxwavelength;
    double centralwavelength;
    double domelight;
    double domewave;
    double chipangle;
    double decenterx;
    double decentery;
    int telconfig;
    int checkpointcount;
    int checkpointtotal;
    int impurityvariation;
    int fieldanisotropy;
    int fringeflag;
    int detectorcollimate;
    int deadlayer;
    int chargesharing;
    int pixelerror;
    int chargediffusion;
    int photoelectric;
    int airrefraction;
    int aberration;
    int nutation;
    int precession;
    double raydensity;
    double scalenumber;
    double devvalue;
    double airglowvariation;
    float nbulk;
    float nf;
    float nb;
    float sf;
    float sb;
    float sensorthickness;
    float impurityX;
    float impurityY;
    float overdepbias;
    float siliconreflectivity;
    float sensorTempNominal;
    float sensorTempDelta;
    float qevariation;
    float *airglow;
    long long nphot;
    long long *sourceXpos;
    long long *sourceYpos;
    long long *sourcePhoton;
    long airglowScreenSize;
    long telescopeMode;
    long backgroundMode;
    long coatingmode;
    long poissonMode;
    long contaminationmode;
    long trackingMode;
    long ranseed;
    long obsseed;
    long zernikemode;
    long atmospheric_dispersion;
    long atmosphericdispcenter;
    long natmospherefile;
    long straylight;
    double straylightcut;
    long detectorMode;
    long opticsonlymode;
    long additivemode;
    long diffractionMode;
    long spiderMode;
    long pupilscreenMode;
    long spaceMode;
    long sequenceMode;
    long aperturemode;
    long areaExposureOverride;
    long filter;
    std::string filterName;
    long saturation;
    long eventfile;
    long opdfile;
    long opdsize;
    long opdsampling;
    long centroidfile;
    long throughputfile;
    long pixelsx, pixelsy, minx, maxx, miny, maxy;
    std::string obshistid;
    long pairid;
    long blooming;
    long well_depth;
    long *sedN;
    long *sedPtr;
    long nsedptr;
    long sedptr;
    long nsource;
    long nimage;
    long nreallocs;
    long sedMax;
    long ghostonly;
    long atmdebug;
    long largeGrid;
    long coarseGrid;
    long mediumGrid;
    long fineGrid;
    long nsnap;
    int nframes;
    int nskip;
    int ngroups; // not exposed
    std::vector<int> ghost;
    int flatdir;
    int tarfile;
    int date;
    int numthread;
    std::string devmaterial;
    std::string devtype;
    std::string devmode;
    std::vector<int> feaflag;
    std::vector<std::string> feafile;
    std::vector<double> feascaling;
    double normwave;
    int atmospheremode;
    int opacitymode;
    int sourceperthread;

    // should be part of image but have conflict with settings.c
    long nsurf;
    long nmirror;
    double airmass;

    // remainder should be ok
    std::vector<std::string> atmospherefile;
    std::vector<std::string> cloudfile;
    std::string trackingfile;
    long trackinglines;
    std::string outputfilename;
    std::string chipid;
    std::string focalplanefile;
    std::string eventFitsFileName;
    std::string outputdir, workdir, seddir, imagedir, datadir, instrdir, bindir;
    Vector tpx, tpy, tpz;

    long naxesb[MAX_IMAGE][2];
    float *tempptr[MAX_IMAGE];
    float *cumulativex[MAX_IMAGE];
    float cumulative[MAX_IMAGE];
    float cumulativef[MAX_IMAGE];

    Source sources;

    int addSource(const std::string &object, int sourcetype) {

        char tempstring[4096];
        int nspatialpar, ndustpar, ndustparz;
        long i, j;
        int status;
        char *ffptr;
        fitsfile *faptr;
        long naxes[2];
        int nfound;
        int anynull;
        float nullval;
        double x, y;
        double nn;
        double mag;
        double ra, dec, redshift, gamma1, gamma2, kappa, magnification, deltara,
            deltadec;
        std::string id;

        if (nsource >= MAX_SOURCE) {
            std::perror("Too many sources in catalog");
        }

        nspatialpar = 0;

        std::istringstream iss(object);
        iss >> id >> ra >> dec;
        sources.id.push_back(id);
        sources.ra.push_back(ra);
        sources.dec.push_back(dec);
        sources.ra[nsource] *= DEGREE;
        sources.dec[nsource] *= DEGREE;
        iss >> mag >> sources.sedfilename[nsource] >> redshift >> gamma1 >>
            gamma2 >> kappa >> deltara >> deltadec;
        sources.redshift.push_back(redshift);
        sources.gamma1.push_back(gamma1);
        sources.gamma2.push_back(gamma2);
        sources.kappa.push_back(kappa);
        sources.deltara.push_back(deltara);
        sources.deltadec.push_back(deltadec);
        sources.deltara[nsource] *= DEGREE;
        sources.deltadec[nsource] *= DEGREE;
        sources.type[nsource] = sourcetype;
        magnification =
            2.5 *
            log10((1 - sources.kappa[nsource]) * (1 - sources.kappa[nsource]) -
                  sources.gamma1[nsource] * sources.gamma1[nsource] -
                  sources.gamma2[nsource] * sources.gamma2[nsource]);
        sources.norm[nsource] =
            pow(10.0, ((mag + magnification + 48.6) / (-2.5)));
        sources.mag[nsource] = mag + magnification;

        // read SED file
        if (nsource > 0) {
            for (i = 0; i < nsource; i++) {
                if (sources.sedfilename[i] == sources.sedfilename[nsource]) {
                    sources.sedptr[nsource] = sources.sedptr[i];
                    goto skipsedread;
                }
            }
        }
        sources.sedptr[nsource] = nsedptr;
        readSed(sources.sedfilename[nsource], 0);

    skipsedread:;
        sources.norm[nsource] = sources.norm[nsource] / (normwave) *
                                (1 + sources.redshift[nsource]) *
                                sedDwdp[sources.sedptr[nsource]];

        iss >> sources.spatialname[nsource];

        if (sources.spatialname[nsource] == "point") {
            sources.spatialtype[nsource] = POINT;
            nspatialpar = 0;
        }
        if (sources.spatialname[nsource].find("fit") != std::string::npos) {
            sources.spatialtype[nsource] = IMAGE;
            nspatialpar = 2;
        }
        if (sources.spatialname[nsource] == "gauss") {
            sources.spatialtype[nsource] = GAUSSIAN;
            nspatialpar = 1;
        }
        if (sources.spatialname[nsource] == "sersic") {
            sources.spatialtype[nsource] = SERSIC;
            nspatialpar = 6;
        }
        if (sources.spatialname[nsource] == "sersic2d") {
            sources.spatialtype[nsource] = SERSIC2D;
            nspatialpar = 4;
        }
        if (sources.spatialname[nsource] == "sersic2D") {
            sources.spatialtype[nsource] = SERSIC2D;
            nspatialpar = 4;
        }
        if (sources.spatialname[nsource] == "sersicComplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersiccomplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersicDiskComplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersicdiskcomplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersicDiscComplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersicdisccomplex") {
            sources.spatialtype[nsource] = SERSICCOMPLEX;
            nspatialpar = 14;
        }
        if (sources.spatialname[nsource] == "sersicDisk") {
            sources.spatialtype[nsource] = SERSICDISK;
            nspatialpar = 6;
        }
        if (sources.spatialname[nsource] == "sersicdisc") {
            sources.spatialtype[nsource] = SERSICDISK;
            nspatialpar = 6;
        }
        if (sources.spatialname[nsource] == "sersicDisc") {
            sources.spatialtype[nsource] = SERSICDISK;
            nspatialpar = 6;
        }
        if (sources.spatialname[nsource] == "sersicdisk") {
            sources.spatialtype[nsource] = SERSICDISK;
            nspatialpar = 6;
        }
        if (sources.spatialname[nsource] == "distortedSphere") {
            sources.spatialtype[nsource] = DISTORTEDSPHERE;
            nspatialpar = 5;
        }
        if (sources.spatialname[nsource] == "distortedSphere") {
            sources.spatialtype[nsource] = DISTORTEDSPHERE;
            nspatialpar = 10;
        }
        if (sources.spatialname[nsource] == "movingpoint") {
            sources.spatialtype[nsource] = MOVINGPOINT;
            nspatialpar = 2;
        }
        if (sources.spatialname[nsource] == "pinhole") {
            sources.spatialtype[nsource] = PINHOLE;
            nspatialpar = 4;
        }

        if (sources.spatialtype[nsource] == 1) {
            if (nsource > 0) {
                sources.skysameas[nsource] = -1;
                for (i = 0; i < nsource; i++) {
                    if (sources.spatialname[nsource] == sources.spatialname[i])
                        sources.skysameas[nsource] = i;
                }
            } else {
                sources.skysameas[nsource] = -1;
            }

            // read image file

            if (sources.skysameas[nsource] == -1) {
                sprintf(tempstring,
                        "%s/%s+0",
                        imagedir.c_str(),
                        sources.spatialname[nsource].c_str());

                ffptr = tempstring;
                status = 0;

                if (fits_open_file(&faptr, ffptr, READONLY, &status))
                    printf("Error opening %s\n", ffptr);
                fits_read_keys_lng(
                    faptr, (char *)"NAXIS", 1, 2, naxes, &nfound, &status);
                tempptr[nimage] = static_cast<float *>(
                    malloc(naxes[0] * naxes[1] * sizeof(float)));
                naxesb[nimage][0] = naxes[1];
                naxesb[nimage][1] = naxes[0];
                fits_read_img(faptr,
                              TFLOAT,
                              1,
                              naxes[0] * naxes[1],
                              &nullval,
                              tempptr[nimage],
                              &anynull,
                              &status);
                fits_close_file(faptr, &status);

                cumulative[nimage] = 0;
                cumulativex[nimage] = static_cast<float *>(
                    malloc(naxesb[nimage][0] * sizeof(float)));
                for (i = 0; i < naxesb[nimage][0]; i++) {
                    cumulativex[nimage][i] = cumulative[nimage];
                    for (j = 0; j < naxesb[nimage][1]; j++) {
                        if (*(tempptr[nimage] + i * naxesb[nimage][1] + j) <
                            0) {
                            *(tempptr[nimage] + i * naxesb[nimage][1] + j) = 0;
                        }
                        cumulative[nimage] +=
                            *(tempptr[nimage] + i * naxesb[nimage][1] + j);
                    }
                }

                sources.spatialpar[nsource][2] = nimage;
                nimage++;
            }
        }

        if (nspatialpar > 0) {
            for (i = 0; i < nspatialpar; i++) {
                iss >> sources.spatialpar[nsource][i];
            }
        }

        iss >> sources.dustnamez[nsource];

        sources.dusttypez[nsource] = 0;
        ndustparz = 0;
        if (sources.dustnamez[nsource] == "ccm") {
            sources.dusttypez[nsource] = 1;
            ndustparz = 2;
        } else if (sources.dustnamez[nsource] == "calzetti") {
            sources.dusttypez[nsource] = 2;
            ndustparz = 2;
        } else if (sources.dustnamez[nsource] == "CCM") {
            sources.dusttypez[nsource] = 1;
            ndustparz = 2;
        } else if (sources.dustnamez[nsource] == "CALZETTI") {
            sources.dusttypez[nsource] = 2;
            ndustparz = 2;
        }

        if (ndustparz > 0) {
            for (i = 0; i < ndustparz; i++) {
                iss >> sources.dustparz[nsource][i];
            }
        }

        iss >> sources.dustname[nsource];

        sources.dusttype[nsource] = 0;
        ndustpar = 0;
        if (sources.dustname[nsource] == "ccm") {
            sources.dusttype[nsource] = 1;
            ndustpar = 2;
        } else if (sources.dustname[nsource] == "calzetti") {
            sources.dusttype[nsource] = 2;
            ndustpar = 2;
        } else if (sources.dustname[nsource] == "CCM") {
            sources.dusttype[nsource] = 1;
            ndustpar = 2;
        } else if (sources.dustname[nsource] == "CALZETTI") {
            sources.dusttype[nsource] = 2;
            ndustpar = 2;
        }

        if (ndustpar > 0) {
            for (i = 0; i < ndustpar; i++) {
                iss >> sources.dustpar[nsource][i];
            }
        }

        double spra, spdec;
        spra = pra;
        spdec = pdec;
        if (aberration == 1) {
            double xprime, yprime, zprime;
            aberrationAxes(tai, &xprime, &yprime, &zprime);
            aberrationShift(&spra, &spdec, xprime, yprime, zprime);
        }
        if (precession == 1) {
            precessionShift(&spra, &spdec, tai, 51544.5);
        }
        if (nutation == 1) {
            double dlong, dobl, obl;
            nutationValues(tai, &dlong, &dobl);
            obl = obliquity(tai);
            nutationShift(&spra, &spdec, obl, dobl, dlong);
        }
        setup_tangent(spra, spdec, &tpx, &tpy, &tpz);

        ra = sources.ra[nsource] + sources.deltara[nsource];
        dec = sources.dec[nsource] + sources.deltadec[nsource];
        if (aberration == 1) {
            double xprime, yprime, zprime;
            aberrationAxes(tai, &xprime, &yprime, &zprime);
            aberrationShift(&ra, &dec, xprime, yprime, zprime);
        }
        if (precession == 1) {
            precessionShift(&ra, &dec, tai, 51544.5);
        }
        if (nutation == 1) {
            double dlong, dobl, obl;
            nutationValues(tai, &dlong, &dobl);
            obl = obliquity(tai);
            nutationShift(&ra, &dec, obl, dobl, dlong);
        }
        tangent(ra, dec, &x, &y, &tpx, &tpy, &tpz);

        sources.vx[nsource] = x * cos(rotatez) - y * sin(rotatez) + rotatex;
        sources.vy[nsource] = x * sin(rotatez) + y * cos(rotatez) + rotatey;
        sources.vz[nsource] = -1.0;
        nn = sqrt((sources.vx[nsource]) * (sources.vx[nsource]) +
                  (sources.vy[nsource]) * (sources.vy[nsource]) + 1);
        sources.vx[nsource] = sources.vx[nsource] / nn;
        sources.vy[nsource] = sources.vy[nsource] / nn;
        sources.vz[nsource] = sources.vz[nsource] / nn;

        nsource++;
        return (0);
    }

    int addOpd(const std::string &opd) {

        double x, y;
        double nn;
        double ra, dec;
        double wave;
        std::string id;

        if (nsource >= MAX_SOURCE) {
            std::perror("Too many OPD sources");
        }

        std::istringstream iss(opd);
        iss >> id >> ra >> dec;
        sources.id.push_back(id);
        sources.ra.push_back(ra);
        sources.dec.push_back(dec);
        sources.ra[nsource] *= DEGREE;
        sources.dec[nsource] *= DEGREE;
        iss >> wave;
        sources.redshift.push_back(0.0);
        sources.gamma1.push_back(wave);
        sources.gamma2.push_back(0.0);
        sources.kappa.push_back(0.0);
        sources.deltara.push_back(0.0);
        sources.deltadec.push_back(0.0);
        sources.type[nsource] = 6;
        sources.norm[nsource] = opdsize * opdsize * opdsampling * opdsampling;
        sources.mag[nsource] = opdsize * opdsize * opdsampling * opdsampling;
        sources.spatialtype[nsource] = OPD;
        sources.dusttypez[nsource] = 0;
        sources.dusttype[nsource] = 0;

        setup_tangent(pra, pdec, &tpx, &tpy, &tpz);

        tangent(sources.ra[nsource] + sources.deltara[nsource],
                sources.dec[nsource] + sources.deltadec[nsource],
                &x,
                &y,
                &tpx,
                &tpy,
                &tpz);

        sources.vx[nsource] = x * cos(rotatez) - y * sin(rotatez) + rotatex;
        sources.vy[nsource] = x * sin(rotatez) + y * cos(rotatez) + rotatey;
        sources.vz[nsource] = -1.0;
        nn = sqrt((sources.vx[nsource]) * (sources.vx[nsource]) +
                  (sources.vy[nsource]) * (sources.vy[nsource]) + 1);
        sources.vx[nsource] = sources.vx[nsource] / nn;
        sources.vy[nsource] = sources.vy[nsource] / nn;
        sources.vz[nsource] = sources.vz[nsource] / nn;

        nsource++;
        return (0);
    }

    int background() {

        // Bookkeeping setup
        char tempstring[4096];
        double focalLength = platescale / DEGREE;
        double aa, f, xp, yp, ra, dec, xv, yv, maxDistanceY, currbuffer,
            sourceDistanceX, sourceDistanceY, maxDistanceX;
        double dx, dy;
        long ndeci, nrai, deci, rai;
        long over, i, j;
        double dra, dis, cosdis;
        int nspatialpar, ii;
        char line[4096];
        std::string dir;
        std::string dir2;
        double x, y;
        double nn;
        double mag = 100;
        int diffusetype;
        double angularSeparationDeg, angularSepRadians, moonMagnitude,
            moonApparentMagnitude, scatter_function, moon_illuminance,
            lunar_illuminance, darkskyc_magnitude, darkskyp_magnitude;

        if (flatdir == 0) {
            dir = datadir + "/sky";
            dir2 = "../sky";
        } else if (flatdir == 1) {
            dir = ".";
            dir2 = ".";
        }

        if (atmospheremode) {
            airglow = static_cast<float *>(calloc(
                (airglowScreenSize) * (airglowScreenSize), sizeof(float)));
            sprintf(tempstring, "airglowscreen_%s.fits.gz", obshistid.c_str());
            fitsReadImage(tempstring, airglow);
        }

        // Calculations setup
        moonApparentMagnitude = -12.73 + 0.026 * (fabs(phaseang / DEGREE)) +
                                (4E-9) * pow(phaseang / DEGREE, 4);
        moon_illuminance = pow(10, -0.4 * (moonApparentMagnitude + 16.57));
        lunar_illuminance = 26.3311157 - 1.08572918 * log(moon_illuminance);

        // Twighlight
        float temp;
        float backgroundMagnitude =
            -2.5 * log10(0.5 * pow(10., -0.4 * airglowpintensity) +
                         0.5 * pow(10., -0.4 * airglowcintensity));
        float background_brightness =
            34.08 * exp(20.72333 - 0.92104 * backgroundMagnitude);
        float darksky[6];
        float darksky_data[2511][2];
        char darksky_sedfile[4096];
        FILE *darkskyF;

        sprintf(darksky_sedfile, "%s/darksky_sed.txt", dir.c_str());

        darkskyF = fopen(darksky_sedfile, "r");
        for (i = 0; i < 2511; i++) {
            fgets(line, 4096, darkskyF);
            sscanf(line, "%f %f\n", &darksky_data[i][0], &temp);
            darksky_data[i][1] = temp * background_brightness / 6.299537E-18;
        }
        fclose(darkskyF);
        darksky[0] = darksky_data[39][1];
        darksky[1] = darksky_data[491][1];
        darksky[2] = darksky_data[1019][1];
        darksky[3] = darksky_data[1547][1];
        darksky[4] = darksky_data[1961][1];
        darksky[5] = darksky_data[2250][1];

        float lunar[6];
        float lunarData[7500][2];
        char lunarSedfile[4096];
        FILE *lunarFp;

        sprintf(lunarSedfile, "%s/lunar_sed.txt", dir.c_str());

        lunarFp = fopen(lunarSedfile, "r");
        for (i = 0; i < 1441; i++) {
            fgets(line, 200, lunarFp);
            sscanf(line, "%f %f\n", &lunarData[i][0], &temp);
            lunarData[i][1] = temp * background_brightness / 3.882815E-16;
        }
        fclose(lunarFp);

        lunar[0] = lunarData[600][1];
        lunar[1] = lunarData[1800][1];
        lunar[2] = lunarData[3200][1];
        lunar[3] = lunarData[4600][1];
        lunar[4] = lunarData[5700][1];
        lunar[5] = lunarData[6700][1];

        float a[6][3] = {{11.78, 1.376, -0.039},
                         {11.84, 1.411, -0.041},
                         {11.84, 1.518, -0.057},
                         {11.40, 1.567, -0.064},
                         {10.93, 1.470, -0.062},
                         {10.43, 1.420, -0.052}};
        float color[6] = {0.67, 1.03, 0, -0.74, -1.90, -2.20};

        float magnitude = 100.0, brightness = 0.0,
              angle = solarzen / PH_PI * 180.0;
        j = filter;
        float alpha = 0.0, beta = 0.0;

        if (angle <= 106) {
            alpha = 1.0 - (angle - 95.0) / 11.0;
            beta = 1.0 - alpha;
            magnitude = a[j][0] + a[j][1] * (angle - 95.0) +
                        a[j][2] * (angle - 95.0) * (angle - 95.0);
        } else if (angle >= 254) {
            alpha = 1.0 - (265 - angle) / 11.0;
            beta = 1.0 - alpha;
            magnitude = a[j][0] + a[j][1] * (265.0 - angle) +
                        a[j][2] * (265.0 - angle) * (265.0 - angle);
        } else if ((angle > 106) && (angle < 254)) {
            alpha = 0.0;
            beta = 1.0;
            magnitude = a[j][0] + a[j][1] * (11.0) + a[j][2] * (11.0) * (11.0);
        }

        brightness = 34.08 * exp(20.72333 - 0.92104 * (magnitude - color[j]));

        float lunarMagnitude, darkskyMagnitude;
        float lunarBrightness, darkskyBrightness;
        float lunarPhotonCount, darkskyPhotonCount;

        lunarBrightness = 0.5 * (brightness - 2.0 * alpha * lunar[j]);
        darkskyBrightness = 0.5 * (brightness - 2.0 * beta * darksky[j]);

        if (lunarBrightness < 0)
            lunarBrightness = 0.0;
        if (darkskyBrightness < 0)
            darkskyBrightness = 0.0;

        lunarMagnitude = 26.33111 - 1.08573 * log(lunarBrightness) + color[j];
        darkskyMagnitude =
            26.33111 - 1.08573 * log(darkskyBrightness) + color[j];

        lunarPhotonCount = expf(-0.4 * lunarMagnitude * 2.30258509);
        if (lunarPhotonCount < 0)
            lunarPhotonCount = 0;

        darkskyPhotonCount = expf(-0.4 * darkskyMagnitude * 2.30258509);
        if (darkskyPhotonCount < 0)
            darkskyPhotonCount = 0;

        if ((angle > 106) && (angle < 130))
            darkskyPhotonCount *= exp(1 - 24.0 / fabs(angle - 130.0));
        if ((angle > 230) && (angle < 254))
            darkskyPhotonCount *= exp(1 - 24.0 / fabs(angle - 230.0));
        if ((angle >= 130) && (angle <= 230))
            darkskyPhotonCount = 0;

        if ((angle > 106) && (angle < 130))
            lunarPhotonCount *= exp(1 - 24.0 / fabs(angle - 130.0));
        if ((angle > 230) && (angle < 254))
            lunarPhotonCount *= exp(1 - 24.0 / fabs(angle - 230.0));
        if ((angle >= 130) && (angle <= 230))
            lunarPhotonCount = 0;

        // Zodiacal light
        double zl; // Zodiacal light luminosity
        double xx;
        double yy;
        double zz;
        double xEcliptic;
        double yEcliptic;
        double zEcliptic;
        double lambdaEcliptic; // Ecliptic longitude
        double betaEcliptic; // Ecliptic latitude
        double eclipticObliquity = 23.4 * DEGREE;
        double lambdaSun; // Sun longitude
        double deltaLambda; // Ecliptic longitude - Sun longitude

        // Source type loop
        for (diffusetype = 0; diffusetype < 6; diffusetype++) {
            // Conditional checks for observational parameters and modes
            if ((diffusetype == 0 && domelight < 100 && spaceMode == 0) ||
                (diffusetype == 1 && airglowcintensity < 100 &&
                 backgroundMode == 1 && telconfig == 0 && spaceMode == 0) ||
                (diffusetype == 2 && airglowpintensity < 100 &&
                 backgroundMode == 1 && telconfig == 0 && spaceMode == 0) ||
                (diffusetype == 3 && moonalt > 0 && backgroundMode == 1 &&
                 telconfig == 0 && spaceMode == 0) ||
                (diffusetype == 4 && moonalt > 0 && backgroundMode == 1 &&
                 telconfig == 0 && spaceMode == 0) ||
                (diffusetype == 5 && backgroundMode == 1 && telconfig == 0)) {

                // Populate sources
                ndeci = (long)(180 * 3600 / backRadius);
                over = (long)(60 / backRadius);
                if (over == 0)
                    over = 1;
                for (deci = 0; deci < ndeci; deci += over) {
                    dec = ((deci + 0.5 - ndeci / 2) / (ndeci)) * PH_PI;
                    nrai = ((long)(ndeci * cos(dec) * 2));

                    // Coarse placement
                    for (rai = 0; rai < nrai; rai += over) {
                        ra = 2 * PH_PI * ((rai + 0.5 - nrai / 2) / (nrai));

                        aa = cos(dec) * cos(ra - pra);
                        f = focalLength /
                            (sin(pdec) * sin(dec) + aa * cos(pdec));
                        yp = f * (cos(pdec) * sin(dec) - aa * sin(pdec));
                        xp = f * cos(dec) * sin(ra - pra);
                        xv = (xp * cos(-rotatez) + yp * sin(-rotatez));
                        yv = (-xp * sin(-rotatez) + yp * cos(-rotatez));
                        currbuffer =
                            backBuffer + (3 * backBeta * backRadius + 60) *
                                             ARCSEC * focalLength / pixsize;
                        maxDistanceX =
                            pixelsx * pixsize / 2.0 + currbuffer * pixsize;
                        maxDistanceY =
                            pixelsy * pixsize / 2.0 + currbuffer * pixsize;
                        dx = xv - centerx - decenterx;
                        dy = yv - centery - decentery;
                        sourceDistanceX =
                            fabs(cos(chipangle) * dx + sin(chipangle) * dy);
                        sourceDistanceY =
                            fabs(-sin(chipangle) * dx + cos(chipangle) * dy);

                        dra = fabs(ra - pra);
                        if (dra > PH_PI)
                            dra = 2 * PH_PI - dra;
                        cosdis = sin(dec) * sin(pdec) +
                                 cos(dec) * cos(pdec) * cos(dra);
                        if (cosdis > 1)
                            cosdis = 1.0;
                        if (cosdis < -1)
                            cosdis = -1.0;
                        dis = acos(cosdis);

                        if ((sourceDistanceX <= maxDistanceX) &&
                            (sourceDistanceY <= maxDistanceY) &&
                            (dis < PH_PI / 2)) {

                            // Fine placement
                            for (i = 0; i < over; i++) {
                                for (j = 0; j < over; j++) {
                                    dec =
                                        ((deci + i + 0.5 - ndeci / 2) / ndeci) *
                                        PH_PI;
                                    ra = 2 * PH_PI *
                                         ((rai + j + 0.5 - nrai / 2) / (nrai));
                                    aa = cos(dec) * cos(ra - pra);
                                    f = focalLength /
                                        (sin(pdec) * sin(dec) + aa * cos(pdec));
                                    yp = f * (cos(pdec) * sin(dec) -
                                              aa * sin(pdec));
                                    xp = f * cos(dec) * sin(ra - pra);
                                    xv = (xp * cos(-rotatez) +
                                          yp * sin(-rotatez));
                                    yv = (-xp * sin(-rotatez) +
                                          yp * cos(-rotatez));
                                    currbuffer =
                                        backBuffer + 3 * backBeta * backRadius *
                                                         ARCSEC * focalLength /
                                                         pixsize;
                                    maxDistanceX = pixelsx * pixsize / 2.0 +
                                                   currbuffer * pixsize;
                                    maxDistanceY = pixelsy * pixsize / 2.0 +
                                                   currbuffer * pixsize;
                                    dx = xv - centerx - decenterx;
                                    dy = yv - centery - decentery;
                                    sourceDistanceX = fabs(cos(chipangle) * dx +
                                                           sin(chipangle) * dy);
                                    sourceDistanceY =
                                        fabs(-sin(chipangle) * dx +
                                             cos(chipangle) * dy);

                                    dra = fabs(ra - pra);
                                    if (dra > PH_PI)
                                        dra = 2 * PH_PI - dra;
                                    cosdis = sin(dec) * sin(pdec) +
                                             cos(dec) * cos(pdec) * cos(dra);
                                    if (cosdis > 1)
                                        cosdis = 1.0;
                                    if (cosdis < -1)
                                        cosdis = -1.0;
                                    dis = acos(cosdis);

                                    if ((sourceDistanceX <= maxDistanceX) &&
                                        (sourceDistanceY <= maxDistanceY) &&
                                        (dis < PH_PI / 2)) {

                                        dra = fabs(ra - moonra);
                                        if (dra > PH_PI)
                                            dra = 2 * PH_PI - dra;
                                        cosdis =
                                            sin(dec) * sin(moondec) +
                                            cos(dec) * cos(moondec) * cos(dra);
                                        if (cosdis > 1)
                                            cosdis = 1.0;
                                        if (cosdis < -1)
                                            cosdis = -1.0;
                                        angularSepRadians = acos(cosdis);
                                        angularSeparationDeg =
                                            angularSepRadians * 180.0 / PH_PI;

                                        if (diffusetype == 3) {
                                            scatter_function =
                                                16.57 + 26.3311157 -
                                                2.5 * log10(4.0 * PH_PI) +
                                                2.5 * log10(PH_PI / 180.0 /
                                                            3600.0 * PH_PI /
                                                            180.0 / 3600.0) +
                                                2.5 *
                                                    log10(
                                                        2.0 / 2.78666 *
                                                        (1.06 +
                                                         cos(angularSepRadians) *
                                                             cos(angularSepRadians)));
                                        } else {
                                            scatter_function =
                                                16.57 + 26.3311157 -
                                                2.5 * log10(4.0 * PH_PI) +
                                                2.5 * log10(PH_PI / 180.0 /
                                                            3600.0 * PH_PI /
                                                            180.0 / 3600.0) +
                                                2.5 *
                                                    log10(
                                                        2.0 / 0.284181 *
                                                        (exp((-angularSeparationDeg /
                                                              40.0) *
                                                             2.30258509) +
                                                         1e-1));
                                        }
                                        if (moonalt < 0)
                                            moonMagnitude = 10000;
                                        else
                                            moonMagnitude = lunar_illuminance -
                                                            scatter_function;

                                        moonMagnitude =
                                            -2.5 *
                                            log10(lunarPhotonCount +
                                                  pow(10.0,
                                                      -0.4 * moonMagnitude));
                                        darkskyc_magnitude =
                                            -2.5 *
                                            log10(
                                                darkskyPhotonCount +
                                                pow(10.0,
                                                    -0.4 * airglowcintensity));
                                        darkskyp_magnitude =
                                            -2.5 *
                                            log10(
                                                darkskyPhotonCount +
                                                pow(10.0,
                                                    -0.4 * airglowpintensity));

                                        // Airglow variation
                                        long ax0, ax1, ay0, ay1;
                                        double dx, dy;
                                        find_linear_wrap(xv,
                                                         platescale * 15.0 /
                                                             3600,
                                                         airglowScreenSize,
                                                         &ax0,
                                                         &ax1,
                                                         &dx);
                                        find_linear_wrap(yv,
                                                         platescale * 15.0 /
                                                             3600,
                                                         airglowScreenSize,
                                                         &ay0,
                                                         &ay1,
                                                         &dy);

                                        if (diffusetype == 0)
                                            mag = domelight -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        if (diffusetype == 1) {
                                            double airglowv;
                                            airglowv =
                                                airglowvariation *
                                                (static_cast<double>(
                                                    interpolate_bilinear_float_wrap(
                                                        airglow,
                                                        airglowScreenSize,
                                                        ax0,
                                                        ax1,
                                                        dx,
                                                        ay0,
                                                        ay1,
                                                        dy)));
                                            mag = darkskyc_magnitude +
                                                  airglowv -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        }
                                        if (diffusetype == 2)
                                            mag = darkskyp_magnitude -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        if (diffusetype == 3)
                                            mag = moonMagnitude -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        if (diffusetype == 4)
                                            mag = moonMagnitude -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        if (diffusetype == 5) {

                                            if (overrideZodiacalLightMagnitude ==
                                                0) {

                                                // Calculate ecliptic longitude
                                                // and latitude Source position
                                                // in Cartesian coordinates
                                                xx = cos(ra) * cos(dec);
                                                yy = sin(ra) * cos(dec);
                                                zz = sin(dec);
                                                // Transform to ecliptic
                                                // coordinates
                                                xEcliptic = xx;
                                                yEcliptic =
                                                    yy *
                                                        cos(eclipticObliquity) +
                                                    zz * sin(eclipticObliquity);
                                                zEcliptic =
                                                    -yy *
                                                        sin(eclipticObliquity) +
                                                    zz * cos(eclipticObliquity);
                                                // Ecliptic longitue and
                                                // latitude
                                                lambdaEcliptic =
                                                    atan2(yEcliptic,
                                                          xEcliptic) /
                                                    DEGREE; // Degrees
                                                betaEcliptic =
                                                    atan(
                                                        zEcliptic /
                                                        sqrt(pow(xEcliptic, 2) +
                                                             pow(yEcliptic,
                                                                 2))) /
                                                    DEGREE; // Degrees

                                                // Zodiacal light variation
                                                // Follows Kwon et al. New
                                                // Astronomy 10-2 (2004)
                                                // pp91-107
                                                if (lambdaEcliptic < 0) {
                                                    lambdaEcliptic += 360.0;
                                                }
                                                lambdaSun =
                                                    fmod((640.466 -
                                                          0.985607 *
                                                              (day - 51544.0)),
                                                         360.0); // Degrees
                                                if (lambdaSun < 0) {
                                                    lambdaSun += 360.0;
                                                }
                                                deltaLambda = fmod(
                                                    lambdaEcliptic - lambdaSun,
                                                    360.0); // Degrees
                                                if (deltaLambda < 0) {
                                                    deltaLambda += 360.0;
                                                }
                                                zl =
                                                    (140.0 +
                                                     20.0 /
                                                         (1 +
                                                          pow(fabs(
                                                                  180.0 -
                                                                  deltaLambda) /
                                                                  20.0,
                                                              2.0))) *
                                                    (5.0 / 14.0 +
                                                     9.0 / 14.0 /
                                                         (1 +
                                                          pow((betaEcliptic) /
                                                                  20.0,
                                                              2.0)));

                                                zodiacalLightMagnitude =
                                                    -2.5 * (log10(zl)) + 27.78;
                                            }

                                            mag = zodiacalLightMagnitude -
                                                  2.5 * log10(backRadius *
                                                              backRadius);
                                        }

                                        if (mag < 100) {

                                            nspatialpar = 0;

                                            if (nsource >= MAX_SOURCE) {
                                                std::perror(
                                                    "Too many background "
                                                    "sources");
                                            }

                                            sources.id.push_back("0.0");
                                            sources.ra.push_back(ra);
                                            sources.dec.push_back(dec);
                                            if (diffusetype == 0 &&
                                                domewave == 0.0)
                                                sources.sedfilename[nsource] =
                                                    dir + "/sed_dome.txt";
                                            if (diffusetype == 0 &&
                                                domewave != 0.0)
                                                sources.sedfilename[nsource] =
                                                    "laser";
                                            if (diffusetype == 1)
                                                sources.sedfilename[nsource] =
                                                    dir + "/airglowc_sed.txt";
                                            if (diffusetype == 2)
                                                sources.sedfilename[nsource] =
                                                    dir + "/airglowp_sed.txt";
                                            if (diffusetype == 3)
                                                sources.sedfilename[nsource] =
                                                    dir + "/lunar_sed.txt";
                                            if (diffusetype == 4)
                                                sources.sedfilename[nsource] =
                                                    dir + "/lunar_sed.txt";
                                            if (diffusetype == 5)
                                                sources.sedfilename[nsource] =
                                                    dir + "/zodiacal_sed.txt";
                                            sources.redshift.push_back(0.0);
                                            sources.gamma1.push_back(0.0);
                                            sources.gamma2.push_back(0.0);
                                            sources.kappa.push_back(0.0);
                                            sources.deltara.push_back(0.0);
                                            sources.deltadec.push_back(0.0);
                                            sources.type[nsource] = diffusetype;
                                            sources.norm[nsource] = pow(
                                                10.0, ((mag + 48.6) / (-2.5)));
                                            sources.mag[nsource] = mag;

                                            // Read SED file
                                            if (nsource > 0) {
                                                for (ii = 0; ii < nsource;
                                                     ii++) {
                                                    if (sources
                                                            .sedfilename[ii] ==
                                                        sources.sedfilename
                                                            [nsource]) {
                                                        sources
                                                            .sedptr[nsource] =
                                                            sources.sedptr[ii];
                                                        goto skipsedread;
                                                    }
                                                }
                                            }

                                            sources.sedptr[nsource] = nsedptr;
                                            readSed(
                                                sources.sedfilename[nsource],
                                                1);

                                        skipsedread:;
                                            sources.norm[nsource] =
                                                sources.norm[nsource] /
                                                (normwave) *
                                                (1 +
                                                 sources.redshift[nsource]) *
                                                sedDwdp[sources
                                                            .sedptr[nsource]];

                                            sources.spatialname[nsource] =
                                                "gauss";
                                            if (sources.spatialname[nsource] ==
                                                "gauss") {
                                                sources.spatialtype[nsource] =
                                                    2;
                                                nspatialpar = 1;
                                            }
                                            sources.spatialpar[nsource][0] =
                                                backRadius * backEpsilon;
                                            sources.dustnamez[nsource] = "none";
                                            sources.dustname[nsource] = "none";

                                            setup_tangent(
                                                pra, pdec, &tpx, &tpy, &tpz);

                                            tangent(
                                                sources.ra[nsource] +
                                                    sources.deltara[nsource],
                                                sources.dec[nsource] +
                                                    sources.deltadec[nsource],
                                                &x,
                                                &y,
                                                &tpx,
                                                &tpy,
                                                &tpz);

                                            sources.vx[nsource] =
                                                x * cos(rotatez) -
                                                y * sin(rotatez) + rotatex;
                                            sources.vy[nsource] =
                                                x * sin(rotatez) +
                                                y * cos(rotatez) + rotatey;
                                            sources.vz[nsource] = -1.0;
                                            nn =
                                                sqrt((sources.vx[nsource]) *
                                                         (sources.vx[nsource]) +
                                                     (sources.vy[nsource]) *
                                                         (sources.vy[nsource]) +
                                                     1);
                                            sources.vx[nsource] =
                                                sources.vx[nsource] / nn;
                                            sources.vy[nsource] =
                                                sources.vy[nsource] / nn;
                                            sources.vz[nsource] =
                                                sources.vz[nsource] / nn;
                                            nsource++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return (0);
    }

    int filterTruncateSources() {

        double filterlow, filterhigh;
        long lowptr, highptr;
        double lowvalue, highvalue;

        filterlow = 0;
        filterhigh = maxwavelength;

        for (long i = 0; i < nsedptr; i++) {
            highvalue = 0;
            lowvalue = 0;
            lowptr = 0;
            highptr = 0;
            for (long j = 0; j < sedN[i]; j++) {

                if (sedW[sedPtr[i] + j] < filterlow) {
                    lowptr = j;
                    lowvalue = sedC[sedPtr[i] + j];
                }
                if (sedW[sedPtr[i] + j] < filterhigh) {
                    highptr = j;
                    highvalue = sedC[sedPtr[i] + j];
                }
            }

            sedCorr[i] = (sedC[sedPtr[i] + highptr] - sedC[sedPtr[i] + lowptr]);

            for (long j = lowptr; j < highptr + 1; j++) {
                sedC[sedPtr[i] + j] =
                    (sedC[sedPtr[i] + j] - lowvalue) / (highvalue - lowvalue);
            }
            sedPtr[i] = sedPtr[i] + lowptr;
            sedN[i] = highptr - lowptr;
        }

        for (long i = 0; i < nsource; i++) {
            if (sources.spatialtype[i] != OPD) {
                sources.norm[i] = sources.norm[i] * sedCorr[sources.sedptr[i]];
            }
        }

        return (0);
    }

    void readSed(const std::string &filename, int mode) {

        long lsedptr;
        char *sptr;
        char *sptr2;
        char *sptr3;
        char tempstring[4096];
        double tempf1;
        double tempf2;
        double dw;
        double cdw;
        long closestw = 0;
        long j;
        FILE *indafile;
        gzFile ingzfile;
        char line[4096];
        char tempstring1[512];
        char tempstring2[512];
        double monochromaticFactor;
        double standardwavelength = 500.0; // nm

        normwave = standardwavelength;
        monochromaticFactor = 1.0;

        if (sedptr == 0) {
            nreallocs = 0;
            sedMax = 4194304; // 4M elements, 32MB memory
            sedW =
                static_cast<double *>(malloc((long)(sedMax * sizeof(double))));
            sedC =
                static_cast<double *>(malloc((long)(sedMax * sizeof(double))));
        } else {
            if (sedptr > (sedMax - 100000)) {
                nreallocs++;
                sedMax = 2 * sedMax;
                sedW = static_cast<double *>(
                    realloc(sedW, (long)((sedMax) * sizeof(double))));
                sedC = static_cast<double *>(
                    realloc(sedC, (long)((sedMax) * sizeof(double))));
            }
        }

        lsedptr = 0;
        if (mode == 0)
            sprintf(tempstring, "%s/%s", seddir.c_str(), filename.c_str());
        if (mode == 1)
            sprintf(tempstring, "%s", filename.c_str());
        if (flatdir == 1) {
            sptr = strtok_r(tempstring, "/", &sptr2);
            do {
                sptr3 = sptr;
                sptr = strtok_r(nullptr, "/", &sptr2);
            } while (sptr != nullptr);
            sprintf(tempstring, "%s", sptr3);
        }

        if (strstr(tempstring, "laser") != nullptr) {
            normwave = domewave;
            closestw = 0;
            cdw = 1e30;

            tempf1 = 0.0;
            tempf2 = 0.0;
            for (int k = 0; k < 5; k++) {

                if (k == 0) {
                    tempf1 = minwavelength;
                    tempf2 = 0.0;
                }
                if (k == 1) {
                    tempf1 = domewave - 0.5e-6;
                    tempf2 = 0.0;
                }
                if (k == 2) {
                    tempf1 = domewave;
                    tempf2 = 1.0 / tempf1;
                }
                if (k == 3) {
                    tempf1 = domewave + 0.5e-6;
                    tempf2 = 0.0;
                }
                if (k == 4) {
                    tempf1 = maxwavelength;
                    tempf2 = 0.0;
                }

                sedW[sedptr + lsedptr] = tempf1;
                sedC[sedptr + lsedptr] = tempf2 * tempf1;

                dw = fabs(tempf1 - domewave);

                if (dw < cdw) {
                    cdw = dw;
                    closestw = lsedptr;
                }
                lsedptr++;
            }
            monochromaticFactor =
                normwave / 0.5e-6 *
                (0.01 * H_CGS / pow(10.0, (20.0 + 48.6) / (-2.5)));

        } else {

            if (strstr(tempstring, ".gz") == nullptr) {

                indafile = fopen(tempstring, "r");
                if (indafile == nullptr) {
                    fprintf(stderr, "Can't find SED file: %s\n", tempstring);
                    exit(1);
                }

                closestw = 0;
                cdw = 1e30;
                while (fgets(line, 4096, indafile)) {
                    sscanf(line, "%s %s", tempstring1, tempstring2);
                    tempf1 = strtod(tempstring1, nullptr);
                    tempf2 = strtod(tempstring2, nullptr);
                    sedW[sedptr + lsedptr] = tempf1;
                    sedC[sedptr + lsedptr] = tempf2 * tempf1;
                    if (tempstring1[0] != '#' &&
                        (tempstring2[0] == 'n' || tempstring2[0] == 'N')) {
                        fprintf(stderr,
                                "Warning:   SED file: %s contains a NaN!\n",
                                tempstring);
                        sedC[sedptr + lsedptr] = 0.0;
                    }

                    dw = fabs(tempf1 - standardwavelength);

                    if (dw < cdw && tempf2 > 0.0) {
                        cdw = dw;
                        closestw = lsedptr;
                    }
                    lsedptr++;
                    if (lsedptr >= 100000) {
                        fprintf(stderr,
                                "Error:  Too many lines in SED file: %s\n",
                                tempstring);
                        exit(1);
                    }
                }
                fclose(indafile);

            } else {

                ingzfile = gzopen(tempstring, "r");
                if (ingzfile == nullptr) {
                    fprintf(stderr, "Can't find SED file: %s\n", tempstring);
                    exit(1);
                }

                closestw = 0;
                cdw = 1e30;
                while (gzgets(ingzfile, line, 4096)) {
                    sscanf(line, "%s %s", tempstring1, tempstring2);
                    tempf1 = strtod(tempstring1, nullptr);
                    tempf2 = strtod(tempstring2, nullptr);

                    sedW[sedptr + lsedptr] = tempf1;
                    sedC[sedptr + lsedptr] = tempf2 * tempf1;
                    if (tempstring1[0] != '#' &&
                        (tempstring2[0] == 'n' || tempstring2[0] == 'N')) {
                        fprintf(stderr,
                                "Warning:   SED file: %s contains a NaN!\n",
                                tempstring);
                        sedC[sedptr + lsedptr] = 0.0;
                    }

                    dw = fabs(tempf1 - standardwavelength);

                    if (dw < cdw && tempf2 > 0.0) {
                        cdw = dw;
                        closestw = lsedptr;
                    }
                    lsedptr = lsedptr + 1;
                    if (lsedptr >= 100000) {
                        fprintf(stderr,
                                "Error:  Too many lines in SED file: %s\n",
                                tempstring);
                        exit(1);
                    }
                }
                gzclose(ingzfile);
            }

            // monochromatic exception
            if (lsedptr == 1) {
                lsedptr--;
                normwave = tempf1;
                closestw = 0;
                cdw = 1e30;

                tempf1 = 0.0;
                tempf2 = 0.0;
                for (int k = 0; k < 5; k++) {

                    if (k == 0) {
                        tempf1 = minwavelength;
                        tempf2 = 0.0;
                    }
                    if (k == 1) {
                        tempf1 = normwave - 0.5e-6;
                        tempf2 = 0.0;
                    }
                    if (k == 2) {
                        tempf1 = normwave;
                        tempf2 = 1.0 / normwave;
                    }
                    if (k == 3) {
                        tempf1 = normwave + 0.5e-6;
                        tempf2 = 0.0;
                    }
                    if (k == 4) {
                        tempf1 = maxwavelength;
                        tempf2 = 0.0;
                    }

                    sedW[sedptr + lsedptr] = tempf1;
                    sedC[sedptr + lsedptr] = tempf2 * tempf1;

                    dw = fabs(tempf1 - normwave);

                    if (dw < cdw) {
                        cdw = dw;
                        closestw = lsedptr;
                    }
                    lsedptr++;
                }
                monochromaticFactor =
                    normwave / 0.5e-6 *
                    (0.01 * H_CGS / pow(10.0, (20.0 + 48.6) / (-2.5)));
            }
        }

        for (j = 0; j < lsedptr; j++) {
            if (j != 0 && j != (lsedptr - 1))
                sedC[sedptr + j] =
                    sedC[sedptr + j] *
                    (sedW[sedptr + j + 1] - sedW[sedptr + j - 1]) / 2.0;
            if (j == 0)
                sedC[sedptr + j] = sedC[sedptr + j] *
                                   (sedW[sedptr + j + 1] - sedW[sedptr + j]);
            if (j == (lsedptr - 1))
                sedC[sedptr + j] = sedC[sedptr + j] *
                                   (sedW[sedptr + j] - sedW[sedptr + j - 1]);
        }

        tempf1 = 0;
        for (j = 0; j < lsedptr; j++) {
            tempf1 += sedC[sedptr + j];
        }
        for (j = 0; j < lsedptr; j++) {
            sedC[sedptr + j] = sedC[sedptr + j] / tempf1;
        }

        if (closestw == 0) {
            sedDwdp[nsedptr] =
                (sedW[sedptr + closestw + 1] - sedW[sedptr + closestw]) /
                sedC[sedptr + closestw];
        } else {
            sedDwdp[nsedptr] =
                (sedW[sedptr + closestw + 1] - sedW[sedptr + closestw - 1]) /
                sedC[sedptr + closestw] / 2.0;
        }

        if (sedC[sedptr + closestw] <= 0.0) {
            printf("Error in SED file; 0 value at normalizing wavelength\n");
            sedDwdp[nsedptr] = 0.0;
        }
        sedDwdp[nsedptr] = sedDwdp[nsedptr] * monochromaticFactor;

        for (j = 1; j < lsedptr; j++) {
            sedC[sedptr + j] += sedC[sedptr + j - 1];
        }

        sedN[nsedptr] = lsedptr;
        sedPtr[nsedptr] = sedptr;
        sedptr += lsedptr;
        nsedptr++;

        if (nsedptr >= 10000) {
            printf("Error:   Too many SED files\n");
            exit(1);
        }
    }

    int parser() {

        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Photon Raytrace" << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Installing Universe." << std::endl;

        double altitude, solaralt;
        int extraCommandFlag = 0;
        sourceperthread = 1;
        nphot = 0;
        totalnorm = 0.0;
        nsedptr = 0;
        sedptr = 0;
        nsource = 0;
        nimage = 0;
        nsurf = 0;
        maxr = 4180; // mm
        minr = 2558; // mm
        exptime = 15.0;
        vistime = 33.0;
        nsnap = 2;
        nframes = 1;
        nskip = 0;
        shuttererror = 0.0;
        timeoffset = 0.0;
        pra = 0.0;
        pdec = 0.0;
        rotatex = -1;
        rotatey = -1;
        rotatez = 0.0;
        spiderangle = 0.0;
        zenith = 0.0;
        airmass = 1.0;
        azimuth = 0.0;
        windjitter = 2.5;
        rotationjitter = 1;
        elevationjitter = 0.02;
        azimuthjitter = 0.02;
        impurityvariation = 1;
        fieldanisotropy = 1;
        fringeflag = 1;
        deadlayer = 1;
        chargesharing = 1;
        pixelerror = 1;
        poissonMode = 1;
        telescopeMode = 1;
        backgroundMode = 1;
        coatingmode = 1;
        chargediffusion = 1;
        photoelectric = 1;
        detectorcollimate = 0;
        contaminationmode = 1;
        trackingMode = 1;
        detectorMode = 1;
        diffractionMode = 1;
        spiderMode = 1;
        pupilscreenMode = 0;
        spaceMode = -1;
        pressure = 520;
        waterPressure = 8;
        temperature = 5;
        airrefraction = 1;
        raynorm = 1;
        o2norm = 1;
        o3norm = 1;
        h2onorm = 1;
        aerosoltau = 0.02;
        aerosolindex = -1.28;
        o2gradient = 0.0;
        o3gradient = 0.0;
        h2ogradient = 0.0;
        aerosolgradient = 0.0;
        raygradient = 0.0;
        rayAngle = 0.0;
        o2Angle = 0.0;
        o3Angle = 0.0;
        h2oAngle = 0.0;
        aerosolAngle = 0.0;
        ranseed = -1;
        obsseed = 0;
        zernikemode = 1;
        atmospheric_dispersion = 1;
        atmosphericdispcenter = 1;
        aberration = 0;
        precession = 0;
        nutation = 0;
        outputdir = "../output";
        workdir = ".";
        seddir = "../data/SEDs";
        imagedir = "../data/images";
        datadir = "../data";
        instrdir = "../data/lsst";
        bindir = "../data/lsst";
        outputfilename = "focalplane";
        chipid = "R22_S11";
        trackingfile = ".";
        natmospherefile = 0;
        straylight = 1;
        straylightcut = 10.0;
        aperturemode = 0;
        ghostonly = 0;
        saturation = 1;
        eventfile = 0;
        opdfile = 0;
        opdsize = OPD_SCREEN_SIZE;
        opdsampling = OPD_SAMPLING;
        eventFitsFileName = "output.fits.gz";
        centroidfile = 0;
        throughputfile = 0;
        filter = 0;
        blooming = 1;
        obshistid = "0";
        pairid = 0;
        tai = 0.0;
        domeseeing = 0.1;
        toypsf = 0.0;
        finiteDistance = 0.0;
        transtol = 1e-2;
        backAlpha = 0.1;
        backGamma = 1.0;
        backDelta = 80.0;
        screentol = 0.01;
        overrideZodiacalLightMagnitude = 0;
        backBeta = 4.0;
        backRadius = 10.0; // arcseconds
        backBuffer = 100.0;
        activeBuffer = 10;
        backEpsilon = 1.0;
        np = 0.90;
        satbuffer = 5;
        date = 1;
        overdepbias = -45.0;
        sensorTempNominal = -1; // K
        sensorTempDelta = 0; // K
        qevariation = 0.0;
        airglowvariation = 1.0;
        airglowScreenSize = 1024;
        laScatterProb = 0.135;
        totalseeing = 0.67;
        flatdir = 0;
        tarfile = 0;
        atmdebug = 0;
        largeScale = 1.0;
        coarseScale = 1.0;
        mediumScale = 1.0;
        fineScale = 1.0;
        largeGrid = 1;
        coarseGrid = 1;
        mediumGrid = 1;
        fineGrid = 1;
        moonalt = -1.0 * PH_PI / 2.0 + 0.00001;
        moondist = PH_PI - 0.00001;
        phaseang = PH_PI - 0.00001;
        solarzen = PH_PI - 0.00001;
        airglowcintensity = 22.08;
        airglowpintensity = 22.08;
        moonra = 0; // degrees
        moondec = 0; // degrees
        domelight = 1000.0;
        domewave = 0.0;
        raydensity = 0.6;
        scalenumber = 8.0;
        checkpointtotal = 0;
        checkpointcount = 0;
        areaExposureOverride = 0;
        opticsonlymode = 0;
        additivemode = 1;

        // update these values when instrdir is set
        // variables in focalplanelayout.txt
        centerx = -1; // microns
        centery = -1; // microns
        pixsize = -1; // microns
        pixelsx = -1;
        pixelsy = -1;
        minx = -1;
        miny = -1;
        maxx = -1;
        maxy = -1;
        sensorthickness = -1; // microns
        // variables in location.txt
        groundlevel = -1;
        xtelloc = -1; // m
        ytelloc = -1; // m
        latitude = -1; // degrees
        longitude = -1; // degrees
        // variables in central_wavelengths.txt
        filterName = "";
        minwavelength = -1;
        maxwavelength = -1;
        centralwavelength = -1;
        platescale = -1;

        // variables in sensor.txt
        well_depth = -1; // electrons
        nbulk = -1;
        nf = -1;
        nb = -1;
        sf = -1;
        sb = -1;
        // variables in tracking.txt
        windjitter = -1;
        rotationjitter = -1;
        elevationjitter = -1;
        azimuthjitter = -1;
        // bookkeeping for atmosphere-related control structure
        opacitymode = 0;
        atmospheremode =
            2; // 2 = everything on for the purposes of the code (use
               // Kolmogorov diffraction) 1 = no turbulence (use pupil
               // diffraction) 0 = atmosphere all off (skip some setup)

        /* atmosphere parameter arrays */
        atmospherefile.resize(MAX_LAYER);
        cloudfile.resize(MAX_LAYER);
        seefactor.resize(MAX_LAYER, 0);
        wind.resize(MAX_LAYER, 0);
        winddir.resize(MAX_LAYER, 0);
        outerscale.resize(MAX_LAYER, 0);
        height.resize(MAX_LAYER, 0);
        cloudmean.resize(MAX_LAYER, 0);
        cloudvary.resize(MAX_LAYER, 0);
        dtau = static_cast<double *>(calloc(MAX_LAYER, sizeof(double)));

        /* telescope parameter arrays */
        std::vector<std::vector<double>> tbody;
        std::vector<std::vector<double>> tizernike;
        izernike.resize(MAX_SURF);
        tizernike.resize(MAX_SURF);
        pertType.resize(MAX_SURF);
        NTERM = NZERN;
        if (NZERN < NCHEB)
            NTERM = NCHEB;
        for (int i = 0; i < MAX_SURF; i++) {
            izernike[i].resize(NTERM, 0);
            tizernike[i].resize(NTERM, 0);
        }
        for (int i = 0; i < MAX_SURF; i++) {
            surfaceLink.push_back(i);
        }
        body.resize(MAX_SURF);
        tbody.resize(MAX_SURF);
        for (int i = 0; i < MAX_SURF; i++) {
            body[i].resize(6, 0);
            tbody[i].resize(6, 0);
        }
        ghost.resize(MAX_SURF, 0);
        feaflag.resize(MAX_SURF, 0);
        feafile.resize(MAX_SURF * MAX_SURF * 2);
        feascaling.resize(MAX_SURF * MAX_SURF * 2);

        /* sky parameter arrays */
        sources.vx = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.vy = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.vz = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.norm =
            static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.mag = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.type = static_cast<int *>(calloc(MAX_SOURCE, sizeof(int)));
        sedCorr = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sedDwdp = static_cast<double *>(calloc(MAX_SOURCE, sizeof(double)));
        sources.spatialtype =
            static_cast<int *>(calloc(MAX_SOURCE, sizeof(int)));
        sources.dusttype = static_cast<int *>(calloc(MAX_SOURCE, sizeof(int)));
        sources.dusttypez = static_cast<int *>(calloc(MAX_SOURCE, sizeof(int)));
        sources.sedptr = static_cast<long *>(calloc(MAX_SOURCE, sizeof(long)));
        // sources.id = (double*)calloc(MAX_SOURCE, sizeof(double));
        sources.skysameas =
            static_cast<long *>(calloc(MAX_SOURCE, sizeof(long)));
        sedN = static_cast<long *>(calloc(MAX_SOURCE, sizeof(long)));
        sedPtr = static_cast<long *>(calloc(MAX_SOURCE, sizeof(long)));
        sourceXpos =
            static_cast<long long *>(calloc(MAX_SOURCE, sizeof(long long)));
        sourceYpos =
            static_cast<long long *>(calloc(MAX_SOURCE, sizeof(long long)));
        sourcePhoton =
            static_cast<long long *>(calloc(MAX_SOURCE, sizeof(long long)));
        sources.spatialpar = (double **)calloc(MAX_SOURCE, sizeof(double *));
        for (int i = 0; i < MAX_SOURCE; i++) {
            sources.spatialpar[i] =
                static_cast<double *>(calloc(14, sizeof(double)));
        }
        sources.dustpar =
            static_cast<double **>(calloc(MAX_SOURCE, sizeof(double *)));
        for (int i = 0; i < MAX_SOURCE; i++) {
            sources.dustpar[i] =
                static_cast<double *>(calloc(2, sizeof(double)));
        }
        sources.dustparz =
            static_cast<double **>(calloc(MAX_SOURCE, sizeof(double *)));
        for (int i = 0; i < MAX_SOURCE; i++) {
            sources.dustparz[i] =
                static_cast<double *>(calloc(2, sizeof(double)));
        }
        sources.sedfilename.resize(MAX_SOURCE);
        sources.spatialname.resize(MAX_SOURCE);
        sources.dustname.resize(MAX_SOURCE);
        sources.dustnamez.resize(MAX_SOURCE);

        // get instrdir first
        readText pars(std::cin);
        for (size_t t(0); t < pars.getSize(); t++) {
            std::string line(pars[t]);
            readText::get(line, "instrdir", instrdir);
        }

        // read central wavelengths
        std::istringstream wavelengthPars(
            readText::get(instrdir + "/central_wavelengths.txt", filter));
        std::string filterNameT;
        double minwavelengthT, maxwavelengthT, centralwavelengthT, platescaleT;
        wavelengthPars >> filterNameT >> minwavelengthT >> maxwavelengthT >>
            centralwavelengthT >> platescaleT;
        if (filterName == "")
            filterName = filterNameT;
        if (minwavelength == -1)
            minwavelength = minwavelengthT * 1000; // nm
        if (maxwavelength == -1)
            maxwavelength = maxwavelengthT * 1000; // nm
        if (centralwavelength == -1)
            centralwavelength = centralwavelengthT; // um
        if (platescale == -1)
            platescale = platescaleT; // um deg-1

        // read tracking file
        // need to know optional telescope rotation before we look for sources
        std::string sss;
        sss = instrdir + "/tracking.txt";
        std::ifstream inStream3(sss.c_str());
        if (inStream3) {
            readText trackingPars(instrdir + "/tracking.txt");
            for (size_t t(0); t < trackingPars.getSize(); t++) {
                std::string line(trackingPars[t]);
                if (windjitter == -1)
                    readText::get(line, "windjitter", windjitter);
                if (rotationjitter == -1)
                    readText::get(line, "rotationjitter", rotationjitter);
                if (elevationjitter == -1)
                    readText::get(line, "elevationjitter", elevationjitter);
                if (azimuthjitter == -1)
                    readText::get(line, "azimuthjitter", azimuthjitter);
                // fiducial field angles (equivalent to rotating entire
                // telescope)
                if (rotatex == -1) {
                    if (readText::getKey(line, "rotatex", rotatex))
                        rotatex *= (-DEGREE);
                }
                if (rotatey == -1) {
                    if (readText::getKey(line, "rotatey", rotatey))
                        rotatey *= (-DEGREE);
                }
            }
        } else {
            windjitter = 2.5;
            rotationjitter = 0.0;
            elevationjitter = 0.0;
            azimuthjitter = 0.0;
        }
        if (rotatex == -1)
            rotatex = 0.0;
        if (rotatey == -1)
            rotatey = 0.0;

        // read parameters file
        for (size_t t(0); t < pars.getSize(); t++) {
            std::string line(pars[t]);
            std::istringstream iss(line);
            std::string keyName;
            iss >> keyName;

            if (keyName == "object") {
                std::string object;
                std::getline(iss, object);
                addSource(object, 6);
                continue;
            }
            if (keyName == "opd") {
                std::string opd;
                std::getline(iss, opd);
                addOpd(opd);
                continue;
            }

            readText::get(line, "outputdir", outputdir);
            readText::get(line, "workdir", workdir);
            readText::get(line, "seddir", seddir);
            readText::get(line, "imagedir", imagedir);
            readText::get(line, "datadir", datadir);
            readText::get(line, "bindir", bindir);
            readText::get(line, "sourceperthread", sourceperthread);
            readText::get(line, "thread", numthread);
            readText::get(line, "atmospheremode", atmospheremode);
            readText::get(line, "telescopemode", telescopeMode);
            readText::get(line, "poissonmode", poissonMode);
            readText::get(line, "backgroundmode", backgroundMode);
            readText::get(line, "impurityvariation", impurityvariation);
            readText::get(line, "fieldanisotropy", fieldanisotropy);
            readText::get(line, "fringing", fringeflag);
            readText::get(line, "deadlayer", deadlayer);
            readText::get(line, "chargesharing", chargesharing);
            readText::get(line, "photoelectric", photoelectric);
            readText::get(line, "detectorcollimate", detectorcollimate);
            readText::get(line, "pixelerror", pixelerror);
            readText::get(line, "chargediffusion", chargediffusion);
            readText::get(line, "coatingmode", coatingmode);
            readText::get(line, "contaminationmode", contaminationmode);
            readText::get(line, "trackingmode", trackingMode);
            readText::get(line, "detectormode", detectorMode);
            readText::get(line, "diffractionmode", diffractionMode);
            readText::get(line, "spidermode", spiderMode);
            readText::get(line, "pupilscreenmode", pupilscreenMode);
            readText::get(line, "spacemode", spaceMode);
            readText::get(line, "zernikemode", zernikemode);
            readText::get(line, "straylight", straylight);
            readText::get(line, "straylightcut", straylightcut); // not exposed
            readText::get(line, "ghost", ghost); // no exposed
            readText::get(line, "ghostonly", ghostonly);
            readText::get(line, "aperturemode", aperturemode);
            readText::get(line, "areaexposureoverride", areaExposureOverride);
            readText::get(line, "opticsonlymode", opticsonlymode);
            readText::get(line, "additivemode", additivemode);
            readText::get(line, "minr", minr);
            readText::get(line, "maxr", maxr);
            readText::get(line, "exptime", exptime);
            if (readText::getKey(line, "nsnap", nsnap))
                extraCommandFlag = 1;
            readText::get(line, "nframes", nframes);
            readText::get(line, "nskip", nskip);
            readText::get(line, "shuttererror", shuttererror);
            readText::get(line, "timeoffset", timeoffset);
            readText::get(line, "finitedistance", finiteDistance);
            readText::get(line, "transtol", transtol); // not exposed
            readText::get(line, "np", np); // not exposed
            readText::get(line, "satbuffer", satbuffer); // not exposed
            readText::get(line, "activebuffer", activeBuffer); // not exposed
            readText::get(line, "backalpha", backAlpha); // not exposed
            readText::get(line, "backgamma", backGamma); // not exposed
            readText::get(line, "backdelta", backDelta); // not exposed
            readText::get(line, "backepsilon", backEpsilon); // not exposed
            readText::get(line, "screentol", screentol); // not exposed
            if (readText::getKey(
                    line, "zodiacallightmag", zodiacalLightMagnitude))
                overrideZodiacalLightMagnitude = 1;
            readText::get(line, "backbeta", backBeta); //  not exposed
            readText::get(line, "backradius", backRadius); // not exposed
            readText::get(line, "backbuffer", backBuffer); // not exposed
            readText::get(line, "date", date); // not exposed
            readText::get(line, "dayofyear", day); // not exposed
            readText::get(line, "flatdir", flatdir); // not exposed
            readText::get(line, "tarfile", tarfile); // not exposed
            readText::get(line, "atmdebug", atmdebug); // not exposed
            readText::get(line, "large_grid", largeGrid); // not exposed
            readText::get(line, "coarse_grid", coarseGrid); // not exposed
            readText::get(line, "medium_grid", mediumGrid); // not exposed
            readText::get(line, "fine_grid", fineGrid); // not exposed
            readText::get(line, "large_scale", largeScale); // not exposed
            readText::get(line, "coarse_scale", coarseScale); // not exposed
            readText::get(line, "medium_scale", mediumScale); // not exposed
            readText::get(line, "fine_scale", fineScale); // not exposed
            readText::get(line, "opdfile", opdfile); // not exposed
            readText::get(line, "opdsampling", opdsampling);
            readText::get(line, "opdsize", opdsize);
            if (readText::getKey(line, "filter", filter)) {
                // read central wavelengths again
                std::istringstream wavelengthParsQ(readText::get(
                    instrdir + "/central_wavelengths.txt", filter));
                std::string filterNameQ;
                double minwavelengthQ, maxwavelengthQ, centralwavelengthQ,
                    platescaleQ;
                wavelengthParsQ >> filterNameQ >> minwavelengthQ >>
                    maxwavelengthQ >> centralwavelengthQ >> platescaleQ;
                filterName = filterNameQ;
                minwavelength = minwavelengthQ * 1000; // nm
                maxwavelength = maxwavelengthQ * 1000; // nm
                centralwavelength = centralwavelengthQ; // um
                platescale = platescaleQ; // um deg-1
            }
            readText::get(line, "saturation", saturation);
            readText::get(line, "aberration", aberration);
            readText::get(line, "nutation", nutation);
            readText::get(line, "precession", precession);
            readText::get(line, "blooming", blooming);
            readText::get(line, "eventfile", eventfile);
            readText::get(line, "eventFitsFileName", eventFitsFileName);
            readText::get(line, "centroidfile", centroidfile);
            readText::get(line, "throughputfile", throughputfile);
            readText::get(line, "well_depth", well_depth);
            readText::get(line, "nbulk", nbulk);
            readText::get(line, "nf", nf);
            readText::get(line, "nb", nb);
            readText::get(line, "sf", sf);
            readText::get(line, "sb", sb);
            readText::get(line, "sensortempnominal", sensorTempNominal);
            readText::get(line, "sensorthickness", sensorthickness);
            readText::get(line, "overdepbias", overdepbias);
            readText::get(line, "sensortempdelta", sensorTempDelta);
            readText::get(line, "qevariation", qevariation);
            readText::get(line, "obshistid", obshistid); //
            readText::get(line, "exposureid", pairid); //
            readText::get(line, "tai", tai); //
            readText::get(line, "windjitter", windjitter);
            readText::get(line, "rotationjitter", rotationjitter);
            readText::get(line, "elevationjitter", elevationjitter);
            readText::get(line, "azimuthjitter", azimuthjitter);

            if (keyName == "izernike") {
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        tizernike[i][j] = izernike[i][j];
                        if (additivemode == 1)
                            izernike[i][j] = 0.0;
                    }
                }
                long surfaceIndex;
                readText::get(line, "izernike", izernike);
                iss >> surfaceIndex;
                pertType[surfaceIndex].assign("zern");
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        if (additivemode == 1)
                            izernike[i][j] += tizernike[i][j];
                    }
                }
            }
            if (keyName == "ichebyshev") {
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        tizernike[i][j] = izernike[i][j];
                        if (additivemode == 1)
                            izernike[i][j] = 0.0;
                    }
                }
                long surfaceIndex;
                readText::get(line, "ichebyshev", izernike);
                iss >> surfaceIndex;
                pertType[surfaceIndex].assign("chebyshev");
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        if (additivemode == 1)
                            izernike[i][j] += tizernike[i][j];
                    }
                }
            }
            readText::get(line, "surfacelink", surfaceLink);
            if (keyName == "body") {
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < 6; j++) {
                        tbody[i][j] = body[i][j];
                        if (additivemode == 1)
                            body[i][j] = 0.0;
                    }
                }
                readText::get(line, "body", body);
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < 6; j++) {
                        if (additivemode == 1)
                            body[i][j] += tbody[i][j];
                    }
                }
            }
            readText::get(line, "natmospherefile", natmospherefile);
            readText::get(line, "atmospherefile", atmospherefile);
            readText::get(line, "cloudfile", cloudfile);
            readText::get(line, "trackingfile", trackingfile);
            readText::get(line, "chipid", chipid);
            readText::get(line, "seeing", seefactor);
            readText::get(line, "wind", wind);
            readText::get(line, "winddir", winddir);
            readText::get(line, "outerscale", outerscale);
            readText::get(line, "height", height);
            readText::get(line, "rayangle", rayAngle);
            readText::get(line, "o2angle", o2Angle);
            readText::get(line, "o3angle", o3Angle);
            readText::get(line, "h2oangle", h2oAngle);
            readText::get(line, "aerosolangle", aerosolAngle);
            readText::get(line, "raygradient", raygradient);
            readText::get(line, "o2gradient", o2gradient);
            readText::get(line, "o3gradient", o3gradient);
            readText::get(line, "h2ogradient", h2ogradient);
            readText::get(line, "aerosolgradient", aerosolgradient);
            readText::get(line, "cloudmean", cloudmean);
            readText::get(line, "cloudvary", cloudvary);
            readText::get(
                line, "atmosphericdispersion", atmospheric_dispersion);
            readText::get(line, "atmosphericdispcenter", atmosphericdispcenter);
            readText::get(line, "seed", ranseed);
            readText::get(line, "obsseed", obsseed); //
            readText::get(line, "vistime", vistime); //
            readText::get(line, "pressure", pressure);
            readText::get(line, "waterpressure", waterPressure);
            readText::get(line, "temperature", temperature);
            readText::get(line, "airrefraction", airrefraction);
            readText::get(line, "reldensity", raynorm);
            readText::get(line, "relo2", o2norm);
            readText::get(line, "relo3", o3norm);
            readText::get(line, "relh2o", h2onorm);
            readText::get(line, "aerosoltau", aerosoltau);
            readText::get(line, "aerosolindex", aerosolindex);
            readText::get(line, "lascatprob", laScatterProb);
            readText::get(line, "domeseeing", domeseeing);
            readText::get(line, "toypsf", toypsf);
            readText::get(line, "airglowvariation", airglowvariation); //
            readText::get(line, "totalseeing", totalseeing); //
            readText::get(line, "airglowcintensity", airglowcintensity); //
            readText::get(line, "airglowpintensity", airglowpintensity); //
            readText::get(line, "domelight", domelight); //
            readText::get(line, "telconfig", telconfig); //
            readText::get(line, "checkpointcount", checkpointcount); //
            readText::get(line, "checkpointtotal", checkpointtotal); //
            readText::get(line, "domewave", domewave); //
            readText::get(line, "raydensity", raydensity); //
            readText::get(line, "scalenumber", scalenumber); //
            readText::get(line, "centerx", centerx);
            readText::get(line, "centery", centery);
            readText::get(line, "pixelsize", pixsize);
            readText::get(line, "pixelsx", pixelsx);
            readText::get(line, "pixelsy", pixelsy);
            readText::get(line, "minx", minx);
            readText::get(line, "miny", miny);
            readText::get(line, "maxx", maxx);
            readText::get(line, "maxy", maxy);
            readText::get(line, "wavelength", centralwavelength);
            readText::get(line, "platescale", platescale);
            readText::get(line, "groundlevel", groundlevel);
            readText::get(line, "xtellocation", xtelloc);
            readText::get(line, "ytellocation", ytelloc); // up to here
            if (readText::getKey(line, "latitude", latitude))
                latitude *= DEGREE;
            if (readText::getKey(line, "longitude", longitude))
                longitude *= DEGREE;
            if (readText::getKey(line, "pointingra", pra))
                pra *= DEGREE;
            if (readText::getKey(line, "pointingdec", pdec))
                pdec *= DEGREE;
            if (readText::getKey(line, "rotatex", rotatex))
                rotatex *= (-DEGREE);
            if (readText::getKey(line, "rotatey", rotatey))
                rotatey *= (-DEGREE);
            if (readText::getKey(line, "rotationangle", rotatez))
                rotatez *= (-DEGREE);
            if (readText::getKey(line, "spiderangle", spiderangle))
                spiderangle *= DEGREE;
            if (readText::getKey(line, "altitude", altitude)) {
                altitude *= DEGREE;
                zenith = PH_PI / 2.0 - altitude;
            }
            if (readText::getKey(line, "zenith", zenith))
                zenith *= DEGREE;
            if (readText::getKey(line, "azimuth", azimuth))
                azimuth *= DEGREE;
            if (readText::getKey(line, "moonra", moonra))
                moonra *= DEGREE;
            if (readText::getKey(line, "moondec", moondec))
                moondec *= DEGREE;
            if (readText::getKey(line, "moonalt", moonalt))
                moonalt *= DEGREE;
            if (readText::getKey(line, "moondist", moondist))
                moondist *= DEGREE;
            if (readText::getKey(line, "phaseang", phaseang))
                phaseang = PH_PI - phaseang * PH_PI / 100.0;
            if (readText::getKey(line, "solaralt", solaralt)) {
                solaralt *= DEGREE;
                solarzen = PH_PI / 2.0 - solaralt;
            }
            if (readText::getKey(line, "solarzen", solarzen))
                solarzen *= DEGREE;

            if (keyName == "clearperturbations") {
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < 6; j++) {
                        body[i][j] = 0.0;
                    }
                }
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        izernike[i][j] = 0.0;
                    }
                }
            }
            if (keyName == "cleartracking") {
                rotationjitter = 0.0;
                elevationjitter = 0.0;
                azimuthjitter = 0.0;
            }
            if (keyName == "clearclouds") {
                for (int i = 0; i < MAX_LAYER; i++) {
                    cloudmean[i] = 0.0;
                    cloudvary[i] = 0.0;
                }
            }
            if (keyName == "clearopacity") {
                h2onorm = 0.0;
                raynorm = 0.0;
                o2norm = 0.0;
                o3norm = 0.0;
                aerosoltau = 0.0;
                h2ogradient = 0.0;
                raygradient = 0.0;
                o2gradient = 0.0;
                o3gradient = 0.0;
                aerosolgradient = 0.0;
                for (int i = 0; i < MAX_LAYER; i++) {
                    cloudmean[i] = 0.0;
                    cloudvary[i] = 0.0;
                }
                opacitymode = 0;
            }
            if (keyName == "clearturbulence") {
                for (int i = 0; i < MAX_LAYER; i++) {
                    seefactor[i] = 0.0;
                }
                domeseeing = 0.0;
                if (atmospheremode > 1)
                    atmospheremode = 1;
            }
            if (keyName == "cleardefects") {
                impurityvariation = 0;
                fieldanisotropy = 0;
                deadlayer = 0;
                chargesharing = 0;
                pixelerror = 0;
            }
            if (keyName == "cleareverything") {
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < 6; j++) {
                        body[i][j] = 0.0;
                    }
                }
                for (int i = 0; i < MAX_SURF; i++) {
                    for (int j = 0; j < NTERM; j++) {
                        izernike[i][j] = 0.0;
                    }
                }
                rotationjitter = 0.0;
                elevationjitter = 0.0;
                azimuthjitter = 0.0;
                for (int i = 0; i < MAX_LAYER; i++) {
                    cloudmean[i] = 0.0;
                    cloudvary[i] = 0.0;
                }
                h2onorm = 0.0;
                raynorm = 0.0;
                o2norm = 0.0;
                o3norm = 0.0;
                aerosoltau = 0.0;
                h2ogradient = 0.0;
                raygradient = 0.0;
                o2gradient = 0.0;
                o3gradient = 0.0;
                aerosolgradient = 0.0;
                for (int i = 0; i < MAX_LAYER; i++) {
                    cloudmean[i] = 0.0;
                    cloudvary[i] = 0.0;
                }
                for (int i = 0; i < MAX_LAYER; i++) {
                    seefactor[i] = 0.0;
                }
                domeseeing = 0.0;
                impurityvariation = 0;
                fieldanisotropy = 0;
                deadlayer = 0;
                chargesharing = 0;
                photoelectric = 0;
                pixelerror = 0;
                detectorMode = 0;
                telescopeMode = 0;
                atmospheric_dispersion = 0;
                diffractionMode = 0;
                laScatterProb = 0.0;
                contaminationmode = 0;
                atmospheremode = 0;
                airrefraction = 0;
            }
            if (keyName == "surfacemap") {
                long surfaceIndex;
                iss >> surfaceIndex;
                iss >> feafile[surfaceIndex * MAX_SURF * 2 +
                               feaflag[surfaceIndex]];
                iss >> feascaling[surfaceIndex * MAX_SURF * 2 +
                                  feaflag[surfaceIndex]];
                feaflag[surfaceIndex] += 1;
            }
            if (keyName == "dlsm") {
                long surfaceIndex;
                std::ostringstream fileName1, fileName2;
                iss >> surfaceIndex;
                fileName1 << "fea_" << obshistid << "_" << surfaceIndex
                          << ".txt";
                feafile[surfaceIndex].assign(fileName1.str());
                std::cout << surfaceIndex << " " << feafile[surfaceIndex]
                          << std::endl;
                feaflag[surfaceIndex] = 1;
                iss >> surfaceIndex;
                fileName2 << "fea_" << obshistid << "_" << surfaceIndex
                          << ".txt";
                feafile[surfaceIndex].assign(fileName2.str());
                std::cout << surfaceIndex << " " << feafile[surfaceIndex]
                          << std::endl;
                feaflag[surfaceIndex] = 1;
            }
            if (extraCommandFlag > 1) {
                extraCommandString.push_back(line);
                extraCommandFlag++;
            }
            if (extraCommandFlag == 1)
                extraCommandFlag++;
        }

        // read location file
        sss = instrdir + "/location.txt";
        std::ifstream inStream(sss.c_str());
        if (inStream) {
            readText locationPars(instrdir + "/location.txt");
            for (size_t t(0); t < locationPars.getSize(); t++) {
                std::string line(locationPars[t]);
                if (groundlevel == -1)
                    readText::get(line, "groundlevel", groundlevel);
                if (xtelloc == -1)
                    readText::get(line, "xtellocation", xtelloc);
                if (ytelloc == -1)
                    readText::get(line, "ytellocation", ytelloc);
                if (latitude == -1)
                    if (readText::getKey(line, "latitude", latitude))
                        latitude *= DEGREE;
                if (longitude == -1)
                    if (readText::getKey(line, "longitude", longitude))
                        longitude *= DEGREE;
                if (spaceMode == -1)
                    readText::get(line, "spacemode", spaceMode);
            }
        } else {
            groundlevel = 0.0;
            xtelloc = 0.0;
            ytelloc = 0.0;
            latitude = 0.0;
            longitude = 0.0;
        }
        if (spaceMode == -1)
            spaceMode = 0;

        if (spaceMode > 0) {
            atmospheric_dispersion = 0;
            // clear turbulence
            for (int i = 0; i < MAX_LAYER; i++) {
                seefactor[i] = 0.0;
            }
            domeseeing = 0.0;
            // clear opacity
            h2onorm = 0.0;
            raynorm = 0.0;
            o2norm = 0.0;
            o3norm = 0.0;
            aerosoltau = 0.0;
            h2ogradient = 0.0;
            raygradient = 0.0;
            o2gradient = 0.0;
            o3gradient = 0.0;
            aerosolgradient = 0.0;
            for (int i = 0; i < MAX_LAYER; i++) {
                cloudmean[i] = 0.0;
                cloudvary[i] = 0.0;
            }
            opacitymode = 0;
            windjitter = 0.0;
            atmospheremode = 0;
        }

        if (opticsonlymode == 1) {
            detectorMode = 0;
            diffractionMode = 0;
            contaminationmode = 0;
            laScatterProb = 0.0;
            atmospheric_dispersion = 0;
            straylight = 0;
            rotationjitter = 0.0;
            elevationjitter = 0.0;
            azimuthjitter = 0.0;
            for (int i = 0; i < MAX_LAYER; i++) {
                seefactor[i] = 0.0;
            }
            domeseeing = 0.0;
            h2onorm = 0.0;
            raynorm = 0.0;
            o2norm = 0.0;
            o3norm = 0.0;
            aerosoltau = 0.0;
            h2ogradient = 0.0;
            raygradient = 0.0;
            o2gradient = 0.0;
            o3gradient = 0.0;
            aerosolgradient = 0.0;
            for (int i = 0; i < MAX_LAYER; i++) {
                cloudmean[i] = 0.0;
                cloudvary[i] = 0.0;
            }
            airrefraction = 0;
            atmospheremode = 0;
        }

        // this will signal that the atmosphere is completely off
        if (atmospheremode == 1 && atmospheric_dispersion == 0.0 &&
            opacitymode == 0)
            atmospheremode = 0;

        if (atmospheremode == 0) {
            natmospherefile = 0;
            h2onorm = 0.0;
            raynorm = 0.0;
            o2norm = 0.0;
            o3norm = 0.0;
            aerosoltau = 0.0;
            h2ogradient = 0.0;
            raygradient = 0.0;
            o2gradient = 0.0;
            o3gradient = 0.0;
            aerosolgradient = 0.0;
            airrefraction = 0;
            for (int i = 0; i < MAX_LAYER; i++) {
                cloudmean[i] = 0.0;
                cloudvary[i] = 0.0;
            }
            for (int i = 0; i < MAX_LAYER; i++) {
                seefactor[i] = 0.0;
            }
        }

        if (telconfig != 2 && telconfig != 3)
            domelight = 1000.0;

        if (opdfile)
            aperturemode = 2;

        std::ostringstream outfile;
        std::ostringstream outfileevent;
        unsigned pos = instrdir.rfind("/") + 1;
        for (unsigned i = pos; i < instrdir.length(); i++) {
            outfile << instrdir[i];
            outfileevent << instrdir[i];
        }
        outfile << "_e_" << obshistid << "_f" << filter << "_" << chipid << "_E"
                << std::setfill('0') << std::setw(3) << pairid;
        outputfilename = outfile.str();
        outfileevent << "_r_" << obshistid << "_f" << filter << "_" << chipid
                     << "_E" << std::setfill('0') << std::setw(3) << pairid;
        eventFitsFileName = outfileevent.str();

        if (flatdir == 1) {
            instrdir = ".";
            bindir = ".";
        }

        if (tarfile == 1) {
            std::ostringstream tarName;
            tarName << "raytrace_" << obshistid << ".tar";
            std::ifstream tarFile(tarName.str().c_str());
            if (tarFile.good()) {
                std::cout << "Untarring " << tarName.str() << std::endl;
                std::string tarCommand = "tar xf " + tarName.str();
                system(tarCommand.c_str());
            }
        }

        // OPD
        focalplanefile = instrdir + "/focalplanelayout.txt";
        readText focalplanePar(focalplanefile);
        std::string tchipid;
        if (chipid == "opd") {
            std::string line(focalplanePar[focalplanePar.getSize() / 2]);
            std::istringstream iss(line);
            iss >> tchipid;
            opdfile = 1;
            transtol = 1.0;
            aperturemode = 2;
            detectorMode = 0;
            diffractionMode = 0;
            coatingmode = 0;
            contaminationmode = 0;
            laScatterProb = 0.0;
            atmospheric_dispersion = 0;
            straylight = 0;
            airrefraction = 0;
            rotationjitter = 0.0;
            elevationjitter = 0.0;
            azimuthjitter = 0.0;
            backgroundMode = 0;
            for (int i = 0; i < MAX_LAYER; i++) {
                seefactor[i] = 0.0;
            }
            domeseeing = 0.0;
            h2onorm = 0.0;
            raynorm = 0.0;
            o2norm = 0.0;
            o3norm = 0.0;
            aerosoltau = 0.0;
            h2ogradient = 0.0;
            raygradient = 0.0;
            o2gradient = 0.0;
            o3gradient = 0.0;
            aerosolgradient = 0.0;
            for (int i = 0; i < MAX_LAYER; i++) {
                cloudmean[i] = 0.0;
                cloudvary[i] = 0.0;
            }
        } else {
            tchipid = chipid;
        }
        std::istringstream focalplanePars(
            readText::get(focalplanefile, tchipid));
        double centerxT, centeryT, pixsizeT;
        long pixelsxT, pixelsyT;
        double angle1, angle2;
        float sensorthicknessT;
        std::string grouptype;
        focalplanePars >> centerxT >> centeryT >> pixsizeT >> pixelsxT >>
            pixelsyT >> devmaterial >> devtype >> devmode >> devvalue >>
            sensorthicknessT >> grouptype >> chipangle >> angle1 >> angle2 >>
            decenterx >> decentery;
        decenterx *= 1000.0;
        decentery *= 1000.0;
        chipangle *= PH_PI / 180.0;
        if (centerx == -1)
            centerx = centerxT;
        if (centery == -1)
            centery = centeryT;
        if (pixsize == -1)
            pixsize = pixsizeT;
        if (pixelsx == -1)
            pixelsx = pixelsxT;
        if (pixelsy == -1)
            pixelsy = pixelsyT;
        if (minx == -1)
            minx = 0;
        if (miny == -1)
            miny = 0;
        if (maxx == -1)
            maxx = pixelsx - 1;
        if (maxy == -1)
            maxy = pixelsy - 1;
        if (sensorthickness == -1)
            sensorthickness = sensorthicknessT;
        int nsamples = nframes + nskip;
        if (nframes == 1 && nskip == 0)
            devmode = "frame"; // override
        if (devmode == "frame") {
            ngroups = 1;
            nframes = 1;
            nskip = 0;
            nsamples = 1;
        } else {
            ngroups =
                ceil((vistime + devvalue * nskip) / (devvalue * nsamples));
        }

        // read sensor file
        sss = instrdir + "/sensor.txt";
        std::ifstream inStream2(sss.c_str());
        if (inStream2) {
            readText siliconPars(instrdir + "/sensor.txt");
            for (size_t t(0); t < siliconPars.getSize(); t++) {
                std::string line(siliconPars[t]);
                if (well_depth == -1)
                    readText::get(line, "wellDepth", well_depth);
                if (nbulk == -1)
                    readText::get(line, "nbulk", nbulk);
                if (nf == -1)
                    readText::get(line, "nf", nf);
                if (nb == -1)
                    readText::get(line, "nb", nb);
                if (sf == -1)
                    readText::get(line, "sf", sf);
                if (sb == -1)
                    readText::get(line, "sb", sb);
                if (sensorTempNominal == -1)
                    readText::get(line, "sensorTempNominal", sensorTempNominal);
            }
        } else {
            well_depth = 1e5;
            nbulk = 1e12;
            nf = 0.0;
            nb = 0.0;
            sf = 0.0;
            sb = 0.0;
            sensorTempNominal = 173;
        }

        // scaling background parameters
        backRadius = backRadius * 180000.0 / platescale * pixsize / 10.0;
        backBuffer = backBuffer * platescale / 180000.0 * 10.0 / pixsize;
        activeBuffer = activeBuffer * platescale / 180000.0 * 10.0 / pixsize;
        backBeta = backBeta * 180000.0 / platescale * pixsize / 10.0;
        if (backGamma > 1)
            backGamma = backGamma * 180000.0 / platescale * pixsize / 10.0;

        windjitter = windjitter * pow(vistime / 60, 0.25);
        return (0);
    }

    int settings() {

        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Basic Setup" << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout
            << "[outputdir] Output directory:                              "
            << outputdir << std::endl;
        std::cout
            << "[outputfilename] Output file name:                         "
            << outputfilename << std::endl;
        std::cout
            << "[seddir] SED directory:                                    "
            << seddir << std::endl;
        std::cout
            << "[imagedir] Image directory:                                "
            << imagedir << std::endl;
        std::cout
            << "[centroidfile] Output centroid file (0=no/1=yes):          "
            << centroidfile << std::endl;
        std::cout
            << "[throughputfile] Output throughput file (0=no/1=yes):      "
            << throughputfile << std::endl;
        std::cout
            << "[eventfile] Output event file (0=no/1=yes):                "
            << eventfile << std::endl;
        std::cout
            << "[eventFitsFileName] Output event Fits file name:           "
            << eventFitsFileName << std::endl;
        std::cout
            << "[bindir] Binary directory:                                 "
            << bindir << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Module Switches" << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout
            << "[backgroundmode] Background mode (0=off/1=on):             "
            << backgroundMode << std::endl;
        std::cout
            << "[telescopemode] Telescope mode (0=off/1=on):               "
            << telescopeMode << std::endl;
        std::cout
            << "[trackingmode] Tracking mode (0=off/1=on):                 "
            << trackingMode << std::endl;
        std::cout
            << "[detectormode] Detector mode (0=off/1=on):                 "
            << detectorMode << std::endl;
        std::cout
            << "[diffractionmode] Diffraction mode (0=off/1=on):           "
            << diffractionMode << std::endl;
        std::cout
            << "[spacemode] Space mode (0=off/1=LEO/2=L1/etc.):            "
            << spaceMode << std::endl;
        std::cout
            << "[zernikemode] Zernike mode (0=off/1=on):                   "
            << zernikemode << std::endl;
        std::cout
            << "[straylight] Straylight mode (0=off/1=on):                 "
            << straylight << std::endl;
        std::cout
            << "[aperturemode] Aperture mode (0=normal/1=on):              "
            << aperturemode << std::endl;
        std::cout
            << "[ghostonly] Ghost-only mode (0=normal/1=on):               "
            << ghostonly << std::endl;
        std::cout
            << "[saturation] Saturation mode (0=off/1=on):                 "
            << saturation << std::endl;
        std::cout
            << "[blooming] Blooming mode (0=off/1=on):                     "
            << blooming << std::endl;
        std::cout
            << "[atmosphericdispersion] Atmos. dispersion (0=off/1=on):    "
            << atmospheric_dispersion << std::endl;
        std::cout
            << "[atmosphericdispcenter] Atmos. disp. ctr. corr.:           "
            << atmosphericdispcenter << std::endl;
        std::cout
            << "[impurityvariation] Impurity variation (0=off/1=on):       "
            << impurityvariation << std::endl;
        std::cout
            << "[fieldanisotropy] Field anisotropy (0=off/1=on):           "
            << fieldanisotropy << std::endl;
        std::cout
            << "[fringing] Fringing (0=off/1=on):                          "
            << fringeflag << std::endl;
        std::cout
            << "[deadlayer] Dead layer (0=off/1=on):                       "
            << deadlayer << std::endl;
        std::cout
            << "[chargediffusion] Charge diffusion (0=off/1=on):           "
            << chargediffusion << std::endl;
        std::cout
            << "[photoelectric] Photoelectric (0=off/1=on):                "
            << photoelectric << std::endl;
        std::cout
            << "[chargesharing] Charge sharing (0=off/1=on):               "
            << chargesharing << std::endl;
        std::cout
            << "[pixelerror] Pixel error (0=off/1=on):                     "
            << pixelerror << std::endl;
        std::cout
            << "[coatingmode] Coating mode (0=off/1=on):                   "
            << coatingmode << std::endl;
        std::cout
            << "[contaminationmode] Contamination mode (0=off/1=on):       "
            << contaminationmode << std::endl;
        std::cout
            << "[aberration] Aberration mode (0=off/1=on):                 "
            << aberration << std::endl;
        std::cout
            << "[precession] Precession mode (0=off/1=on):                 "
            << precession << std::endl;
        std::cout
            << "[nutation] Nutation mode (0=off/1=on):                     "
            << nutation << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Telescope Operator and Bookkeeping" << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout
            << "Pointing right ascension (degrees):                        "
            << pra / DEGREE << std::endl;
        std::cout
            << "Pointing declination (degrees):                            "
            << pdec / DEGREE << std::endl;
        std::cout
            << "[rotationangle] Rotation angle (rotSkyPos) (degrees):      "
            << -rotatez / DEGREE << std::endl;
        std::cout
            << "Angle of spider (rotTelPos) (degrees):                     "
            << spiderangle / DEGREE << std::endl;
        if (spaceMode == 0) {
            std::cout
                << "Zenith angle (degrees):                                    "
                << zenith / DEGREE << std::endl;
            std::cout
                << "Azimuthal angle (degrees):                                 "
                << azimuth / DEGREE << std::endl;
        }
        std::cout
            << "Filter (number starting with 0):                           "
            << filter << std::endl;
        std::cout
            << "Filter Name:                                               "
            << filterName << std::endl;
        std::cout
            << "Random seed:                                               "
            << ranseed << std::endl;
        std::cout
            << "Sensor temperature (K):                                    "
            << (sensorTempNominal + sensorTempDelta) << std::endl;
        std::cout
            << "[sensortempdelta] Delta sensor temperature (K):            "
            << sensorTempDelta << std::endl;

        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout << "Instantaneous Instrument and Site Characteristics"
                  << std::endl;
        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;
        std::cout
            << "[instrdir] Instrument & site directory:                    "
            << instrdir << std::endl;

        // instrument
        std::cout
            << "[platescale] Plate scale:                                  "
            << platescale << std::endl;
        std::cout
            << "[minr] Minimum aperture radius:                            "
            << minr << std::endl;
        std::cout
            << "[maxr] Maximum aperture radius:                            "
            << maxr << std::endl;
        std::cout
            << "[chipid] Chip/Amplifier ID:                                "
            << chipid << std::endl;
        std::cout
            << "[centerx] Chip center x (microns):                         "
            << centerx << std::endl;
        std::cout
            << "[centery] Chip center y (microns):                         "
            << centery << std::endl;
        std::cout
            << "[pixelsx] Chip x pixels:                                   "
            << pixelsx << std::endl;
        std::cout
            << "[pixelsy] Chip y pixels:                                   "
            << pixelsy << std::endl;
        std::cout
            << "[minx] Minimum x pixel of amplifier:                       "
            << minx << std::endl;
        std::cout
            << "[maxx] Maximum x pixel of amplifier:                       "
            << maxx << std::endl;
        std::cout
            << "[miny] Minimum y pixel of amplifier:                       "
            << miny << std::endl;
        std::cout
            << "[maxy] Maximum y pixel of amplifier:                       "
            << maxy << std::endl;
        std::cout
            << "[pixelsize] Pixel Size (microns):                          "
            << pixsize << std::endl;
        std::cout
            << "[welldepth] Full well depth:                               "
            << well_depth << std::endl;
        std::cout
            << "[nbulk] Bulk doping density:                               "
            << std::scientific << nbulk << std::endl;
        std::cout
            << "[nf] Front side doping density:                            "
            << std::scientific << nf << std::endl;
        std::cout
            << "[nb] Back side doping density:                             "
            << std::scientific << nb << std::endl;
        std::cout
            << "[sf] Front side doping scale:                              "
            << std::scientific << sf << std::endl;
        std::cout
            << "[sb] Back side doping scale:                               "
            << std::scientific << sb << std::endl;
        std::cout
            << "[sensorthickness] Sensor Thickness (microns):              "
            << std::fixed << sensorthickness << std::endl;
        std::cout
            << "[overdepbias] Over depletion bias (volts):                 "
            << overdepbias << std::endl;
        std::cout
            << "[sensorTempNominal] Nominal sensor temperature (K):        "
            << sensorTempNominal << std::endl;
        std::cout
            << "[qevariation] QE variation:                                "
            << qevariation << std::endl;
        std::cout
            << "[exptime] Exposure time (s):                               "
            << exptime << std::endl;
        std::cout
            << "[nsnap] Number of snaps:                                   "
            << nsnap << std::endl;
        std::cout
            << "Number of groups:                                          "
            << ngroups << std::endl;
        std::cout
            << "[nframes] Number of frames:                                "
            << nframes << std::endl;
        std::cout
            << "[nskip] Number of frames to skip:                          "
            << nskip << std::endl;
        std::cout
            << "[shuttererror] Shutter error (s):                          "
            << shuttererror << std::endl;
        std::cout
            << "[timeoffset] Time offset (s):                              "
            << timeoffset << std::endl;
        std::cout
            << "[windjitter] Wind jitter (degrees):                        "
            << windjitter << std::endl;
        std::cout
            << "[rotationjitter] Rotation jitter (arcseconds):             "
            << rotationjitter << std::endl;
        std::cout
            << "[elevationjitter] Elevation jitter (arcseconds):           "
            << elevationjitter << std::endl;
        std::cout
            << "[azimuthjitter] Azimuthal jitter (arcseconds):             "
            << azimuthjitter << std::endl;
        std::cout
            << "[rotatex] Fiducial field angle X (degrees):                "
            << -rotatex / DEGREE << std::endl;
        std::cout
            << "[rotatey] Fiducial field angle Y (degrees):                "
            << -rotatey / DEGREE << std::endl;
        std::cout
            << "[izernike optic# zernike#] Zernike amplitude:              "
            << std::endl;
        for (long i = 0; i < nsurf + 1; i++) {
            for (long j = 0; j < NTERM / 2; j++) {
                std::cout << izernike[i][j] << " ";
            }
            std::cout << std::endl;
            std::cout << " ";
            for (long j = NTERM / 2; j < NTERM; j++) {
                std::cout << izernike[i][j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout
            << "[body optic# dof#] Body motion of optics:                  "
            << std::endl;
        for (long i = 0; i < nsurf + 1; i++) {
            for (long j = 0; j < 6; j++) {
                std::cout << body[i][j] << " ";
            }
            std::cout << std::endl;
        }
        std::cout
            << "[lascatprob] Large angle scattering probability:           "
            << laScatterProb << std::endl;
        std::cout
            << "[toypsf] Toy PSF:                                          "
            << toypsf << std::endl;

        // site
        if (spaceMode == 0) {
            std::cout
                << "[domeseeing] Dome seeing:                                  "
                << domeseeing << std::endl;
            std::cout
                << "[groundlevel] Ground level (m):                            "
                << groundlevel << std::endl;
            std::cout
                << "[xtellocation] X Telescope location (m):                   "
                << xtelloc << std::endl;
            std::cout
                << "[ytellocation] Y Telescope location (m):                   "
                << ytelloc << std::endl;
            std::cout
                << "[latitude] Telescope latitude (degrees):                   "
                << latitude / DEGREE << std::endl;
            std::cout
                << "[longitude] Telescope longitude (degrees):                 "
                << longitude / DEGREE << std::endl;
        } else if (spaceMode == 1) {
            std::cout
                << "Location:                                                "
                   "  low Earth orbit"
                << std::endl;
        } else if (spaceMode == 2) {
            std::cout
                << "Location:                                                "
                   "  Lagrange point 1"
                << std::endl;
        } else if (spaceMode == 3) {
            std::cout
                << "Location:                                                "
                   "  Lagrange point 2"
                << std::endl;
        } else if (spaceMode == 4) {
            std::cout
                << "Location:                                                "
                   "  Lagrange point 3"
                << std::endl;
        } else if (spaceMode == 5) {
            std::cout
                << "Location:                                                "
                   "  Lagrange point 4"
                << std::endl;
        } else if (spaceMode == 6) {
            std::cout
                << "Location:                                                "
                   "  Lagrange point 5"
                << std::endl;
        }
        if (natmospherefile > 0) {
            std::cout
                << "[pressure] Air pressure (mmHg):                            "
                << pressure << std::endl;
            std::cout
                << "[waterpressure] Water vapor pressure (mmHg):               "
                << waterPressure << std::endl;
            std::cout
                << "[temperature] Ground temperature (degrees C):              "
                << temperature << std::endl;
            std::cout
                << "[reldensity] Relative density:                             "
                << raynorm << std::endl;
            std::cout
                << "[relo2] Relative O2 fraction:                              "
                << o2norm << std::endl;
            std::cout
                << "[relh2o] Relative H2O fraction:                            "
                << h2onorm << std::endl;
            std::cout
                << "[aerosoltau] Aerosol optical depth:                        "
                << aerosoltau << std::endl;
            std::cout
                << "[aerosolindex] Aerosol index:                              "
                << aerosolindex << std::endl;
            std::cout
                << "[relo3] Relative O3 fraction:                              "
                << o3norm << std::endl;
            std::cout
                << "[raygradient] Density gradient (fraction/km):              "
                << raygradient << std::endl;
            std::cout
                << "[o2gradient] O2 gradient (fraction/km):                    "
                << o2gradient << std::endl;
            std::cout
                << "[o3gradient] O3 gradient (fraction/km):                    "
                << o3gradient << std::endl;
            std::cout
                << "[h2ogradient] H2O gradient (fraction/km):                  "
                << h2ogradient << std::endl;
            std::cout
                << "[aerosolgradient] Aerosol gradient:                        "
                << aerosolgradient << std::endl;
            std::cout
                << "[rayangle] Density angle:                                  "
                << rayAngle << std::endl;
            std::cout
                << "[o2angle] O2 angle:                                        "
                << o2Angle << std::endl;
            std::cout
                << "[o3angle] O3 angle:                                        "
                << o3Angle << std::endl;
            std::cout
                << "[h2oangle] H2O angle:                                      "
                << h2oAngle << std::endl;
            std::cout
                << "[aerosolangle] Aerosol angle:                              "
                << aerosolAngle << std::endl;
        }
        std::cout
            << "[natmospherefile] Number of atmosphere layers:             "
            << natmospherefile << std::endl;
        if (natmospherefile > 0) {
            std::cout << "[seeing layer#] Seeing at 5000 angstroms (arcsec):"
                      << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << seefactor[i] / pow(1 / cos(zenith), 0.6) << " ";
            }
            std::cout << std::endl;
            std::cout << "[wind layer#] Wind speed (m/s):" << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << wind[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "[winddir layer#] Wind direction (degrees):"
                      << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << winddir[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "[height layer#] Layer height (km):" << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << height[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "[outerscale layer#] Outer scale (m):" << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << outerscale[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "[cloudmean layer#] Mean cloud extinction (km):"
                      << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << cloudmean[i] << " ";
            }
            std::cout << std::endl;
            std::cout
                << "[cloudvary layer#] Variation of cloud extinction (km):"
                << std::endl;
            for (long i = 0; i < natmospherefile; i++) {
                std::cout << cloudvary[i] << " ";
            }
            std::cout << std::endl;
        }

        std::cout
            << "-------------------------------------------------------------"
               "-----------------------------"
            << std::endl;

        return (1);
    }

    int header(fitsfile *faptr) {

        long i;
        int status = 0;
        char tempstring[4096];
        char tempstring2[4096];
        double tempf1;

        for (long i = 0; i < static_cast<long>(extraCommandString.size());
             i++) {
            sprintf(tempstring, "PHOV%ld", i);
            sprintf(tempstring2, "Physics Override Command %ld", i);
            fits_write_key(faptr,
                           TSTRING,
                           (char *)tempstring,
                           (char *)extraCommandString[i].c_str(),
                           (char *)tempstring2,
                           &status);
        }
        fitsWriteKey(faptr, "OBSID", obshistid, "Observation ID");
        fitsWriteKey(
            faptr,
            "TAI",
            convertTai(tai + (timeoffset - exptime / 2.0) / 24.0 / 3600.0),
            "International Atomic Time scale");
        fitsWriteKey(faptr,
                     "MJD-OBS",
                     tai + (timeoffset - exptime / 2.0) / 24.0 / 3600.0,
                     "Modified Julian date");
        fitsWriteKey(faptr, "OUTPDIR", outputdir, "Output directory");
        fitsWriteKey(faptr, "OUTFILE", outputfilename, "Output file name");
        fitsWriteKey(faptr, "SEDDIR", seddir, "SED directory");
        fitsWriteKey(faptr, "IMGDIR", imagedir, "Image directory");
        fitsWriteKey(
            faptr, "BACMODE", backgroundMode, "Background mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "TELMODE", telescopeMode, "Telescope mode (0=off/1=on)");
        fitsWriteKey(faptr, "SPCMODE", spaceMode, "Space mode (0=off)");
        fitsWriteKey(
            faptr, "TRKMODE", trackingMode, "Tracking mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "DIFMODE", diffractionMode, "Diffraction mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "DETMODE", detectorMode, "Detector mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "ZERMODE", zernikemode, "Zernike mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "STRLGHT", straylight, "Straylight mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "APRMODE", aperturemode, "Aperture mode (0=normal/1=on)");
        fitsWriteKey(
            faptr, "GHOMODE", ghostonly, "Ghost-only mode (0=normal/1=on)");
        fitsWriteKey(
            faptr, "SATMODE", saturation, "Saturation mode (0=off/1=on)");
        fitsWriteKey(faptr, "BLOOMNG", blooming, "Blooming mode (0=off/1=on)");
        fitsWriteKey(
            faptr, "EVTFILE", eventfile, "Output event file (0=no/1=yes)");

        fits_write_key(faptr,
                       TLONG,
                       (char *)"THRFILE",
                       &throughputfile,
                       (char *)"Output throughput file (0=no/1=yes)",
                       &status);
        tempf1 = (3600.0 * 1000.0) / platescale;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"PLTSCAL",
                       &tempf1,
                       (char *)"Approx. Plate scale (arcsec/mm)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"MINR",
                       &minr,
                       (char *)"Minimum aperture radius",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"MAXR",
                       &maxr,
                       (char *)"Maximum aperture radius",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"EXPTIME",
                       &exptime,
                       (char *)"Exposure time",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"DARKTIME",
                       &exptime,
                       (char *)"Actual exposed time",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"SHUTERR",
                       &shuttererror,
                       (char *)"Shutter error",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"TIMEOFF",
                       &timeoffset,
                       (char *)"Time offset",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"FILTNM",
                       &filter,
                       (char *)"Filter/optics configuration number",
                       &status);
        fits_write_key(faptr,
                       TSTRING,
                       (char *)"FILTER",
                       &filterName,
                       (char *)"Filter",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"SEED",
                       &ranseed,
                       (char *)"Random seed",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"PRA",
                       &pra,
                       (char *)"Pointing RA (radians)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"PDEC",
                       &pdec,
                       (char *)"Pointing Dec (radians)",
                       &status);
        tempf1 = pra * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RA_DEG",
                       &tempf1,
                       (char *)"Pointing RA (decimal degrees)",
                       &status);
        tempf1 = pdec * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"DEC_DEG",
                       &tempf1,
                       (char *)"Pointing Dec (decimal degrees)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AIRMASS",
                       &airmass,
                       (char *)"Airmass",
                       &status);
        tempf1 = -rotatex * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"FLDANGX",
                       &tempf1,
                       (char *)"Fiducial field angle X (degrees)",
                       &status);
        tempf1 = -rotatey * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"FLDANGY",
                       &tempf1,
                       (char *)"Fiducial field angle Y (degrees)",
                       &status);
        tempf1 = -rotatez * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"ROTANG",
                       &tempf1,
                       (char *)"Rotation angle (rotSkyPos) (degrees)",
                       &status);
        tempf1 = spiderangle * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"SPIDANG",
                       &tempf1,
                       (char *)"Angle of spider (rotTelPos)",
                       &status);
        tempf1 = zenith * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"ZENITH",
                       &tempf1,
                       (char *)"Zenith angle (degrees)",
                       &status);
        tempf1 = azimuth * 180 / PH_PI;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AZIMUTH",
                       &tempf1,
                       (char *)"Azimuthal angle (degrees)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"ROTJITT",
                       &rotationjitter,
                       (char *)"Rotation jitter (arcseconds)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"ELEJITT",
                       &elevationjitter,
                       (char *)"Elevation jitter (arcseconds)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AZIJITT",
                       &azimuthjitter,
                       (char *)"Azimuthal jitter (arcseconds)",
                       &status);
        fits_write_key(faptr,
                       TSTRING,
                       (char *)"TRKFILE",
                       (char *)trackingfile.c_str(),
                       (char *)"Tracking file",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"GNDLEVL",
                       &groundlevel,
                       (char *)"Ground level (m)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"XTELLOC",
                       &xtelloc,
                       (char *)"X telescope location (m)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"YTELLOC",
                       &ytelloc,
                       (char *)"Y telescope location (m)",
                       &status);

        for (long i = 0; i < nsurf + 1; i++) {
            for (long j = 0; j < NZERN; j++) {
                sprintf(tempstring, "ZER%2ld%2ld", i, j);
                fits_write_key(faptr,
                               TDOUBLE,
                               tempstring,
                               &izernike[i][j],
                               (char *)"Zernike amplitude",
                               &status);
            }
        }
        for (long i = 0; i < nsurf + 1; i++) {
            for (long j = 0; j < 6; j++) {
                sprintf(tempstring, "BOD%2ld%2ld", i, j);
                fits_write_key(faptr,
                               TDOUBLE,
                               tempstring,
                               &izernike[i][j],
                               (char *)"Body motion misalignment",
                               &status);
            }
        }

        fits_write_key(faptr,
                       TLONG,
                       (char *)"ATMFILE",
                       &natmospherefile,
                       (char *)"Number of atmosphere files",
                       &status);
        for (i = 0; i < natmospherefile; i++) {
            sprintf(tempstring, "AFILE%ld", i);
            fits_write_key(faptr,
                           TSTRING,
                           tempstring,
                           (char *)atmospherefile[i].c_str(),
                           (char *)"Atmosphere file",
                           &status);
            sprintf(tempstring, "CFILE%ld", i);
            fits_write_key(faptr,
                           TSTRING,
                           tempstring,
                           (char *)cloudfile[i].c_str(),
                           (char *)"Cloud file",
                           &status);
            sprintf(tempstring, "SEE%ld", i);
            tempf1 = seefactor[i] / (pow(1 / cos(zenith), 0.6));
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &tempf1,
                           (char *)"Seeing at 5000 angstrom (sigma)",
                           &status);
            sprintf(tempstring, "WIND%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &wind[i],
                           (char *)"Wind speed (m/s)",
                           &status);
            sprintf(tempstring, "WDIR%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &winddir[i],
                           (char *)"Wind direction (degrees)",
                           &status);
            sprintf(tempstring, "OSCL%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &outerscale[i],
                           (char *)"Outer scale (m)",
                           &status);
            sprintf(tempstring, "HGT%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &height[i],
                           (char *)"Height (km)",
                           &status);
            sprintf(tempstring, "CMEAN%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &cloudmean[i],
                           (char *)"Mean cloud extinction (mag)",
                           &status);
            sprintf(tempstring, "CVARY%ld", i);
            fits_write_key(faptr,
                           TDOUBLE,
                           tempstring,
                           &cloudvary[i],
                           (char *)"Variation of cloud ext. (mag)",
                           &status);
        }
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RAYGRAD",
                       &raygradient,
                       (char *)"Density gradient",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"O2GRAD",
                       &o2gradient,
                       (char *)"O2 gradient",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"O3GRAD",
                       &o3gradient,
                       (char *)"O3 gradient",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"H2OGRAD",
                       &h2ogradient,
                       (char *)"H2O gradient",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AERGRAD",
                       &aerosolgradient,
                       (char *)"Aerosol gradient",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RAYANG",
                       &rayAngle,
                       (char *)"Density angle",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"O2ANG",
                       &o2Angle,
                       (char *)"O2 angle",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"O3ANG",
                       &o3Angle,
                       (char *)"O3 angle",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"H2OANG",
                       &h2oAngle,
                       (char *)"H2O angle",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AERANG",
                       &aerosolAngle,
                       (char *)"Aerosol angle",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"ATMDISP",
                       &atmospheric_dispersion,
                       (char *)"Atmos. dispersion (0=off/1=on)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"PRESS",
                       &pressure,
                       (char *)"Air pressure (mmHg)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"H2OPRESS",
                       &waterPressure,
                       (char *)"Water vapor pressure (mmHg)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"TEMPERA",
                       &temperature,
                       (char *)"Ground temperature (degrees C)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RELDENS",
                       &raynorm,
                       (char *)"Relative density",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RELO2",
                       &o2norm,
                       (char *)"Relative O2 fraction",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RELH2O",
                       &h2onorm,
                       (char *)"Relative H2O fraction",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AERTAU",
                       &aerosoltau,
                       (char *)"Aerosol optical depth",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"AERIND",
                       &aerosolindex,
                       (char *)"Aerosol index",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"RELO3",
                       &o3norm,
                       (char *)"Relative O3 fraction",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"LASCPR",
                       &laScatterProb,
                       (char *)"Large angle scattering probability",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"DOMESEE",
                       &domeseeing,
                       (char *)"Dome Seeing (arcseconds FWHM)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"TOYPSF",
                       &toypsf,
                       (char *)"Toy PSF (arcseconds FWHM)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"PIXSIZE",
                       &pixsize,
                       (char *)"Pixel Size (microns)",
                       &status);
        fits_write_key(faptr,
                       TSTRING,
                       (char *)"CHIPID",
                       (char *)chipid.c_str(),
                       (char *)"Chip/Amplifier ID",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"PAIRID",
                       &pairid,
                       (char *)"Pair ID",
                       &status);
        fits_write_key(faptr,
                       TSTRING,
                       (char *)"FPFILE",
                       (char *)focalplanefile.c_str(),
                       (char *)"Focal plane file name",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"CENTX",
                       &centerx,
                       (char *)"Chip center x (microns)",
                       &status);
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"CENTY",
                       &centery,
                       (char *)"Chip center y (microns)",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"PIXX",
                       &pixelsx,
                       (char *)"Chip x pixels",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"PIXY",
                       &pixelsy,
                       (char *)"Chip y pixels",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"MINX",
                       &minx,
                       (char *)"Minimum x pixel of amplifier",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"MAXX",
                       &maxx,
                       (char *)"Maximum x pixel of amplifier",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"MINY",
                       &miny,
                       (char *)"Minimum y pixel of amplifier",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"MAXY",
                       &maxy,
                       (char *)"Maximum y pixel of amplifier",
                       &status);
        fits_write_key(faptr,
                       TLONG,
                       (char *)"WELDPT",
                       &well_depth,
                       (char *)"Full well depth (electrons)",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"NBULK",
                       &nbulk,
                       (char *)"Bulk doping density",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"NF",
                       &nf,
                       (char *)"Front side doping density",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"NB",
                       &nb,
                       (char *)"Back side doping density",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"SF",
                       &sf,
                       (char *)"Front side doping scale",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"SB",
                       &sb,
                       (char *)"Back side doping scale",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"SETHICK",
                       &sensorthickness,
                       (char *)"Sensor thickness (microns)",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"OVRDEP",
                       &overdepbias,
                       (char *)"Over depletion bias (volts)",
                       &status);
        tempf1 = sensorTempNominal + sensorTempDelta;
        fits_write_key(faptr,
                       TDOUBLE,
                       (char *)"CCDTEMP",
                       &tempf1,
                       (char *)"Sensor temperature (K):",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"TRX0",
                       &impurityX,
                       (char *)"Tree ring center",
                       &status);
        fits_write_key(faptr,
                       TFLOAT,
                       (char *)"TRY0",
                       &impurityY,
                       (char *)"Tree ring center",
                       &status);

        return (0);
    }
};

#endif
