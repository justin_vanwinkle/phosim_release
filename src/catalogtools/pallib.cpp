/*
*+
*  Name:
*     pal1Atms

*  Purpose:
*     Calculate stratosphere parameters

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void pal1Atms ( double rt, double tt, double dnt, double gamal,
*                     double r, double * dn, double * rdndr );

*  Arguments:
*     rt = double (Given)
*         Height of the tropopause from centre of the Earth (metre)
*     tt = double (Given)
*         Temperature at the tropopause (K)
*     dnt = double (Given)
*         Refractive index at the tropopause
*     gamal = double (Given)
*         Constant of the atmospheric model = G*MD/R
*     r = double (Given)
*         Current distance from the centre of the Earth (metre)
*     dn = double * (Returned)
*         Refractive index at r
*     rdndr = double * (Returned)
*         r * rate the refractive index is changing at r

*  Description:
*     Refractive index and derivative with respect to height for the
*     stratosphere.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - Internal routine used by palRefro.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal1.h"

#include <math.h>

void pal1Atms(double rt,
              double tt,
              double dnt,
              double gamal,
              double r,
              double *dn,
              double *rdndr) {

    double b;
    double w;

    b = gamal / tt;
    w = (dnt - 1.0) * exp(-b * (r - rt));
    *dn = 1.0 + w;
    *rdndr = -r * b * w;
}

/*
*+
*  Name:
*     pal1Atmt

*  Purpose:
*     Calculate troposphere parameters

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void pal1Atmt ( double r0, double t0, double alpha, double gamm2,
*                     double delm2, double c1, double c2, double c3,
*                     double c4, double c5, double c6, double r,
*                     double *t, double *dn, double *rdndr );

*  Arguments:
*     r0 = double (Given)
*         Height of observer from centre of the Earth (metre)
*     t0 = double (Given)
*         Temperature of the observer (K)
*     alpha = double (Given)
*         Alpha (see HMNAO paper)
*     gamm2 = double (Given)
*         Gamma minus 2 (see HMNAO paper)
*     delm2 = double (Given)
*         Delta minus 2 (see HMNAO paper)
*     c1 = double (Given)
*         Useful term (see palRefro source)
*     c2 = double (Given)
*         Useful term (see palRefro source)
*     c3 = double (Given)
*         Useful term (see palRefro source)
*     c4 = double (Given)
*         Useful term (see palRefro source)
*     c5 = double (Given)
*         Useful term (see palRefro source)
*     c6 = double (Given)
*         Useful term (see palRefro source)
*     r = double (Given)
*         Current distance from the centre of the Earth (metre)
*     t = double * (Returned)
*         Temperature at r (K)
*     dn = double * (Returned)
*         Refractive index at r.
*     rdndr = double * (Returned)
*         r * rate the refractive index is changing at r.

*  Description:
*     Refractive index and derivative with respect to height for
*     the troposphere.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - Internal routine used by palRefro
*     - Note that in the optical case c5 and c6 are zero.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version, copied from Fortran SLA source.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal1.h"
#include "palmac.h"

#include <math.h>

void pal1Atmt(double r0,
              double t0,
              double alpha,
              double gamm2,
              double delm2,
              double c1,
              double c2,
              double c3,
              double c4,
              double c5,
              double c6,
              double r,
              double *t,
              double *dn,
              double *rdndr) {

    double tt0;
    double tt0gm2;
    double tt0dm2;

    *t = DMAX(DMIN(t0 - alpha * (r - r0), 320.0), 100.0);
    tt0 = *t / t0;
    tt0gm2 = pow(tt0, gamm2);
    tt0dm2 = pow(tt0, delm2);
    *dn = 1.0 + (c1 * tt0gm2 - (c2 - c5 / *t) * tt0dm2) * tt0;
    *rdndr = r * (-c3 * tt0gm2 + (c4 - c6 / tt0) * tt0dm2);
}
/*
*+
*  Name:
*     palAddet

*  Purpose:
*     Add the E-terms to a pre IAU 1976 mean place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAddet ( double rm, double dm, double eq,
*                     double *rc, double *dc );

*  Arguments:
*     rm = double (Given)
*        RA without E-terms (radians)
*     dm = double (Given)
*        Dec without E-terms (radians)
*     eq = double (Given)
*        Besselian epoch of mean equator and equinox
*     rc = double * (Returned)
*        RA with E-terms included (radians)
*     dc = double * (Returned)
*        Dec with E-terms included (radians)

*  Description:
*     Add the E-terms (elliptic component of annual aberration)
*     to a pre IAU 1976 mean place to conform to the old
*     catalogue convention.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     Most star positions from pre-1984 optical catalogues (or
*     derived from astrometry using such stars) embody the
*     E-terms.  If it is necessary to convert a formal mean
*     place (for example a pulsar timing position) to one
*     consistent with such a star catalogue, then the RA,Dec
*     should be adjusted using this routine.

*  See Also:
*     Explanatory Supplement to the Astronomical Ephemeris,
*     section 2D, page 48.

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palAddet(double rm, double dm, double eq, double *rc, double *dc) {
    double a[3]; /* The E-terms */
    double v[3];
    int i;

    /* Note the preference for IAU routines */

    /* Retrieve the E-terms */
    palEtrms(eq, a);

    /* Spherical to Cartesian */
    eraS2c(rm, dm, v);

    /* Include the E-terms */
    for (i = 0; i < 3; i++) {
        v[i] += a[i];
    }

    /* Cartesian to spherical */
    eraC2s(v, rc, dc);

    /* Bring RA into conventional range */
    *rc = eraAnp(*rc);
}
/*
*+
*  Name:
*     palAirmas

*  Purpose:
*     Air mass at given zenith distance

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palAirmas( double zd );

*  Arguments:
*     zd = double (Given)
*        Observed zenith distance (radians)

*  Description:
*     Calculates the airmass at the observed zenith distance.

*  Authors:
*     PTW: Patrick Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The "observed" zenith distance referred to above means "as
*       affected by refraction".
*     - Uses Hardie's (1962) polynomial fit to Bemporad's data for
*       the relative air mass, X, in units of thickness at the zenith
*       as tabulated by Schoenberg (1929). This is adequate for all
*       normal needs as it is accurate to better than 0.1% up to X =
*       6.8 and better than 1% up to X = 10. Bemporad's tabulated
*       values are unlikely to be trustworthy to such accuracy
*       because of variations in density, pressure and other
*       conditions in the atmosphere from those assumed in his work.
*     - The sign of the ZD is ignored.
*     - At zenith distances greater than about ZD = 87 degrees the
*       air mass is held constant to avoid arithmetic overflows.

*  See Also:
*     - Hardie, R.H., 1962, in "Astronomical Techniques"
*         ed. W.A. Hiltner, University of Chicago Press, p180.
*     - Schoenberg, E., 1929, Hdb. d. Ap.,
*         Berlin, Julius Springer, 2, 268.

*  History:
*     2012-03-02 (TIMJ):
*        Initial version from the SLA/F version including documentation.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory.
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

double palAirmas(double zd) {
    double seczm1;
    double airmass;

    /* Have maximum zenith distance of 87 deg */
    const double MAXZD = 87.0 * PAL__DD2R;

    zd = fabs(zd);
    zd = (zd > MAXZD ? MAXZD : zd);

    seczm1 = (1.0 / cos(zd)) - 1.0;
    airmass =
        1.0 + seczm1 * (0.9981833 - seczm1 * (0.002875 + 0.0008083 * seczm1));
    return airmass;
}
/*
*+
*  Name:
*     palAmp

*  Purpose:
*     Convert star RA,Dec from geocentric apparaent to mean place.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*      void palAmp ( double ra, double da, double date, double eq,
*                    double *rm, double *dm );

*  Arguments:
*     ra = double (Given)
*        Apparent RA (radians)
*     dec = double (Given)
*        Apparent Dec (radians)
*     date = double (Given)
*        TDB for apparent place (JD-2400000.5)
*     eq = double (Given)
*        Equinox: Julian epoch of mean place.
*     rm = double * (Returned)
*        Mean RA (radians)
*     dm = double * (Returned)
*        Mean Dec (radians)

*  Description:
*     Convert star RA,Dec from geocentric apparent to mean place. The
*     mean coordinate system is close to ICRS. See palAmpqk for details.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - See palMappa and palAmpqk for details.

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2001 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palAmp(
    double ra, double da, double date, double eq, double *rm, double *dm) {
    double amprms[21];
    palMappa(eq, date, amprms);
    palAmpqk(ra, da, amprms, rm, dm);
}
/*
*+
*  Name:
*     palAmpqk

*  Purpose:
*     Convert star RA,Dec from geocentric apparent to mean place.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAmpqk ( double ra, double da, double amprms[21],
*                     double *rm, double *dm )

*  Arguments:
*     ra = double (Given)
*        Apparent RA (radians).
*     da = double (Given)
*        Apparent Dec (radians).
*     amprms = double[21] (Given)
*        Star-independent mean-to-apparent parameters (see palMappa):
*        (0)      time interval for proper motion (Julian years)
*        (1-3)    barycentric position of the Earth (AU)
*        (4-6)    not used
*        (7)      not used
*        (8-10)   abv: barycentric Earth velocity in units of c
*        (11)     sqrt(1-v*v) where v=modulus(abv)
*        (12-20)  precession/nutation (3,3) matrix
*     rm = double (Returned)
*        Mean RA (radians).
*     dm = double (Returned)
*        Mean Dec (radians).

*  Description:
*     Convert star RA,Dec from geocentric apparent to mean place. The "mean"
*     coordinate system is in fact close to ICRS. Use of this function
*     is appropriate when efficiency is important and where many star
*     positions are all to be transformed for one epoch and equinox.  The
*     star-independent parameters can be obtained by calling the palMappa
*     function.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (PTW):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2000 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palAmpqk(double ra, double da, double amprms[21], double *rm, double *dm) {

    /* Local Variables: */
    double ab1; /* sqrt(1-v*v) where v=modulus of Earth vel */
    double abv[3]; /* Earth velocity wrt SSB (c, FK5) */
    double p1[3], p2[3], p3[3]; /* work vectors */
    double ab1p1, p1dv, p1dvp1, w;
    int i, j;

    /* Unpack some of the parameters */
    ab1 = amprms[11];
    for (i = 0; i < 3; i++) {
        abv[i] = amprms[i + 8];
    }

    /* Apparent RA,Dec to Cartesian */
    eraS2c(ra, da, p3);

    /* Precession and nutation */
    eraTrxp((double(*)[3]) & amprms[12], p3, p2);

    /* Aberration */
    ab1p1 = ab1 + 1.0;
    for (i = 0; i < 3; i++) {
        p1[i] = p2[i];
    }
    for (j = 0; j < 2; j++) {
        p1dv = eraPdp(p1, abv);
        p1dvp1 = 1.0 + p1dv;
        w = 1.0 + p1dv / ab1p1;
        for (i = 0; i < 3; i++) {
            p1[i] = (p1dvp1 * p2[i] - w * abv[i]) / ab1;
        }
        eraPn(p1, &w, p3);
        for (i = 0; i < 3; i++) {
            p1[i] = p3[i];
        }
    }

    /* Mean RA,Dec */
    eraC2s(p1, rm, dm);
    *rm = eraAnp(*rm);
}
/*
*+
*  Name:
*     palAop

*  Purpose:
*     Apparent to observed place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAop ( double rap, double dap, double date, double dut,
*                   double elongm, double phim, double hm, double xp,
*                   double yp, double tdk, double pmb, double rh,
*                   double wl, double tlr,
*                   double *aob, double *zob, double *hob,
*                   double *dob, double *rob );

*  Arguments:
*     rap = double (Given)
*        Geocentric apparent right ascension
*     dap = double (Given)
*        Geocentirc apparent declination
*     date = double (Given)
*        UTC date/time (Modified Julian Date, JD-2400000.5)
*     dut = double (Given)
*        delta UT: UT1-UTC (UTC seconds)
*     elongm = double (Given)
*        Mean longitude of the observer (radians, east +ve)
*     phim = double (Given)
*        Mean geodetic latitude of the observer (radians)
*     hm = double (Given)
*        Observer's height above sea level (metres)
*     xp = double (Given)
*        Polar motion x-coordinates (radians)
*     yp = double (Given)
*        Polar motion y-coordinates (radians)
*     tdk = double (Given)
*        Local ambient temperature (K; std=273.15)
*     pmb = double (Given)
*        Local atmospheric pressure (mb; std=1013.25)
*     rh = double (Given)
*        Local relative humidity (in the range 0.0-1.0)
*     wl = double (Given)
*        Effective wavelength (micron, e.g. 0.55)
*     tlr = double (Given)
*        Tropospheric laps rate (K/metre, e.g. 0.0065)
*     aob = double * (Returned)
*        Observed azimuth (radians: N=0; E=90)
*     zob = double * (Returned)
*        Observed zenith distance (radians)
*     hob = double * (Returned)
*        Observed Hour Angle (radians)
*     dob = double * (Returned)
*        Observed Declination (radians)
*     rob = double * (Returned)
*        Observed Right Ascension (radians)


*  Description:
*     Apparent to observed place for sources distant from the solar system.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - This routine returns zenith distance rather than elevation
*       in order to reflect the fact that no allowance is made for
*       depression of the horizon.
*
*     - The accuracy of the result is limited by the corrections for
*       refraction.  Providing the meteorological parameters are
*       known accurately and there are no gross local effects, the
*       predicted apparent RA,Dec should be within about 0.1 arcsec
*       for a zenith distance of less than 70 degrees.  Even at a
*       topocentric zenith distance of 90 degrees, the accuracy in
*       elevation should be better than 1 arcmin;  useful results
*       are available for a further 3 degrees, beyond which the
*       palRefro routine returns a fixed value of the refraction.
*       The complementary routines palAop (or palAopqk) and palOap
*       (or palOapqk) are self-consistent to better than 1 micro-
*       arcsecond all over the celestial sphere.
*
*     - It is advisable to take great care with units, as even
*       unlikely values of the input parameters are accepted and
*       processed in accordance with the models used.
*
*     - "Apparent" place means the geocentric apparent right ascension
*       and declination, which is obtained from a catalogue mean place
*       by allowing for space motion, parallax, precession, nutation,
*       annual aberration, and the Sun's gravitational lens effect.  For
*       star positions in the FK5 system (i.e. J2000), these effects can
*       be applied by means of the palMap etc routines.  Starting from
*       other mean place systems, additional transformations will be
*       needed;  for example, FK4 (i.e. B1950) mean places would first
*       have to be converted to FK5, which can be done with the
*       palFk425 etc routines.
*
*     - "Observed" Az,El means the position that would be seen by a
*       perfect theodolite located at the observer.  This is obtained
*       from the geocentric apparent RA,Dec by allowing for Earth
*       orientation and diurnal aberration, rotating from equator
*       to horizon coordinates, and then adjusting for refraction.
*       The HA,Dec is obtained by rotating back into equatorial
*       coordinates, using the geodetic latitude corrected for polar
*       motion, and is the position that would be seen by a perfect
*       equatorial located at the observer and with its polar axis
*       aligned to the Earth's axis of rotation (n.b. not to the
*       refracted pole).  Finally, the RA is obtained by subtracting
*       the HA from the local apparent ST.
*
*     - To predict the required setting of a real telescope, the
*       observed place produced by this routine would have to be
*       adjusted for the tilt of the azimuth or polar axis of the
*       mounting (with appropriate corrections for mount flexures),
*       for non-perpendicularity between the mounting axes, for the
*       position of the rotator axis and the pointing axis relative
*       to it, for tube flexure, for gear and encoder errors, and
*       finally for encoder zero points.  Some telescopes would, of
*       course, exhibit other properties which would need to be
*       accounted for at the appropriate point in the sequence.
*
*     - This routine takes time to execute, due mainly to the
*       rigorous integration used to evaluate the refraction.
*       For processing multiple stars for one location and time,
*       call palAoppa once followed by one call per star to palAopqk.
*       Where a range of times within a limited period of a few hours
*       is involved, and the highest precision is not required, call
*       palAoppa once, followed by a call to palAoppat each time the
*       time changes, followed by one call per star to palAopqk.
*
*     - The DATE argument is UTC expressed as an MJD.  This is,
*       strictly speaking, wrong, because of leap seconds.  However,
*       as long as the delta UT and the UTC are consistent there
*       are no difficulties, except during a leap second.  In this
*       case, the start of the 61st second of the final minute should
*       begin a new MJD day and the old pre-leap delta UT should
*       continue to be used.  As the 61st second completes, the MJD
*       should revert to the start of the day as, simultaneously,
*       the delta UTC changes by one second to its post-leap new value.
*
*     - The delta UT (UT1-UTC) is tabulated in IERS circulars and
*       elsewhere.  It increases by exactly one second at the end of
*       each UTC leap second, introduced in order to keep delta UT
*       within +/- 0.9 seconds.
*
*     - IMPORTANT -- TAKE CARE WITH THE LONGITUDE SIGN CONVENTION.
*       The longitude required by the present routine is east-positive,
*       in accordance with geographical convention (and right-handed).
*       In particular, note that the longitudes returned by the
*       palObs routine are west-positive, following astronomical
*       usage, and must be reversed in sign before use in the present
*       routine.
*
*     - The polar coordinates XP,YP can be obtained from IERS
*       circulars and equivalent publications.  The maximum amplitude
*       is about 0.3 arcseconds.  If XP,YP values are unavailable,
*       use XP=YP=0.0.  See page B60 of the 1988 Astronomical Almanac
*       for a definition of the two angles.
*
*     - The height above sea level of the observing station, HM,
*       can be obtained from the Astronomical Almanac (Section J
*       in the 1988 edition), or via the routine palObs.  If P,
*       the pressure in millibars, is available, an adequate
*       estimate of HM can be obtained from the expression
*
*             HM ~ -29.3*TSL*LOG(P/1013.25).
*
*       where TSL is the approximate sea-level air temperature in K
*       (see Astrophysical Quantities, C.W.Allen, 3rd edition,
*       section 52).  Similarly, if the pressure P is not known,
*       it can be estimated from the height of the observing
*       station, HM, as follows:
*
*             P ~ 1013.25*EXP(-HM/(29.3*TSL)).
*
*       Note, however, that the refraction is nearly proportional to the
*       pressure and that an accurate P value is important for precise
*       work.
*
*     - The azimuths etc produced by the present routine are with
*       respect to the celestial pole.  Corrections to the terrestrial
*       pole can be computed using palPolmo.

*  History:
*     2012-08-25 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palAop(double rap,
            double dap,
            double date,
            double dut,
            double elongm,
            double phim,
            double hm,
            double xp,
            double yp,
            double tdk,
            double pmb,
            double rh,
            double wl,
            double tlr,
            double *aob,
            double *zob,
            double *hob,
            double *dob,
            double *rob) {

    double aoprms[14];

    palAoppa(
        date, dut, elongm, phim, hm, xp, yp, tdk, pmb, rh, wl, tlr, aoprms);
    palAopqk(rap, dap, aoprms, aob, zob, hob, dob, rob);
}
/*
*+
*  Name:
*     palAoppa

*  Purpose:
*     Precompute apparent to observed place parameters

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAoppa ( double date, double dut, double elongm, double phim,
*                     double hm, double xp, double yp, double tdk, double pmb,
*                     double rh, double wl, double tlr, double aoprms[14] );

*  Arguments:
*     date = double (Given)
*        UTC date/time (modified Julian Date, JD-2400000.5)
*     dut = double (Given)
*        delta UT:  UT1-UTC (UTC seconds)
*     elongm = double (Given)
*        mean longitude of the observer (radians, east +ve)
*     phim = double (Given)
*        mean geodetic latitude of the observer (radians)
*     hm = double (Given)
*        observer's height above sea level (metres)
*     xp = double (Given)
*        polar motion x-coordinate (radians)
*     yp = double (Given)
*        polar motion y-coordinate (radians)
*     tdk = double (Given)
*        local ambient temperature (K; std=273.15)
*     pmb = double (Given)
*        local atmospheric pressure (mb; std=1013.25)
*     rh = double (Given)
*        local relative humidity (in the range 0.0-1.0)
*     wl = double (Given)
*        effective wavelength (micron, e.g. 0.55)
*     tlr = double (Given)
*        tropospheric lapse rate (K/metre, e.g. 0.0065)
*     aoprms = double [14] (Returned)
*        Star-independent apparent-to-observed parameters
*
*         (0)      geodetic latitude (radians)
*         (1,2)    sine and cosine of geodetic latitude
*         (3)      magnitude of diurnal aberration vector
*         (4)      height (hm)
*         (5)      ambient temperature (tdk)
*         (6)      pressure (pmb)
*         (7)      relative humidity (rh)
*         (8)      wavelength (wl)
*         (9)     lapse rate (tlr)
*         (10,11)  refraction constants A and B (radians)
*         (12)     longitude + eqn of equinoxes + sidereal DUT (radians)
*         (13)     local apparent sidereal time (radians)

*  Description:
*     Precompute apparent to observed place parameters required by palAopqk
*     and palOapqk.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - It is advisable to take great care with units, as even
*       unlikely values of the input parameters are accepted and
*       processed in accordance with the models used.
*
*     - The DATE argument is UTC expressed as an MJD.  This is,
*       strictly speaking, improper, because of leap seconds.  However,
*       as long as the delta UT and the UTC are consistent there
*       are no difficulties, except during a leap second.  In this
*       case, the start of the 61st second of the final minute should
*       begin a new MJD day and the old pre-leap delta UT should
*       continue to be used.  As the 61st second completes, the MJD
*       should revert to the start of the day as, simultaneously,
*       the delta UTC changes by one second to its post-leap new value.
*
*     - The delta UT (UT1-UTC) is tabulated in IERS circulars and
*       elsewhere.  It increases by exactly one second at the end of
*       each UTC leap second, introduced in order to keep delta UT
*       within +/- 0.9 seconds.
*
*     - IMPORTANT -- TAKE CARE WITH THE LONGITUDE SIGN CONVENTION.
*       The longitude required by the present routine is east-positive,
*       in accordance with geographical convention (and right-handed).
*       In particular, note that the longitudes returned by the
*       palObs routine are west-positive, following astronomical
*       usage, and must be reversed in sign before use in the present
*       routine.
*
*     - The polar coordinates XP,YP can be obtained from IERS
*       circulars and equivalent publications.  The maximum amplitude
*       is about 0.3 arcseconds.  If XP,YP values are unavailable,
*       use XP=YP=0.0.  See page B60 of the 1988 Astronomical Almanac
*       for a definition of the two angles.
*
*     - The height above sea level of the observing station, HM,
*       can be obtained from the Astronomical Almanac (Section J
*       in the 1988 edition), or via the routine palObs.  If P,
*       the pressure in millibars, is available, an adequate
*       estimate of HM can be obtained from the expression
*
*             HM ~ -29.3*TSL*log(P/1013.25).
*
*       where TSL is the approximate sea-level air temperature in K
*       (see Astrophysical Quantities, C.W.Allen, 3rd edition,
*       section 52).  Similarly, if the pressure P is not known,
*       it can be estimated from the height of the observing
*       station, HM, as follows:
*
*             P ~ 1013.25*exp(-HM/(29.3*TSL)).
*
*       Note, however, that the refraction is nearly proportional to the
*       pressure and that an accurate P value is important for precise
*       work.
*
*     - Repeated, computationally-expensive, calls to palAoppa for
*       times that are very close together can be avoided by calling
*       palAoppa just once and then using palAoppat for the subsequent
*       times.  Fresh calls to palAoppa will be needed only when
*       changes in the precession have grown to unacceptable levels or
*       when anything affecting the refraction has changed.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version, ported directly from Fortran SLA.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "math.h"
#include "pal.h"
#include "palmac.h"

/* These are local SLA implementations to aid in testing. Switch
 * to native PAL implementations when tests are complete. */
static void pal__Geoc(double p, double h, double *r, double *z);
static void pal__Nutc(double date, double *dpsi, double *deps, double *eps0);
static double pal__Eqeqx(double date);

void palAoppa(double date,
              double dut,
              double elongm,
              double phim,
              double hm,
              double xp,
              double yp,
              double tdk,
              double pmb,
              double rh,
              double wl,
              double tlr,
              double aoprms[14]) {

    /* Constants */
    const double C = 173.14463331; /* Speed of light (AU per day) */
    const double SOLSID =
        1.0027379093; /* Ratio between solar and sidereal time */

    /* Local variables */
    double cphim, xt, yt, zt, xc, yc, zc, elong, phi, uau, vau;

    /*  Observer's location corrected for polar motion */
    cphim = cos(phim);
    xt = cos(elongm) * cphim;
    yt = sin(elongm) * cphim;
    zt = sin(phim);
    xc = xt - xp * zt;
    yc = yt + yp * zt;
    zc = xp * xt - yp * yt + zt;
    if (xc == 0.0 && yc == 0.0) {
        elong = 0.0;
    } else {
        elong = atan2(yc, xc);
    }
    phi = atan2(zc, sqrt(xc * xc + yc * yc));
    aoprms[0] = phi;
    aoprms[1] = sin(phi);
    aoprms[2] = cos(phi);

    /*  magnitude of the diurnal aberration vector */
    pal__Geoc(phi, hm, &uau, &vau);
    aoprms[3] = PAL__D2PI * uau * SOLSID / C;

    /*  copy the refraction parameters and compute the a & b constants */
    aoprms[4] = hm;
    aoprms[5] = tdk;
    aoprms[6] = pmb;
    aoprms[7] = rh;
    aoprms[8] = wl;
    aoprms[9] = tlr;
    palRefco(hm, tdk, pmb, rh, wl, phi, tlr, 1e-10, &aoprms[10], &aoprms[11]);

    /*  longitude + equation of the equinoxes + sidereal equivalent of DUT
     *  (ignoring change in equation of the equinoxes between UTC and TDB) */
    aoprms[12] = elong + pal__Eqeqx(date) + dut * SOLSID * PAL__DS2R;

    /*  sidereal time */
    palAoppat(date, aoprms);
}

/* Private reimplementation of slaEqeqx for testing the algorithm */

#include <math.h>

static void pal__Geoc(double p, double h, double *r, double *z) {
    /*  earth equatorial radius (metres) */
    const double A0 = 6378140.0;

    /*  reference spheroid flattening factor and useful function */
    const double f = 1.0 / 298.257;
    double b;

    /*  astronomical unit in metres */
    const double AU = 1.49597870e11;

    double sp, cp, c, s;

    b = pow(1.0 - f, 2.0);

    /*  geodetic to geocentric conversion */
    sp = sin(p);
    cp = cos(p);
    c = 1.0 / sqrt(cp * cp + b * sp * sp);
    s = b * c;
    *r = (A0 * c + h) * cp / AU;
    *z = (A0 * s + h) * sp / AU;
}

static double pal__Eqeqx(double date) {

    const double T2AS = 1296000.0;

    double sla_eqeqx;
    double t, om, dpsi, deps, eps0;

    /*  interval between basic epoch j2000.0 and current epoch (jc) */
    t = (date - 51544.5) / 36525.0;

    /*  longitude of the mean ascending node of the lunar orbit on the
     *   ecliptic, measured from the mean equinox of date */
    om =
        PAL__DAS2R *
        (450160.280 + (-5.0 * T2AS - 482890.539 + (7.455 + 0.008 * t) * t) * t);

    /*  nutation */
    pal__Nutc(date, &dpsi, &deps, &eps0);

    /*  equation of the equinoxes */
    sla_eqeqx = dpsi * cos(eps0) +
                PAL__DAS2R * (0.00264 * sin(om) + 0.000063 * sin(om + om));

    return sla_eqeqx;
}

#include "palmac.h"

static void pal__Nutc(double date, double *dpsi, double *deps, double *eps0) {

    const double DJC = 36525.0;
    const double DJM0 = 51544.5;
    const double TURNAS = 1296000.0;

#define NTERMS 194

    int j;
    double t, el, elp, f, d, om, ve, ma, ju, sa, theta, c, s, dp, de;

    int na[194][9] = {
        {0, 0, 0, 0, -1, 0, 0, 0, 0},   {0, 0, 2, -2, 2, 0, 0, 0, 0},
        {0, 0, 2, 0, 2, 0, 0, 0, 0},    {0, 0, 0, 0, -2, 0, 0, 0, 0},
        {0, 1, 0, 0, 0, 0, 0, 0, 0},    {0, 1, 2, -2, 2, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 0},    {0, 0, 2, 0, 1, 0, 0, 0, 0},
        {1, 0, 2, 0, 2, 0, 0, 0, 0},    {0, -1, 2, -2, 2, 0, 0, 0, 0},
        {0, 0, 2, -2, 1, 0, 0, 0, 0},   {-1, 0, 2, 0, 2, 0, 0, 0, 0},
        {-1, 0, 0, 2, 0, 0, 0, 0, 0},   {1, 0, 0, 0, 1, 0, 0, 0, 0},
        {1, 0, 0, 0, -1, 0, 0, 0, 0},   {-1, 0, 2, 2, 2, 0, 0, 0, 0},
        {1, 0, 2, 0, 1, 0, 0, 0, 0},    {-2, 0, 2, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 2, 0, 0, 0, 0, 0},    {0, 0, 2, 2, 2, 0, 0, 0, 0},
        {2, 0, 0, -2, 0, 0, 0, 0, 0},   {2, 0, 2, 0, 2, 0, 0, 0, 0},
        {1, 0, 2, -2, 2, 0, 0, 0, 0},   {-1, 0, 2, 0, 1, 0, 0, 0, 0},
        {2, 0, 0, 0, 0, 0, 0, 0, 0},    {0, 0, 2, 0, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 1, 0, 0, 0, 0},    {-1, 0, 0, 2, 1, 0, 0, 0, 0},
        {0, 2, 2, -2, 2, 0, 0, 0, 0},   {0, 0, 2, -2, 0, 0, 0, 0, 0},
        {-1, 0, 0, 2, -1, 0, 0, 0, 0},  {0, 1, 0, 0, -1, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 0, 0, 0},    {-1, 0, 2, 2, 1, 0, 0, 0, 0},
        {1, 0, 2, 2, 2, 0, 0, 0, 0},    {0, 1, 2, 0, 2, 0, 0, 0, 0},
        {-2, 0, 2, 0, 0, 0, 0, 0, 0},   {0, 0, 2, 2, 1, 0, 0, 0, 0},
        {0, -1, 2, 0, 2, 0, 0, 0, 0},   {0, 0, 0, 2, 1, 0, 0, 0, 0},
        {1, 0, 2, -2, 1, 0, 0, 0, 0},   {2, 0, 0, -2, -1, 0, 0, 0, 0},
        {2, 0, 2, -2, 2, 0, 0, 0, 0},   {2, 0, 2, 0, 1, 0, 0, 0, 0},
        {0, 0, 0, 2, -1, 0, 0, 0, 0},   {0, -1, 2, -2, 1, 0, 0, 0, 0},
        {-1, -1, 0, 2, 0, 0, 0, 0, 0},  {2, 0, 0, -2, 1, 0, 0, 0, 0},
        {1, 0, 0, 2, 0, 0, 0, 0, 0},    {0, 1, 2, -2, 1, 0, 0, 0, 0},
        {1, -1, 0, 0, 0, 0, 0, 0, 0},   {-2, 0, 2, 0, 2, 0, 0, 0, 0},
        {0, -1, 0, 2, 0, 0, 0, 0, 0},   {3, 0, 2, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0, 0, 0, 0},    {1, -1, 2, 0, 2, 0, 0, 0, 0},
        {1, 0, 0, -1, 0, 0, 0, 0, 0},   {-1, -1, 2, 2, 2, 0, 0, 0, 0},
        {-1, 0, 2, 0, 0, 0, 0, 0, 0},   {2, 0, 0, 0, -1, 0, 0, 0, 0},
        {0, -1, 2, 2, 2, 0, 0, 0, 0},   {1, 1, 2, 0, 2, 0, 0, 0, 0},
        {2, 0, 0, 0, 1, 0, 0, 0, 0},    {1, 1, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, -2, 2, -1, 0, 0, 0, 0},  {1, 0, 2, 0, 0, 0, 0, 0, 0},
        {-1, 1, 0, 1, 0, 0, 0, 0, 0},   {1, 0, 0, 0, 2, 0, 0, 0, 0},
        {-1, 0, 1, 0, 1, 0, 0, 0, 0},   {0, 0, 2, 1, 2, 0, 0, 0, 0},
        {-1, 1, 0, 1, 1, 0, 0, 0, 0},   {-1, 0, 2, 4, 2, 0, 0, 0, 0},
        {0, -2, 2, -2, 1, 0, 0, 0, 0},  {1, 0, 2, 2, 1, 0, 0, 0, 0},
        {1, 0, 0, 0, -2, 0, 0, 0, 0},   {-2, 0, 2, 2, 2, 0, 0, 0, 0},
        {1, 1, 2, -2, 2, 0, 0, 0, 0},   {-2, 0, 2, 4, 2, 0, 0, 0, 0},
        {-1, 0, 4, 0, 2, 0, 0, 0, 0},   {2, 0, 2, -2, 1, 0, 0, 0, 0},
        {1, 0, 0, -1, -1, 0, 0, 0, 0},  {2, 0, 2, 2, 2, 0, 0, 0, 0},
        {1, 0, 0, 2, 1, 0, 0, 0, 0},    {3, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 2, -2, -1, 0, 0, 0, 0},  {3, 0, 2, -2, 2, 0, 0, 0, 0},
        {0, 0, 4, -2, 2, 0, 0, 0, 0},   {-1, 0, 0, 4, 0, 0, 0, 0, 0},
        {0, 1, 2, 0, 1, 0, 0, 0, 0},    {0, 0, 2, -2, 3, 0, 0, 0, 0},
        {-2, 0, 0, 4, 0, 0, 0, 0, 0},   {-1, -1, 0, 2, 1, 0, 0, 0, 0},
        {-2, 0, 2, 0, -1, 0, 0, 0, 0},  {0, 0, 2, 0, -1, 0, 0, 0, 0},
        {0, -1, 2, 0, 1, 0, 0, 0, 0},   {0, 1, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 2, -1, 2, 0, 0, 0, 0},   {2, 1, 0, -2, 0, 0, 0, 0, 0},
        {0, 0, 2, 4, 2, 0, 0, 0, 0},    {-1, -1, 0, 2, -1, 0, 0, 0, 0},
        {-1, 1, 0, 2, 0, 0, 0, 0, 0},   {1, -1, 0, 0, 1, 0, 0, 0, 0},
        {0, -1, 2, -2, 0, 0, 0, 0, 0},  {0, 1, 0, 0, -2, 0, 0, 0, 0},
        {1, -1, 2, 2, 2, 0, 0, 0, 0},   {1, 0, 0, 2, -1, 0, 0, 0, 0},
        {-1, 1, 2, 2, 2, 0, 0, 0, 0},   {3, 0, 2, 0, 1, 0, 0, 0, 0},
        {0, 1, 2, 2, 2, 0, 0, 0, 0},    {1, 0, 2, -2, 0, 0, 0, 0, 0},
        {-1, 0, -2, 4, -1, 0, 0, 0, 0}, {-1, -1, 2, 2, 1, 0, 0, 0, 0},
        {0, -1, 2, 2, 1, 0, 0, 0, 0},   {2, -1, 2, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 2, 2, 0, 0, 0, 0},    {1, -1, 2, 0, 1, 0, 0, 0, 0},
        {-1, 1, 2, 0, 2, 0, 0, 0, 0},   {0, 1, 0, 2, 0, 0, 0, 0, 0},
        {0, 1, 2, -2, 0, 0, 0, 0, 0},   {0, 3, 2, -2, 2, 0, 0, 0, 0},
        {0, 0, 0, 1, 1, 0, 0, 0, 0},    {-1, 0, 2, 2, 0, 0, 0, 0, 0},
        {2, 1, 2, 0, 2, 0, 0, 0, 0},    {1, 1, 0, 0, 1, 0, 0, 0, 0},
        {2, 0, 0, 2, 0, 0, 0, 0, 0},    {1, 1, 2, 0, 1, 0, 0, 0, 0},
        {-1, 0, 0, 2, 2, 0, 0, 0, 0},   {1, 0, -2, 2, 0, 0, 0, 0, 0},
        {0, -1, 0, 2, -1, 0, 0, 0, 0},  {-1, 0, 1, 0, 2, 0, 0, 0, 0},
        {0, 1, 0, 1, 0, 0, 0, 0, 0},    {1, 0, -2, 2, -2, 0, 0, 0, 0},
        {0, 0, 0, 1, -1, 0, 0, 0, 0},   {1, -1, 0, 0, -1, 0, 0, 0, 0},
        {0, 0, 0, 4, 0, 0, 0, 0, 0},    {1, -1, 0, 2, 0, 0, 0, 0, 0},
        {1, 0, 2, 1, 2, 0, 0, 0, 0},    {1, 0, 2, -1, 2, 0, 0, 0, 0},
        {-1, 0, 0, 2, -2, 0, 0, 0, 0},  {0, 0, 2, 1, 1, 0, 0, 0, 0},
        {-1, 0, 2, 0, -1, 0, 0, 0, 0},  {-1, 0, 2, 4, 1, 0, 0, 0, 0},
        {0, 0, 2, 2, 0, 0, 0, 0, 0},    {1, 1, 2, -2, 1, 0, 0, 0, 0},
        {0, 0, 1, 0, 1, 0, 0, 0, 0},    {-1, 0, 2, -1, 1, 0, 0, 0, 0},
        {-2, 0, 2, 2, 1, 0, 0, 0, 0},   {2, -1, 0, 0, 0, 0, 0, 0, 0},
        {4, 0, 2, 0, 2, 0, 0, 0, 0},    {2, 1, 2, -2, 2, 0, 0, 0, 0},
        {0, 1, 2, 1, 2, 0, 0, 0, 0},    {1, 0, 4, -2, 2, 0, 0, 0, 0},
        {1, 1, 0, 0, -1, 0, 0, 0, 0},   {-2, 0, 2, 4, 1, 0, 0, 0, 0},
        {2, 0, 2, 0, 0, 0, 0, 0, 0},    {-1, 0, 1, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 1, 0, 0, 0, 0, 0},    {0, 1, 0, 2, 1, 0, 0, 0, 0},
        {-1, 0, 4, 0, 1, 0, 0, 0, 0},   {-1, 0, 0, 4, 1, 0, 0, 0, 0},
        {2, 0, 2, 2, 1, 0, 0, 0, 0},    {2, 1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, -5, 5, -3, 0, 0, 0},  {0, 0, 0, 0, 0, 0, 0, 2, 0},
        {0, 0, 1, -1, 1, 0, 0, -1, 0},  {0, 0, -1, 1, -1, 1, 0, 0, 0},
        {0, 0, -1, 1, 0, 0, 2, 0, 0},   {0, 0, 3, -3, 3, 0, 0, -1, 0},
        {0, 0, -8, 8, -7, 5, 0, 0, 0},  {0, 0, -1, 1, -1, 0, 2, 0, 0},
        {0, 0, -2, 2, -2, 2, 0, 0, 0},  {0, 0, -6, 6, -6, 4, 0, 0, 0},
        {0, 0, -2, 2, -2, 0, 8, -3, 0}, {0, 0, 6, -6, 6, 0, -8, 3, 0},
        {0, 0, 4, -4, 4, -2, 0, 0, 0},  {0, 0, -3, 3, -3, 2, 0, 0, 0},
        {0, 0, 4, -4, 3, 0, -8, 3, 0},  {0, 0, -4, 4, -5, 0, 8, -3, 0},
        {0, 0, 0, 0, 0, 2, 0, 0, 0},    {0, 0, -4, 4, -4, 3, 0, 0, 0},
        {0, 1, -1, 1, -1, 0, 0, 1, 0},  {0, 0, 0, 0, 0, 0, 0, 1, 0},
        {0, 0, 1, -1, 1, 1, 0, 0, 0},   {0, 0, 2, -2, 2, 0, -2, 0, 0},
        {0, -1, -7, 7, -7, 5, 0, 0, 0}, {-2, 0, 2, 0, 2, 0, 0, -2, 0},
        {-2, 0, 2, 0, 1, 0, 0, -3, 0},  {0, 0, 2, -2, 2, 0, 0, -2, 0},
        {0, 0, 1, -1, 1, 0, 0, 1, 0},   {0, 0, 0, 0, 0, 0, 0, 0, 2},
        {0, 0, 0, 0, 0, 0, 0, 0, 1},    {2, 0, -2, 0, -2, 0, 0, 3, 0},
        {0, 0, 1, -1, 1, 0, 0, -2, 0},  {0, 0, -7, 7, -7, 5, 0, 0, 0}};
    double psi[194][4] = {{3341.5000000000000,
                           17206241.800000001,
                           3.1000000000000001,
                           17409.500000000000},
                          {-1716.8000000000000,
                           -1317185.3000000000,
                           1.3999999999999999,
                           -156.80000000000001},
                          {285.69999999999999,
                           -227667.00000000000,
                           0.29999999999999999,
                           -23.500000000000000},
                          {-68.599999999999994,
                           -207448.00000000000,
                           0.0000000000000000,
                           -21.399999999999999},
                          {950.29999999999995,
                           147607.89999999999,
                           -2.2999999999999998,
                           -355.00000000000000},
                          {-66.700000000000003,
                           -51689.099999999999,
                           0.20000000000000001,
                           122.59999999999999},
                          {-108.59999999999999,
                           71117.600000000006,
                           0.0000000000000000,
                           7.0000000000000000},
                          {35.600000000000001,
                           -38740.199999999997,
                           0.10000000000000001,
                           -36.200000000000003},
                          {85.400000000000006,
                           -30127.599999999999,
                           0.0000000000000000,
                           -3.1000000000000001},
                          {9.0000000000000000,
                           21583.000000000000,
                           0.10000000000000001,
                           -50.299999999999997},
                          {22.100000000000001,
                           12822.799999999999,
                           0.0000000000000000,
                           13.300000000000001},
                          {3.3999999999999999,
                           12350.799999999999,
                           0.0000000000000000,
                           1.3000000000000000},
                          {-21.100000000000001,
                           15699.400000000000,
                           0.0000000000000000,
                           1.6000000000000001},
                          {4.2000000000000002,
                           6313.8000000000002,
                           0.0000000000000000,
                           6.2000000000000002},
                          {-22.800000000000001,
                           5796.8999999999996,
                           0.0000000000000000,
                           6.0999999999999996},
                          {15.699999999999999,
                           -5961.1000000000004,
                           0.0000000000000000,
                           -0.59999999999999998},
                          {13.100000000000000,
                           -5159.1000000000004,
                           0.0000000000000000,
                           -4.5999999999999996},
                          {1.8000000000000000,
                           4592.6999999999998,
                           0.0000000000000000,
                           4.5000000000000000},
                          {-17.500000000000000,
                           6336.0000000000000,
                           0.0000000000000000,
                           0.69999999999999996},
                          {16.300000000000001,
                           -3851.0999999999999,
                           0.0000000000000000,
                           -0.40000000000000002},
                          {-2.7999999999999998,
                           4771.6999999999998,
                           0.0000000000000000,
                           0.50000000000000000},
                          {13.800000000000001,
                           -3099.3000000000002,
                           0.0000000000000000,
                           -0.29999999999999999},
                          {0.20000000000000001,
                           2860.3000000000002,
                           0.0000000000000000,
                           0.29999999999999999},
                          {1.3999999999999999,
                           2045.3000000000000,
                           0.0000000000000000,
                           2.0000000000000000},
                          {-8.5999999999999996,
                           2922.5999999999999,
                           0.0000000000000000,
                           0.29999999999999999},
                          {-7.7000000000000002,
                           2587.9000000000001,
                           0.0000000000000000,
                           0.20000000000000001},
                          {8.8000000000000007,
                           -1408.0999999999999,
                           0.0000000000000000,
                           3.7000000000000002},
                          {1.3999999999999999,
                           1517.5000000000000,
                           0.0000000000000000,
                           1.5000000000000000},
                          {-1.8999999999999999,
                           -1579.7000000000000,
                           0.0000000000000000,
                           7.7000000000000002},
                          {1.3000000000000000,
                           -2178.5999999999999,
                           0.0000000000000000,
                           -0.20000000000000001},
                          {-4.7999999999999998,
                           1286.8000000000000,
                           0.0000000000000000,
                           1.3000000000000000},
                          {6.2999999999999998,
                           1267.2000000000000,
                           0.0000000000000000,
                           -4.0000000000000000},
                          {-1.0000000000000000,
                           1669.3000000000000,
                           0.0000000000000000,
                           -8.3000000000000007},
                          {2.3999999999999999,
                           -1020.0000000000000,
                           0.0000000000000000,
                           -0.90000000000000002},
                          {4.5000000000000000,
                           -766.89999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.1000000000000001,
                           756.50000000000000,
                           0.0000000000000000,
                           -1.7000000000000000},
                          {-1.3999999999999999,
                           -1097.3000000000000,
                           0.0000000000000000,
                           -0.50000000000000000},
                          {2.6000000000000001,
                           -663.00000000000000,
                           0.0000000000000000,
                           -0.59999999999999998},
                          {0.80000000000000004,
                           -714.10000000000002,
                           0.0000000000000000,
                           1.6000000000000001},
                          {0.40000000000000002,
                           -629.89999999999998,
                           0.0000000000000000,
                           -0.59999999999999998},
                          {0.29999999999999999,
                           580.39999999999998,
                           0.0000000000000000,
                           0.59999999999999998},
                          {-1.6000000000000001,
                           577.29999999999995,
                           0.0000000000000000,
                           0.50000000000000000},
                          {-0.90000000000000002,
                           644.39999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {2.2000000000000002,
                           -534.00000000000000,
                           0.0000000000000000,
                           -0.50000000000000000},
                          {-2.5000000000000000,
                           493.30000000000001,
                           0.0000000000000000,
                           0.50000000000000000},
                          {-0.10000000000000001,
                           -477.30000000000001,
                           0.0000000000000000,
                           -2.3999999999999999},
                          {-0.90000000000000002,
                           735.00000000000000,
                           0.0000000000000000,
                           -1.7000000000000000},
                          {0.69999999999999996,
                           406.19999999999999,
                           0.0000000000000000,
                           0.40000000000000002},
                          {-2.7999999999999998,
                           656.89999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.59999999999999998,
                           358.00000000000000,
                           0.0000000000000000,
                           2.0000000000000000},
                          {-0.69999999999999996,
                           472.50000000000000,
                           0.0000000000000000,
                           -1.1000000000000001},
                          {-0.10000000000000001,
                           -300.50000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.2000000000000000,
                           435.10000000000002,
                           0.0000000000000000,
                           -1.0000000000000000},
                          {1.8000000000000000,
                           -289.39999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.59999999999999998,
                           -422.60000000000002,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.80000000000000004,
                           -287.60000000000002,
                           0.0000000000000000,
                           0.59999999999999998},
                          {-38.600000000000001,
                           -392.30000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.69999999999999996,
                           -281.80000000000001,
                           0.0000000000000000,
                           0.59999999999999998},
                          {0.59999999999999998,
                           -405.69999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.2000000000000000,
                           229.00000000000000,
                           0.0000000000000000,
                           0.20000000000000001},
                          {1.1000000000000001,
                           -264.30000000000001,
                           0.0000000000000000,
                           0.50000000000000000},
                          {-0.69999999999999996,
                           247.90000000000001,
                           0.0000000000000000,
                           -0.50000000000000000},
                          {-0.20000000000000001,
                           218.00000000000000,
                           0.0000000000000000,
                           0.20000000000000001},
                          {0.59999999999999998,
                           -339.00000000000000,
                           0.0000000000000000,
                           0.80000000000000004},
                          {-0.69999999999999996,
                           198.69999999999999,
                           0.0000000000000000,
                           0.20000000000000001},
                          {-1.5000000000000000,
                           334.00000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           334.00000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           -198.09999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-106.59999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.50000000000000000,
                           165.80000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           134.80000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.90000000000000002,
                           -151.59999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -129.69999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.80000000000000004,
                           -132.80000000000001,
                           0.0000000000000000,
                           -0.10000000000000001},
                          {0.50000000000000000,
                           -140.69999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           138.40000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           129.00000000000000,
                           0.0000000000000000,
                           -0.29999999999999999},
                          {0.50000000000000000,
                           -121.20000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.29999999999999999,
                           114.50000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           101.80000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-3.6000000000000001,
                           -101.90000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.80000000000000004,
                           -109.40000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.20000000000000001,
                           -97.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.69999999999999996,
                           157.30000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.20000000000000001,
                           -83.299999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.29999999999999999,
                           93.299999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           92.099999999999994,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.50000000000000000,
                           133.59999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           81.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           123.90000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.29999999999999999,
                           128.09999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           74.099999999999994,
                           0.0000000000000000,
                           -0.29999999999999999},
                          {-0.20000000000000001,
                           -70.299999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.40000000000000002,
                           66.599999999999994,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -66.700000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.69999999999999996,
                           69.299999999999997,
                           0.0000000000000000,
                           -0.29999999999999999},
                          {0.0000000000000000,
                           -70.400000000000006,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           101.50000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.50000000000000000,
                           -69.099999999999994,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           58.500000000000000,
                           0.0000000000000000,
                           0.20000000000000001},
                          {0.10000000000000001,
                           -94.900000000000006,
                           0.0000000000000000,
                           0.20000000000000001},
                          {0.0000000000000000,
                           52.899999999999999,
                           0.0000000000000000,
                           -0.20000000000000001},
                          {0.10000000000000001,
                           86.700000000000003,
                           0.0000000000000000,
                           -0.20000000000000001},
                          {-0.10000000000000001,
                           -59.200000000000003,
                           0.0000000000000000,
                           0.20000000000000001},
                          {0.29999999999999999,
                           -58.799999999999997,
                           0.0000000000000000,
                           0.10000000000000001},
                          {-0.29999999999999999,
                           49.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           56.899999999999999,
                           0.0000000000000000,
                           -0.10000000000000001},
                          {0.29999999999999999,
                           -50.200000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           53.399999999999999,
                           0.0000000000000000,
                           -0.10000000000000001},
                          {0.10000000000000001,
                           -76.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           45.299999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -46.799999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.20000000000000001,
                           -44.600000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.20000000000000001,
                           -48.700000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -46.799999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -42.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           46.399999999999999,
                           0.0000000000000000,
                           -0.10000000000000001},
                          {0.20000000000000001,
                           -67.299999999999997,
                           0.0000000000000000,
                           0.10000000000000001},
                          {0.0000000000000000,
                           -65.799999999999997,
                           0.0000000000000000,
                           0.20000000000000001},
                          {-0.10000000000000001,
                           -43.899999999999999,
                           0.0000000000000000,
                           0.29999999999999999},
                          {0.0000000000000000,
                           -38.899999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.29999999999999999,
                           63.899999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           41.200000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -36.100000000000001,
                           0.0000000000000000,
                           0.20000000000000001},
                          {-0.29999999999999999,
                           58.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           36.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -39.700000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -57.700000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           33.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {36.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           55.700000000000003,
                           0.0000000000000000,
                           -0.10000000000000001},
                          {0.10000000000000001,
                           -35.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -31.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           30.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.29999999999999999,
                           49.200000000000003,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           49.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           33.600000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -33.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -31.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           28.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -25.199999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -26.199999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           41.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           24.500000000000000,
                           0.0000000000000000,
                           0.10000000000000001},
                          {-16.199999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -22.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           23.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           37.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.20000000000000001,
                           -25.699999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           25.199999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -24.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           24.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -20.699999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -20.800000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           33.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {32.899999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -32.600000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           19.899999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.10000000000000001,
                           19.600000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -18.699999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -19.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.10000000000000001,
                           -28.600000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {4.0000000000000000,
                           178.80000000000001,
                           -11.800000000000001,
                           0.29999999999999999},
                          {39.799999999999997,
                           -107.30000000000000,
                           -5.5999999999999996,
                           -1.0000000000000000},
                          {9.9000000000000004,
                           164.00000000000000,
                           -4.0999999999999996,
                           0.10000000000000001},
                          {-4.7999999999999998,
                           -135.30000000000001,
                           -3.3999999999999999,
                           -0.10000000000000001},
                          {50.500000000000000,
                           75.000000000000000,
                           1.3999999999999999,
                           -1.2000000000000000},
                          {-1.1000000000000001,
                           -53.500000000000000,
                           1.3000000000000000,
                           0.0000000000000000},
                          {-45.000000000000000,
                           -2.3999999999999999,
                           -0.40000000000000002,
                           6.5999999999999996},
                          {-11.500000000000000,
                           -61.000000000000000,
                           -0.90000000000000002,
                           0.40000000000000002},
                          {4.4000000000000004,
                           -68.400000000000006,
                           -3.3999999999999999,
                           0.0000000000000000},
                          {7.7000000000000002,
                           -47.100000000000001,
                           -4.7000000000000002,
                           -1.0000000000000000},
                          {-42.899999999999999,
                           -12.600000000000000,
                           -1.2000000000000000,
                           4.2000000000000002},
                          {-42.799999999999997,
                           12.699999999999999,
                           -1.2000000000000000,
                           -4.2000000000000002},
                          {-7.5999999999999996,
                           -44.100000000000001,
                           2.1000000000000001,
                           -0.50000000000000000},
                          {-64.099999999999994,
                           1.7000000000000000,
                           0.20000000000000001,
                           4.5000000000000000},
                          {36.399999999999999,
                           -10.400000000000000,
                           1.0000000000000000,
                           3.5000000000000000},
                          {35.600000000000001,
                           10.199999999999999,
                           1.0000000000000000,
                           -3.5000000000000000},
                          {-1.7000000000000000,
                           39.500000000000000,
                           2.0000000000000000,
                           0.0000000000000000},
                          {50.899999999999999,
                           -8.1999999999999993,
                           -0.80000000000000004,
                           -5.0000000000000000},
                          {0.0000000000000000,
                           52.299999999999997,
                           1.2000000000000000,
                           0.0000000000000000},
                          {-42.899999999999999,
                           -17.800000000000001,
                           0.40000000000000002,
                           0.0000000000000000},
                          {2.6000000000000001,
                           34.299999999999997,
                           0.80000000000000004,
                           0.0000000000000000},
                          {-0.80000000000000004,
                           -48.600000000000001,
                           2.3999999999999999,
                           -0.10000000000000001},
                          {-4.9000000000000004,
                           30.500000000000000,
                           3.7000000000000002,
                           0.69999999999999996},
                          {0.0000000000000000,
                           -43.600000000000001,
                           2.1000000000000001,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -25.399999999999999,
                           1.2000000000000000,
                           0.0000000000000000},
                          {2.0000000000000000,
                           40.899999999999999,
                           -2.0000000000000000,
                           0.0000000000000000},
                          {-2.1000000000000001,
                           26.100000000000001,
                           0.59999999999999998,
                           0.0000000000000000},
                          {22.600000000000001,
                           -3.2000000000000002,
                           -0.50000000000000000,
                           -0.50000000000000000},
                          {-7.5999999999999996,
                           24.899999999999999,
                           -0.40000000000000002,
                           -0.20000000000000001},
                          {-6.2000000000000002,
                           34.899999999999999,
                           1.7000000000000000,
                           0.29999999999999999},
                          {2.0000000000000000,
                           17.399999999999999,
                           -0.40000000000000002,
                           0.10000000000000001},
                          {-3.8999999999999999,
                           20.500000000000000,
                           2.3999999999999999,
                           0.59999999999999998}};
    double eps[194][4] = {{9205365.8000000007,
                           -1506.2000000000000,
                           885.70000000000005,
                           -0.20000000000000001},
                          {573095.90000000002,
                           -570.20000000000005,
                           -305.00000000000000,
                           -0.29999999999999999},
                          {97845.500000000000,
                           147.80000000000001,
                           -48.799999999999997,
                           -0.20000000000000001},
                          {-89753.600000000006,
                           28.000000000000000,
                           46.899999999999999,
                           0.0000000000000000},
                          {7406.6999999999998,
                           -327.10000000000002,
                           -18.199999999999999,
                           0.80000000000000004},
                          {22442.299999999999,
                           -22.300000000000001,
                           -67.599999999999994,
                           0.0000000000000000},
                          {-683.60000000000002,
                           46.799999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {20070.700000000001,
                           36.000000000000000,
                           1.6000000000000001,
                           0.0000000000000000},
                          {12893.799999999999,
                           39.500000000000000,
                           -6.2000000000000002,
                           0.0000000000000000},
                          {-9593.2000000000007,
                           14.400000000000000,
                           30.199999999999999,
                           -0.10000000000000001},
                          {-6899.5000000000000,
                           4.7999999999999998,
                           -0.59999999999999998,
                           0.0000000000000000},
                          {-5332.5000000000000,
                           -0.10000000000000001,
                           2.7000000000000002,
                           0.0000000000000000},
                          {-125.20000000000000,
                           10.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-3323.4000000000001,
                           -0.90000000000000002,
                           -0.29999999999999999,
                           0.0000000000000000},
                          {3142.3000000000002,
                           8.9000000000000004,
                           0.29999999999999999,
                           0.0000000000000000},
                          {2552.5000000000000,
                           7.2999999999999998,
                           -1.2000000000000000,
                           0.0000000000000000},
                          {2634.4000000000001,
                           8.8000000000000007,
                           0.20000000000000001,
                           0.0000000000000000},
                          {-2424.4000000000001,
                           1.6000000000000001,
                           -0.40000000000000002,
                           0.0000000000000000},
                          {-123.30000000000000,
                           3.8999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {1642.4000000000001,
                           7.2999999999999998,
                           -0.80000000000000004,
                           0.0000000000000000},
                          {47.899999999999999,
                           3.2000000000000002,
                           0.0000000000000000,
                           0.0000000000000000},
                          {1321.2000000000000,
                           6.2000000000000002,
                           -0.59999999999999998,
                           0.0000000000000000},
                          {-1234.0999999999999,
                           -0.29999999999999999,
                           0.59999999999999998,
                           0.0000000000000000},
                          {-1076.5000000000000,
                           -0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-61.600000000000001,
                           1.8000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-55.399999999999999,
                           1.6000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {856.89999999999998,
                           -4.9000000000000004,
                           -2.1000000000000001,
                           0.0000000000000000},
                          {-800.70000000000005,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {685.10000000000002,
                           -0.59999999999999998,
                           -3.7999999999999998,
                           0.0000000000000000},
                          {-16.899999999999999,
                           -1.5000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {695.70000000000005,
                           1.8000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {642.20000000000005,
                           -2.6000000000000001,
                           -1.6000000000000001,
                           0.0000000000000000},
                          {13.300000000000001,
                           1.1000000000000001,
                           -0.10000000000000001,
                           0.0000000000000000},
                          {521.89999999999998,
                           1.6000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {325.80000000000001,
                           2.0000000000000000,
                           -0.10000000000000001,
                           0.0000000000000000},
                          {-325.10000000000002,
                           -0.50000000000000000,
                           0.90000000000000002,
                           0.0000000000000000},
                          {10.100000000000000,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {334.50000000000000,
                           1.6000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {307.10000000000002,
                           0.40000000000000002,
                           -0.90000000000000002,
                           0.0000000000000000},
                          {327.19999999999999,
                           0.50000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-304.60000000000002,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {304.00000000000000,
                           0.59999999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-276.80000000000001,
                           -0.50000000000000000,
                           0.10000000000000001,
                           0.0000000000000000},
                          {268.89999999999998,
                           1.3000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {271.80000000000001,
                           1.1000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {271.50000000000000,
                           -0.40000000000000002,
                           -0.80000000000000004,
                           0.0000000000000000},
                          {-5.2000000000000002,
                           0.50000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-220.50000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-20.100000000000001,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-191.00000000000000,
                           0.10000000000000001,
                           0.50000000000000000,
                           0.0000000000000000},
                          {-4.0999999999999996,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {130.59999999999999,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {3.0000000000000000,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {122.90000000000001,
                           0.80000000000000004,
                           0.0000000000000000,
                           0.0000000000000000},
                          {3.7000000000000002,
                           -0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {123.09999999999999,
                           0.40000000000000002,
                           -0.29999999999999999,
                           0.0000000000000000},
                          {-52.700000000000003,
                           15.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {120.70000000000000,
                           0.29999999999999999,
                           -0.29999999999999999,
                           0.0000000000000000},
                          {4.0000000000000000,
                           -0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {126.50000000000000,
                           0.50000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {112.70000000000000,
                           0.50000000000000000,
                           -0.29999999999999999,
                           0.0000000000000000},
                          {-106.09999999999999,
                           -0.29999999999999999,
                           0.29999999999999999,
                           0.0000000000000000},
                          {-112.90000000000001,
                           -0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {3.6000000000000001,
                           -0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {107.40000000000001,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-10.900000000000000,
                           0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.90000000000000002,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {85.400000000000006,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -88.799999999999997,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-71.000000000000000,
                           -0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-70.299999999999997,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {64.500000000000000,
                           0.40000000000000002,
                           0.0000000000000000,
                           0.0000000000000000},
                          {69.799999999999997,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {66.099999999999994,
                           0.40000000000000002,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-61.000000000000000,
                           -0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-59.500000000000000,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-55.600000000000001,
                           0.0000000000000000,
                           0.20000000000000001,
                           0.0000000000000000},
                          {51.700000000000003,
                           0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-49.000000000000000,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-52.700000000000003,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-49.600000000000001,
                           1.3999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {46.299999999999997,
                           0.40000000000000002,
                           0.0000000000000000,
                           0.0000000000000000},
                          {49.600000000000001,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-5.0999999999999996,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-44.000000000000000,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-39.899999999999999,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-39.500000000000000,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-3.8999999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-42.100000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-17.199999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-2.2999999999999998,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-39.200000000000003,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-38.399999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {36.799999999999997,
                           0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {34.600000000000001,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-32.700000000000003,
                           0.29999999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {30.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.40000000000000002,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {29.300000000000001,
                           0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {31.600000000000001,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.80000000000000004,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-27.899999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {2.8999999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-25.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {25.000000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {27.500000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-24.399999999999999,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {24.899999999999999,
                           0.20000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-22.800000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.90000000000000002,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {24.399999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {23.899999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {22.500000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {20.800000000000001,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {20.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {21.500000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-20.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {1.3999999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.20000000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {19.000000000000000,
                           0.0000000000000000,
                           -0.10000000000000001,
                           0.0000000000000000},
                          {20.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-2.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-17.600000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {19.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-2.3999999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-18.399999999999999,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {17.100000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.40000000000000002,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {18.399999999999999,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           17.399999999999999,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.59999999999999998,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-15.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-16.800000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {16.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-2.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.5000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-14.300000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {14.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-13.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-14.300000000000001,
                           -0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-13.699999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {13.100000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.7000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-12.800000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           -14.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {12.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-12.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-0.80000000000000004,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {10.900000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-10.800000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {10.500000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-10.400000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-11.199999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {10.500000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-1.3999999999999999,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.0000000000000000,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.69999999999999996,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-10.300000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-10.000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {9.5999999999999996,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {9.4000000000000004,
                           0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.59999999999999998,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-87.700000000000003,
                           4.4000000000000004,
                           -0.40000000000000002,
                           -6.2999999999999998},
                          {46.299999999999997,
                           22.399999999999999,
                           0.50000000000000000,
                           -2.3999999999999999},
                          {15.600000000000000,
                           -3.3999999999999999,
                           0.10000000000000001,
                           0.40000000000000002},
                          {5.2000000000000002,
                           5.7999999999999998,
                           0.20000000000000001,
                           -0.10000000000000001},
                          {-30.100000000000001,
                           26.899999999999999,
                           0.69999999999999996,
                           0.0000000000000000},
                          {23.199999999999999,
                           -0.50000000000000000,
                           0.0000000000000000,
                           0.59999999999999998},
                          {1.0000000000000000,
                           23.199999999999999,
                           3.3999999999999999,
                           0.0000000000000000},
                          {-12.199999999999999,
                           -4.2999999999999998,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-2.1000000000000001,
                           -3.7000000000000002,
                           -0.20000000000000001,
                           0.10000000000000001},
                          {-18.600000000000001,
                           -3.7999999999999998,
                           -0.40000000000000002,
                           1.8000000000000000},
                          {5.5000000000000000,
                           -18.699999999999999,
                           -1.8000000000000000,
                           -0.50000000000000000},
                          {-5.5000000000000000,
                           -18.699999999999999,
                           1.8000000000000000,
                           -0.50000000000000000},
                          {18.399999999999999,
                           -3.6000000000000001,
                           0.29999999999999999,
                           0.90000000000000002},
                          {-0.59999999999999998,
                           1.3000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-5.5999999999999996,
                           -19.500000000000000,
                           1.8999999999999999,
                           0.0000000000000000},
                          {5.5000000000000000,
                           -19.100000000000001,
                           -1.8999999999999999,
                           0.0000000000000000},
                          {-17.300000000000001,
                           -0.80000000000000004,
                           0.0000000000000000,
                           0.90000000000000002},
                          {-3.2000000000000002,
                           -8.3000000000000007,
                           -0.80000000000000004,
                           0.29999999999999999},
                          {-0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-5.4000000000000004,
                           7.7999999999999998,
                           -0.29999999999999999,
                           0.0000000000000000},
                          {-14.800000000000001,
                           1.3999999999999999,
                           0.0000000000000000,
                           0.29999999999999999},
                          {-3.7999999999999998,
                           0.40000000000000002,
                           0.0000000000000000,
                           -0.20000000000000001},
                          {12.600000000000000,
                           3.2000000000000002,
                           0.50000000000000000,
                           -1.5000000000000000},
                          {0.10000000000000001,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-13.600000000000000,
                           2.3999999999999999,
                           -0.10000000000000001,
                           0.0000000000000000},
                          {0.90000000000000002,
                           1.2000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {-11.900000000000000,
                           -0.50000000000000000,
                           0.0000000000000000,
                           0.29999999999999999},
                          {0.40000000000000002,
                           12.000000000000000,
                           0.29999999999999999,
                           -0.20000000000000001},
                          {8.3000000000000007,
                           6.0999999999999996,
                           -0.10000000000000001,
                           0.10000000000000001},
                          {0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000,
                           0.0000000000000000},
                          {0.40000000000000002,
                           -10.800000000000001,
                           0.29999999999999999,
                           0.0000000000000000},
                          {9.5999999999999996,
                           2.2000000000000002,
                           0.29999999999999999,
                           -1.2000000000000000}};

    /*  interval between fundamental epoch j2000.0 and given epoch (jc). */
    t = (date - DJM0) / DJC;

    /*  mean anomaly of the moon. */
    el = 134.96340251 * PAL__DD2R +
         fmod(t * (1717915923.2178 +
                   t * (31.8792 + t * (0.051635 + t * (-0.00024470)))),
              TURNAS) *
             PAL__DAS2R;

    /*  mean anomaly of the sun. */
    elp = 357.52910918 * PAL__DD2R +
          fmod(t * (129596581.0481 +
                    t * (-0.5532 + t * (0.000136 + t * (-0.00001149)))),
               TURNAS) *
              PAL__DAS2R;

    /*  mean argument of the latitude of the moon. */
    f = 93.27209062 * PAL__DD2R +
        fmod(t * (1739527262.8478 +
                  t * (-12.7512 + t * (-0.001037 + t * (0.00000417)))),
             TURNAS) *
            PAL__DAS2R;

    /*  mean elongation of the moon from the sun. */
    d = 297.85019547 * PAL__DD2R +
        fmod(t * (1602961601.2090 +
                  t * (-6.3706 + t * (0.006539 + t * (-0.00003169)))),
             TURNAS) *
            PAL__DAS2R;

    /*  mean longitude of the ascending node of the moon. */
    om = 125.04455501 * PAL__DD2R +
         fmod(t * (-6962890.5431 +
                   t * (7.4722 + t * (0.007702 + t * (-0.00005939)))),
              TURNAS) *
             PAL__DAS2R;

    /*  mean longitude of venus. */
    ve = 181.97980085 * PAL__DD2R +
         fmod(210664136.433548 * t, TURNAS) * PAL__DAS2R;

    /*  mean longitude of mars.*/
    ma = 355.43299958 * PAL__DD2R +
         fmod(68905077.493988 * t, TURNAS) * PAL__DAS2R;

    /*  mean longitude of jupiter. */
    ju = 34.35151874 * PAL__DD2R +
         fmod(10925660.377991 * t, TURNAS) * PAL__DAS2R;

    /*  mean longitude of saturn. */
    sa =
        50.07744430 * PAL__DD2R + fmod(4399609.855732 * t, TURNAS) * PAL__DAS2R;

    /*  geodesic nutation (fukushima 1991) in microarcsec. */
    dp = -153.1 * sin(elp) - 1.9 * sin(2 * elp);
    de = 0.0;

    /*  shirai & fukushima (2001) nutation series. */
    for (j = NTERMS - 1; j >= 0; j--) {
        theta = ((double)na[j][0]) * el + ((double)na[j][1]) * elp +
                ((double)na[j][2]) * f + ((double)na[j][3]) * d +
                ((double)na[j][4]) * om + ((double)na[j][5]) * ve +
                ((double)na[j][6]) * ma + ((double)na[j][7]) * ju +
                ((double)na[j][8]) * sa;
        c = cos(theta);
        s = sin(theta);
        dp += (psi[j][0] + psi[j][2] * t) * c + (psi[j][1] + psi[j][3] * t) * s;
        de += (eps[j][0] + eps[j][2] * t) * c + (eps[j][1] + eps[j][3] * t) * s;
    }

    /*  change of units, and addition of the precession correction.*/
    *dpsi = (dp * 1e-6 - 0.042888 - 0.29856 * t) * PAL__DAS2R;
    *deps = (de * 1e-6 - 0.005171 - 0.02408 * t) * PAL__DAS2R;

    /*  mean obliquity of date (simon et al. 1994). */
    *eps0 = (84381.412 +
             (-46.80927 +
              (-0.000152 +
               (0.0019989 + (-0.00000051 + (-0.000000025) * t) * t) * t) *
                  t) *
                 t) *
            PAL__DAS2R;
}
/*
*+
*  Name:
*     palAoppat

*  Purpose:
*     Recompute sidereal time to support apparent to observed place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAoppat( double date, double aoprms[14] );

*  Arguments:
*     date = double (Given)
*         UTC date/time (modified Julian Date, JD-2400000.5)
*         (see palAoppa description for comments on leap seconds)
*     aoprms = double[14] (Given & Returned)
*         Star-independent apparent-to-observed parameters. Updated
*         by this routine. Requires element 12 to be the longitude +
*         eqn of equinoxes + sidereal DUT and fills in element 13
*         with the local apparent sidereal time (in radians).

*  Description:
*     This routine recomputes the sidereal time in the apparent to
*     observed place star-independent parameter block.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - See palAoppa for more information.
*     - The star-independent parameters are not treated as an opaque
*       struct in order to retain compatibility with SLA.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version, ported from Fortran SLA source.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

static double pal__Gmst(double ut1);

void palAoppat(double date, double aoprms[14]) {
    aoprms[13] = pal__Gmst(date) + aoprms[12];
}

/* Use a private implementation of palGmst for testing that matches
   the SLA rather than SOFA implementation. This is used for comparing
   SLA with PAL refraction code. */

#include "math.h"
#include "palmac.h"

static double pal__Gmst(double ut1) {

    double tu;
    double gmst;

    /*  Julian centuries from fundamental epoch J2000 to this UT */
    tu = (ut1 - 51544.5) / 36525;

    /*  GMST at this UT */
    gmst = palDranrm(
        fmod(ut1, 1.0) * PAL__D2PI +
        (24110.54841 + (8640184.812866 + (0.093104 - 6.2e-6 * tu) * tu) * tu) *
            PAL__DS2R);
    return gmst;
}
/*
*+
*  Name:
*     palAopqk

*  Purpose:
*     Quick apparent to observed place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAopqk ( double rap, double dap, const double aoprms[14],
*                     double *aob, double *zob, double *hob,
*                     double *dob, double *rob );

*  Arguments:
*     rap = double (Given)
*        Geocentric apparent right ascension
*     dap = double (Given)
*        Geocentric apparent declination
*     aoprms = const double [14] (Given)
*        Star-independent apparent-to-observed parameters.
*
*         [0]      geodetic latitude (radians)
*         [1,2]    sine and cosine of geodetic latitude
*         [3]      magnitude of diurnal aberration vector
*         [4]      height (HM)
*         [5]      ambient temperature (T)
*         [6]      pressure (P)
*         [7]      relative humidity (RH)
*         [8]      wavelength (WL)
*         [9]      lapse rate (TLR)
*         [10,11]  refraction constants A and B (radians)
*         [12]     longitude + eqn of equinoxes + sidereal DUT (radians)
*         [13]     local apparent sidereal time (radians)
*     aob = double * (Returned)
*        Observed azimuth (radians: N=0,E=90)
*     zob = double * (Returned)
*        Observed zenith distance (radians)
*     hob = double * (Returned)
*        Observed Hour Angle (radians)
*     dob = double * (Returned)
*        Observed Declination (radians)
*     rob = double * (Returned)
*        Observed Right Ascension (radians)

*  Description:
*     Quick apparent to observed place.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - This routine returns zenith distance rather than elevation
*       in order to reflect the fact that no allowance is made for
*       depression of the horizon.
*
*     - The accuracy of the result is limited by the corrections for
*       refraction.  Providing the meteorological parameters are
*       known accurately and there are no gross local effects, the
*       observed RA,Dec predicted by this routine should be within
*       about 0.1 arcsec for a zenith distance of less than 70 degrees.
*       Even at a topocentric zenith distance of 90 degrees, the
*       accuracy in elevation should be better than 1 arcmin;  useful
*       results are available for a further 3 degrees, beyond which
*       the palRefro routine returns a fixed value of the refraction.
*       The complementary routines palAop (or palAopqk) and palOap
*       (or palOapqk) are self-consistent to better than 1 micro-
*       arcsecond all over the celestial sphere.
*
*     - It is advisable to take great care with units, as even
*       unlikely values of the input parameters are accepted and
*       processed in accordance with the models used.
*
*     - "Apparent" place means the geocentric apparent right ascension
*       and declination, which is obtained from a catalogue mean place
*       by allowing for space motion, parallax, precession, nutation,
*       annual aberration, and the Sun's gravitational lens effect.  For
*       star positions in the FK5 system (i.e. J2000), these effects can
*       be applied by means of the palMap etc routines.  Starting from
*       other mean place systems, additional transformations will be
*       needed;  for example, FK4 (i.e. B1950) mean places would first
*       have to be converted to FK5, which can be done with the
*       palFk425 etc routines.
*
*     - "Observed" Az,El means the position that would be seen by a
*       perfect theodolite located at the observer.  This is obtained
*       from the geocentric apparent RA,Dec by allowing for Earth
*       orientation and diurnal aberration, rotating from equator
*       to horizon coordinates, and then adjusting for refraction.
*       The HA,Dec is obtained by rotating back into equatorial
*       coordinates, using the geodetic latitude corrected for polar
*       motion, and is the position that would be seen by a perfect
*       equatorial located at the observer and with its polar axis
*       aligned to the Earth's axis of rotation (n.b. not to the
*       refracted pole).  Finally, the RA is obtained by subtracting
*       the HA from the local apparent ST.
*
*     - To predict the required setting of a real telescope, the
*       observed place produced by this routine would have to be
*       adjusted for the tilt of the azimuth or polar axis of the
*       mounting (with appropriate corrections for mount flexures),
*       for non-perpendicularity between the mounting axes, for the
*       position of the rotator axis and the pointing axis relative
*       to it, for tube flexure, for gear and encoder errors, and
*       finally for encoder zero points.  Some telescopes would, of
*       course, exhibit other properties which would need to be
*       accounted for at the appropriate point in the sequence.
*
*     - The star-independent apparent-to-observed-place parameters
*       in AOPRMS may be computed by means of the palAoppa routine.
*       If nothing has changed significantly except the time, the
*       palAoppat routine may be used to perform the requisite
*       partial recomputation of AOPRMS.
*
*     - At zenith distances beyond about 76 degrees, the need for
*       special care with the corrections for refraction causes a
*       marked increase in execution time.  Moreover, the effect
*       gets worse with increasing zenith distance.  Adroit
*       programming in the calling application may allow the
*       problem to be reduced.  Prepare an alternative AOPRMS array,
*       computed for zero air-pressure;  this will disable the
*       refraction corrections and cause rapid execution.  Using
*       this AOPRMS array, a preliminary call to the present routine
*       will, depending on the application, produce a rough position
*       which may be enough to establish whether the full, slow
*       calculation (using the real AOPRMS array) is worthwhile.
*       For example, there would be no need for the full calculation
*       if the preliminary call had already established that the
*       source was well below the elevation limits for a particular
*       telescope.
*
*     - The azimuths etc produced by the present routine are with
*       respect to the celestial pole.  Corrections to the terrestrial
*       pole can be computed using palPolmo.

*  History:
*     2012-08-25 (TIMJ):
*        Initial version, copied from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2003 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

#include <math.h>

void palAopqk(double rap,
              double dap,
              const double aoprms[14],
              double *aob,
              double *zob,
              double *hob,
              double *dob,
              double *rob) {

    /*  Breakpoint for fast/slow refraction algorithm:
     *  ZD greater than arctan(4), (see palRefco routine)
     *  or vector Z less than cosine(arctan(Z)) = 1/sqrt(17) */
    const double zbreak = 0.242535625;
    int i;

    double sphi, cphi, st, v[3], xhd, yhd, zhd, diurab, f, xhdt, yhdt, zhdt,
        xaet, yaet, zaet, azobs, zdt, refa, refb, zdobs, dzd, dref, ce, xaeo,
        yaeo, zaeo, hmobs, dcobs, raobs;

    /*  sin, cos of latitude */
    sphi = aoprms[1];
    cphi = aoprms[2];

    /*  local apparent sidereal time */
    st = aoprms[13];

    /*  apparent ra,dec to cartesian -ha,dec */
    palDcs2c(rap - st, dap, v);
    xhd = v[0];
    yhd = v[1];
    zhd = v[2];

    /*  diurnal aberration */
    diurab = aoprms[3];
    f = (1.0 - diurab * yhd);
    xhdt = f * xhd;
    yhdt = f * (yhd + diurab);
    zhdt = f * zhd;

    /*  cartesian -ha,dec to cartesian az,el (s=0,e=90) */
    xaet = sphi * xhdt - cphi * zhdt;
    yaet = yhdt;
    zaet = cphi * xhdt + sphi * zhdt;

    /*  azimuth (n=0,e=90) */
    if (xaet == 0.0 && yaet == 0.0) {
        azobs = 0.0;
    } else {
        azobs = atan2(yaet, -xaet);
    }

    /*  topocentric zenith distance */
    zdt = atan2(sqrt(xaet * xaet + yaet * yaet), zaet);

    /*
     *  refraction
     *  ---------- */

    /*  fast algorithm using two constant model */
    refa = aoprms[10];
    refb = aoprms[11];
    palRefz(zdt, refa, refb, &zdobs);

    /*  large zenith distance? */
    if (cos(zdobs) < zbreak) {

        /*     yes: use rigorous algorithm */

        /*     initialize loop (maximum of 10 iterations) */
        i = 1;
        dzd = 1.0e1;
        while (abs(dzd) > 1e-10 && i <= 10) {

            /*        compute refraction using current estimate of observed zd
             */
            palRefro(zdobs,
                     aoprms[4],
                     aoprms[5],
                     aoprms[6],
                     aoprms[7],
                     aoprms[8],
                     aoprms[0],
                     aoprms[9],
                     1e-8,
                     &dref);

            /*        remaining discrepancy */
            dzd = zdobs + dref - zdt;

            /*        update the estimate */
            zdobs = zdobs - dzd;

            /*        increment the iteration counter */
            i++;
        }
    }

    /*  to cartesian az/zd */
    ce = sin(zdobs);
    xaeo = -cos(azobs) * ce;
    yaeo = sin(azobs) * ce;
    zaeo = cos(zdobs);

    /*  cartesian az/zd to cartesian -ha,dec */
    v[0] = sphi * xaeo + cphi * zaeo;
    v[1] = yaeo;
    v[2] = -cphi * xaeo + sphi * zaeo;

    /*  to spherical -ha,dec */
    palDcc2s(v, &hmobs, &dcobs);

    /*  right ascension */
    raobs = palDranrm(st + hmobs);

    /*  return the results */
    *aob = azobs;
    *zob = zdobs;
    *hob = -hmobs;
    *dob = dcobs;
    *rob = raobs;
}
/*
*+
*  Name:
*     palAtmdsp

*  Purpose:
*     Apply atmospheric-dispersion adjustments to refraction coefficients

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palAtmdsp( double tdk, double pmb, double rh, double wl1,
*                     double a1, double b1, double wl2, double *a2, double *b2
);


*  Arguments:
*     tdk = double (Given)
*        Ambient temperature, K
*     pmb = double (Given)
*        Ambient pressure, millibars
*     rh = double (Given)
*        Ambient relative humidity, 0-1
*     wl1 = double (Given)
*        Reference wavelength, micrometre (0.4 recommended)
*     a1 = double (Given)
*        Refraction coefficient A for wavelength wl1 (radians)
*     b1 = double (Given)
*        Refraction coefficient B for wavelength wl1 (radians)
*     wl2 = double (Given)
*        Wavelength for which adjusted A,B required
*     a2 = double * (Returned)
*        Refraction coefficient A for wavelength WL2 (radians)
*     b2 = double * (Returned)
*        Refraction coefficient B for wavelength WL2 (radians)

*  Description:
*     Apply atmospheric-dispersion adjustments to refraction coefficients.

*  Authors:
*     TIMJ: Tim Jenness
*     PTW: Patrick Wallace
*     {enter_new_authors_here}

*  Notes:
*     - To use this routine, first call palRefco specifying WL1 as the
*     wavelength.  This yields refraction coefficients A1,B1, correct
*     for that wavelength.  Subsequently, calls to palAtmdsp specifying
*     different wavelengths will produce new, slightly adjusted
*     refraction coefficients which apply to the specified wavelength.
*
*     - Most of the atmospheric dispersion happens between 0.7 micrometre
*     and the UV atmospheric cutoff, and the effect increases strongly
*     towards the UV end.  For this reason a blue reference wavelength
*     is recommended, for example 0.4 micrometres.
*
*     - The accuracy, for this set of conditions:
*
*        height above sea level    2000 m
*                      latitude    29 deg
*                      pressure    793 mb
*                   temperature    17 degC
*                      humidity    50%
*                    lapse rate    0.0065 degC/m
*          reference wavelength    0.4 micrometre
*                star elevation    15 deg
*
*     is about 2.5 mas RMS between 0.3 and 1.0 micrometres, and stays
*     within 4 mas for the whole range longward of 0.3 micrometres
*     (compared with a total dispersion from 0.3 to 20.0 micrometres
*     of about 11 arcsec).  These errors are typical for ordinary
*     conditions and the given elevation;  in extreme conditions values
*     a few times this size may occur, while at higher elevations the
*     errors become much smaller.
*
*     - If either wavelength exceeds 100 micrometres, the radio case
*     is assumed and the returned refraction coefficients are the
*     same as the given ones.  Note that radio refraction coefficients
*     cannot be turned into optical values using this routine, nor
*     vice versa.
*
*     - The algorithm consists of calculation of the refractivity of the
*     air at the observer for the two wavelengths, using the methods
*     of the palRefro routine, and then scaling of the two refraction
*     coefficients according to classical refraction theory.  This
*     amounts to scaling the A coefficient in proportion to (n-1) and
*     the B coefficient almost in the same ratio (see R.M.Green,
*     "Spherical Astronomy", Cambridge University Press, 1985).

*  History:
*     2014-07-15 (TIMJ):
*        Initial version. A direct copy of the Fortran SLA implementation.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2014 Tim Jenness
*     Copyright (C) 2005 Patrick Wallace
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palAtmdsp(double tdk,
               double pmb,
               double rh,
               double wl1,
               double a1,
               double b1,
               double wl2,
               double *a2,
               double *b2) {

    double f, tdkok, pmbok, rhok;
    double psat, pwo, w1, wlok, wlsq, w2, dn1, dn2;

    /*  Check for radio wavelengths */
    if (wl1 > 100.0 || wl2 > 100.0) {

        /*     Radio: no dispersion */
        *a2 = a1;
        *b2 = b1;

    } else {

        /*     Optical: keep arguments within safe bounds */
        tdkok = DMIN(DMAX(tdk, 100.0), 500.0);
        pmbok = DMIN(DMAX(pmb, 0.0), 10000.0);
        rhok = DMIN(DMAX(rh, 0.0), 1.0);

        /*     Atmosphere parameters at the observer */
        psat = pow(10.0, -8.7115 + 0.03477 * tdkok);
        pwo = rhok * psat;
        w1 = 11.2684e-6 * pwo;

        /*     Refractivity at the observer for first wavelength */
        wlok = DMAX(wl1, 0.1);
        wlsq = wlok * wlok;
        w2 = 77.5317e-6 + (0.43909e-6 + 0.00367e-6 / wlsq) / wlsq;
        dn1 = (w2 * pmbok - w1) / tdkok;

        /*     Refractivity at the observer for second wavelength */
        wlok = DMAX(wl2, 0.1);
        wlsq = wlok * wlok;
        w2 = 77.5317e-6 + (0.43909e-6 + 0.00367e-6 / wlsq) / wlsq;
        dn2 = (w2 * pmbok - w1) / tdkok;

        /*     Scale the refraction coefficients (see Green 4.31, p93) */
        if (dn1 != 0.0) {
            f = dn2 / dn1;
            *a2 = a1 * f;
            *b2 = b1 * f;
            if (dn1 != a1) {
                *b2 *= (1.0 + dn1 * (dn1 - dn2) / (2.0 * (dn1 - a1)));
            }
        } else {
            *a2 = a1;
            *b2 = b1;
        }
    }
}
/*
*+
*  Name:
*     palCaldj

*  Purpose:
*     Gregorian Calendar to Modified Julian Date

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palCaldj ( int iy, int im, int id, double *djm, int *j );

*  Arguments:
*     iy = int (Given)
*        Year in the Gregorian calendar
*     im = int (Given)
*        Month in the Gergorian calendar
*     id = int (Given)
*        Day in the Gregorian calendar
*     djm = double * (Returned)
*        Modified Julian Date (JD-2400000.5) for 0 hrs
*     j = status (Returned)
*       0 = OK. See eraCal2jd for other values.

*  Description:
*     Modified Julian Date to Gregorian Calendar with special
*     behaviour for 2-digit years relating to 1950 to 2049.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-11 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Notes:
*     - Uses eraCal2jd
*     - Unlike eraCal2jd this routine treats the years 0-100 as
*       referring to the end of the 20th Century and beginning of
*       the 21st Century. If this behaviour is not acceptable
*       use the SOFA routine directly or palCldj.
*       Acceptable years are 00-49, interpreted as 2000-2049,
*                            50-99,     "       "  1950-1999,
*                            all others, interpreted literally.
*     - Unlike SLA this routine will work with negative years.


*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palCaldj(int iy, int im, int id, double *djm, int *j) {
    int adj = 0; /* Year adjustment */
    double djm0;

    if (iy >= 0 && iy <= 49) {
        adj = 2000;
    } else if (iy >= 50 && iy <= 99) {
        adj = 1900;
    }
    iy += adj;

    *j = eraCal2jd(iy, im, id, &djm0, djm);
}
/*
*+
*  Name:
*     palDafin

*  Purpose:
*     Sexagesimal character string to angle

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDafin ( const char *string, int *ipos, double *a, int *j );

*  Arguments:
*     string = const char * (Given)
*        String containing deg, arcmin, arcsec fields
*     ipos = int * (Given & Returned)
*        Position to start decoding "string". First character
*        is position 1 for compatibility with SLA. After
*        calling this routine "iptr" will be positioned after
*        the sexagesimal string.
*     a = double * (Returned)
*        Angle in radians.
*     j = int * (Returned)
*        status:  0 = OK
*                +1 = default, A unchanged
*                -1 = bad degrees      )
*                -2 = bad arcminutes   )  (note 3)
*                -3 = bad arcseconds   )

*  Description:
*     Extracts an angle from a sexagesimal string with degrees, arcmin,
*     arcsec fields using space or comma delimiters.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Example:
*     argument    before                           after
*
*     STRING      '-57 17 44.806  12 34 56.7'      unchanged
*     IPTR        1                                16 (points to 12...)
*     A           ?                                -1.00000D0
*     J           ?                                0

*  Notes:
*     - The first three "fields" in STRING are degrees, arcminutes,
*       arcseconds, separated by spaces or commas.  The degrees field
*       may be signed, but not the others.  The decoding is carried
*       out by the palDfltin routine and is free-format.
*     - Successive fields may be absent, defaulting to zero.  For
*       zero status, the only combinations allowed are degrees alone,
*       degrees and arcminutes, and all three fields present.  If all
*       three fields are omitted, a status of +1 is returned and A is
*       unchanged.  In all other cases A is changed.
*     - Range checking:
*
*           The degrees field is not range checked.  However, it is
*           expected to be integral unless the other two fields are absent.
*
*           The arcminutes field is expected to be 0-59, and integral if
*           the arcseconds field is present.  If the arcseconds field
*           is absent, the arcminutes is expected to be 0-59.9999...
*
*           The arcseconds field is expected to be 0-59.9999...
*
*     - Decoding continues even when a check has failed.  Under these
*       circumstances the field takes the supplied value, defaulting
*       to zero, and the result A is computed and returned.
*     - Further fields after the three expected ones are not treated
*       as an error.  The pointer IPOS is left in the correct state
*       for further decoding with the present routine or with palDfltin
*       etc. See the example, above.
*     - If STRING contains hours, minutes, seconds instead of degrees
*       etc, or if the required units are turns (or days) instead of
*       radians, the result A should be multiplied as follows:
*
*           for        to obtain    multiply
*           STRING     A in         A by
*
*           d ' "      radians      1       =  1.0
*           d ' "      turns        1/2pi   =  0.1591549430918953358
*           h m s      radians      15      =  15.0
*           h m s      days         15/2pi  =  2.3873241463784300365

*  History:
*     2012-03-08 (TIMJ):
*        Initial version from SLA/F using Fortran documentation
*        Adapted with permission from the Fortran SLALIB library.
*     2012-10-17 (TIMJ):
*        Fix range check on arcminute value.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

void palDafin(const char *string, int *ipos, double *a, int *j) {

    int jd = 0; /* Status for degree parsing */
    int jm = 0; /* Status for arcmin parsing */
    int js = 0; /* Status for arcsec parsing */
    int jf = 0; /* Internal copy of status */
    double deg = 0.0;
    double arcmin = 0.0;
    double arcsec = 0.0;

    /* Decode degrees, arcminutes, arcseconds */
    palDfltin(string, ipos, &deg, &jd);
    if (jd > 1) {
        jf = -1;
    } else {

        palDfltin(string, ipos, &arcmin, &jm);
        if (jm < 0 || jm > 1) {
            jf = -2;
        } else {

            palDfltin(string, ipos, &arcsec, &js);
            if (js < 0 || js > 1) {
                jf = -3;

            } else if (jd > 0) { /* See if combination of fields is credible */
                /* No degrees: arcmin, arcsec ought also to be absent */
                if (jm == 0) {
                    /* Suspect arcmin */
                    jf = -2;
                } else if (js == 0) {
                    /* Suspect arcsec */
                    jf = -3;
                } else {
                    /* All three fields absent */
                    jf = 1;
                }

            } else if (jm != 0 && js == 0) { /* Deg present: if arcsec present
                                                should have arcmin */
                jf = -3;

                /* Tests for range and integrality */
            } else if (jm == 0 && DINT(deg) != deg) { /* Degrees */
                jf = -1;

            } else if ((js == 0 && DINT(arcmin) != arcmin) ||
                       arcmin >= 60.0) { /* Arcmin */
                jf = -2;

            } else if (arcsec >= 60.0) { /* Arcsec */
                jf = -3;
            }
        }
    }

    /* Unless all three fields absent, compute angle value */
    if (jf <= 0) {
        *a = PAL__DAS2R * (60.0 * (60.0 * fabs(deg) + arcmin) + arcsec);
        if (jd < 0)
            *a *= -1.;
    }

    *j = jf;
}
/*
*+
*  Name:
*     palDtt

*  Purpose:
*     Return offset between UTC and TT

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     dat = palDat( double utc );

*  Arguments:
*     utc = double (Given)
*        UTC date as a modified JD (JD-2400000.5)

*  Returned Value:
*     dat = double
*        TAI-UTC in seconds

*  Description:
*     Increment to be applied to Coordinated Universal Time UTC to give
*     International Atomic Time (TAI).

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - This routine converts the MJD argument to calendar date before calling
*       the SOFA eraDat function.
*     - This routine matches the slaDat interface which differs from the eraDat
*       interface. Consider coding directly to the SOFA interface.
*     - See eraDat for a description of error conditions when calling this
function
*       with a time outside of the UTC range.
*     - The status argument from eraDat is ignored. This is reasonable since the
*       error codes are mainly related to incorrect calendar dates when
calculating
*       the JD internally.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library
*        although the core algorithm is now from SOFA.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

double palDat(double dju) {
    int iy;
    int im;
    int id;
    // int status;
    double fd;
    double deltat;

    eraJd2cal(PAL__MJD0, dju, &iy, &im, &id, &fd);

    // status = eraDat( iy, im, id, fd, &deltat );
    eraDat(iy, im, id, fd, &deltat);
    return deltat;
}
/*
*+
*  Name:
*     palDe2h

*  Purpose:
*     Equatorial to horizon coordinates: HA,Dec to Az,E

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDe2h( double ha, double dec, double phi, double * az, double * el );

*  Arguments:
*     ha = double * (Given)
*        Hour angle (radians)
*     dec = double * (Given)
*        Declination (radians)
*     phi = double (Given)
*        Observatory latitude (radians)
*     az = double * (Returned)
*        Azimuth (radians)
*     el = double * (Returned)
*        Elevation (radians)

*  Description:
*     Convert equatorial to horizon coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - All the arguments are angles in radians.
*     - Azimuth is returned in the range 0-2pi;  north is zero,
*       and east is +pi/2.  Elevation is returned in the range
*       +/-pi/2.
*     - The latitude must be geodetic.  In critical applications,
*       corrections for polar motion should be applied.
*     - In some applications it will be important to specify the
*       correct type of hour angle and declination in order to
*       produce the required type of azimuth and elevation.  In
*       particular, it may be important to distinguish between
*       elevation as affected by refraction, which would
*       require the "observed" HA,Dec, and the elevation
*       in vacuo, which would require the "topocentric" HA,Dec.
*       If the effects of diurnal aberration can be neglected, the
*       "apparent" HA,Dec may be used instead of the topocentric
*       HA,Dec.
*     - No range checking of arguments is carried out.
*     - In applications which involve many such calculations, rather
*       than calling the present routine it will be more efficient to
*       use inline code, having previously computed fixed terms such
*       as sine and cosine of latitude, and (for tracking a star)
*       sine and cosine of declination.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palDe2h(double ha, double dec, double phi, double *az, double *el) {

    double sh;
    double ch;
    double sd;
    double cd;
    double sp;
    double cp;

    double a;

    double x;
    double y;
    double z;
    double r;

    /*  Useful trig functions */
    sh = sin(ha);
    ch = cos(ha);
    sd = sin(dec);
    cd = cos(dec);
    sp = sin(phi);
    cp = cos(phi);

    /*  Az,El as x,y,z */
    x = -ch * cd * sp + sd * cp;
    y = -sh * cd;
    z = ch * cd * cp + sd * sp;

    /*  To spherical */
    r = sqrt(x * x + y * y);
    if (r == 0.) {
        a = 0.;
    } else {
        a = atan2(y, x);
    }
    if (a < 0.) {
        a += PAL__D2PI;
    }
    *az = a;
    *el = atan2(z, r);

    return;
}
/*
*+
*  Name:
*     palDeuler

*  Purpose:
*     Form a rotation matrix from the Euler angles

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDeuler ( const char *order, double phi, double theta, double psi,
*                      double rmat[3][3] );

*  Arguments:
*     order = const char[] (Given)
*        Specifies about which axes the rotation occurs
*     phi = double (Given)
*        1st rotation (radians)
*     theta = double (Given)
*        2nd rotation (radians)
*     psi = double (Given)
*        3rd rotation (radians)
*     rmat = double[3][3] (Given & Returned)
*        Rotation matrix

*  Description:
*     A rotation is positive when the reference frame rotates
*     anticlockwise as seen looking towards the origin from the
*     positive region of the specified axis.
*
*     The characters of ORDER define which axes the three successive
*     rotations are about.  A typical value is 'ZXZ', indicating that
*     RMAT is to become the direction cosine matrix corresponding to
*     rotations of the reference frame through PHI radians about the
*     old Z-axis, followed by THETA radians about the resulting X-axis,
*     then PSI radians about the resulting Z-axis.
*
*     The axis names can be any of the following, in any order or
*     combination:  X, Y, Z, uppercase or lowercase, 1, 2, 3.  Normal
*     axis labelling/numbering conventions apply;  the xyz (=123)
*     triad is right-handed.  Thus, the 'ZXZ' example given above
*     could be written 'zxz' or '313' (or even 'ZxZ' or '3xZ').  ORDER
*     is terminated by length or by the first unrecognized character.
*
*     Fewer than three rotations are acceptable, in which case the later
*     angle arguments are ignored.  If all rotations are zero, the
*     identity matrix is produced.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1997 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palDeuler(const char *order,
               double phi,
               double theta,
               double psi,
               double rmat[3][3]) {
    int i = 0;
    double rotations[3];

    /* Initialise rmat */
    eraIr(rmat);

    /* copy the rotations into an array */
    rotations[0] = phi;
    rotations[1] = theta;
    rotations[2] = psi;

    /* maximum three rotations */
    while (i < 3 && order[i] != '\0') {

        switch (order[i]) {
        case 'X':
        case 'x':
        case '1':
            eraRx(rotations[i], rmat);
            break;

        case 'Y':
        case 'y':
        case '2':
            eraRy(rotations[i], rmat);
            break;

        case 'Z':
        case 'z':
        case '3':
            eraRz(rotations[i], rmat);
            break;

        default:
            /* break out the loop if we do not recognize something */
            i = 3;
        }

        /* Go to the next position */
        i++;
    }

    return;
}
/*
*+
*  Name:
*     palDfltin

*  Purpose:
*     Convert free-format input into double precision floating point

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDfltin( const char * string, int *nstrt,
*                     double *dreslt, int *jflag );

*  Arguments:
*     string = const char * (Given)
*        String containing number to be decoded.
*     nstrt = int * (Given and Returned)
*        Character number indicating where decoding should start.
*        On output its value is updated to be the location of the
*        possible next value. For compatibility with SLA the first
*        character is index 1.
*     dreslt = double * (Returned)
*        Result. Not updated when jflag=1.
*     jflag = int * (Returned)
*        status: -1 = -OK, 0 = +OK, 1 = null, 2 = error

*  Description:
*     Extracts a number from an input string starting at the specified
*     index.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Uses the strtod() system call to do the parsing. This may lead to
*       subtle differences when compared to the SLA/F parsing.
*     - All "D" characters are converted to "E" to handle fortran exponents.
*     - Commas are recognized as a special case and are skipped if one happens
*       to be the next character when updating nstrt. Additionally the output
*       nstrt position will skip past any trailing space.
*     - If no number can be found flag will be set to 1.
*     - If the number overflows or underflows jflag will be set to 2. For
overflow
*       the returned result will have the value HUGE_VAL, for underflow it
*       will have the value 0.0.
*     - For compatiblity with SLA/F -0 will be returned as "0" with jflag == -1.
*     - Unlike slaDfltin a standalone "E" will return status 1 (could not find
*       a number) rather than 2 (bad number).

*  Implementation Status:
*     - The code is more robust if the C99 copysign() function is available.
*     This can recognize the -0.0 values returned by strtod. If copysign() is
*     missing we try to scan the string looking for minus signs.

*  History:
*     2012-03-08 (TIMJ):
*        Initial version based on strtod
*        Adapted with permission from the Fortran SLALIB library
*        although this is a completely distinct implementation of the SLA API.
*     2012-06-21 (TIMJ):
*        Provide a backup for missing copysign.
*     2012-06-22 (TIMJ):
*        Check __STDC_VERSION__
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

/* Use the config file if we have one, else look at
   compiler defines to see if we have C99 */
#if HAVE_CONFIG_H
#include <config.h>
#else
#ifdef __STDC_VERSION__
#if (__STDC_VERSION__ >= 199901L)
#define HAVE_COPYSIGN 1
#endif
#endif
#endif

/* isblank() is a C99 feature so we just reimplement it if it is missing */
#if HAVE_ISBLANK
#define _POSIX_C_SOURCE 200112L
#define _ISOC99_SOURCE
#include <ctype.h>
#define ISBLANK isblank
#else

static int ISBLANK(int c) {
    return (c == ' ' || c == '\t');
}

#endif

/* System include files */
#include "pal.h"

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#if HAVE_COPYSIGN
#define SCAN_FOR_MINUS 0
#else
#define SCAN_FOR_MINUS 1
#endif

/* We prefer to use the starutil package */
#if HAVE_STAR_UTIL_H
#include "star/util.h"
#else
#endif

void palDfltin(const char *string, int *nstrt, double *dreslt, int *jflag) {

    char *ctemp = nullptr; /* Pointer into string */
    char *endptr = nullptr; /* Pointer to string after number */
    double retval; /* Return value from strtod */

    /* We have to copy the string in order to modify the exponents
       from Fortran style. Rather than using malloc we have a static
       buffer. Technically we only have to do the copy if we have a
       D or d in the string. */
    char tempbuf[256];

#if SCAN_FOR_MINUS
    int dreslt_sign = 1;
    int ipos = *nstrt;
    const char *cctemp = nullptr;

    /* Scan the string looking for a minus sign. Then update the
       start position for the subsequent copy iff we find a '-'.
       Note  that commas are a special delimiter so we stop looking for a
       minus if we find one or if we find a digit. */
    cctemp = &(string[ipos - 1]);
    while (!isdigit(*cctemp) && (*cctemp != ',') && (*cctemp != '\0')) {
        if (*cctemp == '-') {
            *nstrt = ipos;
            dreslt_sign = -1;
            break;
        }
        ipos++;
        cctemp++;
    }
#endif

    /* Correct for SLA use of fortran convention */
#if HAVE_STAR_UTIL_H
    star_strlcpy(tempbuf, &(string[*nstrt - 1]), sizeof(tempbuf));
#else
#if HAVE_STRLCPY
    strlcpy(tempbuf, &(string[*nstrt - 1]), sizeof(tempbuf));
#else
    /* Use standard C interface */
    strncpy(tempbuf, &(string[*nstrt - 1]), sizeof(tempbuf));
    tempbuf[sizeof(tempbuf) - 1] = '\0';
#endif
#endif

    /* Convert d or D to E */
    ctemp = tempbuf;
    while (*ctemp != '\0') {
        if (*ctemp == 'd' || *ctemp == 'D')
            *ctemp = 'E';
        ctemp++;
    }

    /* strtod man page indicates that we should reset errno before
       calling strtod */
    errno = 0;

    /* We know we are starting at the beginning of the string now */
    retval = strtod(tempbuf, &endptr);
    if (retval == 0.0 && endptr == tempbuf) {
        /* conversion did not find anything */
        *jflag = 1;

        /* but SLA compatibility requires that we step
           through to remove leading spaces. We also step
           through alphabetic characters since they can never
           be numbers standalone (no number starts with an 'E') */
        while (ISBLANK(*endptr) || isalpha(*endptr)) {
            endptr++;
        }

    } else if (errno == ERANGE) {
        *jflag = 2;
    } else {
#if SCAN_FOR_MINUS
        *jflag = (dreslt_sign < 0 ? -1 : 0);
#else
        if (retval < 0.0) {
            *jflag = -1;
        } else if (retval == 0.0) {
            /* Need to distinguish -0 from +0 */
            double test = copysign(1.0, retval);
            if (test < 0.0) {
                *jflag = -1;
            } else {
                *jflag = 0;
            }
        } else {
            *jflag = 0;
        }
#endif
    }

    /* Sort out the position for the next index */
    *nstrt += endptr - tempbuf;

    /* Skip a comma */
    if (*endptr == ',') {
        (*nstrt)++;
    } else {
        /* jump past any leading spaces for the next part of the string */
        ctemp = endptr;
        while (ISBLANK(*ctemp)) {
            (*nstrt)++;
            ctemp++;
        }
    }

    /* And the result unless we found nothing */
    if (*jflag != 1)
        *dreslt = retval;
}
/*
*+
*  Name:
*     palDh2e

*  Purpose:
*     Horizon to equatorial coordinates: Az,El to HA,Dec

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDh2e( double az, double el, double phi, double * ha, double * dec );

*  Arguments:
*     az = double (Given)
*        Azimuth (radians)
*     el = double (Given)
*        Elevation (radians)
*     phi = double (Given)
*        Observatory latitude (radians)
*     ha = double * (Returned)
*        Hour angle (radians)
*     dec = double * (Returned)
*        Declination (radians)

*  Description:
*     Convert horizon to equatorial coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - All the arguments are angles in radians.
*     - The sign convention for azimuth is north zero, east +pi/2.
*     - HA is returned in the range +/-pi.  Declination is returned
*       in the range +/-pi/2.
*     - The latitude is (in principle) geodetic.  In critical
*       applications, corrections for polar motion should be applied.
*     - In some applications it will be important to specify the
*       correct type of elevation in order to produce the required
*       type of HA,Dec.  In particular, it may be important to
*       distinguish between the elevation as affected by refraction,
*       which will yield the "observed" HA,Dec, and the elevation
*       in vacuo, which will yield the "topocentric" HA,Dec.  If the
*       effects of diurnal aberration can be neglected, the
*       topocentric HA,Dec may be used as an approximation to the
*       "apparent" HA,Dec.
*     - No range checking of arguments is done.
*     - In applications which involve many such calculations, rather
*       than calling the present routine it will be more efficient to
*       use inline code, having previously computed fixed terms such
*       as sine and cosine of latitude.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

#include <math.h>

void palDh2e(double az, double el, double phi, double *ha, double *dec) {

    double sa;
    double ca;
    double se;
    double ce;
    double sp;
    double cp;

    double x;
    double y;
    double z;
    double r;

    /*  Useful trig functions */
    sa = sin(az);
    ca = cos(az);
    se = sin(el);
    ce = cos(el);
    sp = sin(phi);
    cp = cos(phi);

    /*  HA,Dec as x,y,z */
    x = -ca * ce * sp + se * cp;
    y = -sa * ce;
    z = ca * ce * cp + se * sp;

    /*  To HA,Dec */
    r = sqrt(x * x + y * y);
    if (r == 0.) {
        *ha = 0.;
    } else {
        *ha = atan2(y, x);
    }
    *dec = atan2(z, r);

    return;
}
/*
*+
*  Name:
*     palDjcal

*  Purpose:
*     Modified Julian Date to Gregorian Calendar

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDjcal ( int ndp, double djm, int iymdf[4], int *j );

*  Arguments:
*     ndp = int (Given)
*        Number of decimal places of days in fraction.
*     djm = double (Given)
*        Modified Julian Date (JD-2400000.5)
*     iymdf[4] = int[] (Returned)
*       Year, month, day, fraction in Gregorian calendar.
*     j = status (Returned)
*       0 = OK. See eraJd2cal for other values.

*  Description:
*     Modified Julian Date to Gregorian Calendar, expressed
*     in a form convenient for formatting messages (namely
*     rounded to a specified precision, and with the fields
*     stored in a single array)

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-10 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Notes:
*     - Uses eraJd2cal

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

void palDjcal(int ndp, double djm, int iymdf[4], int *j) {
    double frac = 0.0;
    double nfd;

    *j =
        eraJd2cal(PAL__MJD0, djm, &(iymdf[0]), &(iymdf[1]), &(iymdf[2]), &frac);

    /* Convert ndp to a power of 10 */
    nfd = pow(10., (double)ndp);

    /* Multiply the fraction */
    frac *= nfd;

    /* and now we want to round to the nearest integer */
    iymdf[3] = (int)DNINT(frac);
}
/*
*+
*  Name:
*     palDmat

*  Purpose:
*     Matrix inversion & solution of simultaneous equations

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDmat( int n, double *a, double *y, double *d, int *jf,
*                    int *iw );

*  Arguments:
*     n = int (Given)
*        Number of simultaneous equations and number of unknowns.
*     a = double[] (Given & Returned)
*        A non-singular NxN matrix (implemented as a contiguous block
*        of memory). After calling this routine "a" contains the
*        inverse of the matrix.
*     y = double[] (Given & Returned)
*        On input the vector of N knowns. On exit this vector contains the
*        N solutions.
*     d = double * (Returned)
*        The determinant.
*     jf = int * (Returned)
*        The singularity flag.  If the matrix is non-singular, jf=0
*        is returned.  If the matrix is singular, jf=-1 & d=0.0 are
*        returned.  In the latter case, the contents of array "a" on
*        return are undefined.
*     iw = int[] (Given)
*        Integer workspace of size N.

*  Description:
*     Matrix inversion & solution of simultaneous equations
*     For the set of n simultaneous equations in n unknowns:
*          A.Y = X
*     this routine calculates the inverse of A, the determinant
*     of matrix A and the vector of N unknowns.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-11 (TIMJ):
*        Combination of a port of the Fortran and a comparison
*        with the obfuscated GPL C routine.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Notes:
*     - Implemented using Gaussian elimination with partial pivoting.
*     - Optimized for speed rather than accuracy with errors 1 to 4
*       times those of routines optimized for accuracy.

*  Copyright:
*     Copyright (C) 2001 Rutherford Appleton Laboratory.
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palDmat(int n, double *a, double *y, double *d, int *jf, int *iw) {

    const double SFA = 1e-20;

    int k;
    double *aoff;

    *jf = 0;
    *d = 1.0;
    for (k = 0, aoff = a; k < n; k++, aoff += n) {
        int imx;
        double *aoff2 = aoff;
        double amx = fabs(aoff[k]);
        imx = k;
        if (k != n) {
            int i;
            double *apos2;
            for (i = k + 1, apos2 = aoff + n; i < n; i++, apos2 += n) {
                double t = fabs(apos2[k]);
                if (t > amx) {
                    amx = t;
                    imx = i;
                    aoff2 = apos2;
                }
            }
        }
        if (amx < SFA) {
            *jf = -1;
        } else {
            if (imx != k) {
                double t;
                int j;
                for (j = 0; j < n; j++) {
                    t = aoff[j];
                    aoff[j] = aoff2[j];
                    aoff2[j] = t;
                }
                t = y[k];
                y[k] = y[imx];
                y[imx] = t;
                *d = -*d;
            }
            iw[k] = imx;
            *d *= aoff[k];
            if (fabs(*d) < SFA) {
                *jf = -1;
            } else {
                double yk;
                double *apos2;
                int i, j;
                aoff[k] = 1.0 / aoff[k];
                for (j = 0; j < n; j++) {
                    if (j != k) {
                        aoff[j] *= aoff[k];
                    }
                }
                yk = y[k] * aoff[k];
                y[k] = yk;
                for (i = 0, apos2 = a; i < n; i++, apos2 += n) {
                    if (i != k) {
                        for (j = 0; j < n; j++) {
                            if (j != k) {
                                apos2[j] -= apos2[k] * aoff[j];
                            }
                        }
                        y[i] -= apos2[k] * yk;
                    }
                }
                for (i = 0, apos2 = a; i < n; i++, apos2 += n) {
                    if (i != k) {
                        apos2[k] *= -aoff[k];
                    }
                }
            }
        }
    }
    if (*jf != 0) {
        *d = 0.0;
    } else {
        for (k = n; k-- > 0;) {
            int ki = iw[k];
            if (k != ki) {
                int i;
                double *apos = a;
                for (i = 0; i < n; i++, apos += n) {
                    double t = apos[k];
                    apos[k] = apos[ki];
                    apos[ki] = t;
                }
            }
        }
    }
}
/*
*+
*  Name:
*     palDmoon

*  Purpose:
*     Approximate geocentric position and velocity of the Moon

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palDmoon( double date, double pv[6] );

*  Arguments:
*     date = double (Given)
*        TDB as a Modified Julian Date (JD-2400000.5)
*     pv = double [6] (Returned)
*        Moon x,y,z,xdot,ydot,zdot, mean equator and
*        equinox of date (AU, AU/s)

*  Description:
*      Calculate the approximate geocentric position of the Moon
*      using a full implementation of the algorithm published by
*      Meeus (l'Astronomie, June 1984, p348).

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - Meeus quotes accuracies of 10 arcsec in longitude, 3 arcsec in
*       latitude and 0.2 arcsec in HP (equivalent to about 20 km in
*       distance).  Comparison with JPL DE200 over the interval
*       1960-2025 gives RMS errors of 3.7 arcsec and 83 mas/hour in
*       longitude, 2.3 arcsec and 48 mas/hour in latitude, 11 km
*       and 81 mm/s in distance.  The maximum errors over the same
*       interval are 18 arcsec and 0.50 arcsec/hour in longitude,
*       11 arcsec and 0.24 arcsec/hour in latitude, 40 km and 0.29 m/s
*       in distance.
*     - The original algorithm is expressed in terms of the obsolete
*       timescale Ephemeris Time.  Either TDB or TT can be used, but
*       not UT without incurring significant errors (30 arcsec at
*       the present time) due to the Moon's 0.5 arcsec/sec movement.
*     - The algorithm is based on pre IAU 1976 standards.  However,
*       the result has been moved onto the new (FK5) equinox, an
*       adjustment which is in any case much smaller than the
*       intrinsic accuracy of the procedure.
*     - Velocity is obtained by a complete analytical differentiation
*       of the Meeus model.

*  History:
*     2012-03-07 (TIMJ):
*        Initial version based on a direct port of the SLA/F code.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1998 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

/* Autoconf can give us -DPIC */
#undef PIC

void palDmoon(double date, double pv[6]) {

    /*  Seconds per Julian century (86400*36525) */
    const double CJ = 3155760000.0;

    /*  Julian epoch of B1950 */
    const double B1950 = 1949.9997904423;

    /*  Earth equatorial radius in AU ( = 6378.137 / 149597870 ) */
    const double ERADAU = 4.2635212653763e-5;

    double T, THETA, SINOM, COSOM, DOMCOM, WA, DWA, WB, DWB, WOM, DWOM, SINWOM,
        COSWOM, V, DV, COEFF, EMN, EMPN, DN, FN, EN, DEN, DTHETA, FTHETA, EL,
        DEL, B, DB, BF, DBF, P, DP, SP, R, DR, X, Y, Z, XD, YD, ZD, SEL, CEL,
        SB, CB, RCB, RBD, W, EPJ, EQCOR, EPS, SINEPS, COSEPS, ES, EC;
    double ELP, DELP;
    double EM, DEM, EMP, DEMP, D, DD, F, DF, OM, DOM, E, DESQ, ESQ, DE;
    int N, I;

    /*
     *  Coefficients for fundamental arguments
     *
     *   at J1900:  T**0, T**1, T**2, T**3
     *   at epoch:  T**0, T**1
     *
     *  Units are degrees for position and Julian centuries for time
     *
     */

    /*  Moon's mean longitude */
    const double ELP0 = 270.434164;
    const double ELP1 = 481267.8831;
    const double ELP2 = -0.001133;
    const double ELP3 = 0.0000019;

    /*  Sun's mean anomaly */
    const double EM0 = 358.475833;
    const double EM1 = 35999.0498;
    const double EM2 = -0.000150;
    const double EM3 = -0.0000033;

    /*  Moon's mean anomaly */
    const double EMP0 = 296.104608;
    const double EMP1 = 477198.8491;
    const double EMP2 = 0.009192;
    const double EMP3 = 0.0000144;

    /*  Moon's mean elongation */
    const double D0 = 350.737486;
    const double D1 = 445267.1142;
    const double D2 = -0.001436;
    const double D3 = 0.0000019;

    /*  Mean distance of the Moon from its ascending node */
    const double F0 = 11.250889;
    const double F1 = 483202.0251;
    const double F2 = -0.003211;
    const double F3 = -0.0000003;

    /*  Longitude of the Moon's ascending node */
    const double OM0 = 259.183275;
    const double OM1 = -1934.1420;
    const double OM2 = 0.002078;
    const double OM3 = 0.0000022;

    /*  Coefficients for (dimensionless) E factor */
    const double E1 = -0.002495;
    const double E2 = -0.00000752;

    /*  Coefficients for periodic variations etc */
    const double PAC = 0.000233;
    const double PA0 = 51.2;
    const double PA1 = 20.2;
    const double PBC = -0.001778;
    const double PCC = 0.000817;
    const double PDC = 0.002011;
    const double PEC = 0.003964;
    const double PE0 = 346.560;
    const double PE1 = 132.870;
    const double PE2 = -0.0091731;
    const double PFC = 0.001964;
    const double PGC = 0.002541;
    const double PHC = 0.001964;
    const double PIC = -0.024691;
    const double PJC = -0.004328;
    const double PJ0 = 275.05;
    const double PJ1 = -2.30;
    const double CW1 = 0.0004664;
    const double CW2 = 0.0000754;

    /*
     *  Coefficients for Moon position
     *
     *   Tx(N)       = coefficient of L, B or P term (deg)
     *   ITx(N,1-5)  = coefficients of M, M', D, F, E**n in argument
     */
#define NL 50
#define NB 45
#define NP 31

    /*
     *  Longitude
     */
    const double TL[NL] = {
        6.28875,  1.274018, .658309,  .213616, -.185596, -.114336, .058793,
        .057212,  .05332,   .045874,  .041024, -.034718, -.030465, .015326,
        -.012528, -.01098,  .010674,  .010034, .008548,  -.00791,  -.006783,
        .005162,  .005,     .004049,  .003996, .003862,  .003665,  .002695,
        .002602,  .002396,  -.002349, .002249, -.002125, -.002079, .002059,
        -.001773, -.001595, .00122,   -.00111, 8.92e-4,  -8.11e-4, 7.61e-4,
        7.17e-4,  7.04e-4,  6.93e-4,  5.98e-4, 5.5e-4,   5.38e-4,  5.21e-4,
        4.86e-4};

    const int ITL[NL][5] = {
        /* M   M'  D   F   n */
        {+0, +1, +0, +0, 0}, {+0, -1, +2, +0, 0}, {+0, +0, +2, +0, 0},
        {+0, +2, +0, +0, 0}, {+1, +0, +0, +0, 1}, {+0, +0, +0, +2, 0},
        {+0, -2, +2, +0, 0}, {-1, -1, +2, +0, 1}, {+0, +1, +2, +0, 0},
        {-1, +0, +2, +0, 1}, {-1, +1, +0, +0, 1}, {+0, +0, +1, +0, 0},
        {+1, +1, +0, +0, 1}, {+0, +0, +2, -2, 0}, {+0, +1, +0, +2, 0},
        {+0, -1, +0, +2, 0}, {+0, -1, +4, +0, 0}, {+0, +3, +0, +0, 0},
        {+0, -2, +4, +0, 0}, {+1, -1, +2, +0, 1}, {+1, +0, +2, +0, 1},
        {+0, +1, -1, +0, 0}, {+1, +0, +1, +0, 1}, {-1, +1, +2, +0, 1},
        {+0, +2, +2, +0, 0}, {+0, +0, +4, +0, 0}, {+0, -3, +2, +0, 0},
        {-1, +2, +0, +0, 1}, {+0, +1, -2, -2, 0}, {-1, -2, +2, +0, 1},
        {+0, +1, +1, +0, 0}, {-2, +0, +2, +0, 2}, {+1, +2, +0, +0, 1},
        {+2, +0, +0, +0, 2}, {-2, -1, +2, +0, 2}, {+0, +1, +2, -2, 0},
        {+0, +0, +2, +2, 0}, {-1, -1, +4, +0, 1}, {+0, +2, +0, +2, 0},
        {+0, +1, -3, +0, 0}, {+1, +1, +2, +0, 1}, {-1, -2, +4, +0, 1},
        {-2, +1, +0, +0, 2}, {-2, +1, -2, +0, 2}, {+1, -2, +2, +0, 1},
        {-1, +0, +2, -2, 1}, {+0, +1, +4, +0, 0}, {+0, +4, +0, +0, 0},
        {-1, +0, +4, +0, 1}, {+0, +2, -1, +0, 0}};

    /*
     *  Latitude
     */
    const double TB[NB] = {
        5.128189, .280606,  .277693,  .173238, .055413,  .046272,  .032573,
        .017198,  .009267,  .008823,  .008247, .004323,  .0042,    .003372,
        .002472,  .002222,  .002072,  .001877, .001828,  -.001803, -.00175,
        .00157,   -.001487, -.001481, .001417, .00135,   .00133,   .001106,
        .00102,   8.33e-4,  7.81e-4,  6.7e-4,  6.06e-4,  5.97e-4,  4.92e-4,
        4.5e-4,   4.39e-4,  4.23e-4,  4.22e-4, -3.67e-4, -3.53e-4, 3.31e-4,
        3.17e-4,  3.06e-4,  -2.83e-4};

    const int ITB[NB][5] = {
        /* M   M'  D   F   n */
        {+0, +0, +0, +1, 0}, {+0, +1, +0, +1, 0}, {+0, +1, +0, -1, 0},
        {+0, +0, +2, -1, 0}, {+0, -1, +2, +1, 0}, {+0, -1, +2, -1, 0},
        {+0, +0, +2, +1, 0}, {+0, +2, +0, +1, 0}, {+0, +1, +2, -1, 0},
        {+0, +2, +0, -1, 0}, {-1, +0, +2, -1, 1}, {+0, -2, +2, -1, 0},
        {+0, +1, +2, +1, 0}, {-1, +0, -2, +1, 1}, {-1, -1, +2, +1, 1},
        {-1, +0, +2, +1, 1}, {-1, -1, +2, -1, 1}, {-1, +1, +0, +1, 1},
        {+0, -1, +4, -1, 0}, {+1, +0, +0, +1, 1}, {+0, +0, +0, +3, 0},
        {-1, +1, +0, -1, 1}, {+0, +0, +1, +1, 0}, {+1, +1, +0, +1, 1},
        {-1, -1, +0, +1, 1}, {-1, +0, +0, +1, 1}, {+0, +0, -1, +1, 0},
        {+0, +3, +0, +1, 0}, {+0, +0, +4, -1, 0}, {+0, -1, +4, +1, 0},
        {+0, +1, +0, -3, 0}, {+0, -2, +4, +1, 0}, {+0, +0, +2, -3, 0},
        {+0, +2, +2, -1, 0}, {-1, +1, +2, -1, 1}, {+0, +2, -2, -1, 0},
        {+0, +3, +0, -1, 0}, {+0, +2, +2, +1, 0}, {+0, -3, +2, -1, 0},
        {+1, -1, +2, +1, 1}, {+1, +0, +2, +1, 1}, {+0, +0, +4, +1, 0},
        {-1, +1, +2, +1, 1}, {-2, +0, +2, -1, 2}, {+0, +1, +0, +3, 0}};

    /*
     *  Parallax
     */
    const double TP[NP] = {
        .950724,  .051818, .009531,  .007843,  .002824,  8.57e-4, 5.33e-4,
        4.01e-4,  3.2e-4,  -2.71e-4, -2.64e-4, -1.98e-4, 1.73e-4, 1.67e-4,
        -1.11e-4, 1.03e-4, -8.4e-5,  -8.3e-5,  7.9e-5,   7.2e-5,  6.4e-5,
        -6.3e-5,  4.1e-5,  3.5e-5,   -3.3e-5,  -3e-5,    -2.9e-5, -2.9e-5,
        2.6e-5,   -2.3e-5, 1.9e-5};

    const int ITP[NP][5] = {
        /* M   M'  D   F   n */
        {+0, +0, +0, +0, 0}, {+0, +1, +0, +0, 0}, {+0, -1, +2, +0, 0},
        {+0, +0, +2, +0, 0}, {+0, +2, +0, +0, 0}, {+0, +1, +2, +0, 0},
        {-1, +0, +2, +0, 1}, {-1, -1, +2, +0, 1}, {-1, +1, +0, +0, 1},
        {+0, +0, +1, +0, 0}, {+1, +1, +0, +0, 1}, {+0, -1, +0, +2, 0},
        {+0, +3, +0, +0, 0}, {+0, -1, +4, +0, 0}, {+1, +0, +0, +0, 1},
        {+0, -2, +4, +0, 0}, {+0, +2, -2, +0, 0}, {+1, +0, +2, +0, 1},
        {+0, +2, +2, +0, 0}, {+0, +0, +4, +0, 0}, {-1, +1, +2, +0, 1},
        {+1, -1, +2, +0, 1}, {+1, +0, +1, +0, 1}, {-1, +2, +0, +0, 1},
        {+0, +3, -2, +0, 0}, {+0, +1, +1, +0, 0}, {+0, +0, -2, +2, 0},
        {+1, +2, +0, +0, 1}, {-2, +0, +2, +0, 2}, {+0, +1, -2, +2, 0},
        {-1, -1, +4, +0, 1}};

    /*  Centuries since J1900 */
    T = (date - 15019.5) / 36525.;

    /*
     *  Fundamental arguments (radians) and derivatives (radians per
     *  Julian century) for the current epoch
     */

    /*  Moon's mean longitude */
    ELP = PAL__DD2R * fmod(ELP0 + (ELP1 + (ELP2 + ELP3 * T) * T) * T, 360.);
    DELP = PAL__DD2R * (ELP1 + (2. * ELP2 + 3 * ELP3 * T) * T);

    /*  Sun's mean anomaly */
    EM = PAL__DD2R * fmod(EM0 + (EM1 + (EM2 + EM3 * T) * T) * T, 360.);
    DEM = PAL__DD2R * (EM1 + (2. * EM2 + 3 * EM3 * T) * T);

    /*  Moon's mean anomaly */
    EMP = PAL__DD2R * fmod(EMP0 + (EMP1 + (EMP2 + EMP3 * T) * T) * T, 360.);
    DEMP = PAL__DD2R * (EMP1 + (2. * EMP2 + 3 * EMP3 * T) * T);

    /*  Moon's mean elongation */
    D = PAL__DD2R * fmod(D0 + (D1 + (D2 + D3 * T) * T) * T, 360.);
    DD = PAL__DD2R * (D1 + (2. * D2 + 3. * D3 * T) * T);

    /*  Mean distance of the Moon from its ascending node */
    F = PAL__DD2R * fmod(F0 + (F1 + (F2 + F3 * T) * T) * T, 360.);
    DF = PAL__DD2R * (F1 + (2. * F2 + 3. * F3 * T) * T);

    /*  Longitude of the Moon's ascending node */
    OM = PAL__DD2R * fmod(OM0 + (OM1 + (OM2 + OM3 * T) * T) * T, 360.);
    DOM = PAL__DD2R * (OM1 + (2. * OM2 + 3. * OM3 * T) * T);
    SINOM = sin(OM);
    COSOM = cos(OM);
    DOMCOM = DOM * COSOM;

    /*  Add the periodic variations */
    THETA = PAL__DD2R * (PA0 + PA1 * T);
    WA = sin(THETA);
    DWA = PAL__DD2R * PA1 * cos(THETA);
    THETA = PAL__DD2R * (PE0 + (PE1 + PE2 * T) * T);
    WB = PEC * sin(THETA);
    DWB = PAL__DD2R * PEC * (PE1 + 2. * PE2 * T) * cos(THETA);
    ELP = ELP + PAL__DD2R * (PAC * WA + WB + PFC * SINOM);
    DELP = DELP + PAL__DD2R * (PAC * DWA + DWB + PFC * DOMCOM);
    EM = EM + PAL__DD2R * PBC * WA;
    DEM = DEM + PAL__DD2R * PBC * DWA;
    EMP = EMP + PAL__DD2R * (PCC * WA + WB + PGC * SINOM);
    DEMP = DEMP + PAL__DD2R * (PCC * DWA + DWB + PGC * DOMCOM);
    D = D + PAL__DD2R * (PDC * WA + WB + PHC * SINOM);
    DD = DD + PAL__DD2R * (PDC * DWA + DWB + PHC * DOMCOM);
    WOM = OM + PAL__DD2R * (PJ0 + PJ1 * T);
    DWOM = DOM + PAL__DD2R * PJ1;
    SINWOM = sin(WOM);
    COSWOM = cos(WOM);
    F = F + PAL__DD2R * (WB + PIC * SINOM + PJC * SINWOM);
    DF = DF + PAL__DD2R * (DWB + PIC * DOMCOM + PJC * DWOM * COSWOM);

    /*  E-factor, and square */
    E = 1. + (E1 + E2 * T) * T;
    DE = E1 + 2. * E2 * T;
    ESQ = E * E;
    DESQ = 2. * E * DE;

    /*
     *  Series expansions
     */

    /*  Longitude */
    V = 0.;
    DV = 0.;
    for (N = NL - 1; N >= 0; N--) { /* DO N=NL, 1, -1 */
        COEFF = TL[N];
        EMN = (double)(ITL[N][0]);
        EMPN = (double)(ITL[N][1]);
        DN = (double)(ITL[N][2]);
        FN = (double)(ITL[N][3]);
        I = ITL[N][4];
        if (I == 0) {
            EN = 1.;
            DEN = 0.;
        } else if (I == 1) {
            EN = E;
            DEN = DE;
        } else {
            EN = ESQ;
            DEN = DESQ;
        }
        THETA = EMN * EM + EMPN * EMP + DN * D + FN * F;
        DTHETA = EMN * DEM + EMPN * DEMP + DN * DD + FN * DF;
        FTHETA = sin(THETA);
        V = V + COEFF * FTHETA * EN;
        DV = DV + COEFF * (cos(THETA) * DTHETA * EN + FTHETA * DEN);
    }
    EL = ELP + PAL__DD2R * V;
    DEL = (DELP + PAL__DD2R * DV) / CJ;

    /*  Latitude */
    V = 0.;
    DV = 0.;
    for (N = NB - 1; N >= 0; N--) { /* DO N=NB,1,-1 */
        COEFF = TB[N];
        EMN = (double)(ITB[N][0]);
        EMPN = (double)(ITB[N][1]);
        DN = (double)(ITB[N][2]);
        FN = (double)(ITB[N][3]);
        I = ITB[N][4];
        if (I == 0) {
            EN = 1.;
            DEN = 0.;
        } else if (I == 1) {
            EN = E;
            DEN = DE;
        } else {
            EN = ESQ;
            DEN = DESQ;
        }
        THETA = EMN * EM + EMPN * EMP + DN * D + FN * F;
        DTHETA = EMN * DEM + EMPN * DEMP + DN * DD + FN * DF;
        FTHETA = sin(THETA);
        V = V + COEFF * FTHETA * EN;
        DV = DV + COEFF * (cos(THETA) * DTHETA * EN + FTHETA * DEN);
    }
    BF = 1. - CW1 * COSOM - CW2 * COSWOM;
    DBF = CW1 * DOM * SINOM + CW2 * DWOM * SINWOM;
    B = PAL__DD2R * V * BF;
    DB = PAL__DD2R * (DV * BF + V * DBF) / CJ;

    /*  Parallax */
    V = 0.;
    DV = 0.;
    for (N = NP - 1; N >= 0; N--) { /* DO N=NP,1,-1 */
        COEFF = TP[N];
        EMN = (double)(ITP[N][0]);
        EMPN = (double)(ITP[N][1]);
        DN = (double)(ITP[N][2]);
        FN = (double)(ITP[N][3]);
        I = ITP[N][4];
        if (I == 0) {
            EN = 1.;
            DEN = 0.;
        } else if (I == 1) {
            EN = E;
            DEN = DE;
        } else {
            EN = ESQ;
            DEN = DESQ;
        }
        THETA = EMN * EM + EMPN * EMP + DN * D + FN * F;
        DTHETA = EMN * DEM + EMPN * DEMP + DN * DD + FN * DF;
        FTHETA = cos(THETA);
        V = V + COEFF * FTHETA * EN;
        DV = DV + COEFF * (-sin(THETA) * DTHETA * EN + FTHETA * DEN);
    }
    P = PAL__DD2R * V;
    DP = PAL__DD2R * DV / CJ;

    /*
     *  Transformation into final form
     */

    /*  Parallax to distance (AU, AU/sec) */
    SP = sin(P);
    R = ERADAU / SP;
    DR = -R * DP * cos(P) / SP;

    /*  Longitude, latitude to x,y,z (AU) */
    SEL = sin(EL);
    CEL = cos(EL);
    SB = sin(B);
    CB = cos(B);
    RCB = R * CB;
    RBD = R * DB;
    W = RBD * SB - CB * DR;
    X = RCB * CEL;
    Y = RCB * SEL;
    Z = R * SB;
    XD = -Y * DEL - W * CEL;
    YD = X * DEL - W * SEL;
    ZD = RBD * CB + SB * DR;

    /*  Julian centuries since J2000 */
    T = (date - 51544.5) / 36525.;

    /*  Fricke equinox correction */
    EPJ = 2000. + T * 100.;
    EQCOR = PAL__DS2R * (0.035 + 0.00085 * (EPJ - B1950));

    /*  Mean obliquity (IAU 1976) */
    EPS = PAL__DAS2R *
          (84381.448 + (-46.8150 + (-0.00059 + 0.001813 * T) * T) * T);

    /*  To the equatorial system, mean of date, FK5 system */
    SINEPS = sin(EPS);
    COSEPS = cos(EPS);
    ES = EQCOR * SINEPS;
    EC = EQCOR * COSEPS;
    pv[0] = X - EC * Y + ES * Z;
    pv[1] = EQCOR * X + Y * COSEPS - Z * SINEPS;
    pv[2] = Y * SINEPS + Z * COSEPS;
    pv[3] = XD - EC * YD + ES * ZD;
    pv[4] = EQCOR * XD + YD * COSEPS - ZD * SINEPS;
    pv[5] = YD * SINEPS + ZD * COSEPS;
}
/*
*+
*  Name:
*     palDrange

*  Purpose:
*     Normalize angle into range +/- pi

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDrange( double angle )

*  Arguments:
*     angle = double (Given)
*        The angle in radians.

*  Description:
*     The result is "angle" expressed in the range +/- pi. If the
*     supplied value for "angle" is equal to +/- pi, it is returned
*     unchanged.

*  Authors:
*     DSB: David S Berry (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  History:
*     2012-05-09 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

double palDrange(double angle) {
    double result = fmod(angle, PAL__D2PI);
    if (result > PAL__DPI) {
        result -= PAL__D2PI;
    } else if (result < -PAL__DPI) {
        result += PAL__D2PI;
    }
    return result;
}

/*
*+
*  Name:
*     palDs2tp

*  Purpose:
*     Spherical to tangent plane projection

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDs2tp( double ra, double dec, double raz, double decz,
*               double *xi, double *eta, int *j );

*  Arguments:
*     ra = double (Given)
*        RA spherical coordinate of point to be projected (radians)
*     dec = double (Given)
*        Dec spherical coordinate of point to be projected (radians)
*     raz = double (Given)
*        RA spherical coordinate of tangent point (radians)
*     decz = double (Given)
*        Dec spherical coordinate of tangent point (radians)
*     xi = double * (Returned)
*        First rectangular coordinate on tangent plane (radians)
*     eta = double * (Returned)
*        Second rectangular coordinate on tangent plane (radians)
*     j = int * (Returned)
*        status: 0 = OK, star on tangent plane
*                1 = error, star too far from axis
*                2 = error, antistar on tangent plane
*                3 = error, antistar too far from axis

*  Description:
*     Projection of spherical coordinates onto tangent plane:
*     "gnomonic" projection - "standard coordinates"

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

#include <math.h>

void palDs2tp(double ra,
              double dec,
              double raz,
              double decz,
              double *xi,
              double *eta,
              int *j) {

    const double TINY = 1.0e-6;

    double cdec;
    double sdec;
    double radif;
    double cdecz;
    double denom;
    double sdecz;
    double cradif;
    double sradif;

    /*  Trig functions */
    sdecz = sin(decz);
    sdec = sin(dec);
    cdecz = cos(decz);
    cdec = cos(dec);
    radif = ra - raz;
    sradif = sin(radif);
    cradif = cos(radif);

    /*  Reciprocal of star vector length to tangent plane */
    denom = sdec * sdecz + cdec * cdecz * cradif;

    /*  Handle vectors too far from axis */
    if (denom > TINY) {
        *j = 0;
    } else if (denom >= 0.) {
        *j = 1;
        denom = TINY;
    } else if (denom > -TINY) {
        *j = 2;
        denom = -TINY;
    } else {
        *j = 3;
    }

    /*  Compute tangent plane coordinates (even in dubious cases) */
    *xi = cdec * sradif / denom;
    *eta = (sdec * cdecz - cdec * sdecz * cradif) / denom;

    return;
}
/*
*+
*  Name:
*     palDt

*  Purpose:
*     Estimate the offset between dynamical time and UT

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palDt( double epoch );

*  Arguments:
*     epoch = double (Given)
*        Julian epoch (e.g. 1850.0)

*  Returned Value:
*     palDt = double
*        Rough estimate of ET-UT (after 1984, TT-UT) at the
*        given epoch, in seconds.

*  Description:
*     Estimate the offset between dynamical time and Universal Time
*     for a given historical epoch.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - Depending on the epoch, one of three parabolic approximations
*       is used:
*
*         before 979    Stephenson & Morrison's 390 BC to AD 948 model
*         979 to 1708   Stephenson & Morrison's 948 to 1600 model
*         after 1708    McCarthy & Babcock's post-1650 model
*
*       The breakpoints are chosen to ensure continuity:  they occur
*       at places where the adjacent models give the same answer as
*       each other.
*     - The accuracy is modest, with errors of up to 20 sec during
*       the interval since 1650, rising to perhaps 30 min by 1000 BC.
*       Comparatively accurate values from AD 1600 are tabulated in
*       the Astronomical Almanac (see section K8 of the 1995 AA).
*     - The use of double-precision for both argument and result is
*       purely for compatibility with other SLALIB time routines.
*     - The models used are based on a lunar tidal acceleration value
*       of -26.00 arcsec per century.
*
*  See Also:
*     Explanatory Supplement to the Astronomical Almanac,
*     ed P.K.Seidelmann, University Science Books (1992),
*     section 2.553, p83.  This contains references to
*     the Stephenson & Morrison and McCarthy & Babcock
*     papers.

*  History:
*     2012-03-08 (TIMJ):
*        Initial version with documentation from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

double palDt(double epoch) {

    double t, w, s;

    /* Centuries since 1800 */
    t = (epoch - 1800.0) / 100.0;

    /* Select model */
    if (epoch >= 1708.185161980887) {

        /* Post-1708: use McCarthy & Babcock */
        w = t - 0.19;
        s = 5.156 + 13.3066 * w * w;

    } else if (epoch >= 979.0258204760233) {

        /* 978-1708: use Stephenson & Morrison's 948-1600 model */
        s = 25.5 * t * t;

    } else {

        /* Pre-979: use Stephenson & Morrison's 390 BC to AD 948 model */
        s = 1360.0 + (320.0 + 44.3 * t) * t;
    }

    return s;
}
/*
*+
*  Name:
*     palDtp2s

*  Purpose:
*     Tangent plane to spherical coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDtp2s( double xi, double eta, double raz, double decz,
*               double *ra, double *dec);

*  Arguments:
*     xi = double (Given)
*        First rectangular coordinate on tangent plane (radians)
*     eta = double (Given)
*        Second rectangular coordinate on tangent plane (radians)
*     raz = double (Given)
*        RA spherical coordinate of tangent point (radians)
*     decz = double (Given)
*        Dec spherical coordinate of tangent point (radians)
*     ra = double * (Returned)
*        RA spherical coordinate of point to be projected (radians)
*     dec = double * (Returned)
*        Dec spherical coordinate of point to be projected (radians)

*  Description:
*     Transform tangent plane coordinates into spherical.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

#include <math.h>

void palDtp2s(
    double xi, double eta, double raz, double decz, double *ra, double *dec) {

    double cdecz;
    double denom;
    double sdecz;
    double d;

    sdecz = sin(decz);
    cdecz = cos(decz);
    denom = cdecz - eta * sdecz;
    d = atan2(xi, denom) + raz;
    *ra = eraAnp(d);
    *dec = atan2(sdecz + eta * cdecz, sqrt(xi * xi + denom * denom));

    return;
}
/*
*+
*  Name:
*     palDtps2c

*  Purpose:
*     Determine RA,Dec of tangent point from coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palDtps2c( double xi, double eta, double ra, double dec,
*                double * raz1, double decz1,
*                double * raz2, double decz2, int *n);

*  Arguments:
*     xi = double (Given)
*        First rectangular coordinate on tangent plane (radians)
*     eta = double (Given)
*        Second rectangular coordinate on tangent plane (radians)
*     ra = double (Given)
*        RA spherical coordinate of star (radians)
*     dec = double (Given)
*        Dec spherical coordinate of star (radians)
*     raz1 = double * (Returned)
*        RA spherical coordinate of tangent point, solution 1 (radians)
*     decz1 = double * (Returned)
*        Dec spherical coordinate of tangent point, solution 1 (radians)
*     raz2 = double * (Returned)
*        RA spherical coordinate of tangent point, solution 2 (radians)
*     decz2 = double * (Returned)
*        Dec spherical coordinate of tangent point, solution 2 (radians)
*     n = int * (Returned)
*        number of solutions: 0 = no solutions returned (note 2)
*                             1 = only the first solution is useful (note 3)
*                             2 = both solutions are useful (note 3)


*  Description:
*     From the tangent plane coordinates of a star of known RA,Dec,
*     determine the RA,Dec of the tangent point.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The RAZ1 and RAZ2 values are returned in the range 0-2pi.
*     - Cases where there is no solution can only arise near the poles.
*       For example, it is clearly impossible for a star at the pole
*       itself to have a non-zero XI value, and hence it is
*       meaningless to ask where the tangent point would have to be
*       to bring about this combination of XI and DEC.
*     - Also near the poles, cases can arise where there are two useful
*       solutions.  The argument N indicates whether the second of the
*       two solutions returned is useful.  N=1 indicates only one useful
*       solution, the usual case;  under these circumstances, the second
*       solution corresponds to the "over-the-pole" case, and this is
*       reflected in the values of RAZ2 and DECZ2 which are returned.
*     - The DECZ1 and DECZ2 values are returned in the range +/-pi, but
*       in the usual, non-pole-crossing, case, the range is +/-pi/2.
*     - This routine is the spherical equivalent of the routine sla_DTPV2C.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

#include <math.h>

void palDtps2c(double xi,
               double eta,
               double ra,
               double dec,
               double *raz1,
               double *decz1,
               double *raz2,
               double *decz2,
               int *n) {

    double x2;
    double y2;
    double sd;
    double cd;
    double sdf;
    double r2;

    x2 = xi * xi;
    y2 = eta * eta;
    sd = sin(dec);
    cd = cos(dec);
    sdf = sd * sqrt(x2 + 1. + y2);
    r2 = cd * cd * (y2 + 1.) - sd * sd * x2;
    if (r2 >= 0.) {
        double r;
        double s;
        double c;

        r = sqrt(r2);
        s = sdf - eta * r;
        c = sdf * eta + r;
        if (xi == 0. && r == 0.) {
            r = 1.;
        }
        *raz1 = eraAnp(ra - atan2(xi, r));
        *decz1 = atan2(s, c);
        r = -r;
        s = sdf - eta * r;
        c = sdf * eta + r;
        *raz2 = eraAnp(ra - atan2(xi, r));
        *decz2 = atan2(s, c);
        if (fabs(sdf) < 1.) {
            *n = 1;
        } else {
            *n = 2;
        }
    } else {
        *n = 0;
    }
    return;
}
/*
*+
*  Name:
*     palDtt

*  Purpose:
*     Return offset between UTC and TT

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     dtt = palDtt( double utc );

*  Arguments:
*     utc = double (Given)
*        UTC date as a modified JD (JD-2400000.5)

*  Returned Value:
*     dtt = double
*        TT-UTC in seconds

*  Description:
*     Increment to be applied to Coordinated Universal Time UTC to give
*     Terrestrial Time TT (formerly Ephemeris Time ET)

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     PTW: Patrick T. Wallace
*     {enter_new_authors_here}

*  Notes:
*     - Consider a comprehensive upgrade to use the time transformations in
SOFA's time
*       cookbook:  http://www.erasofa.org/sofa_ts_c.pdf.
*     - See eraDat for a description of error conditions when calling this
function
*       with a time outside of the UTC range. This behaviour differs from
slaDtt.

*  History:
*     2012-02-08 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

double palDtt(double utc) {
    return 32.184 + palDat(utc);
}
/*
*+
*  Name:
*     palEcmat

*  Purpose:
*     Form the equatorial to ecliptic rotation matrix - IAU 2006
*     precession model.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palEcmat( double date, double rmat[3][3] )

*  Arguments:
*     date = double (Given)
*        TT as Modified Julian Date (JD-2400000.5). The difference
*        between TT and TDB is of the order of a millisecond or two
*        (i.e. about 0.02 arc-seconds).
*     rmat = double[3][3] (Returned)
*        Rotation matrix

*  Description:
*     The equatorial to ecliptic rotation matrix is found and returned.
*     The matrix is in the sense   V(ecl)  =  RMAT * V(equ);  the
*     equator, equinox and ecliptic are mean of date.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-10 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palEcmat(double date, double rmat[3][3]) {

    /* Mean obliquity (the angle between the ecliptic and mean equator of
       date). */
    double eps0 = eraObl06(PAL__MJD0, date);

    /* Matrix */
    palDeuler("X", eps0, 0.0, 0.0, rmat);
}
/*
*+
*  Name:
*     palEl2ue

*  Purpose:
*     Transform conventional elements into "universal" form

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEl2ue ( double date, int jform, double epoch, double orbinc,
*                     double anode, double perih, double aorq, double e,
*                     double aorl, double dm, double u[13], int *jstat );

*  Arguments:
*     date = double (Given)
*        Epoch (TT MJD) of osculation (Note 3)
*     jform = int (Given)
*        Element set actually returned (1-3; Note 6)
*     epoch = double (Given)
*        Epoch of elements (TT MJD)
*     orbinc = double (Given)
*        inclination (radians)
*     anode = double (Given)
*        longitude of the ascending node (radians)
*     perih = double (Given)
*        longitude or argument of perihelion (radians)
*     aorq = double (Given)
*        mean distance or perihelion distance (AU)
*     e = double (Given)
*        eccentricity
*     aorl = double (Given)
*        mean anomaly or longitude (radians, JFORM=1,2 only)
*     dm = double (Given)
*        daily motion (radians, JFORM=1 only)
*     u = double [13] (Returned)
*        Universal orbital elements (Note 1)
*          -   (0)  combined mass (M+m)
*          -   (1)  total energy of the orbit (alpha)
*          -   (2)  reference (osculating) epoch (t0)
*          - (3-5)  position at reference epoch (r0)
*          - (6-8)  velocity at reference epoch (v0)
*          -   (9)  heliocentric distance at reference epoch
*          -  (10)  r0.v0
*          -  (11)  date (t)
*          -  (12)  universal eccentric anomaly (psi) of date, approx
*     jstat = int * (Returned)
*        status:  0 = OK
*              - -1 = illegal JFORM
*              - -2 = illegal E
*              - -3 = illegal AORQ
*              - -4 = illegal DM
*              - -5 = numerical error

*  Description:
*      Transform conventional osculating elements into "universal" form.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - The companion routine is palUe2pv.  This takes the set of numbers
*       that the present routine outputs and uses them to derive the
*       object's position and velocity.  A single prediction requires one
*       call to the present routine followed by one call to palUe2pv;
*       for convenience, the two calls are packaged as the routine
*       palPlanel.  Multiple predictions may be made by again calling the
*       present routine once, but then calling palUe2pv multiple times,
*       which is faster than multiple calls to palPlanel.
*     - DATE is the epoch of osculation.  It is in the TT timescale
*       (formerly Ephemeris Time, ET) and is a Modified Julian Date
*       (JD-2400000.5).
*     - The supplied orbital elements are with respect to the J2000
*       ecliptic and equinox.  The position and velocity parameters
*       returned in the array U are with respect to the mean equator and
*       equinox of epoch J2000, and are for the perihelion prior to the
*       specified epoch.
*     - The universal elements returned in the array U are in canonical
*       units (solar masses, AU and canonical days).
*     - Three different element-format options are available:
*
*       Option JFORM=1, suitable for the major planets:
*
*       EPOCH  = epoch of elements (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = longitude of perihelion, curly pi (radians)
*       AORQ   = mean distance, a (AU)
*       E      = eccentricity, e (range 0 to <1)
*       AORL   = mean longitude L (radians)
*       DM     = daily motion (radians)
*
*       Option JFORM=2, suitable for minor planets:
*
*       EPOCH  = epoch of elements (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = argument of perihelion, little omega (radians)
*       AORQ   = mean distance, a (AU)
*       E      = eccentricity, e (range 0 to <1)
*       AORL   = mean anomaly M (radians)
*
*       Option JFORM=3, suitable for comets:
*
*       EPOCH  = epoch of perihelion (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = argument of perihelion, little omega (radians)
*       AORQ   = perihelion distance, q (AU)
*       E      = eccentricity, e (range 0 to 10)
*
*     - Unused elements (DM for JFORM=2, AORL and DM for JFORM=3) are
*       not accessed.
*     - The algorithm was originally adapted from the EPHSLA program of
*       D.H.P.Jones (private communication, 1996).  The method is based
*       on Stumpff's Universal Variables.
*
*  See Also:
*     Everhart & Pitkin, Am.J.Phys. 51, 712 (1983).

*  History:
*     2012-03-12 (TIMJ):
*        Initial version taken directly from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palEl2ue(double date,
              int jform,
              double epoch,
              double orbinc,
              double anode,
              double perih,
              double aorq,
              double e,
              double aorl,
              double dm,
              double u[13],
              int *jstat) {

    /*  Sin and cos of J2000 mean obliquity (IAU 1976) */
    const double SE = 0.3977771559319137;
    const double CE = 0.9174820620691818;

    int J;

    double PHT, ARGPH, Q, W, CM, ALPHA, PHS, SW, CW, SI, CI, SO, CO, X, Y, Z,
        PX, PY, PZ, VX, VY, VZ, DT, FC, FP, PSI, UL[13], PV[6];

    /*  Validate arguments. */
    if (jform < 1 || jform > 3) {
        *jstat = -1;
        return;
    }
    if (e < 0.0 || e > 10.0 || (e >= 1.0 && jform != 3)) {
        *jstat = -2;
        return;
    }
    if (aorq <= 0.0) {
        *jstat = -3;
        return;
    }
    if (jform == 1 && dm <= 0.0) {
        *jstat = -4;
        return;
    }

    /*
     *  Transform elements into standard form:
     *
     *  PHT   = epoch of perihelion passage
     *  ARGPH = argument of perihelion (little omega)
     *  Q     = perihelion distance (q)
     *  CM    = combined mass, M+m (mu)
     */

    if (jform == 1) {

        /*     Major planet. */
        PHT = epoch - (aorl - perih) / dm;
        ARGPH = perih - anode;
        Q = aorq * (1.0 - e);
        W = dm / PAL__GCON;
        CM = W * W * aorq * aorq * aorq;

    } else if (jform == 2) {

        /*     Minor planet. */
        PHT = epoch - aorl * sqrt(aorq * aorq * aorq) / PAL__GCON;
        ARGPH = perih;
        Q = aorq * (1.0 - e);
        CM = 1.0;

    } else {

        /*     Comet. */
        PHT = epoch;
        ARGPH = perih;
        Q = aorq;
        CM = 1.0;
    }

    /*  The universal variable alpha.  This is proportional to the total
     *  energy of the orbit:  -ve for an ellipse, zero for a parabola,
     *  +ve for a hyperbola. */

    ALPHA = CM * (e - 1.0) / Q;

    /*  Speed at perihelion. */

    PHS = sqrt(ALPHA + 2.0 * CM / Q);

    /*  In a Cartesian coordinate system which has the x-axis pointing
     *  to perihelion and the z-axis normal to the orbit (such that the
     *  object orbits counter-clockwise as seen from +ve z), the
     *  perihelion position and velocity vectors are:
     *
     *    position   [Q,0,0]
     *    velocity   [0,PHS,0]
     *
     *  To express the results in J2000 equatorial coordinates we make a
     *  series of four rotations of the Cartesian axes:
     *
     *           axis      Euler angle
     *
     *     1      z        argument of perihelion (little omega)
     *     2      x        inclination (i)
     *     3      z        longitude of the ascending node (big omega)
     *     4      x        J2000 obliquity (epsilon)
     *
     *  In each case the rotation is clockwise as seen from the +ve end of
     *  the axis concerned.
     */

    /*  Functions of the Euler angles. */
    SW = sin(ARGPH);
    CW = cos(ARGPH);
    SI = sin(orbinc);
    CI = cos(orbinc);
    SO = sin(anode);
    CO = cos(anode);

    /*  Position at perihelion (AU). */
    X = Q * CW;
    Y = Q * SW;
    Z = Y * SI;
    Y = Y * CI;
    PX = X * CO - Y * SO;
    Y = X * SO + Y * CO;
    PY = Y * CE - Z * SE;
    PZ = Y * SE + Z * CE;

    /*  Velocity at perihelion (AU per canonical day). */
    X = -PHS * SW;
    Y = PHS * CW;
    Z = Y * SI;
    Y = Y * CI;
    VX = X * CO - Y * SO;
    Y = X * SO + Y * CO;
    VY = Y * CE - Z * SE;
    VZ = Y * SE + Z * CE;

    /*  Time from perihelion to date (in Canonical Days: a canonical day
     *  is 58.1324409... days, defined as 1/PAL__GCON). */

    DT = (date - PHT) * PAL__GCON;

    /*  First approximation to the Universal Eccentric Anomaly, PSI,
     *  based on the circle (FC) and parabola (FP) values. */

    FC = DT / Q;
    W = pow(3.0 * DT + sqrt(9.0 * DT * DT + 8.0 * Q * Q * Q), 1.0 / 3.0);
    FP = W - 2.0 * Q / W;
    PSI = (1.0 - e) * FC + e * FP;

    /*  Assemble local copy of element set. */
    UL[0] = CM;
    UL[1] = ALPHA;
    UL[2] = PHT;
    UL[3] = PX;
    UL[4] = PY;
    UL[5] = PZ;
    UL[6] = VX;
    UL[7] = VY;
    UL[8] = VZ;
    UL[9] = Q;
    UL[10] = 0.0;
    UL[11] = date;
    UL[12] = PSI;

    /*  Predict position+velocity at epoch of osculation. */
    palUe2pv(date, UL, PV, &J);
    if (J != 0) {
        *jstat = -5;
        return;
    }

    /*  Convert back to universal elements. */
    palPv2ue(PV, date, CM - 1.0, u, &J);
    if (J != 0) {
        *jstat = -5;
        return;
    }

    /*  OK exit. */
    *jstat = 0;
}
/*
*+
*  Name:
*     palEpco

*  Purpose:
*     Convert an epoch into the appropriate form - 'B' or 'J'

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palEpco( char k0, char k, double e );

*  Arguments:
*     k0 = char (Given)
*       Form of result: 'B'=Besselian, 'J'=Julian
*     k = char (Given)
*       Form of given epoch: 'B' or 'J'.

*  Description:
*     Converts a Besselian or Julian epoch to a Julian or Besselian
*     epoch.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The result is always either equal to or very close to
*       the given epoch E.  The routine is required only in
*       applications where punctilious treatment of heterogeneous
*       mixtures of star positions is necessary.
*     - k and k0 are case insensitive. This differes slightly from the
*       Fortran SLA implementation.
*     - k and k0 are not validated. They are interpreted as follows:
*       o If k0 and k are the same the result is e
*       o If k0 is 'b' or 'B' and k isn't the conversion is J to B.
*       o In all other cases, the conversion is B to J.

*  History:
*     2012-03-01 (TIMJ):
*        Initial version. Documentation from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
*    USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

#include <ctype.h>

double palEpco(char k0, char k, double e) {

    double new_epoch = 0.0;
    double djm;
    double djm0;

    /* Use upper case */
    k0 = toupper(k0);
    k = toupper(k);

    if (k == k0) {
        new_epoch = e;
    } else if (k0 == 'B') {
        eraEpj2jd(e, &djm0, &djm);
        new_epoch = eraEpb(djm0, djm);
    } else {
        eraEpb2jd(e, &djm0, &djm);
        new_epoch = eraEpj(djm0, djm);
    }
    return new_epoch;
}
/*
*+
*  Name:
*     palEpv

*  Purpose:
*     Earth position and velocity with respect to the BCRS

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEpv( double date, double ph[3], double vh[3],
*                  double pb[3], double vb[3] );

*  Arguments:
*     date = double (Given)
*        Date, TDB Modified Julian Date (JD-2400000.5)
*     ph = double [3] (Returned)
*        Heliocentric Earth position (AU)
*     vh = double [3] (Returned)
*        Heliocentric Earth velocity (AU/day)
*     pb = double [3] (Returned)
*        Barycentric Earth position (AU)
*     vb = double [3] (Returned)
*        Barycentric Earth velocity (AU/day)

*  Description:
*     Earth position and velocity, heliocentric and barycentric, with
*     respect to the Barycentric Celestial Reference System.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - See eraEpv00 for details on accuracy
*     - Note that the status argument from eraEpv00 is ignored

*  History:
*     2012-03-12 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library
*        but now mainly calls SOFA routines.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palEpv(
    double date, double ph[3], double vh[3], double pb[3], double vb[3]) {

    int i;
    double pvh[2][3];
    double pvb[2][3];

    eraEpv00(PAL__MJD0, date, pvh, pvb);

    /* Copy into output arrays */
    for (i = 0; i < 3; i++) {
        ph[i] = pvh[0][i];
        vh[i] = pvh[1][i];
        pb[i] = pvb[0][i];
        vb[i] = pvb[1][i];
    }
}
/*
*+
*  Name:
*     palEqecl

*  Purpose:
*     Transform from J2000.0 equatorial coordinates to ecliptic coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEqecl( double dr, double dd, double date,
*                    double *dl, double *db);

*  Arguments:
*     dr = double (Given)
*        J2000.0 mean RA (radians)
*     dd = double (Given)
*        J2000.0 mean Dec (Radians)
*     date = double (Given)
*        TT as Modified Julian Date (JD-2400000.5). The difference
*        between TT and TDB is of the order of a millisecond or two
*        (i.e. about 0.02 arc-seconds).
*     dl = double * (Returned)
*        Ecliptic longitude (mean of date, IAU 1980 theory, radians)
*     db = double * (Returned)
*        Ecliptic latitude (mean of date, IAU 1980 theory, radians)

*  Description:
*     Transform from J2000.0 equatorial coordinates to ecliptic coordinates.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palEqecl(double dr, double dd, double date, double *dl, double *db) {
    double v1[3], v2[3];
    double rmat[3][3];

    /* Spherical to Cartesian */
    eraS2c(dr, dd, v1);

    /* Mean J2000 to mean of date */
    palPrec(2000.0, palEpj(date), rmat);
    eraRxp(rmat, v1, v2);

    /* Equatorial to ecliptic */
    palEcmat(date, rmat);
    eraRxp(rmat, v2, v1);

    /* Cartesian to spherical */
    eraC2s(v1, dl, db);
}
/*
*+
*  Name:
*     palEqgal

*  Purpose:
*     Convert from J2000.0 equatorial coordinates to Galactic

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEqgal ( double dr, double dd, double *dl, double *db );

*  Arguments:
*     dr = double (Given)
*       J2000.0 RA (radians)
*     dd = double (Given)
*       J2000.0 Dec (radians
*     dl = double * (Returned)
*       Galactic longitude (radians).
*     db = double * (Returned)
*       Galactic latitude (radians).

*  Description:
*     Transformation from J2000.0 equatorial coordinates
*     to IAU 1958 galactic coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     The equatorial coordinates are J2000.0.  Use the routine
*     palGe50 if conversion to B1950.0 'FK4' coordinates is
*     required.

*  See Also:
*     Blaauw et al, Mon.Not.R.Astron.Soc.,121,123 (1960)

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1998 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palEqgal(double dr, double dd, double *dl, double *db) {

    double v1[3];
    double v2[3];

    /*
     *  L2,B2 system of galactic coordinates
     *
     *  P = 192.25       RA of galactic north pole (mean B1950.0)
     *  Q =  62.6        inclination of galactic to mean B1950.0 equator
     *  R =  33          longitude of ascending node
     *
     *  P,Q,R are degrees
     *
     *  Equatorial to galactic rotation matrix (J2000.0), obtained by
     *  applying the standard FK4 to FK5 transformation, for zero proper
     *  motion in FK5, to the columns of the B1950 equatorial to
     *  galactic rotation matrix:
     */
    double rmat[3][3] = {{-0.054875539726, -0.873437108010, -0.483834985808},
                         {+0.494109453312, -0.444829589425, +0.746982251810},
                         {-0.867666135858, -0.198076386122, +0.455983795705}};

    /* Spherical to Cartesian */
    eraS2c(dr, dd, v1);

    /* Equatorial to Galactic */
    eraRxp(rmat, v1, v2);

    /* Cartesian to spherical */
    eraC2s(v2, dl, db);

    /* Express in conventional ranges */
    *dl = eraAnp(*dl);
    *db = eraAnpm(*db);
}
/*
*+
*  Name:
*     palEtrms

*  Purpose:
*     Compute the E-terms vector

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEtrms ( double ep, double ev[3] );

*  Arguments:
*     ep = double (Given)
*        Besselian epoch
*     ev = double [3] (Returned)
*        E-terms as (dx,dy,dz)

*  Description:
*     Computes the E-terms (elliptic component of annual aberration)
*     vector.
*
*     Note the use of the J2000 aberration constant (20.49552 arcsec).
*     This is a reflection of the fact that the E-terms embodied in
*     existing star catalogues were computed from a variety of
*     aberration constants.  Rather than adopting one of the old
*     constants the latest value is used here.
*
*  See also:
*     - Smith, C.A. et al., 1989.  Astr.J. 97, 265.
*     - Yallop, B.D. et al., 1989.  Astr.J. 97, 274.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-12 (TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

void palEtrms(double ep, double ev[3]) {

    /* Use the J2000 aberration constant */
    const double ABCONST = 20.49552;

    double t, e, e0, p, ek, cp;

    /*  Julian centuries since B1950 */
    t = (ep - 1950.) * .0100002135903;

    /*  Eccentricity */
    e = .01673011 - (t * 1.26e-7 + 4.193e-5) * t;

    /*  Mean obliquity */
    e0 = (84404.836 - ((t * .00181 + .00319) * t + 46.8495) * t) * PAL__DAS2R;

    /*  Mean longitude of perihelion */
    p = (((t * .012 + 1.65) * t + 6190.67) * t + 1015489.951) * PAL__DAS2R;

    /*  E-terms */
    ek = e * ABCONST * PAL__DAS2R;
    cp = cos(p);
    ev[0] = ek * sin(p);
    ev[1] = -ek * cp * cos(e0);
    ev[2] = -ek * cp * sin(e0);
}
/*
*+
*  Name:
*     palEvp

*  Purpose:
*     Returns the barycentric and heliocentric velocity and position of the
*     Earth.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palEvp( double date, double deqx, double dvb[3], double dpb[3],
*                  double dvh[3], double dph[3] )

*  Arguments:
*     date = double (Given)
*        TDB (loosely ET) as a Modified Julian Date (JD-2400000.5)
*     deqx = double (Given)
*        Julian epoch (e.g. 2000.0) of mean equator and equinox of the
*        vectors returned.  If deqx <= 0.0, all vectors are referred to the
*        mean equator and equinox (FK5) of epoch date.
*     dvb = double[3] (Returned)
*        Barycentric velocity (AU/s, AU)
*     dpb = double[3] (Returned)
*        Barycentric position (AU/s, AU)
*     dvh = double[3] (Returned)
*        heliocentric velocity (AU/s, AU)
*     dph = double[3] (Returned)
*        Heliocentric position (AU/s, AU)

*  Description:
*     Returns the barycentric and heliocentric velocity and position of the
*     Earth at a given epoch, given with respect to a specified equinox.
*     For information about accuracy, see the function eraEpv00.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (PTW):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palEvp(double date,
            double deqx,
            double dvb[3],
            double dpb[3],
            double dvh[3],
            double dph[3]) {

    /* Local Variables; */
    int i;
    double pvh[2][3], pvb[2][3], d1, d2, r[3][3];

    /* BCRS PV-vectors. */
    eraEpv00(2400000.5, date, pvh, pvb);

    /* Was precession to another equinox requested? */
    if (deqx > 0.0) {

        /* Yes: compute precession matrix from J2000.0 to deqx. */
        eraEpj2jd(deqx, &d1, &d2);
        eraPmat06(d1, d2, r);

        /* Rotate the PV-vectors. */
        eraRxpv(r, pvh, pvh);
        eraRxpv(r, pvb, pvb);
    }

    /* Return the required vectors. */
    for (i = 0; i < 3; i++) {
        dvh[i] = pvh[1][i] / PAL__SPD;
        dvb[i] = pvb[1][i] / PAL__SPD;
        dph[i] = pvh[0][i];
        dpb[i] = pvb[0][i];
    }
}
/*
*+
*  Name:
*     palFk45z

*  Purpose:
*     Convert B1950.0 FK4 star data to J2000.0 FK5 assuming zero
*     proper motion in the FK5 frame

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palFk45z( double r1950, double d1950, double bepoch, double *r2000,
*               double *d2000 )

*  Arguments:
*     r1950 = double (Given)
*        B1950.0 FK4 RA at epoch (radians).
*     d1950 = double (Given)
*        B1950.0 FK4 Dec at epoch (radians).
*     bepoch = double (Given)
*        Besselian epoch (e.g. 1979.3)
*     r2000 = double (Returned)
*        J2000.0 FK5 RA (Radians).
*     d2000 = double (Returned)
*        J2000.0 FK5 Dec(Radians).

*  Description:
*     Convert B1950.0 FK4 star data to J2000.0 FK5 assuming zero
*     proper motion in the FK5 frame (double precision)
*
*     This function converts stars from the Bessel-Newcomb, FK4
*     system to the IAU 1976, FK5, Fricke system, in such a
*     way that the FK5 proper motion is zero.  Because such a star
*     has, in general, a non-zero proper motion in the FK4 system,
*     the routine requires the epoch at which the position in the
*     FK4 system was determined.
*
*     The method is from Appendix 2 of Ref 1, but using the constants
*     of Ref 4.

*  Notes:
*     - The epoch BEPOCH is strictly speaking Besselian, but if a
*     Julian epoch is supplied the result will be affected only to
*     a negligible extent.
*
*     - Conversion from Besselian epoch 1950.0 to Julian epoch 2000.0
*     only is provided for.  Conversions involving other epochs will
*     require use of the appropriate precession, proper motion, and
*     E-terms routines before and/or after palFk45z is called.
*
*     - In the FK4 catalogue the proper motions of stars within 10
*     degrees of the poles do not embody the differential E-term effect
*     and should, strictly speaking, be handled in a different manner
*     from stars outside these regions. However, given the general lack
*     of homogeneity of the star data available for routine astrometry,
*     the difficulties of handling positions that may have been
*     determined from astrometric fields spanning the polar and non-polar
*     regions, the likelihood that the differential E-terms effect was not
*     taken into account when allowing for proper motion in past
*     astrometry, and the undesirability of a discontinuity in the
*     algorithm, the decision has been made in this routine to include the
*     effect of differential E-terms on the proper motions for all stars,
*     whether polar or not.  At epoch 2000, and measuring on the sky rather
*     than in terms of dRA, the errors resulting from this simplification
*     are less than 1 milliarcsecond in position and 1 milliarcsecond per
*     century in proper motion.
*
*  References:
*     - Aoki,S., et al, 1983.  Astron.Astrophys., 128, 263.
*     - Smith, C.A. et al, 1989.  "The transformation of astrometric
*       catalog systems to the equinox J2000.0".  Astron.J. 97, 265.
*     - Yallop, B.D. et al, 1989.  "Transformation of mean star places
*       from FK4 B1950.0 to FK5 J2000.0 using matrices in 6-space".
*       Astron.J. 97, 274.
*     - Seidelmann, P.K. (ed), 1992.  "Explanatory Supplement to
*       the Astronomical Almanac", ISBN 0-935702-68-7.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-10 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1998 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palFk45z(
    double r1950, double d1950, double bepoch, double *r2000, double *d2000) {

    /* Local Variables: */
    double w;
    int i;
    int j;
    double r0[3], a1[3], v1[3],
        v2[6]; /* Position and position+velocity vectors */

    /* CANONICAL CONSTANTS  (see references) */

    /* Vector A. */
    double a[3] = {-1.62557E-6, -0.31919E-6, -0.13843E-6};

    /* Vectors Adot. */
    double ad[3] = {1.245E-3, -1.580E-3, -0.659E-3};

    /* Matrix M (only half of which is needed here). */
    double em[6][3] = {{0.9999256782, -0.0111820611, -0.0048579477},
                       {0.0111820610, 0.9999374784, -0.0000271765},
                       {0.0048579479, -0.0000271474, 0.9999881997},
                       {-0.000551, -0.238565, 0.435739},
                       {0.238514, -0.002667, -0.008541},
                       {-0.435623, 0.012254, 0.002117}};

    /* Spherical to Cartesian. */
    eraS2c(r1950, d1950, r0);

    /* Adjust vector A to give zero proper motion in FK5. */
    w = (bepoch - 1950.0) / PAL__PMF;
    for (i = 0; i < 3; i++) {
        a1[i] = a[i] + w * ad[i];
    }

    /* Remove e-terms. */
    w = r0[0] * a1[0] + r0[1] * a1[1] + r0[2] * a1[2];
    for (i = 0; i < 3; i++) {
        v1[i] = r0[i] - a1[i] + w * r0[i];
    }

    /* Convert position vector to Fricke system. */
    for (i = 0; i < 6; i++) {
        w = 0.0;
        for (j = 0; j < 3; j++) {
            w += em[i][j] * v1[j];
        }
        v2[i] = w;
    }

    /* Allow for fictitious proper motion in FK4. */
    w = (palEpj(palEpb2d(bepoch)) - 2000.0) / PAL__PMF;
    for (i = 0; i < 3; i++) {
        v2[i] += w * v2[i + 3];
    }

    /* Revert to spherical coordinates. */
    eraC2s(v2, &w, d2000);
    *r2000 = eraAnp(w);
}

/*
*+
*  Name:
*     palFk524

*  Purpose:
*     Convert J2000.0 FK5 star data to B1950.0 FK4.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palFk524( double r2000, double d2000, double dr2000, double dd2000,
*               double p2000, double v2000, double *r1950, double *d1950,
*               double *dr1950, double *dd1950, double *p1950, double *v1950 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 FK5 RA (radians).
*     d2000 = double (Given)
*        J2000.0 FK5 Dec (radians).
*     dr2000 = double (Given)
*        J2000.0 FK5 RA proper motion (rad/Jul.yr)
*     dd2000 = double (Given)
*        J2000.0 FK5 Dec proper motion (rad/Jul.yr)
*     p2000 = double (Given)
*        J2000.0 FK5 parallax (arcsec)
*     v2000 = double (Given)
*         J2000.0 FK5 radial velocity (km/s, +ve = moving away)
*     r1950 = double * (Returned)
*        B1950.0 FK4 RA (radians).
*     d1950 = double * (Returned)
*        B1950.0 FK4 Dec (radians).
*     dr1950 = double * (Returned)
*        B1950.0 FK4 RA proper motion (rad/Jul.yr)
*     dd1950 = double * (Returned)
*        B1950.0 FK4 Dec proper motion (rad/Jul.yr)
*     p1950 = double * (Returned)
*        B1950.0 FK4 parallax (arcsec)
*     v1950 = double * (Returned)
*         B1950.0 FK4 radial velocity (km/s, +ve = moving away)

*  Description:
*     This function converts stars from the IAU 1976, FK5, Fricke
*     system, to the Bessel-Newcomb, FK4 system.  The precepts
*     of Smith et al (Ref 1) are followed, using the implementation
*     by Yallop et al (Ref 2) of a matrix method due to Standish.
*     Kinoshita's development of Andoyer's post-Newcomb precession is
*     used.  The numerical constants from Seidelmann et al (Ref 3) are
*     used canonically.

*  Notes:
*     - The proper motions in RA are dRA/dt rather than
*     cos(Dec)*dRA/dt, and are per year rather than per century.
*     - Note that conversion from Julian epoch 2000.0 to Besselian
*     epoch 1950.0 only is provided for.  Conversions involving
*     other epochs will require use of the appropriate precession,
*     proper motion, and E-terms routines before and/or after
*     FK524 is called.
*     - In the FK4 catalogue the proper motions of stars within
*     10 degrees of the poles do not embody the differential
*     E-term effect and should, strictly speaking, be handled
*     in a different manner from stars outside these regions.
*     However, given the general lack of homogeneity of the star
*     data available for routine astrometry, the difficulties of
*     handling positions that may have been determined from
*     astrometric fields spanning the polar and non-polar regions,
*     the likelihood that the differential E-terms effect was not
*     taken into account when allowing for proper motion in past
*     astrometry, and the undesirability of a discontinuity in
*     the algorithm, the decision has been made in this routine to
*     include the effect of differential E-terms on the proper
*     motions for all stars, whether polar or not.  At epoch 2000,
*     and measuring on the sky rather than in terms of dRA, the
*     errors resulting from this simplification are less than
*     1 milliarcsecond in position and 1 milliarcsecond per
*     century in proper motion.
*
*  References:
*     - Smith, C.A. et al, 1989.  "The transformation of astrometric
*       catalog systems to the equinox J2000.0".  Astron.J. 97, 265.
*     - Yallop, B.D. et al, 1989.  "Transformation of mean star places
*       from FK4 B1950.0 to FK5 J2000.0 using matrices in 6-space".
*       Astron.J. 97, 274.
*     - Seidelmann, P.K. (ed), 1992.  "Explanatory Supplement to
*       the Astronomical Almanac", ISBN 0-935702-68-7.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "math.h"
#include "pal.h"
#include "palmac.h"

void palFk524(double r2000,
              double d2000,
              double dr2000,
              double dd2000,
              double p2000,
              double v2000,
              double *r1950,
              double *d1950,
              double *dr1950,
              double *dd1950,
              double *p1950,
              double *v1950) {

    /* Local Variables; */
    double r, d, ur, ud, px, rv;
    double sr, cr, sd, cd, x, y, z, w;
    double v1[6], v2[6];
    double xd, yd, zd;
    double rxyz, wd, rxysq, rxy;
    int i, j;

    /* Small number to avoid arithmetic problems. */
    static const double tiny = 1.0 - 30;

    /* Canonical constants (see references). Constant vector and matrix. */
    double a[6] = {
        -1.62557E-6, -0.31919E-6, -0.13843E-6, +1.245E-3, -1.580E-3, -0.659E-3};
    double emi[6][6] = {
        {0.9999256795,
         0.0111814828,
         0.0048590039,
         -0.00000242389840,
         -0.00000002710544,
         -0.00000001177742},
        {-0.0111814828,
         0.9999374849,
         -0.0000271771,
         0.00000002710544,
         -0.00000242392702,
         0.00000000006585},
        {-0.0048590040,
         -0.0000271557,
         0.9999881946,
         0.00000001177742,
         0.00000000006585,
         -0.00000242404995},
        {-0.000551, 0.238509, -0.435614, 0.99990432, 0.01118145, 0.00485852},
        {-0.238560, -0.002667, 0.012254, -0.01118145, 0.99991613, -0.00002717},
        {0.435730, -0.008541, 0.002117, -0.00485852, -0.00002716, 0.99996684}};

    /* Pick up J2000 data (units radians and arcsec/JC). */
    r = r2000;
    d = d2000;
    ur = dr2000 * PAL__PMF;
    ud = dd2000 * PAL__PMF;
    px = p2000;
    rv = v2000;

    /* Spherical to Cartesian. */
    sr = sin(r);
    cr = cos(r);
    sd = sin(d);
    cd = cos(d);

    x = cr * cd;
    y = sr * cd;
    z = sd;

    w = PAL__VF * rv * px;

    v1[0] = x;
    v1[1] = y;
    v1[2] = z;

    v1[3] = -ur * y - cr * sd * ud + w * x;
    v1[4] = ur * x - sr * sd * ud + w * y;
    v1[5] = cd * ud + w * z;

    /* Convert position+velocity vector to BN system. */
    for (i = 0; i < 6; i++) {
        w = 0.0;
        for (j = 0; j < 6; j++) {
            w += emi[i][j] * v1[j];
        }
        v2[i] = w;
    }

    /* Position vector components and magnitude. */
    x = v2[0];
    y = v2[1];
    z = v2[2];
    rxyz = sqrt(x * x + y * y + z * z);

    /* Apply E-terms to position. */
    w = x * a[0] + y * a[1] + z * a[2];
    x += a[0] * rxyz - w * x;
    y += a[1] * rxyz - w * y;
    z += a[2] * rxyz - w * z;

    /* Recompute magnitude. */
    rxyz = sqrt(x * x + y * y + z * z);

    /* Apply E-terms to both position and velocity. */
    x = v2[0];
    y = v2[1];
    z = v2[2];
    w = x * a[0] + y * a[1] + z * a[2];
    wd = x * a[3] + y * a[4] + z * a[5];
    x += a[0] * rxyz - w * x;
    y += a[1] * rxyz - w * y;
    z += a[2] * rxyz - w * z;
    xd = v2[3] + a[3] * rxyz - wd * x;
    yd = v2[4] + a[4] * rxyz - wd * y;
    zd = v2[5] + a[5] * rxyz - wd * z;

    /* Convert to spherical. */
    rxysq = x * x + y * y;
    rxy = sqrt(rxysq);

    if (x == 0.0 && y == 0.0) {
        r = 0.0;
    } else {
        r = atan2(y, x);
        if (r < 0.0)
            r += PAL__D2PI;
    }
    d = atan2(z, rxy);

    if (rxy > tiny) {
        ur = (x * yd - y * xd) / rxysq;
        ud = (zd * rxysq - z * (x * xd + y * yd)) / ((rxysq + z * z) * rxy);
    }

    /* Radial velocity and parallax. */
    if (px > tiny) {
        rv = (x * xd + y * yd + z * zd) / (px * PAL__VF * rxyz);
        px /= rxyz;
    }

    /* Return results. */
    *r1950 = r;
    *d1950 = d;
    *dr1950 = ur / PAL__PMF;
    *dd1950 = ud / PAL__PMF;
    *p1950 = px;
    *v1950 = rv;
}
/*
*+
*  Name:
*     palFk54z

*  Purpose:
*     Convert a J2000.0 FK5 star position to B1950.0 FK4 assuming
*     zero proper motion and parallax.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palFk54z( double r2000, double d2000, double bepoch, double *r1950,
*               double *d1950, double *dr1950, double *dd1950 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 FK5 RA (radians).
*     d2000 = double (Given)
*        J2000.0 FK5 Dec (radians).
*     bepoch = double (Given)
*         Besselian epoch (e.g. 1950.0).
*     r1950 = double * (Returned)
*        B1950 FK4 RA (radians) at epoch "bepoch".
*     d1950 = double * (Returned)
*        B1950 FK4 Dec (radians) at epoch "bepoch".
*     dr1950 = double * (Returned)
*        B1950 FK4 proper motion (RA) (radians/trop.yr)).
*     dr1950 = double * (Returned)
*        B1950 FK4 proper motion (Dec) (radians/trop.yr)).

*  Description:
*     This function converts star positions from the IAU 1976,
*     FK5, Fricke system to the Bessel-Newcomb, FK4 system.

*  Notes:
*     - The proper motion in RA is dRA/dt rather than cos(Dec)*dRA/dt.
*     - Conversion from Julian epoch 2000.0 to Besselian epoch 1950.0
*     only is provided for.  Conversions involving other epochs will
*     require use of the appropriate precession functions before and
*     after this function is called.
*     - The FK5 proper motions, the parallax and the radial velocity
*      are presumed zero.
*     - It is the intention that FK5 should be a close approximation
*     to an inertial frame, so that distant objects have zero proper
*     motion;  such objects have (in general) non-zero proper motion
*     in FK4, and this function returns those fictitious proper
*     motions.
*     - The position returned by this function is in the B1950
*     reference frame but at Besselian epoch BEPOCH.  For comparison
*     with catalogues the "bepoch" argument will frequently be 1950.0.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palFk54z(double r2000,
              double d2000,
              double bepoch,
              double *r1950,
              double *d1950,
              double *dr1950,
              double *dd1950) {

    /* Local Variables: */
    double r, d, px, rv, y;

    /* FK5 equinox J2000 (any epoch) to FK4 equinox B1950 epoch B1950. */
    palFk524(
        r2000, d2000, 0.0, 0.0, 0.0, 0.0, &r, &d, dr1950, dd1950, &px, &rv);

    /* Fictitious proper motion to epoch "bepoch". */
    y = bepoch - 1950.0;
    *r1950 = r + *dr1950 * y;
    *d1950 = d + *dd1950 * y;
}

/*
*+
*  Name:
*     palGaleq

*  Purpose:
*     Convert from galactic to J2000.0 equatorial coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palGaleq ( double dl, double db, double *dr, double *dd );

*  Arguments:
*     dl = double (Given)
*       Galactic longitude (radians).
*     db = double (Given)
*       Galactic latitude (radians).
*     dr = double * (Returned)
*       J2000.0 RA (radians)
*     dd = double * (Returned)
*       J2000.0 Dec (radians)

*  Description:
*     Transformation from IAU 1958 galactic coordinates to
*     J2000.0 equatorial coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     The equatorial coordinates are J2000.0.  Use the routine
*     palGe50 if conversion to B1950.0 'FK4' coordinates is
*     required.

*  See Also:
*     Blaauw et al, Mon.Not.R.Astron.Soc.,121,123 (1960)

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1998 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palGaleq(double dl, double db, double *dr, double *dd) {

    double v1[3];
    double v2[3];

    /*
     *  L2,B2 system of galactic coordinates
     *
     *  P = 192.25       RA of galactic north pole (mean B1950.0)
     *  Q =  62.6        inclination of galactic to mean B1950.0 equator
     *  R =  33          longitude of ascending node
     *
     *  P,Q,R are degrees
     *
     *  Equatorial to galactic rotation matrix (J2000.0), obtained by
     *  applying the standard FK4 to FK5 transformation, for zero proper
     *  motion in FK5, to the columns of the B1950 equatorial to
     *  galactic rotation matrix:
     */
    double rmat[3][3] = {{-0.054875539726, -0.873437108010, -0.483834985808},
                         {+0.494109453312, -0.444829589425, +0.746982251810},
                         {-0.867666135858, -0.198076386122, +0.455983795705}};

    /* Spherical to Cartesian */
    eraS2c(dl, db, v1);

    /* Galactic to equatorial */
    eraTrxp(rmat, v1, v2);

    /* Cartesian to spherical */
    eraC2s(v2, dr, dd);

    /* Express in conventional ranges */
    *dr = eraAnp(*dr);
    *dd = eraAnpm(*dd);
}
/*
*+
*  Name:
*     palGalsup

*  Purpose:
*     Convert from galactic to supergalactic coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palGalsup ( double dl, double db, double *dsl, double *dsb );

*  Arguments:
*     dl = double (Given)
*       Galactic longitude.
*     db = double (Given)
*       Galactic latitude.
*     dsl = double * (Returned)
*       Supergalactic longitude.
*     dsb = double * (Returned)
*       Supergalactic latitude.

*  Description:
*     Transformation from IAU 1958 galactic coordinates to
*     de Vaucouleurs supergalactic coordinates.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  See Also:
*     - de Vaucouleurs, de Vaucouleurs, & Corwin, Second Reference
*       Catalogue of Bright Galaxies, U. Texas, page 8.
*     - Systems & Applied Sciences Corp., Documentation for the
*       machine-readable version of the above catalogue,
*       Contract NAS 5-26490.
*
*    (These two references give different values for the galactic
*     longitude of the supergalactic origin.  Both are wrong;  the
*     correct value is L2=137.37.)

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palGalsup(double dl, double db, double *dsl, double *dsb) {

    double v1[3];
    double v2[3];

    /*
     *  System of supergalactic coordinates:
     *
     *    SGL   SGB        L2     B2      (deg)
     *     -    +90      47.37  +6.32
     *     0     0         -      0
     *
     *  Galactic to supergalactic rotation matrix:
     */
    double rmat[3][3] = {{-0.735742574804, +0.677261296414, +0.000000000000},
                         {-0.074553778365, -0.080991471307, +0.993922590400},
                         {+0.673145302109, +0.731271165817, +0.110081262225}};

    /* Spherical to Cartesian */
    eraS2c(dl, db, v1);

    /* Galactic to Supergalactic */
    eraRxp(rmat, v1, v2);

    /* Cartesian to spherical */
    eraC2s(v2, dsl, dsb);

    /* Express in conventional ranges */
    *dsl = eraAnp(*dsl);
    *dsb = eraAnpm(*dsb);
}
/*
*+
*  Name:
*     palGe50

*  Purpose:
*     Transform Galactic Coordinate to B1950 FK4

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palGe50( double dl, double db, double *dr, double *dd );

*  Arguments:
*     dl = double (Given)
*        Galactic longitude (radians)
*     db = double (Given)
*        Galactic latitude (radians)
*     dr = double * (Returned)
*        B9150.0 FK4 RA.
*     dd = double * (Returned)
*        B1950.0 FK4 Dec.

*  Description:
*     Transformation from IAU 1958 galactic coordinates to
*     B1950.0 'FK4' equatorial coordinates.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The equatorial coordinates are B1950.0 'FK4'. Use the routine
*     palGaleq if conversion to J2000.0 coordinates is required.

*  See Also:
*     - Blaauw et al, Mon.Not.R.Astron.Soc.,121,123 (1960)

*  History:
*     2012-03-23 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palGe50(double dl, double db, double *dr, double *dd) {

    /*
     *  L2,B2 system of galactic coordinates
     *
     *  P = 192.25       RA of galactic north pole (mean B1950.0)
     *  Q =  62.6        inclination of galactic to mean B1950.0 equator
     *  R =  33          longitude of ascending node
     *
     *  P,Q,R are degrees
     *
     *
     *  Equatorial to galactic rotation matrix
     *
     *  The Euler angles are P, Q, 90-R, about the z then y then
     *  z axes.
     *
     *         +CP.CQ.SR-SP.CR     +SP.CQ.SR+CP.CR     -SQ.SR
     *
     *         -CP.CQ.CR-SP.SR     -SP.CQ.CR+CP.SR     +SQ.CR
     *
     *         +CP.SQ              +SP.SQ              +CQ
     *
     */

    double rmat[3][3] = {{-0.066988739415, -0.872755765852, -0.483538914632},
                         {+0.492728466075, -0.450346958020, +0.744584633283},
                         {-0.867600811151, -0.188374601723, +0.460199784784}};

    double v1[3], v2[3], r, d, re, de;

    /* Spherical to cartesian */
    eraS2c(dl, db, v1);

    /* Rotate to mean B1950.0 */
    eraTrxp(rmat, v1, v2);

    /* Cartesian to spherical */
    eraC2s(v2, &r, &d);

    /* Introduce E-terms */
    palAddet(r, d, 1950.0, &re, &de);

    /* Express in conventional ranges */
    *dr = eraAnp(re);
    *dd = eraAnpm(de);
}
/*
*+
*  Name:
*     palGeoc

*  Purpose:
*     Convert geodetic position to geocentric

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palGeoc( double p, double h, double * r, double *z );

*  Arguments:
*     p = double (Given)
*       latitude (radians)
*     h = double (Given)
*       height above reference spheroid (geodetic, metres)
*     r = double * (Returned)
*       distance from Earth axis (AU)
*     z = double * (Returned)
*       distance from plane of Earth equator (AU)

*  Description:
*     Convert geodetic position to geocentric.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Geocentric latitude can be obtained by evaluating atan2(z,r)
*     - Uses WGS84 reference ellipsoid and calls eraGd2gc

*  History:
*     2012-03-01 (TIMJ):
*        Initial version moved from palOne2One
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palGeoc(double p, double h, double *r, double *z) {
    double xyz[3];
    const double elong = 0.0; /* Use zero longitude */
    const double AU = 1.49597870E11;
    /* WGS84 looks to be the closest match */
    eraGd2gc(ERFA_WGS84, elong, p, h, xyz);
    *r = xyz[0] / (AU * cos(elong));
    *z = xyz[2] / AU;
}
/*
*+
*  Name:
*     palIntin

*  Purpose:
*     Convert free-format input into an integer

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palIntin( const char * string, int *nstrt,
*                     long *ireslt, int *jflag );

*  Arguments:
*     string = const char * (Given)
*        String containing number to be decoded.
*     nstrt = int * (Given and Returned)
*        Character number indicating where decoding should start.
*        On output its value is updated to be the location of the
*        possible next value. For compatibility with SLA the first
*        character is index 1.
*     ireslt = long * (Returned)
*        Result. Not updated when jflag=1.
*     jflag = int * (Returned)
*        status: -1 = -OK, 0 = +OK, 1 = null, 2 = error

*  Description:
*     Extracts a number from an input string starting at the specified
*     index.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Uses the strtol() system call to do the parsing. This may lead to
*       subtle differences when compared to the SLA/F parsing.
*     - Commas are recognized as a special case and are skipped if one happens
*       to be the next character when updating nstrt. Additionally the output
*       nstrt position will skip past any trailing space.
*     - If no number can be found flag will be set to 1.
*     - If the number overflows or underflows jflag will be set to 2. For
overflow
*       the returned result will have the value LONG_MAX, for underflow it
*       will have the value LONG_MIN.

*  History:
*     2012-03-15 (TIMJ):
*        Initial version
*        Matches the SLALIB interface but brand new implementation using
*        C library calls and not a direct port of the Fortran.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

/* Shenanigans for isblank() which is C99 only */

#include "pal.h"

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>

void palIntin(const char *string, int *nstrt, long *ireslt, int *jflag) {

    const char *strstart = nullptr; /* Pointer to start of search */
    const char *ctemp = nullptr; /* Pointer into string */
    char *endptr = nullptr; /* Pointer to string after number */
    int retval; /* Return value from strtol */
    int hasminus; /* is this a -0 */

    /* strtol man page indicates that we should reset errno before
       calling strtod */
    errno = 0;

    /* Locate the start postion */
    strstart = &(string[*nstrt - 1]);

    /* We have to be able to deal with -0 so we have to search the
       string first and look for the negative */
    hasminus = 0;
    ctemp = strstart;
    while (ctemp != nullptr) {
        if (isdigit(*ctemp))
            break;
        /* Reset so that - 12345 is not a negative number */
        hasminus = 0;
        /* Flag that we have found a minus */
        if (*ctemp == '-')
            hasminus = 1;
        ctemp++;
    }

    /* Look for the number using the system call, offsetting using
       1-based counter. */
    retval = strtol(strstart, &endptr, 10);
    if (retval == 0.0 && endptr == strstart) {
        /* conversion did not find anything */
        *jflag = 1;

        /* but SLA compatibility requires that we step
           through to remove leading spaces. We also step
           through alphabetic characters since they can never
           be numbers. Skip past a "+" since it doesn't gain
           us anything and matches slalib. */
        while (isblank(*endptr) || isalpha(*endptr) || *endptr == '+') {
            endptr++;
        }

    } else if (errno == ERANGE) {
        *jflag = 2;
    } else {
        if (retval < 0 || hasminus) {
            *jflag = -1;
        } else {
            *jflag = 0;
        }
    }

    /* Sort out the position for the next index */
    *nstrt = endptr - string + 1;

    /* Skip a comma */
    if (*endptr == ',') {
        (*nstrt)++;
    } else {
        /* jump past any leading spaces for the next part of the string */
        ctemp = endptr;
        while (isblank(*ctemp)) {
            (*nstrt)++;
            ctemp++;
        }
    }

    /* And the result unless we found nothing */
    if (*jflag != 1)
        *ireslt = retval;
}
/*
*+
*  Name:
*     palMap

*  Purpose:
*     Convert star RA,Dec from mean place to geocentric apparent

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palMap( double rm, double dm, double pr, double pd,
*                  double px, double rv, double eq, double date,
*                  double *ra, double *da );

*  Arguments:
*     rm = double (Given)
*        Mean RA (radians)
*     dm = double (Given)
*        Mean declination (radians)
*     pr = double (Given)
*        RA proper motion, changes per Julian year (radians)
*     pd = double (Given)
*        Dec proper motion, changes per Julian year (radians)
*     px = double (Given)
*        Parallax (arcsec)
*     rv = double (Given)
*        Radial velocity (km/s, +ve if receding)
*     eq = double (Given)
*        Epoch and equinox of star data (Julian)
*     date = double (Given)
*        TDB for apparent place (JD-2400000.5)
*     ra = double * (Returned)
*        Apparent RA (radians)
*     dec = double * (Returned)
*        Apparent dec (radians)

*  Description:
*     Convert star RA,Dec from mean place to geocentric apparent.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Calls palMappa and palMapqk

*     - The reference systems and timescales used are IAU 2006.

*     - EQ is the Julian epoch specifying both the reference frame and
*       the epoch of the position - usually 2000.  For positions where
*       the epoch and equinox are different, use the routine palPm to
*       apply proper motion corrections before using this routine.
*
*     - The distinction between the required TDB and TT is always
*       negligible.  Moreover, for all but the most critical
*       applications UTC is adequate.
*
*     - The proper motions in RA are dRA/dt rather than cos(Dec)*dRA/dt.
*
*     - This routine may be wasteful for some applications because it
*       recomputes the Earth position/velocity and the precession-
*       nutation matrix each time, and because it allows for parallax
*       and proper motion.  Where multiple transformations are to be
*       carried out for one epoch, a faster method is to call the
*       palMappa routine once and then either the palMapqk routine
*       (which includes parallax and proper motion) or palMapqkz (which
*       assumes zero parallax and proper motion).
*
*     - The accuracy is sub-milliarcsecond, limited by the
*       precession-nutation model (see palPrenut for details).
*
*     - The accuracy is further limited by the routine palEvp, called
*       by palMappa, which computes the Earth position and velocity.
*       See eraEpv00 for details on that calculation.

*  History:
*     2012-03-01 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2001 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
*    USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palMap(double rm,
            double dm,
            double pr,
            double pd,
            double px,
            double rv,
            double eq,
            double date,
            double *ra,
            double *da) {

    double amprms[21];

    /* Star independent parameters */
    palMappa(eq, date, amprms);

    /* Mean to apparent */
    palMapqk(rm, dm, pr, pd, px, rv, amprms, ra, da);
}
/*
*+
*  Name:
*     palMappa

*  Purpose:
*     Compute parameters needed by palAmpqk and palMapqk.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palMappa( double eq, double date, double amprms[21] )

*  Arguments:
*     eq = double (Given)
*        epoch of mean equinox to be used (Julian)
*     date = double (Given)
*        TDB (JD-2400000.5)
*     amprms =   double[21]  (Returned)
*        star-independent mean-to-apparent parameters:
*        - (0)      time interval for proper motion (Julian years)
*        - (1-3)    barycentric position of the Earth (AU)
*        - (4-6)    heliocentric direction of the Earth (unit vector)
*        - (7)      (grav rad Sun)*2/(Sun-Earth distance)
*        - (8-10)   abv: barycentric Earth velocity in units of c
*        - (11)     sqrt(1-v**2) where v=modulus(abv)
*        - (12-20)  precession/nutation (3,3) matrix

*  Description:
*     Compute star-independent parameters in preparation for
*     transformations between mean place and geocentric apparent place.
*
*     The parameters produced by this function are required in the
*     parallax, aberration, and nutation/bias/precession parts of the
*     mean/apparent transformations.
*
*     The reference systems and timescales used are IAU 2006.

*  Notes:
*     - For date, the distinction between the required TDB and TT
*     is always negligible.  Moreover, for all but the most
*     critical applications UTC is adequate.
*     - The vector amprms(1-3) is referred to the mean equinox and
*     equator of epoch eq.
*     - The parameters amprms produced by this function are used by
*     palAmpqk, palMapqk and palMapqkz.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (PTW):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2003 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <string.h>

void palMappa(double eq, double date, double amprms[21]) {

    /* Local constants */

    /*  Gravitational radius of the Sun x 2 (2*mu/c**2, AU) */
    const double GR2 = 2.0 * 9.87063e-9;

    /* Local Variables; */
    int i;
    double ebd[3], ehd[3], eh[3], e, vn[3], vm;

    /* Initialise so that unsused values are returned holding zero */
    memset(amprms, 0, 21 * sizeof(*amprms));

    /* Time interval for proper motion correction. */
    amprms[0] = eraEpj(PAL__MJD0, date) - eq;

    /* Get Earth barycentric and heliocentric position and velocity. */
    palEvp(date, eq, ebd, &amprms[1], ehd, eh);

    /* Heliocentric direction of Earth (normalized) and modulus. */
    eraPn(eh, &e, &amprms[4]);

    /* Light deflection parameter */
    amprms[7] = GR2 / e;

    /* Aberration parameters. */
    for (i = 0; i < 3; i++) {
        amprms[i + 8] = ebd[i] * PAL__CR;
    }
    eraPn(&amprms[8], &vm, vn);
    amprms[11] = sqrt(1.0 - vm * vm);

    /* NPB matrix. */
    palPrenut(eq, date, (double(*)[3]) & amprms[12]);
}
/*
*+
*  Name:
*     palMapqk

*  Purpose:
*     Quick mean to apparent place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palMapqk ( double rm, double dm, double pr, double pd,
*                     double px, double rv, double amprms[21],
*                     double *ra, double *da );

*  Arguments:
*     rm = double (Given)
*        Mean RA (radians)
*     dm = double (Given)
*        Mean declination (radians)
*     pr = double (Given)
*        RA proper motion, changes per Julian year (radians)
*     pd = double (Given)
*        Dec proper motion, changes per Julian year (radians)
*     px = double (Given)
*        Parallax (arcsec)
*     rv = double (Given)
*        Radial velocity (km/s, +ve if receding)
*     amprms = double [21] (Given)
*        Star-independent mean-to-apparent parameters (see palMappa).
*     ra = double * (Returned)
*        Apparent RA (radians)
*     dec = double * (Returned)
*        Apparent dec (radians)

*  Description:
*     Quick mean to apparent place:  transform a star RA,Dec from
*     mean place to geocentric apparent place, given the
*     star-independent parameters.
*
*     Use of this routine is appropriate when efficiency is important
*     and where many star positions, all referred to the same equator
*     and equinox, are to be transformed for one epoch.  The
*     star-independent parameters can be obtained by calling the
*     palMappa routine.
*
*     If the parallax and proper motions are zero the palMapqkz
*     routine can be used instead.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The reference frames and timescales used are post IAU 2006.

*  History:
*     2012-03-01 (TIMJ):
*        Initial version with documentation from SLA/F
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2000 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palMapqk(double rm,
              double dm,
              double pr,
              double pd,
              double px,
              double rv,
              double amprms[21],
              double *ra,
              double *da) {

    /* local constants */
    const double VF = 0.21094502; /* Km/s to AU/year */

    /* Local Variables: */
    int i;
    double ab1, abv[3], p[3], w, p1dv, p2[3], p3[3];
    double pmt, gr2e, eb[3], q[3], pxr, em[3];
    double pde, pdep1, p1[3], ehn[3], pn[3];

    /* Unpack scalar and vector parameters. */
    pmt = amprms[0];
    gr2e = amprms[7];
    ab1 = amprms[11];
    for (i = 0; i < 3; i++) {
        eb[i] = amprms[i + 1];
        ehn[i] = amprms[i + 4];
        abv[i] = amprms[i + 8];
    }

    /* Spherical to x,y,z. */
    eraS2c(rm, dm, q);

    /* Space motion (radians per year) */
    pxr = px * PAL__DAS2R;
    w = VF * rv * pxr;
    em[0] = -pr * q[1] - pd * cos(rm) * sin(dm) + w * q[0];
    em[1] = pr * q[0] - pd * sin(rm) * sin(dm) + w * q[1];
    em[2] = pd * cos(dm) + w * q[2];

    /* Geocentric direction of star (normalised) */
    for (i = 0; i < 3; i++) {
        p[i] = q[i] + pmt * em[i] - pxr * eb[i];
    }
    eraPn(p, &w, pn);

    /* Light deflection (restrained within the Sun's disc) */
    pde = eraPdp(pn, ehn);
    pdep1 = pde + 1.0;
    w = gr2e / (pdep1 > 1.0e-5 ? pdep1 : 1.0e-5);
    for (i = 0; i < 3; i++) {
        p1[i] = pn[i] + w * (ehn[i] - pde * pn[i]);
    }

    /* Aberration (normalisation omitted). */
    p1dv = eraPdp(p, abv);
    w = 1.0 + p1dv / (ab1 + 1.0);
    for (i = 0; i < 3; i++) {
        p2[i] = (ab1 * p1[i]) + (w * abv[i]);
    }

    /* Precession and nutation. */
    eraRxp((double(*)[3]) & amprms[12], p2, p3);

    /* Geocentric apparent RA,dec. */
    eraC2s(p3, ra, da);
    *ra = eraAnp(*ra);
}
/*
*+
*  Name:
*     palMapqkz

*  Purpose:
*     Quick mean to apparent place.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palMapqkz( double rm, double dm, double amprms[21],
*                     double *ra, double *da )

*  Arguments:
*     rm = double (Given)
*        Mean RA (radians).
*     dm = double (Given)
*        Mean Dec (radians).
*     amprms = double[21] (Given)
*        Star-independent mean-to-apparent parameters (see palMappa):
*        (0-3)    not used
*        (4-6)    not used
*        (7)      not used
*        (8-10)   abv: barycentric Earth velocity in units of c
*        (11)     sqrt(1-v**2) where v=modulus(abv)
*        (12-20)  precession/nutation (3,3) matrix
*     ra = double * (Returned)
*        Apparent RA (radians).
*     da = double * (Returned)
*        Apparent Dec (radians).

*  Description:
*     Quick mean to apparent place:  transform a star RA,dec from
*     mean place to geocentric apparent place, given the
*     star-independent parameters, and assuming zero parallax
*     and proper motion.
*
*     Use of this function is appropriate when efficiency is important
*     and where many star positions, all with parallax and proper
*     motion either zero or already allowed for, and all referred to
*     the same equator and equinox, are to be transformed for one
*     epoch.  The star-independent parameters can be obtained by
*     calling the palMappa function.
*
*     The corresponding function for the case of non-zero parallax
*     and proper motion is palMapqk.
*
*     The reference systems and timescales used are IAU 2006.
*
*     Strictly speaking, the function is not valid for solar-system
*     sources, though the error will usually be extremely small.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     {enter_new_authors_here}

*  History:
*     2012-02-13 (PTW):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palMapqkz(
    double rm, double dm, double amprms[21], double *ra, double *da) {

    /* Local Variables: */
    int i;
    double ab1, abv[3], p[3], w, p1dv, p1dvp1, p2[3], p3[3];

    /* Unpack scalar and vector parameters. */
    ab1 = amprms[11];
    for (i = 0; i < 3; i++) {
        abv[i] = amprms[i + 8];
    }

    /* Spherical to x,y,z. */
    eraS2c(rm, dm, p);

    /* Aberration. */
    p1dv = eraPdp(p, abv);
    p1dvp1 = p1dv + 1.0;
    w = 1.0 + p1dv / (ab1 + 1.0);
    for (i = 0; i < 3; i++) {
        p2[i] = ((ab1 * p[i]) + (w * abv[i])) / p1dvp1;
    }

    /* Precession and nutation. */
    eraRxp((double(*)[3]) & amprms[12], p2, p3);

    /* Geocentric apparent RA,dec. */
    eraC2s(p3, ra, da);
    *ra = eraAnp(*ra);
}
/*
*+
*  Name:
*     palNut

*  Purpose:
*     Form the matrix of nutation

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palNut( double date, double rmatn[3][3] );

*  Arguments:
*     date = double (Given)
*        TT as modified Julian date (JD-2400000.5)
*     rmatn = double [3][3] (Returned)
*        Nutation matrix in the sense v(true)=rmatn * v(mean)
*        where v(true) is the star vector relative to the
*        true equator and equinox of date and v(mean) is the
*        star vector relative to the mean equator and equinox
*        of date.

*  Description:
*     Form the matrix of nutation for a given date using
*     the IAU 2006 nutation model and palDeuler.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Uses eraNut06a via palNutc
*     - The distinction between TDB and TT is negligible. For all but
*       the most critical applications UTC is adequate.

*  History:
*     2012-03-07 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palNut(double date, double rmatn[3][3]) {
    double dpsi, deps, eps0;

    /* Nutation component and mean obliquity */
    palNutc(date, &dpsi, &deps, &eps0);

    /* Rotation matrix */
    palDeuler("XZX", eps0, -dpsi, -(eps0 + deps), rmatn);
}
/*
*+
*  Name:
*     palNutc

*  Purpose:
*     Calculate nutation longitude & obliquoty components

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palNutc( double date, double * dpsi, double *deps, double *eps0 );

*  Arguments:
*     date = double (Given)
*        TT as modified Julian date (JD-2400000.5)
*     dpsi = double * (Returned)
*        Nutation in longitude
*     deps = double * (Returned)
*        Nutation in obliquity
*     eps0 = double * (Returned)
*        Mean obliquity.

*  Description:
*     Calculates the longitude * obliquity components and mean obliquity
*     using the SOFA library.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Calls eraObl06 and eraNut06a and therefore uses the IAU 206
*       precession/nutation model.
*     - Note the change from SLA/F regarding the date. TT is used
*       rather than TDB.

*  History:
*     2012-03-05 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palNutc(double date, double *dpsi, double *deps, double *eps0) {
    eraNut06a(PAL__MJD0, date, dpsi, deps);
    *eps0 = eraObl06(PAL__MJD0, date);
}
/*
*+
*  Name:
*     palOap

*  Purpose:
*     Observed to apparent place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palOap ( const char *type, double ob1, double ob2, double date,
*                   double dut, double elongm, double phim, double hm,
*                   double xp, double yp, double tdk, double pmb,
*                   double rh, double wl, double tlr,
*                   double *rap, double *dap );

*  Arguments:
*     type = const char * (Given)
*        Type of coordinates - 'R', 'H' or 'A' (see below)
*     ob1 = double (Given)
*        Observed Az, HA or RA (radians; Az is N=0;E=90)
*     ob2 = double (Given)
*        Observed ZD or Dec (radians)
*     date = double (Given)
*        UTC date/time (Modified Julian Date, JD-2400000.5)
*     dut = double (Given)
*        delta UT: UT1-UTC (UTC seconds)
*     elongm = double (Given)
*        Mean longitude of the observer (radians, east +ve)
*     phim = double (Given)
*        Mean geodetic latitude of the observer (radians)
*     hm = double (Given)
*        Observer's height above sea level (metres)
*     xp = double (Given)
*        Polar motion x-coordinates (radians)
*     yp = double (Given)
*        Polar motion y-coordinates (radians)
*     tdk = double (Given)
*        Local ambient temperature (K; std=273.15)
*     pmb = double (Given)
*        Local atmospheric pressure (mb; std=1013.25)
*     rh = double (Given)
*        Local relative humidity (in the range 0.0-1.0)
*     wl = double (Given)
*        Effective wavelength (micron, e.g. 0.55)
*     tlr = double (Given)
*        Tropospheric laps rate (K/metre, e.g. 0.0065)
*     rap = double * (Given)
*        Geocentric apparent right ascension
*     dap = double * (Given)
*        Geocentric apparent declination

*  Description:
*     Observed to apparent place.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Only the first character of the TYPE argument is significant.
*     'R' or 'r' indicates that OBS1 and OBS2 are the observed right
*     ascension and declination;  'H' or 'h' indicates that they are
*     hour angle (west +ve) and declination;  anything else ('A' or
*     'a' is recommended) indicates that OBS1 and OBS2 are azimuth
*     (north zero, east 90 deg) and zenith distance.  (Zenith
*     distance is used rather than elevation in order to reflect the
*     fact that no allowance is made for depression of the horizon.)
*
*     - The accuracy of the result is limited by the corrections for
*     refraction.  Providing the meteorological parameters are
*     known accurately and there are no gross local effects, the
*     predicted apparent RA,Dec should be within about 0.1 arcsec
*     for a zenith distance of less than 70 degrees.  Even at a
*     topocentric zenith distance of 90 degrees, the accuracy in
*     elevation should be better than 1 arcmin;  useful results
*     are available for a further 3 degrees, beyond which the
*     palRefro routine returns a fixed value of the refraction.
*     The complementary routines palAop (or palAopqk) and palOap
*     (or palOapqk) are self-consistent to better than 1 micro-
*     arcsecond all over the celestial sphere.
*
*     - It is advisable to take great care with units, as even
*     unlikely values of the input parameters are accepted and
*     processed in accordance with the models used.
*
*     - "Observed" Az,El means the position that would be seen by a
*     perfect theodolite located at the observer.  This is
*     related to the observed HA,Dec via the standard rotation, using
*     the geodetic latitude (corrected for polar motion), while the
*     observed HA and RA are related simply through the local
*     apparent ST.  "Observed" RA,Dec or HA,Dec thus means the
*     position that would be seen by a perfect equatorial located
*     at the observer and with its polar axis aligned to the
*     Earth's axis of rotation (n.b. not to the refracted pole).
*     By removing from the observed place the effects of
*     atmospheric refraction and diurnal aberration, the
*     geocentric apparent RA,Dec is obtained.
*
*     - Frequently, mean rather than apparent RA,Dec will be required,
*     in which case further transformations will be necessary.  The
*     palAmp etc routines will convert the apparent RA,Dec produced
*     by the present routine into an "FK5" (J2000) mean place, by
*     allowing for the Sun's gravitational lens effect, annual
*     aberration, nutation and precession.  Should "FK4" (1950)
*     coordinates be needed, the routines palFk524 etc will also
*     need to be applied.
*
*     - To convert to apparent RA,Dec the coordinates read from a
*     real telescope, corrections would have to be applied for
*     encoder zero points, gear and encoder errors, tube flexure,
*     the position of the rotator axis and the pointing axis
*     relative to it, non-perpendicularity between the mounting
*     axes, and finally for the tilt of the azimuth or polar axis
*     of the mounting (with appropriate corrections for mount
*     flexures).  Some telescopes would, of course, exhibit other
*     properties which would need to be accounted for at the
*     appropriate point in the sequence.
*
*     - This routine takes time to execute, due mainly to the rigorous
*     integration used to evaluate the refraction.  For processing
*     multiple stars for one location and time, call palAoppa once
*     followed by one call per star to palOapqk.  Where a range of
*     times within a limited period of a few hours is involved, and the
*     highest precision is not required, call palAoppa once, followed
*     by a call to palAoppat each time the time changes, followed by
*     one call per star to palOapqk.
*
*     - The DATE argument is UTC expressed as an MJD.  This is, strictly
*     speaking, wrong, because of leap seconds.  However, as long as
*     the delta UT and the UTC are consistent there are no
*     difficulties, except during a leap second.  In this case, the
*     start of the 61st second of the final minute should begin a new
*     MJD day and the old pre-leap delta UT should continue to be used.
*     As the 61st second completes, the MJD should revert to the start
*     of the day as, simultaneously, the delta UTC changes by one
*     second to its post-leap new value.
*
*     - The delta UT (UT1-UTC) is tabulated in IERS circulars and
*     elsewhere.  It increases by exactly one second at the end of
*     each UTC leap second, introduced in order to keep delta UT
*     within +/- 0.9 seconds.
*
*     - IMPORTANT -- TAKE CARE WITH THE LONGITUDE SIGN CONVENTION.
*     The longitude required by the present routine is east-positive,
*     in accordance with geographical convention (and right-handed).
*     In particular, note that the longitudes returned by the
*     palOBS routine are west-positive, following astronomical
*     usage, and must be reversed in sign before use in the present
*     routine.
*
*     - The polar coordinates XP,YP can be obtained from IERS
*     circulars and equivalent publications.  The maximum amplitude
*     is about 0.3 arcseconds.  If XP,YP values are unavailable,
*     use XP=YP=0D0.  See page B60 of the 1988 Astronomical Almanac
*     for a definition of the two angles.
*
*     - The height above sea level of the observing station, HM,
*     can be obtained from the Astronomical Almanac (Section J
*     in the 1988 edition), or via the routine palOBS.  If P,
*     the pressure in millibars, is available, an adequate
*     estimate of HM can be obtained from the expression
*
*            HM ~ -29.3*TSL*LOG(P/1013.25).
*
*     where TSL is the approximate sea-level air temperature in K
*     (see Astrophysical Quantities, C.W.Allen, 3rd edition,
*     section 52).  Similarly, if the pressure P is not known,
*     it can be estimated from the height of the observing
*     station, HM, as follows:
*
*            P ~ 1013.25*EXP(-HM/(29.3*TSL)).
*
*     Note, however, that the refraction is nearly proportional to the
*     pressure and that an accurate P value is important for precise
*     work.
*
*     - The azimuths etc. used by the present routine are with respect
*     to the celestial pole.  Corrections from the terrestrial pole
*     can be computed using palPolmo.

*  History:
*     2012-08-27 (TIMJ):
*        Initial version, copied from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palOap(const char *type,
            double ob1,
            double ob2,
            double date,
            double dut,
            double elongm,
            double phim,
            double hm,
            double xp,
            double yp,
            double tdk,
            double pmb,
            double rh,
            double wl,
            double tlr,
            double *rap,
            double *dap) {

    double aoprms[14];

    palAoppa(
        date, dut, elongm, phim, hm, xp, yp, tdk, pmb, rh, wl, tlr, aoprms);
    palOapqk(type, ob1, ob2, aoprms, rap, dap);
}
/*
*+
*  Name:
*     palOapqk

*  Purpose:
*     Quick observed to apparent place

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palOapqk ( const char *type, double ob1, double ob2,
*                     const  double aoprms[14], double *rap, double *dap );

*  Arguments:
*     Quick observed to apparent place.

*  Description:
*     type = const char * (Given)
*        Type of coordinates - 'R', 'H' or 'A' (see below)
*     ob1 = double (Given)
*        Observed Az, HA or RA (radians; Az is N=0;E=90)
*     ob2 = double (Given)
*        Observed ZD or Dec (radians)
*     aoprms = const double [14] (Given)
*        Star-independent apparent-to-observed parameters.
*        See palAopqk for details.
*     rap = double * (Given)
*        Geocentric apparent right ascension
*     dap = double * (Given)
*        Geocentric apparent declination

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Only the first character of the TYPE argument is significant.
*     'R' or 'r' indicates that OBS1 and OBS2 are the observed right
*     ascension and declination;  'H' or 'h' indicates that they are
*     hour angle (west +ve) and declination;  anything else ('A' or
*     'a' is recommended) indicates that OBS1 and OBS2 are azimuth
*     (north zero, east 90 deg) and zenith distance.  (Zenith distance
*     is used rather than elevation in order to reflect the fact that
*     no allowance is made for depression of the horizon.)
*
*     - The accuracy of the result is limited by the corrections for
*     refraction.  Providing the meteorological parameters are
*     known accurately and there are no gross local effects, the
*     predicted apparent RA,Dec should be within about 0.1 arcsec
*     for a zenith distance of less than 70 degrees.  Even at a
*     topocentric zenith distance of 90 degrees, the accuracy in
*     elevation should be better than 1 arcmin;  useful results
*     are available for a further 3 degrees, beyond which the
*     palREFRO routine returns a fixed value of the refraction.
*     The complementary routines palAop (or palAopqk) and palOap
*     (or palOapqk) are self-consistent to better than 1 micro-
*     arcsecond all over the celestial sphere.
*
*     - It is advisable to take great care with units, as even
*     unlikely values of the input parameters are accepted and
*     processed in accordance with the models used.
*
*     - "Observed" Az,El means the position that would be seen by a
*     perfect theodolite located at the observer.  This is
*     related to the observed HA,Dec via the standard rotation, using
*     the geodetic latitude (corrected for polar motion), while the
*     observed HA and RA are related simply through the local
*     apparent ST.  "Observed" RA,Dec or HA,Dec thus means the
*     position that would be seen by a perfect equatorial located
*     at the observer and with its polar axis aligned to the
*     Earth's axis of rotation (n.b. not to the refracted pole).
*     By removing from the observed place the effects of
*     atmospheric refraction and diurnal aberration, the
*     geocentric apparent RA,Dec is obtained.
*
*     - Frequently, mean rather than apparent RA,Dec will be required,
*     in which case further transformations will be necessary.  The
*     palAmp etc routines will convert the apparent RA,Dec produced
*     by the present routine into an "FK5" (J2000) mean place, by
*     allowing for the Sun's gravitational lens effect, annual
*     aberration, nutation and precession.  Should "FK4" (1950)
*     coordinates be needed, the routines palFk524 etc will also
*     need to be applied.
*
*     - To convert to apparent RA,Dec the coordinates read from a
*     real telescope, corrections would have to be applied for
*     encoder zero points, gear and encoder errors, tube flexure,
*     the position of the rotator axis and the pointing axis
*     relative to it, non-perpendicularity between the mounting
*     axes, and finally for the tilt of the azimuth or polar axis
*     of the mounting (with appropriate corrections for mount
*     flexures).  Some telescopes would, of course, exhibit other
*     properties which would need to be accounted for at the
*     appropriate point in the sequence.
*
*     - The star-independent apparent-to-observed-place parameters
*     in AOPRMS may be computed by means of the palAoppa routine.
*     If nothing has changed significantly except the time, the
*     palAoppat routine may be used to perform the requisite
*     partial recomputation of AOPRMS.
*
*     - The azimuths etc used by the present routine are with respect
*     to the celestial pole.  Corrections from the terrestrial pole
*     can be computed using palPolmo.


*  History:
*     2012-08-27 (TIMJ):
*        Initial version, direct copy of Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palOapqk(const char *type,
              double ob1,
              double ob2,
              const double aoprms[14],
              double *rap,
              double *dap) {

    /*  breakpoint for fast/slow refraction algorithm:
     *  zd greater than arctan(4), (see sla_refco routine)
     *  or vector z less than cosine(arctan(z)) = 1/sqrt(17) */
    const double zbreak = 0.242535625;

    char c;
    double c1, c2, sphi, cphi, st, ce, xaeo, yaeo, zaeo, v[3], xmhdo, ymhdo,
        zmhdo, az, sz, zdo, tz, dref, zdt, xaet, yaet, zaet, xmhda, ymhda,
        zmhda, diurab, f, hma;

    /*  coordinate type */
    c = type[0];

    /*  coordinates */
    c1 = ob1;
    c2 = ob2;

    /*  sin, cos of latitude */
    sphi = aoprms[1];
    cphi = aoprms[2];

    /*  local apparent sidereal time */
    st = aoprms[13];

    /*  standardise coordinate type */
    if (c == 'r' || c == 'R') {
        c = 'r';
    } else if (c == 'h' || c == 'H') {
        c = 'h';
    } else {
        c = 'a';
    }

    /*  if az,zd convert to cartesian (s=0,e=90) */
    if (c == 'a') {
        ce = sin(c2);
        xaeo = -cos(c1) * ce;
        yaeo = sin(c1) * ce;
        zaeo = cos(c2);
    } else {

        /*     if ra,dec convert to ha,dec */
        if (c == 'r') {
            c1 = st - c1;
        }

        /*     to cartesian -ha,dec */
        palDcs2c(-c1, c2, v);
        xmhdo = v[0];
        ymhdo = v[1];
        zmhdo = v[2];

        /*     to cartesian az,el (s=0,e=90) */
        xaeo = sphi * xmhdo - cphi * zmhdo;
        yaeo = ymhdo;
        zaeo = cphi * xmhdo + sphi * zmhdo;
    }

    /*  azimuth (s=0,e=90) */
    if (xaeo != 0.0 || yaeo != 0.0) {
        az = atan2(yaeo, xaeo);
    } else {
        az = 0.0;
    }

    /*  sine of observed zd, and observed zd */
    sz = sqrt(xaeo * xaeo + yaeo * yaeo);
    zdo = atan2(sz, zaeo);

    /*
     *  refraction
     *  ---------- */

    /*  large zenith distance? */
    if (zaeo >= zbreak) {

        /*     fast algorithm using two constant model */
        tz = sz / zaeo;
        dref = (aoprms[10] + aoprms[11] * tz * tz) * tz;

    } else {

        /*     rigorous algorithm for large zd */
        palRefro(zdo,
                 aoprms[4],
                 aoprms[5],
                 aoprms[6],
                 aoprms[7],
                 aoprms[8],
                 aoprms[0],
                 aoprms[9],
                 1e-8,
                 &dref);
    }

    zdt = zdo + dref;

    /*  to cartesian az,zd */
    ce = sin(zdt);
    xaet = cos(az) * ce;
    yaet = sin(az) * ce;
    zaet = cos(zdt);

    /*  cartesian az,zd to cartesian -ha,dec */
    xmhda = sphi * xaet + cphi * zaet;
    ymhda = yaet;
    zmhda = -cphi * xaet + sphi * zaet;

    /*  diurnal aberration */
    diurab = -aoprms[3];
    f = (1.0 - diurab * ymhda);
    v[0] = f * xmhda;
    v[1] = f * (ymhda + diurab);
    v[2] = f * zmhda;

    /*  to spherical -ha,dec */
    palDcc2s(v, &hma, dap);

    /*  Right Ascension */
    *rap = palDranrm(st + hma);
}
/*
*+
*  Name:
*     palObs

*  Purpose:
*     Parameters of selected ground-based observing stations

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     int palObs( size_t n, const char * c,
*                 char * ident, size_t identlen,
*                 char * name, size_t namelen,
*                 double * w, double * p, double * h );

*  Arguments:
*     n = size_t (Given)
*         Number specifying the observing station. If 0
*         the identifier in "c" is used to determine the
*         observing station to use.
*     c = const char * (Given)
*         Identifier specifying the observing station for
*         which the parameters should be returned. Only used
*         if n is 0. Can be nullptr for n>0. Case insensitive.
*     ident = char * (Returned)
*         Identifier of the observing station selected. Will be
*         identical to "c" if n==0. Unchanged if "n" or "c"
*         do not match an observing station. Should be at least
*         11 characters (including the trailing nul).
*     identlen = size_t (Given)
*         Size of the buffer "ident" including trailing nul.
*     name = char * (Returned)
*         Full name of the specified observing station. Contains "?"
*         if "n" or "c" did not correspond to a valid station. Should
*         be at least 41 characters (including the trailing nul).
*     w = double * (Returned)
*         Longitude (radians, West +ve). Unchanged if observing
*         station could not be identified.
*     p = double * (Returned)
*         Geodetic latitude (radians, North +ve). Unchanged if observing
*         station could not be identified.
*     h = double * (Returned)
*         Height above sea level (metres). Unchanged if observing
*         station could not be identified.

*  Returned Value:
*     palObs = int
*         0 if an observing station was returned. -1 if no match was
*         found.

*  Description:
*     Station numbers, identifiers, names and other details are
*     subject to change and should not be hardwired into
*     application programs.
*
*     All characters in "c" up to the first space are
*     checked;  thus an abbreviated ID will return the parameters
*     for the first station in the list which matches the
*     abbreviation supplied, and no station in the list will ever
*     contain embedded spaces. "c" must not have leading spaces.
*
*     IMPORTANT -- BEWARE OF THE LONGITUDE SIGN CONVENTION.  The
*     longitude returned by sla_OBS is west-positive in accordance
*     with astronomical usage.  However, this sign convention is
*     left-handed and is the opposite of the one used by geographers;
*     elsewhere in PAL the preferable east-positive convention is
*     used.  In particular, note that for use in palAop, palAoppa
*     and palOap the sign of the longitude must be reversed.
*
*     Users are urged to inform the author of any improvements
*     they would like to see made.  For example:
*
*         typographical corrections
*         more accurate parameters
*         better station identifiers or names
*         additional stations


*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Differs from the SLA interface in that the output short name
*       is not the same variable as the input short name. This simplifies
*       consting. Additionally the size of the output buffers are now
*       specified in the API and a status integer is returned.

*  History:
*     2012-03-06 (TIMJ):
*        Initial version containing entries from SLA/F as of 15 March 2002
*        with a 2008 tweak to the JCMT GPS position.
*        Adapted with permission from the Fortran SLALIB library.
*     2014-04-08 (TIMJ):
*        Add APEX and NANTEN2
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2002 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     Copyright (C) 2014 Cornell University.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

/* We prefer to use the starutil package. */
#if HAVE_STAR_UTIL_H
#include "star/util.h"
#else
/* This version is just a straight copy without putting ellipsis on the end. */
static void star__strellcpy(char *dest, const char *src, size_t size) {
#if HAVE_STRLCPY
    strlcpy(dest, src, size);
#else
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
#endif
}

#define star_strellcpy(dest, src, size) star__strellcpy(dest, src, size)
#endif

#include "pal.h"
#include "palmac.h"

/* Helper macros to convert degrees to radians in longitude and latitude */
#define WEST(ID, IAM, AS) \
    PAL__DAS2R *((60.0 * (60.0 * (double)ID + (double)IAM)) + (double)AS)
#define NORTH(ID, IAM, AS) WEST(ID, IAM, AS)
#define EAST(ID, IAM, AS) -1.0 * WEST(ID, IAM, AS)
#define SOUTH(ID, IAM, AS) -1.0 * WEST(ID, IAM, AS)

struct telData {
    double w;
    double p;
    double h;
    char shortname[11];
    char longname[41];
};

int palObs(size_t n,
           const char *c,
           char *ident,
           size_t identlen,
           char *name,
           size_t namelen,
           double *w,
           double *p,
           double *h) {
    const struct telData telData[] = {
        /* AAT (Observer's Guide) AAT */
        {EAST(149, 3, 57.91),
         SOUTH(31, 16, 37.34),
         1164E0,
         "AAT",
         "Anglo-Australian 3.9m Telescope"},
        /* WHT (Gemini, April 1987) LPO4.2 */
        {WEST(17, 52, 53.9),
         NORTH(28, 45, 38.1),
         2332E0,
         "LPO4.2",
         "William Herschel 4.2m Telescope"},
        /* INT (Gemini, April 1987) LPO2.5 */
        {WEST(17, 52, 39.5),
         NORTH(28, 45, 43.2),
         2336E0,
         "LPO2.5",
         "Isaac Newton 2.5m Telescope"},
        /* JKT (Gemini, April 1987) LPO1 */
        {WEST(17, 52, 41.2),
         NORTH(28, 45, 39.9),
         2364E0,
         "LPO1",
         "Jacobus Kapteyn 1m Telescope"},
        /* Lick 120" (S.L.Allen, private communication, 2002) LICK120 */
        {WEST(121, 38, 13.689),
         NORTH(37, 20, 34.931),
         1286E0,
         "LICK120",
         "Lick 120 inch"},
        /* MMT 6.5m conversion (MMT Observatory website) MMT */
        {WEST(110, 53, 4.4),
         NORTH(31, 41, 19.6),
         2608E0,
         "MMT",
         "MMT 6.5m, Mt Hopkins"},
        /* Victoria B.C. 1.85m (1984 Almanac) DAO72 */
        {WEST(123, 25, 1.18),
         NORTH(48, 31, 11.9),
         238E0,
         "DAO72",
         "DAO Victoria BC 1.85 metre"},
        /* Las Campanas (1983 Almanac) DUPONT */
        {WEST(70, 42, 9.),
         SOUTH(29, 0, 11.),
         2280E0,
         "DUPONT",
         "Du Pont 2.5m Telescope, Las Campanas"},
        /* Mt Hopkins 1.5m (1983 Almanac) MTHOP1.5 */
        {WEST(110, 52, 39.00),
         NORTH(31, 40, 51.4),
         2344E0,
         "MTHOP1.5",
         "Mt Hopkins 1.5 metre"},
        /* Mt Stromlo 74" (1983 Almanac) STROMLO74 */
        {EAST(149, 0, 27.59),
         SOUTH(35, 19, 14.3),
         767E0,
         "STROMLO74",
         "Mount Stromlo 74 inch"},
        /* ANU 2.3m, SSO (Gary Hovey) ANU2.3 */
        {EAST(149, 3, 40.3),
         SOUTH(31, 16, 24.1),
         1149E0,
         "ANU2.3",
         "Siding Spring 2.3 metre"},
        /* Greenbank 140' (1983 Almanac) GBVA140 */
        {WEST(79, 50, 9.61),
         NORTH(38, 26, 15.4),
         881E0,
         "GBVA140",
         "Greenbank 140 foot"},
        /* Cerro Tololo 4m (1982 Almanac) TOLOLO4M */
        {WEST(70, 48, 53.6),
         SOUTH(30, 9, 57.8),
         2235E0,
         "TOLOLO4M",
         "Cerro Tololo 4 metre"},
        /* Cerro Tololo 1.5m (1982 Almanac) TOLOLO1.5M */
        {WEST(70, 48, 54.5),
         SOUTH(30, 9, 56.3),
         2225E0,
         "TOLOLO1.5M",
         "Cerro Tololo 1.5 metre"},
        /* Tidbinbilla 64m (1982 Almanac) TIDBINBLA */
        {EAST(148, 58, 48.20),
         SOUTH(35, 24, 14.3),
         670E0,
         "TIDBINBLA",
         "Tidbinbilla 64 metre"},
        /* Bloemfontein 1.52m (1981 Almanac) BLOEMF */
        {EAST(26, 24, 18.),
         SOUTH(29, 2, 18.),
         1387E0,
         "BLOEMF",
         "Bloemfontein 1.52 metre"},
        /* Bosque Alegre 1.54m (1981 Almanac) BOSQALEGRE */
        {WEST(64, 32, 48.0),
         SOUTH(31, 35, 53.),
         1250E0,
         "BOSQALEGRE",
         "Bosque Alegre 1.54 metre"},
        /* USNO 61" astrographic reflector, Flagstaff (1981 Almanac)
           FLAGSTF61 */
        {WEST(111, 44, 23.6),
         NORTH(35, 11, 2.5),
         2316E0,
         "FLAGSTF61",
         "USNO 61 inch astrograph, Flagstaff"},
        /* Lowell 72" (1981 Almanac) LOWELL72 */
        {WEST(111, 32, 9.3),
         NORTH(35, 5, 48.6),
         2198E0,
         "LOWELL72",
         "Perkins 72 inch, Lowell"},
        /* Harvard 1.55m (1981 Almanac) HARVARD */
        {WEST(71, 33, 29.32),
         NORTH(42, 30, 19.0),
         185E0,
         "HARVARD",
         "Harvard College Observatory 1.55m"},
        /* Okayama 1.88m (1981 Almanac) OKAYAMA */
        {EAST(133, 35, 47.29),
         NORTH(34, 34, 26.1),
         372E0,
         "OKAYAMA",
         "Okayama 1.88 metre"},
        /* Kitt Peak Mayall 4m (1981 Almanac) KPNO158 */
        {WEST(111, 35, 57.61),
         NORTH(31, 57, 50.3),
         2120E0,
         "KPNO158",
         "Kitt Peak 158 inch"},
        /* Kitt Peak 90 inch (1981 Almanac) KPNO90 */
        {WEST(111, 35, 58.24),
         NORTH(31, 57, 46.9),
         2071E0,
         "KPNO90",
         "Kitt Peak 90 inch"},
        /* Kitt Peak 84 inch (1981 Almanac) KPNO84 */
        {WEST(111, 35, 51.56),
         NORTH(31, 57, 29.2),
         2096E0,
         "KPNO84",
         "Kitt Peak 84 inch"},
        /* Kitt Peak 36 foot (1981 Almanac) KPNO36FT */
        {WEST(111, 36, 51.12),
         NORTH(31, 57, 12.1),
         1939E0,
         "KPNO36FT",
         "Kitt Peak 36 foot"},
        /* Kottamia 74" (1981 Almanac) KOTTAMIA */
        {EAST(31, 49, 30.),
         NORTH(29, 55, 54.),
         476E0,
         "KOTTAMIA",
         "Kottamia 74 inch"},
        /* La Silla 3.6m (1981 Almanac) ESO3.6 */
        {WEST(70, 43, 36.),
         SOUTH(29, 15, 36.),
         2428E0,
         "ESO3.6",
         "ESO 3.6 metre"},
        /* Mauna Kea 88 inch MAUNAK88 */
        /* (IfA website, Richard Wainscoat) */
        {WEST(155, 28, 9.96),
         NORTH(19, 49, 22.77),
         4213.6E0,
         "MAUNAK88",
         "Mauna Kea 88 inch"},
        /* UKIRT (IfA website, Richard Wainscoat) UKIRT */
        {WEST(155, 28, 13.18),
         NORTH(19, 49, 20.75),
         4198.5E0,
         "UKIRT",
         "UK Infra Red Telescope"},
        /* Quebec 1.6m (1981 Almanac) QUEBEC1.6 */
        {WEST(71, 9, 9.7),
         NORTH(45, 27, 20.6),
         1114E0,
         "QUEBEC1.6",
         "Quebec 1.6 metre"},
        /* Mt Ekar 1.82m (1981 Almanac) MTEKAR */
        {EAST(11, 34, 15.),
         NORTH(45, 50, 48.),
         1365E0,
         "MTEKAR",
         "Mt Ekar 1.82 metre"},
        /* Mt Lemmon 60" (1981 Almanac) MTLEMMON60 */
        {WEST(110, 42, 16.9),
         NORTH(32, 26, 33.9),
         2790E0,
         "MTLEMMON60",
         "Mt Lemmon 60 inch"},
        /* Mt Locke 2.7m (1981 Almanac) MCDONLD2.7 */
        {WEST(104, 1, 17.60),
         NORTH(30, 40, 17.7),
         2075E0,
         "MCDONLD2.7",
         "McDonald 2.7 metre"},
        /* Mt Locke 2.1m (1981 Almanac) MCDONLD2.1 */
        {WEST(104, 1, 20.10),
         NORTH(30, 40, 17.7),
         2075E0,
         "MCDONLD2.1",
         "McDonald 2.1 metre"},
        /* Palomar 200" (1981 Almanac) PALOMAR200 */
        {WEST(116, 51, 50.),
         NORTH(33, 21, 22.),
         1706E0,
         "PALOMAR200",
         "Palomar 200 inch"},
        /* Palomar 60" (1981 Almanac) PALOMAR60 */
        {WEST(116, 51, 31.),
         NORTH(33, 20, 56.),
         1706E0,
         "PALOMAR60",
         "Palomar 60 inch"},
        /* David Dunlap 74" (1981 Almanac) DUNLAP74 */
        {WEST(79, 25, 20.),
         NORTH(43, 51, 46.),
         244E0,
         "DUNLAP74",
         "David Dunlap 74 inch"},
        /* Haute Provence 1.93m (1981 Almanac) HPROV1.93 */
        {EAST(5, 42, 46.75),
         NORTH(43, 55, 53.3),
         665E0,
         "HPROV1.93",
         "Haute Provence 1.93 metre"},
        /* Haute Provence 1.52m (1981 Almanac) HPROV1.52 */
        {EAST(5, 42, 43.82),
         NORTH(43, 56, 0.2),
         667E0,
         "HPROV1.52",
         "Haute Provence 1.52 metre"},
        /* San Pedro Martir 83" (1981 Almanac) SANPM83 */
        {WEST(115, 27, 47.),
         NORTH(31, 2, 38.),
         2830E0,
         "SANPM83",
         "San Pedro Martir 83 inch"},
        /* Sutherland 74" (1981 Almanac) SAAO74 */
        {EAST(20, 48, 44.3),
         SOUTH(32, 22, 43.4),
         1771E0,
         "SAAO74",
         "Sutherland 74 inch"},
        /* Tautenburg 2m (1981 Almanac) TAUTNBG */
        {EAST(11, 42, 45.),
         NORTH(50, 58, 51.),
         331E0,
         "TAUTNBG",
         "Tautenburg 2 metre"},
        /* Catalina 61" (1981 Almanac) CATALINA61 */
        {WEST(110, 43, 55.1),
         NORTH(32, 25, 0.7),
         2510E0,
         "CATALINA61",
         "Catalina 61 inch"},
        /* Steward 90" (1981 Almanac) STEWARD90 */
        {WEST(111, 35, 58.24),
         NORTH(31, 57, 46.9),
         2071E0,
         "STEWARD90",
         "Steward 90 inch"},
        /* Russian 6m (1981 Almanac) USSR6 */
        {EAST(41, 26, 30.0),
         NORTH(43, 39, 12.),
         2100E0,
         "USSR6",
         "USSR 6 metre"},
        /* Arecibo 1000' (1981 Almanac) ARECIBO */
        {WEST(66, 45, 11.1),
         NORTH(18, 20, 36.6),
         496E0,
         "ARECIBO",
         "Arecibo 1000 foot"},
        /* Cambridge 5km (1981 Almanac) CAMB5KM */
        {EAST(0, 2, 37.23),
         NORTH(52, 10, 12.2),
         17E0,
         "CAMB5KM",
         "Cambridge 5km"},
        /* Cambridge 1 mile (1981 Almanac) CAMB1MILE */
        {EAST(0, 2, 21.64),
         NORTH(52, 9, 47.3),
         17E0,
         "CAMB1MILE",
         "Cambridge 1 mile"},
        /* Bonn 100m (1981 Almanac) EFFELSBERG */
        {EAST(6, 53, 1.5),
         NORTH(50, 31, 28.6),
         366E0,
         "EFFELSBERG",
         "Effelsberg 100 metre"},
        /* Greenbank 300' (1981 Almanac)                        GBVA300
           (R.I.P.) */
        {WEST(79, 50, 56.36),
         NORTH(38, 25, 46.3),
         894E0,
         "(R.I.P.)",
         "Greenbank 300 foot"},
        /* Jodrell Bank Mk 1 (1981 Almanac) JODRELL1 */
        {WEST(2, 18, 25.),
         NORTH(53, 14, 10.5),
         78E0,
         "JODRELL1",
         "Jodrell Bank 250 foot"},
        /* Australia Telescope Parkes Observatory PARKES */
        /* (Peter te Lintel Hekkert) */
        {EAST(148, 15, 44.3591),
         SOUTH(32, 59, 59.8657),
         391.79E0,
         "PARKES",
         "Parkes 64 metre"},
        /* VLA (1981 Almanac) VLA */
        {WEST(107, 37, 3.82),
         NORTH(34, 4, 43.5),
         2124E0,
         "VLA",
         "Very Large Array"},
        /* Sugar Grove 150' (1981 Almanac) SUGARGROVE */
        {WEST(79, 16, 23.),
         NORTH(38, 31, 14.),
         705E0,
         "SUGARGROVE",
         "Sugar Grove 150 foot"},
        /* Russian 600' (1981 Almanac) USSR600 */
        {EAST(41, 35, 25.5),
         NORTH(43, 49, 32.),
         973E0,
         "USSR600",
         "USSR 600 foot"},
        /* Nobeyama 45 metre mm dish (based on 1981 Almanac entry) NOBEYAMA
         */
        {EAST(138, 29, 12.),
         NORTH(35, 56, 19.),
         1350E0,
         "NOBEYAMA",
         "Nobeyama 45 metre"},
        /* James Clerk Maxwell 15 metre mm telescope, Mauna Kea JCMT */
        /* From GPS measurements on 11Apr2007 for eSMA setup (R. Tilanus) */
        {WEST(155, 28, 37.30),
         NORTH(19, 49, 22.22),
         4124.75E0,
         "JCMT",
         "JCMT 15 metre"},
        /* ESO 3.5 metre NTT, La Silla (K.Wirenstrand) ESONTT */
        {WEST(70, 43, 7.),
         SOUTH(29, 15, 30.),
         2377E0,
         "ESONTT",
         "ESO 3.5 metre NTT"},
        /* St Andrews University Observatory (1982 Almanac) ST.ANDREWS */
        {WEST(2, 48, 52.5),
         NORTH(56, 20, 12.),
         30E0,
         "ST.ANDREWS",
         "St Andrews"},
        /* Apache Point 3.5 metre (R.Owen) APO3.5 */
        {WEST(105, 49, 11.56),
         NORTH(32, 46, 48.96),
         2809E0,
         "APO3.5",
         "Apache Point 3.5m"},
        /* W.M.Keck Observatory, Telescope 1 KECK1 */
        /* (William Lupton) */
        {WEST(155, 28, 28.99),
         NORTH(19, 49, 33.41),
         4160E0,
         "KECK1",
         "Keck 10m Telescope #1"},
        /* Tautenberg Schmidt (1983 Almanac) TAUTSCHM */
        {EAST(11, 42, 45.0),
         NORTH(50, 58, 51.0),
         331E0,
         "TAUTSCHM",
         "Tautenberg 1.34 metre Schmidt"},
        /* Palomar Schmidt (1981 Almanac) PALOMAR48 */
        {WEST(116, 51, 32.0),
         NORTH(33, 21, 26.0),
         1706E0,
         "PALOMAR48",
         "Palomar 48-inch Schmidt"},
        /* UK Schmidt, Siding Spring (1983 Almanac) UKST */
        {EAST(149, 4, 12.8),
         SOUTH(31, 16, 27.8),
         1145E0,
         "UKST",
         "UK 1.2 metre Schmidt, Siding Spring"},
        /* Kiso Schmidt, Japan (1981 Almanac) KISO */
        {EAST(137, 37, 42.2),
         NORTH(35, 47, 38.7),
         1130E0,
         "KISO",
         "Kiso 1.05 metre Schmidt, Japan"},
        /* ESO Schmidt, La Silla (1981 Almanac) ESOSCHM */
        {WEST(70, 43, 46.5),
         SOUTH(29, 15, 25.8),
         2347E0,
         "ESOSCHM",
         "ESO 1 metre Schmidt, La Silla"},
        /* Australia Telescope Compact Array ATCA */
        /* (WGS84 coordinates of Station 35, Mark Calabretta) */
        {EAST(149, 33, 0.500),
         SOUTH(30, 18, 46.385),
         236.9E0,
         "ATCA",
         "Australia Telescope Compact Array"},
        /* Australia Telescope Mopra Observatory MOPRA */
        /* (Peter te Lintel Hekkert) */
        {EAST(149, 5, 58.732),
         SOUTH(31, 16, 4.451),
         850E0,
         "MOPRA",
         "ATNF Mopra Observatory"},
        /* Subaru telescope, Mauna Kea SUBARU */
        /* (IfA website, Richard Wainscoat) */
        {WEST(155, 28, 33.67),
         NORTH(19, 49, 31.81),
         4163E0,
         "SUBARU",
         "Subaru 8m telescope"},
        /* Canada-France-Hawaii Telescope, Mauna Kea CFHT */
        /* (IfA website, Richard Wainscoat) */
        {WEST(155, 28, 7.95),
         NORTH(19, 49, 30.91),
         4204.1E0,
         "CFHT",
         "Canada-France-Hawaii 3.6m Telescope"},
        /* W.M.Keck Observatory, Telescope 2 KECK2 */
        /* (William Lupton) */
        {WEST(155, 28, 27.24),
         NORTH(19, 49, 35.62),
         4159.6E0,
         "KECK2",
         "Keck 10m Telescope #2"},
        /* Gemini North, Mauna Kea GEMININ */
        /* (IfA website, Richard Wainscoat) */
        {WEST(155, 28, 8.57),
         NORTH(19, 49, 25.69),
         4213.4E0,
         "GEMININ",
         "Gemini North 8-m telescope"},
        /* Five College Radio Astronomy Observatory FCRAO */
        /* (Tim Jenness) */
        {WEST(72, 20, 42.0),
         NORTH(42, 23, 30.0),
         314E0,
         "FCRAO",
         "Five College Radio Astronomy Obs"},
        /* NASA Infra Red Telescope Facility IRTF */
        /* (IfA website, Richard Wainscoat) */
        {WEST(155, 28, 19.20),
         NORTH(19, 49, 34.39),
         4168.1E0,
         "IRTF",
         "NASA IR Telescope Facility, Mauna Kea"},
        /* Caltech Submillimeter Observatory CSO */
        /* (IfA website, Richard Wainscoat; height estimated) */
        {WEST(155, 28, 31.79),
         NORTH(19, 49, 20.78),
         4080E0,
         "CSO",
         "Caltech Sub-mm Observatory, Mauna Kea"},
        /* ESO VLT, UT1 VLT1 */
        /* (ESO website, VLT Whitebook Chapter 2) */
        {WEST(70, 24, 11.642),
         SOUTH(24, 37, 33.117),
         2635.43,
         "VLT1",
         "ESO VLT, Paranal, Chile: UT1"},
        /* ESO VLT, UT2 VLT2 */
        /* (ESO website, VLT Whitebook Chapter 2) */
        {WEST(70, 24, 10.855),
         SOUTH(24, 37, 31.465),
         2635.43,
         "VLT2",
         "ESO VLT, Paranal, Chile: UT2"},
        /* ESO VLT, UT3 VLT3 */
        /* (ESO website, VLT Whitebook Chapter 2) */
        {WEST(70, 24, 9.896),
         SOUTH(24, 37, 30.300),
         2635.43,
         "VLT3",
         "ESO VLT, Paranal, Chile: UT3"},
        /* ESO VLT, UT4 VLT4 */
        /* (ESO website, VLT Whitebook Chapter 2) */
        {WEST(70, 24, 8.000),
         SOUTH(24, 37, 31.000),
         2635.43,
         "VLT4",
         "ESO VLT, Paranal, Chile: UT4"},
        /* Gemini South, Cerro Pachon GEMINIS */
        /* (GPS readings by Patrick Wallace) */
        {WEST(70, 44, 11.5),
         SOUTH(30, 14, 26.7),
         2738E0,
         "GEMINIS",
         "Gemini South 8-m telescope"},
        /* Cologne Observatory for Submillimeter Astronomy (KOSMA) KOSMA3M */
        /* (Holger Jakob) */
        {EAST(7, 47, 3.48),
         NORTH(45, 58, 59.772),
         3141E0,
         "KOSMA3M",
         "KOSMA 3m telescope, Gornergrat"},
        /* Magellan 1, 6.5m telescope at Las Campanas, Chile MAGELLAN1 */
        /* (Skip Schaller) */
        {WEST(70, 41, 31.9),
         SOUTH(29, 0, 51.7),
         2408E0,
         "MAGELLAN1",
         "Magellan 1, 6.5m, Las Campanas"},
        /* Magellan 2, 6.5m telescope at Las Campanas, Chile MAGELLAN2 */
        /* (Skip Schaller) */
        {WEST(70, 41, 33.5),
         SOUTH(29, 0, 50.3),
         2408E0,
         "MAGELLAN2",
         "Magellan 2, 6.5m, Las Campanas"},
        /* APEX - Atacama Pathfinder EXperiment, Llano de Chajnantor    APEX */
        /* (APEX web site) */
        {WEST(67, 45, 33.0),
         SOUTH(23, 0, 20.8),
         5105E0,
         "APEX",
         "APEX 12m telescope, Llano de Chajnantor"},
        /* NANTEN2 Submillimeter Observatory, 4m telescope Atacame desert
           NANTEN2 */
        /* (NANTEN2 web site) */
        {WEST(67, 42, 8.0),
         SOUTH(22, 57, 47.0),
         4865E0,
         "NANTEN2",
         "NANTEN2 4m telescope, Pampa la Bola"}};

    int retval = -1; /* Return status. 0 if found. -1 if no match */

    /* Work out the number of telescopes */
    const size_t NTEL = sizeof(telData) / sizeof(struct telData);

    /* Prefill the return buffer in a pessimistic manner */
    star_strellcpy(name, "?", namelen);

    if (n > 0) {
        if (n <= NTEL) {
            /* Index into telData with correction for zero-based indexing */
            struct telData thistel;
            thistel = telData[n - 1];
            *w = thistel.w;
            *p = thistel.p;
            *h = thistel.h;
            star_strellcpy(ident, thistel.shortname, identlen);
            star_strellcpy(name, thistel.longname, namelen);
            retval = 0;
        }

    } else {
        /* Searching */
        size_t i;
        for (i = 0; i < NTEL; i++) {
            struct telData thistel = telData[i];
            if (strcasecmp(c, thistel.shortname) == 0) {
                /* a match */
                *w = thistel.w;
                *p = thistel.p;
                *h = thistel.h;
                star_strellcpy(ident, thistel.shortname, identlen);
                star_strellcpy(name, thistel.longname, namelen);
                retval = 0;
                break;
            }
        }
    }

    return retval;
}
/*
*+
*  Name:
*     palOne2One

*  Purpose:
*     File containing simple PAL wrappers for SLA routines that are identical in
SOFA

*  Invocation:
*     Matches SLA API

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Description:
*     Some SOFA routines are identical to their SLA counterparts. PAL provides
*     direct counterparts to these although it is generally a better idea to
*     use the SOFA routine directly in new code.
*
*     The PAL routines with direct equivalents in SOFA are:
*     - palCldj
*     - palDbear
*     - palDaf2r
*     - palDav2m
*     - palDcc2s
*     - palDcs2c
*     - palDd2tf
*     - palDimxv
*     - palDm2av
*     - palDjcl
*     - palDmxm
*     - palDmxv
*     - palDpav
*     - palDr2af
*     - palDr2tf
*     - palDranrm
*     - palDsep
*     - palDsepv
*     - palDtf2d
*     - palDtf2r
*     - palDvdv
*     - palDvn
*     - palDvxv
*     - palEpb
*     - palEpb2d
*     - palEpj
*     - palEpj2d
*     - palEqeqx
*     - palFk5hz
*     - palGmst
*     - palGmsta
*     - palHfk5z
*     - palRefcoq

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     DSB: David S Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Do not call these functions from other PAL functions. Always use
*       the SOFA routines directly in new code.
*     - These are implemented as real functions rather than C preprocessor
*       macros so there may be a performance penalty in using the PAL
*       version instead of the SOFA version.
*     - Routines that take MJDs have SOFA equivalents that have an explicit
*       MJD offset included.
*     - palEqeqx, palGmst and palGmsta use the IAU 2006 precession model.

*  History:
*     2012-02-10 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     2012-03-23 (TIMJ):
*        Update prologue.
*     2012-05-09 (DSBJ):
*        Move palDrange into a separate file.
*     2014-07-15 (TIMJ):
*        SOFA now has palRefcoq equivalent.
*     {enter_further_changes_here}

*  Copyright:
*     Copyeight (C) 2014 Tim Jenness
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palCldj(int iy, int im, int id, double *djm, int *j) {
    double djm0;
    *j = eraCal2jd(iy, im, id, &djm0, djm);
}

double palDbear(double a1, double b1, double a2, double b2) {
    return eraPas(a1, b1, a2, b2);
}

/* Arguments differ slightly. Assumes that the sign is always positive
   and dealt with externally. */
void palDaf2r(int ideg, int iamin, double asec, double *rad, int *j) {
    *j = eraAf2a(' ', ideg, iamin, asec, rad);
}

void palDav2m(double axvec[3], double rmat[3][3]) {
    eraRv2m(axvec, rmat);
}

void palDcc2s(double v[3], double *a, double *b) {
    eraC2s(v, a, b);
}

void palDcs2c(double a, double b, double v[3]) {
    eraS2c(a, b, v);
}

void palDd2tf(int ndp, double days, char *sign, int ihmsf[4]) {
    eraD2tf(ndp, days, sign, ihmsf);
}

void palDimxv(double dm[3][3], double va[3], double vb[3]) {
    eraTrxp(dm, va, vb);
}

void palDm2av(double rmat[3][3], double axvec[3]) {
    eraRm2v(rmat, axvec);
}

/* Requires additional SLA MJD reference date */
void palDjcl(double djm, int *iy, int *im, int *id, double *fd, int *j) {
    *j = eraJd2cal(PAL__MJD0, djm, iy, im, id, fd);
}

void palDmxm(double a[3][3], double b[3][3], double c[3][3]) {
    eraRxr(a, b, c);
}

void palDmxv(double dm[3][3], double va[3], double vb[3]) {
    eraRxp(dm, va, vb);
}

double palDpav(double v1[3], double v2[3]) {
    return eraPap(v1, v2);
}

void palDr2af(int ndp, double angle, char *sign, int idmsf[4]) {
    eraA2af(ndp, angle, sign, idmsf);
}

void palDr2tf(int ndp, double angle, char *sign, int ihmsf[4]) {
    eraA2tf(ndp, angle, sign, ihmsf);
}

double palDranrm(double angle) {
    return eraAnp(angle);
}

double palDsep(double a1, double b1, double a2, double b2) {
    return eraSeps(a1, b1, a2, b2);
}

double palDsepv(double v1[3], double v2[3]) {
    return eraSepp(v1, v2);
}

/* Assumes that the sign is always positive and is dealt with externally */
void palDtf2d(int ihour, int imin, double sec, double *days, int *j) {
    *j = eraTf2d(' ', ihour, imin, sec, days);
}

/* Assumes that the sign is dealt with outside this routine */
void palDtf2r(int ihour, int imin, double sec, double *rad, int *j) {
    *j = eraTf2a(' ', ihour, imin, sec, rad);
}

double palDvdv(double va[3], double vb[3]) {
    return eraPdp(va, vb);
}

/* Note that the arguments are flipped */
void palDvn(double v[3], double uv[3], double *vm) {
    eraPn(v, vm, uv);
}

void palDvxv(double va[3], double vb[3], double vc[3]) {
    eraPxp(va, vb, vc);
}

/* Requires additional SLA MJD reference date */
double palEpb(double date) {
    return eraEpb(PAL__MJD0, date);
}

double palEpb2d(double epb) {
    double djm0, djm;
    eraEpb2jd(epb, &djm0, &djm);
    return djm;
}

/* Requires additional SLA MJD reference date */
double palEpj(double date) {
    return eraEpj(PAL__MJD0, date);
}

double palEpj2d(double epj) {
    double djm0, djm;
    eraEpj2jd(epj, &djm0, &djm);
    return djm;
}

/* Requires additional SLA MJD reference date */
double palEqeqx(double date) {
    return eraEe06a(PAL__MJD0, date);
}

/* Do not use palEvp just yet */

void palFk5hz(double r5, double d5, double epoch, double *rh, double *dh) {
    /* Need to convert epoch to Julian date first */
    double date1, date2;
    eraEpj2jd(epoch, &date1, &date2);
    eraFk5hz(r5, d5, date1, date2, rh, dh);
}

/* Note that SOFA has more accurate time arguments and we use the 2006
 * precession model */
double palGmst(double ut1) {
    return eraGmst06(PAL__MJD0, ut1, PAL__MJD0, ut1);
}

/* Slightly better but still not as accurate as SOFA */

double palGmsta(double date, double ut) {
    date += PAL__MJD0;
    return eraGmst06(date, ut, date, ut);
}

void palHfk5z(double rh,
              double dh,
              double epoch,
              double *r5,
              double *d5,
              double *dr5,
              double *dd5) {
    /* Need to convert epoch to Julian date first */
    double date1, date2;
    eraEpj2jd(epoch, &date1, &date2);
    eraHfk5z(rh, dh, date1, date2, r5, d5, dr5, dd5);
}

void palRefcoq(
    double tdk, double pmb, double rh, double wl, double *refa, double *refb) {
    /* Note that SLA (and therefore PAL) uses units of kelvin
       but SOFA uses deg C */
    eraRefco(pmb, tdk - 273.15, rh, wl, refa, refb);
}
/*
*+
*  Name:
*     palPa

*  Purpose:
*     HA, Dec to Parallactic Angle

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palPa( double ha, double dec, double phi );

*  Arguments:
*     ha = double (Given)
*        Hour angle in radians (Geocentric apparent)
*     dec = double (Given)
*        Declination in radians (Geocentric apparent)
*     phi = double (Given)
*        Observatory latitude in radians (geodetic)

*  Returned Value:
*     palPa = double
*        Parallactic angle in the range -pi to +pi.

*  Description:
*     Converts HA, Dec to Parallactic Angle.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The parallactic angle at a point in the sky is the position
*       angle of the vertical, i.e. the angle between the direction to
*       the pole and to the zenith.  In precise applications care must
*       be taken only to use geocentric apparent HA,Dec and to consider
*       separately the effects of atmospheric refraction and telescope
*       mount errors.
*     - At the pole a zero result is returned.

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

#include <math.h>

double palPa(double ha, double dec, double phi) {
    double cp, sqsz, cqsz;

    cp = cos(phi);
    sqsz = cp * sin(ha);
    cqsz = sin(phi) * cos(dec) - cp * sin(dec) * cos(ha);
    if (sqsz == 0.0 && cqsz == 0.0)
        cqsz = 1.0;
    return atan2(sqsz, cqsz);
}
/*
*+
*  Name:
*     palPertel

*  Purpose:
*     Update elements by applying planetary perturbations

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPertel (int jform, double date0, double date1,
*                     double epoch0, double orbi0, double anode0,
*                     double perih0, double aorq0, double e0, double am0,
*                     double *epoch1, double *orbi1, double *anode1,
*                     double *perih1, double *aorq1, double *e1, double *am1,
*                     int *jstat );

*  Arguments:
*     jform = int (Given)
*        Element set actually returned (1-3; Note 6)
*     date0 = double (Given)
*        Date of osculation (TT MJD) for the given elements.
*     date1 = double (Given)
*        Date of osculation (TT MJD) for the updated elements.
*     epoch0 = double (Given)
*        Epoch of elements (TT MJD)
*     orbi0 = double (Given)
*        inclination (radians)
*     anode0 = double (Given)
*        longitude of the ascending node (radians)
*     perih0 = double (Given)
*        longitude or argument of perihelion (radians)
*     aorq0 = double (Given)
*        mean distance or perihelion distance (AU)
*     e0 = double (Given)
*        eccentricity
*     am0 = double (Given)
*        mean anomaly (radians, JFORM=2 only)
*     epoch1 = double * (Returned)
*        Epoch of elements (TT MJD)
*     orbi1 = double * (Returned)
*        inclination (radians)
*     anode1 = double * (Returned)
*        longitude of the ascending node (radians)
*     perih1 = double * (Returned)
*        longitude or argument of perihelion (radians)
*     aorq1 = double * (Returned)
*        mean distance or perihelion distance (AU)
*     e1 = double * (Returned)
*        eccentricity
*     am1 = double * (Returned)
*        mean anomaly (radians, JFORM=2 only)
*     jstat = int * (Returned)
*        status:
*          -      +102 = warning, distant epoch
*          -      +101 = warning, large timespan ( > 100 years)
*          - +1 to +10 = coincident with planet (Note 6)
*          -         0 = OK
*          -        -1 = illegal JFORM
*          -        -2 = illegal E0
*          -        -3 = illegal AORQ0
*          -        -4 = internal error
*          -        -5 = numerical error

*  Description:
*     Update the osculating orbital elements of an asteroid or comet by
*     applying planetary perturbations.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Two different element-format options are available:
*
*       Option JFORM=2, suitable for minor planets:
*
*       EPOCH   = epoch of elements (TT MJD)
*       ORBI    = inclination i (radians)
*       ANODE   = longitude of the ascending node, big omega (radians)
*       PERIH   = argument of perihelion, little omega (radians)
*       AORQ    = mean distance, a (AU)
*       E       = eccentricity, e
*       AM      = mean anomaly M (radians)
*
*       Option JFORM=3, suitable for comets:
*
*       EPOCH   = epoch of perihelion (TT MJD)
*       ORBI    = inclination i (radians)
*       ANODE   = longitude of the ascending node, big omega (radians)
*       PERIH   = argument of perihelion, little omega (radians)
*       AORQ    = perihelion distance, q (AU)
*       E       = eccentricity, e
*
*     - DATE0, DATE1, EPOCH0 and EPOCH1 are all instants of time in
*       the TT timescale (formerly Ephemeris Time, ET), expressed
*       as Modified Julian Dates (JD-2400000.5).
*
*       DATE0 is the instant at which the given (i.e. unperturbed)
*       osculating elements are correct.
*
*       DATE1 is the specified instant at which the updated osculating
*       elements are correct.
*
*       EPOCH0 and EPOCH1 will be the same as DATE0 and DATE1
*       (respectively) for the JFORM=2 case, normally used for minor
*       planets.  For the JFORM=3 case, the two epochs will refer to
*       perihelion passage and so will not, in general, be the same as
*       DATE0 and/or DATE1 though they may be similar to one another.
*     - The elements are with respect to the J2000 ecliptic and equinox.
*     - Unused elements (AM0 and AM1 for JFORM=3) are not accessed.
*     - See the palPertue routine for details of the algorithm used.
*     - This routine is not intended to be used for major planets, which
*       is why JFORM=1 is not available and why there is no opportunity
*       to specify either the longitude of perihelion or the daily
*       motion.  However, if JFORM=2 elements are somehow obtained for a
*       major planet and supplied to the routine, sensible results will,
*       in fact, be produced.  This happens because the sla_PERTUE routine
*       that is called to perform the calculations checks the separation
*       between the body and each of the planets and interprets a
*       suspiciously small value (0.001 AU) as an attempt to apply it to
*       the planet concerned.  If this condition is detected, the
*       contribution from that planet is ignored, and the status is set to
*       the planet number (1-10 = Mercury, Venus, EMB, Mars, Jupiter,
*       Saturn, Uranus, Neptune, Earth, Moon) as a warning.
*
*  See Also:
*     - Sterne, Theodore E., "An Introduction to Celestial Mechanics",
*       Interscience Publishers Inc., 1960.  Section 6.7, p199.

*  History:
*     2012-03-12 (TIMJ):
*        Initial version direct conversion of SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palPertel(int jform,
               double date0,
               double date1,
               double epoch0,
               double orbi0,
               double anode0,
               double perih0,
               double aorq0,
               double e0,
               double am0,
               double *epoch1,
               double *orbi1,
               double *anode1,
               double *perih1,
               double *aorq1,
               double *e1,
               double *am1,
               int *jstat) {

    double u[13], dm;
    int j, jf;

    /*  Check that the elements are either minor-planet or comet format. */
    if (jform < 2 || jform > 3) {
        *jstat = -1;
        return;
    } else {

        /*     Provisionally set the status to OK. */
        *jstat = 0;
    }

    /*  Transform the elements from conventional to universal form. */
    palEl2ue(date0,
             jform,
             epoch0,
             orbi0,
             anode0,
             perih0,
             aorq0,
             e0,
             am0,
             0.0,
             u,
             &j);
    if (j != 0) {
        *jstat = j;
        return;
    }

    /*  Update the universal elements. */
    palPertue(date1, u, &j);
    if (j > 0) {
        *jstat = j;
    } else if (j < 0) {
        *jstat = -5;
        return;
    }

    /*  Transform from universal to conventional elements. */
    palUe2el(
        u, jform, &jf, epoch1, orbi1, anode1, perih1, aorq1, e1, am1, &dm, &j);
    if (jf != jform || j != 0)
        *jstat = -5;
}
/*
*+
*  Name:
*     palPertue

*  Purpose:
*     Update the universal elements by applying planetary perturbations

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPertue( double date, double u[13], int *jstat );

*  Arguments:
*     date = double (Given)
*        Final epoch (TT MJD) for the update elements.
*     u = const double [13] (Given & Returned)
*        Universal orbital elements (Note 1)
*            (0)  combined mass (M+m)
*            (1)  total energy of the orbit (alpha)
*            (2)  reference (osculating) epoch (t0)
*          (3-5)  position at reference epoch (r0)
*          (6-8)  velocity at reference epoch (v0)
*            (9)  heliocentric distance at reference epoch
*           (10)  r0.v0
*           (11)  date (t)
*           (12)  universal eccentric anomaly (psi) of date, approx
*     jstat = int * (Returned)
*        status:
*                   +102 = warning, distant epoch
*                   +101 = warning, large timespan ( > 100 years)
*              +1 to +10 = coincident with major planet (Note 5)
*                      0 = OK
*                     -1 = numerical error

*  Description:
*     Update the universal elements of an asteroid or comet by applying
*     planetary perturbations.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference 2).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - The universal elements are with respect to the J2000 equator and
*       equinox.
*     - The epochs DATE, U(3) and U(12) are all Modified Julian Dates
*       (JD-2400000.5).
*     - The algorithm is a simplified form of Encke's method.  It takes as
*       a basis the unperturbed motion of the body, and numerically
*       integrates the perturbing accelerations from the major planets.
*       The expression used is essentially Sterne's 6.7-2 (reference 1).
*       Everhart and Pitkin (reference 2) suggest rectifying the orbit at
*       each integration step by propagating the new perturbed position
*       and velocity as the new universal variables.  In the present
*       routine the orbit is rectified less frequently than this, in order
*       to gain a slight speed advantage.  However, the rectification is
*       done directly in terms of position and velocity, as suggested by
*       Everhart and Pitkin, bypassing the use of conventional orbital
*       elements.
*
*       The f(q) part of the full Encke method is not used.  The purpose
*       of this part is to avoid subtracting two nearly equal quantities
*       when calculating the "indirect member", which takes account of the
*       small change in the Sun's attraction due to the slightly displaced
*       position of the perturbed body.  A simpler, direct calculation in
*       double precision proves to be faster and not significantly less
*       accurate.
*
*       Apart from employing a variable timestep, and occasionally
*       "rectifying the orbit" to keep the indirect member small, the
*       integration is done in a fairly straightforward way.  The
*       acceleration estimated for the middle of the timestep is assumed
*       to apply throughout that timestep;  it is also used in the
*       extrapolation of the perturbations to the middle of the next
*       timestep, to predict the new disturbed position.  There is no
*       iteration within a timestep.
*
*       Measures are taken to reach a compromise between execution time
*       and accuracy.  The starting-point is the goal of achieving
*       arcsecond accuracy for ordinary minor planets over a ten-year
*       timespan.  This goal dictates how large the timesteps can be,
*       which in turn dictates how frequently the unperturbed motion has
*       to be recalculated from the osculating elements.
*
*       Within predetermined limits, the timestep for the numerical
*       integration is varied in length in inverse proportion to the
*       magnitude of the net acceleration on the body from the major
*       planets.
*
*       The numerical integration requires estimates of the major-planet
*       motions.  Approximate positions for the major planets (Pluto
*       alone is omitted) are obtained from the routine palPlanet.  Two
*       levels of interpolation are used, to enhance speed without
*       significantly degrading accuracy.  At a low frequency, the routine
*       palPlanet is called to generate updated position+velocity "state
*       vectors".  The only task remaining to be carried out at the full
*       frequency (i.e. at each integration step) is to use the state
*       vectors to extrapolate the planetary positions.  In place of a
*       strictly linear extrapolation, some allowance is made for the
*       curvature of the orbit by scaling back the radius vector as the
*       linear extrapolation goes off at a tangent.
*
*       Various other approximations are made.  For example, perturbations
*       by Pluto and the minor planets are neglected and relativistic
*       effects are not taken into account.
*
*       In the interests of simplicity, the background calculations for
*       the major planets are carried out en masse.  The mean elements and
*       state vectors for all the planets are refreshed at the same time,
*       without regard for orbit curvature, mass or proximity.
*
*       The Earth-Moon system is treated as a single body when the body is
*       distant but as separate bodies when closer to the EMB than the
*       parameter RNE, which incurs a time penalty but improves accuracy
*       for near-Earth objects.
*
*     - This routine is not intended to be used for major planets.
*       However, if major-planet elements are supplied, sensible results
*       will, in fact, be produced.  This happens because the routine
*       checks the separation between the body and each of the planets and
*       interprets a suspiciously small value (0.001 AU) as an attempt to
*       apply the routine to the planet concerned.  If this condition is
*       detected, the contribution from that planet is ignored, and the
*       status is set to the planet number (1-10 = Mercury, Venus, EMB,
*       Mars, Jupiter, Saturn, Uranus, Neptune, Earth, Moon) as a warning.

*  See Also:
*     - Sterne, Theodore E., "An Introduction to Celestial Mechanics",
*       Interscience Publishers Inc., 1960.  Section 6.7, p199.
*     - Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-12 (TIMJ):
*        Initial version direct conversion of SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     2012-06-21 (TIMJ):
*        Support a lack of copysign() function.
*     2012-06-22 (TIMJ):
*        Check __STDC_VERSION__
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

/* Use the config file if we have one, else look at
   compiler defines to see if we have C99 */
#if HAVE_CONFIG_H
#include <config.h>
#else
#ifdef __STDC_VERSION__
#if (__STDC_VERSION__ >= 199901L)
#define HAVE_COPYSIGN 1
#endif
#endif
#endif

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

/* copysign is C99 */
#if HAVE_COPYSIGN
#define COPYSIGN copysign
#else
#define COPYSIGN(a, b) DSIGN(a, b)
#endif

void palPertue(double date, double u[13], int *jstat) {

    /*  Distance from EMB at which Earth and Moon are treated separately */
    const double RNE = 1.0;

    /*  Coincidence with major planet distance */
    const double COINC = 0.0001;

    /*  Coefficient relating timestep to perturbing force */
    const double TSC = 1e-4;

    /*  Minimum and maximum timestep (days) */
    const double TSMIN = 0.01;
    const double TSMAX = 10.0;

    /*  Age limit for major-planet state vector (days) */
    const double AGEPMO = 5.0;

    /*  Age limit for major-planet mean elements (days) */
    const double AGEPEL = 50.0;

    /*  Margin for error when deciding whether to renew the planetary data */
    const double TINY = 1e-6;

    /*  Age limit for the body's osculating elements (before rectification) */
    const double AGEBEL = 100.0;

    /*  Gaussian gravitational constant squared */
    const double GCON2 = PAL__GCON * PAL__GCON;

    /*  The final epoch */
    double TFINAL;

    /*  The body's current universal elements */
    double UL[13];

    /*  Current reference epoch */
    double T0;

    /*  Timespan from latest orbit rectification to final epoch (days) */
    double TSPAN;

    /*  Time left to go before integration is complete */
    double TLEFT;

    /*  Time direction flag: +1=forwards, -1=backwards */
    double FB;

    /*  First-time flag */
    int FIRST = 0;

    /*
     *  The current perturbations
     */

    /*  Epoch (days relative to current reference epoch) */
    double RTN;
    /*  Position (AU) */
    double PERP[3];
    /*  Velocity (AU/d) */
    double PERV[3];
    /*  Acceleration (AU/d/d) */
    double PERA[3];

    /*  Length of current timestep (days), and half that */
    double TS, HTS;

    /*  Epoch of middle of timestep */
    double T;

    /*  Epoch of planetary mean elements */
    double TPEL = 0.0;

    /*  Planet number (1=Mercury, 2=Venus, 3=EMB...8=Neptune) */
    int numPlanet;

    /*  Planetary universal orbital elements */
    double UP[8][13];

    /*  Epoch of planetary state vectors */
    double TPMO = 0.0;

    /*  State vectors for the major planets (AU,AU/s) */
    double PVIN[8][6];

    /*  Earth velocity and position vectors (AU,AU/s) */
    double VB[3], PB[3], VH[3], PE[3];

    /*  Moon geocentric state vector (AU,AU/s) and position part */
    double PVM[6], PM[3];

    /*  Date to J2000 de-precession matrix */
    double PMAT[3][3];

    /*
     *  Correction terms for extrapolated major planet vectors
     */

    /*  Sun-to-planet distances squared multiplied by 3 */
    double R2X3[8];
    /*  Sunward acceleration terms, G/2R^3 */
    double GC[8];
    /*  Tangential-to-circular correction factor */
    double FC;
    /*  Radial correction factor due to Sunwards acceleration */
    double FG;

    /*  The body's unperturbed and perturbed state vectors (AU,AU/s) */
    double PV0[6], PV[6];

    /*  The body's perturbed and unperturbed heliocentric distances (AU) cubed
     */
    double R03, R3;

    /*  The perturbating accelerations, indirect and direct */
    double FI[3], FD[3];

    /*  Sun-to-planet vector, and distance cubed */
    double RHO[3], RHO3;

    /*  Body-to-planet vector, and distance cubed */
    double DELTA[3], DELTA3;

    /*  Miscellaneous */
    int I, J;
    double R2, W, DT, DT2, R, FT;
    int NE;

    /*  Planetary inverse masses, Mercury through Neptune then Earth and Moon */
    const double AMAS[10] = {6023600.,
                             408523.5,
                             328900.5,
                             3098710.,
                             1047.355,
                             3498.5,
                             22869.,
                             19314.,
                             332946.038,
                             27068709.};

    /*  Preset the status to OK. */
    *jstat = 0;

    /*  Copy the final epoch. */
    TFINAL = date;

    /*  Copy the elements (which will be periodically updated). */
    for (I = 0; I < 13; I++) {
        UL[I] = u[I];
    }

    /*  Initialize the working reference epoch. */
    T0 = UL[2];

    /*  Total timespan (days) and hence time left. */
    TSPAN = TFINAL - T0;
    TLEFT = TSPAN;

    /*  Warn if excessive. */
    if (fabs(TSPAN) > 36525.0)
        *jstat = 101;

    /*  Time direction: +1 for forwards, -1 for backwards. */
    FB = COPYSIGN(1.0, TSPAN);

    /*  Initialize relative epoch for start of current timestep. */
    RTN = 0.0;

    /*  Reset the perturbations (position, velocity, acceleration). */
    for (I = 0; I < 3; I++) {
        PERP[I] = 0.0;
        PERV[I] = 0.0;
        PERA[I] = 0.0;
    }

    /*  Set "first iteration" flag. */
    FIRST = 1;

    /*  Step through the time left. */
    while (FB * TLEFT > 0.0) {

        /*     Magnitude of current acceleration due to planetary attractions.
         */
        if (FIRST) {
            TS = TSMIN;
        } else {
            R2 = 0.0;
            for (I = 0; I < 3; I++) {
                W = FD[I];
                R2 = R2 + W * W;
            }
            W = sqrt(R2);

            /*        Use the acceleration to decide how big a timestep can be
             * tolerated. */
            if (W != 0.0) {
                TS = DMIN(TSMAX, DMAX(TSMIN, TSC / W));
            } else {
                TS = TSMAX;
            }
        }
        TS = TS * FB;

        /*     Override if final epoch is imminent. */
        TLEFT = TSPAN - RTN;
        if (fabs(TS) > fabs(TLEFT))
            TS = TLEFT;

        /*     Epoch of middle of timestep. */
        HTS = TS / 2.0;
        T = T0 + RTN + HTS;

        /*     Is it time to recompute the major-planet elements? */
        if (FIRST || fabs(T - TPEL) - AGEPEL >= TINY) {

            /*        Yes: go forward in time by just under the maximum allowed.
             */
            TPEL = T + FB * AGEPEL;

            /*        Compute the state vector for the new epoch. */
            for (numPlanet = 1; numPlanet <= 8; numPlanet++) {
                palPlanet(TPEL, numPlanet, PV, &J);

                /*           Warning if remote epoch, abort if error. */
                if (J == 1) {
                    *jstat = 102;
                } else if (J != 0) {
                    goto ABORT;
                }

                /*           Transform the vector into universal elements. */
                palPv2ue(PV, TPEL, 0.0, &(UP[numPlanet - 1][0]), &J);
                if (J != 0)
                    goto ABORT;
            }
        }

        /*     Is it time to recompute the major-planet motions? */
        if (FIRST || fabs(T - TPMO) - AGEPMO >= TINY) {

            /*        Yes: look ahead. */
            TPMO = T + FB * AGEPMO;

            /*        Compute the motions of each planet (AU,AU/d). */
            for (numPlanet = 1; numPlanet <= 8; numPlanet++) {

                /*           The planet's position and velocity (AU,AU/s). */
                palUe2pv(TPMO,
                         &(UP[numPlanet - 1][0]),
                         &(PVIN[numPlanet - 1][0]),
                         &J);
                if (J != 0)
                    goto ABORT;

                /*           Scale velocity to AU/d. */
                for (J = 3; J < 6; J++) {
                    PVIN[numPlanet - 1][J] = PVIN[numPlanet - 1][J] * PAL__SPD;
                }

                /*           Precompute also the extrapolation correction terms.
                 */
                R2 = 0.0;
                for (I = 0; I < 3; I++) {
                    W = PVIN[numPlanet - 1][I];
                    R2 = R2 + W * W;
                }
                R2X3[numPlanet - 1] = R2 * 3.0;
                GC[numPlanet - 1] = GCON2 / (2.0 * R2 * sqrt(R2));
            }
        }

        /*     Reset the first-time flag. */
        FIRST = 0;

        /*     Unperturbed motion of the body at middle of timestep (AU,AU/s).
         */
        palUe2pv(T, UL, PV0, &J);
        if (J != 0)
            goto ABORT;

        /*     Perturbed position of the body (AU) and heliocentric distance
         * cubed. */
        R2 = 0.0;
        for (I = 0; I < 3; I++) {
            W = PV0[I] + PERP[I] + (PERV[I] + PERA[I] * HTS / 2.0) * HTS;
            PV[I] = W;
            R2 = R2 + W * W;
        }
        R3 = R2 * sqrt(R2);

        /*     The body's unperturbed heliocentric distance cubed. */
        R2 = 0.0;
        for (I = 0; I < 3; I++) {
            W = PV0[I];
            R2 = R2 + W * W;
        }
        R03 = R2 * sqrt(R2);

        /*     Compute indirect and initialize direct parts of the perturbation.
         */
        for (I = 0; I < 3; I++) {
            FI[I] = PV0[I] / R03 - PV[I] / R3;
            FD[I] = 0.0;
        }

        /*     Ready to compute the direct planetary effects. */

        /*     Reset the "near-Earth" flag. */
        NE = 0;

        /*     Interval from state-vector epoch to middle of current timestep.
         */
        DT = T - TPMO;
        DT2 = DT * DT;

        /*     Planet by planet, including separate Earth and Moon. */
        for (numPlanet = 1; numPlanet < 10; numPlanet++) {

            /*        Which perturbing body? */
            if (numPlanet <= 8) {

                /*           Planet: compute the extrapolation in longitude
                 * (squared). */
                R2 = 0.0;
                for (J = 3; J < 6; J++) {
                    W = PVIN[numPlanet - 1][J] * DT;
                    R2 = R2 + W * W;
                }

                /*           Hence the tangential-to-circular correction factor.
                 */
                FC = 1.0 + R2 / R2X3[numPlanet - 1];

                /*           The radial correction factor due to the inwards
                 * acceleration. */
                FG = 1.0 - GC[numPlanet - 1] * DT2;

                /*           Planet's position. */
                for (I = 0; I < 3; I++) {
                    RHO[I] = FG * (PVIN[numPlanet - 1][I] +
                                   FC * PVIN[numPlanet - 1][I + 3] * DT);
                }

            } else if (NE) {

                /*           Near-Earth and either Earth or Moon. */

                if (numPlanet == 9) {

                    /*              Earth: position. */
                    palEpv(T, PE, VH, PB, VB);
                    for (I = 0; I < 3; I++) {
                        RHO[I] = PE[I];
                    }

                } else {

                    /*              Moon: position. */
                    palPrec(palEpj(T), 2000.0, PMAT);
                    palDmoon(T, PVM);
                    eraRxp(PMAT, PVM, PM);
                    for (I = 0; I < 3; I++) {
                        RHO[I] = PM[I] + PE[I];
                    }
                }
            }

            /*        Proceed unless Earth or Moon and not the near-Earth case.
             */
            if (numPlanet <= 8 || NE) {

                /*           Heliocentric distance cubed. */
                R2 = 0.0;
                for (I = 0; I < 3; I++) {
                    W = RHO[I];
                    R2 = R2 + W * W;
                }
                R = sqrt(R2);
                RHO3 = R2 * R;

                /*           Body-to-planet vector, and distance. */
                R2 = 0.0;
                for (I = 0; I < 3; I++) {
                    W = RHO[I] - PV[I];
                    DELTA[I] = W;
                    R2 = R2 + W * W;
                }
                R = sqrt(R2);

                /*           If this is the EMB, set the near-Earth flag
                 * appropriately. */
                if (numPlanet == 3 && R < RNE)
                    NE = 1;

                /*           Proceed unless EMB and this is the near-Earth case.
                 */
                if (!(NE && numPlanet == 3)) {

                    /*              If too close, ignore this planet and set a
                     * warning. */
                    if (R < COINC) {
                        *jstat = numPlanet;

                    } else {

                        /*                 Accumulate "direct" part of
                         * perturbation acceleration. */
                        DELTA3 = R2 * R;
                        W = AMAS[numPlanet - 1];
                        for (I = 0; I < 3; I++) {
                            FD[I] =
                                FD[I] + (DELTA[I] / DELTA3 - RHO[I] / RHO3) / W;
                        }
                    }
                }
            }
        }

        /*     Update the perturbations to the end of the timestep. */
        RTN += TS;
        for (I = 0; I < 3; I++) {
            W = (FI[I] + FD[I]) * GCON2;
            FT = W * TS;
            PERP[I] = PERP[I] + (PERV[I] + FT / 2.0) * TS;
            PERV[I] = PERV[I] + FT;
            PERA[I] = W;
        }

        /*     Time still to go. */
        TLEFT = TSPAN - RTN;

        /*     Is it either time to rectify the orbit or the last time through?
         */
        if (fabs(RTN) >= AGEBEL || FB * TLEFT <= 0.0) {

            /*        Yes: update to the end of the current timestep. */
            T0 += RTN;
            RTN = 0.0;

            /*        The body's unperturbed motion (AU,AU/s). */
            palUe2pv(T0, UL, PV0, &J);
            if (J != 0)
                goto ABORT;

            /*        Add and re-initialize the perturbations. */
            for (I = 0; I < 3; I++) {
                J = I + 3;
                PV[I] = PV0[I] + PERP[I];
                PV[J] = PV0[J] + PERV[I] / PAL__SPD;
                PERP[I] = 0.0;
                PERV[I] = 0.0;
                PERA[I] = FD[I] * GCON2;
            }

            /*        Use the position and velocity to set up new universal
             * elements. */
            palPv2ue(PV, T0, 0.0, UL, &J);
            if (J != 0)
                goto ABORT;

            /*        Adjust the timespan and time left. */
            TSPAN = TFINAL - T0;
            TLEFT = TSPAN;
        }

        /*     Next timestep. */
    }

    /*  Return the updated universal-element set. */
    for (I = 0; I < 13; I++) {
        u[I] = UL[I];
    }

    /*  Finished. */
    return;

    /*  Miscellaneous numerical error. */
ABORT:
    *jstat = -1;
    return;
}
/*
*+
*  Name:
*     palPlanel

*  Purpose:
*     Transform conventional elements into position and velocity

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPlanel ( double date, int jform, double epoch, double orbinc,
*                      double anode, double perih, double aorq, double e,
*                      double aorl, double dm, double pv[6], int *jstat );

*  Arguments:
*     date = double (Given)
*        Epoch (TT MJD) of osculation (Note 1)
*     jform = int (Given)
*        Element set actually returned (1-3; Note 3)
*     epoch = double (Given)
*        Epoch of elements (TT MJD) (Note 4)
*     orbinc = double (Given)
*        inclination (radians)
*     anode = double (Given)
*        longitude of the ascending node (radians)
*     perih = double (Given)
*        longitude or argument of perihelion (radians)
*     aorq = double (Given)
*        mean distance or perihelion distance (AU)
*     e = double (Given)
*        eccentricity
*     aorl = double (Given)
*        mean anomaly or longitude (radians, JFORM=1,2 only)
*     dm = double (Given)
*        daily motion (radians, JFORM=1 only)
*     u = double [13] (Returned)
*        Universal orbital elements (Note 1)
*            (0)  combined mass (M+m)
*            (1)  total energy of the orbit (alpha)
*            (2)  reference (osculating) epoch (t0)
*          (3-5)  position at reference epoch (r0)
*          (6-8)  velocity at reference epoch (v0)
*            (9)  heliocentric distance at reference epoch
*           (10)  r0.v0
*           (11)  date (t)
*           (12)  universal eccentric anomaly (psi) of date, approx
*     jstat = int * (Returned)
*        status:  0 = OK
*              - -1 = illegal JFORM
*              - -2 = illegal E
*              - -3 = illegal AORQ
*              - -4 = illegal DM
*              - -5 = numerical error

*  Description:
*     Heliocentric position and velocity of a planet, asteroid or comet,
*     starting from orbital elements.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - DATE is the instant for which the prediction is required.  It is
*       in the TT timescale (formerly Ephemeris Time, ET) and is a
*       Modified Julian Date (JD-2400000.5).
*     - The elements are with respect to the J2000 ecliptic and equinox.
*     - A choice of three different element-set options is available:
*
*       Option JFORM = 1, suitable for the major planets:
*
*         EPOCH  = epoch of elements (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = longitude of perihelion, curly pi (radians)
*         AORQ   = mean distance, a (AU)
*         E      = eccentricity, e (range 0 to <1)
*         AORL   = mean longitude L (radians)
*         DM     = daily motion (radians)
*
*       Option JFORM = 2, suitable for minor planets:
*
*         EPOCH  = epoch of elements (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = argument of perihelion, little omega (radians)
*         AORQ   = mean distance, a (AU)
*         E      = eccentricity, e (range 0 to <1)
*         AORL   = mean anomaly M (radians)
*
*       Option JFORM = 3, suitable for comets:
*
*         EPOCH  = epoch of elements and perihelion (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = argument of perihelion, little omega (radians)
*         AORQ   = perihelion distance, q (AU)
*         E      = eccentricity, e (range 0 to 10)
*
*       Unused arguments (DM for JFORM=2, AORL and DM for JFORM=3) are not
*       accessed.
*     - Each of the three element sets defines an unperturbed heliocentric
*       orbit.  For a given epoch of observation, the position of the body
*       in its orbit can be predicted from these elements, which are
*       called "osculating elements", using standard two-body analytical
*       solutions.  However, due to planetary perturbations, a given set
*       of osculating elements remains usable for only as long as the
*       unperturbed orbit that it describes is an adequate approximation
*       to reality.  Attached to such a set of elements is a date called
*       the "osculating epoch", at which the elements are, momentarily,
*       a perfect representation of the instantaneous position and
*       velocity of the body.
*
*       Therefore, for any given problem there are up to three different
*       epochs in play, and it is vital to distinguish clearly between
*       them:
*
*       . The epoch of observation:  the moment in time for which the
*         position of the body is to be predicted.
*
*       . The epoch defining the position of the body:  the moment in time
*         at which, in the absence of purturbations, the specified
*         position (mean longitude, mean anomaly, or perihelion) is
*         reached.
*
*       . The osculating epoch:  the moment in time at which the given
*         elements are correct.
*
*       For the major-planet and minor-planet cases it is usual to make
*       the epoch that defines the position of the body the same as the
*       epoch of osculation.  Thus, only two different epochs are
*       involved:  the epoch of the elements and the epoch of observation.
*
*       For comets, the epoch of perihelion fixes the position in the
*       orbit and in general a different epoch of osculation will be
*       chosen.  Thus, all three types of epoch are involved.
*
*       For the present routine:
*
*       . The epoch of observation is the argument DATE.
*
*       . The epoch defining the position of the body is the argument
*         EPOCH.
*
*       . The osculating epoch is not used and is assumed to be close
*         enough to the epoch of observation to deliver adequate accuracy.
*         If not, a preliminary call to sla_PERTEL may be used to update
*         the element-set (and its associated osculating epoch) by
*         applying planetary perturbations.
*     - The reference frame for the result is with respect to the mean
*       equator and equinox of epoch J2000.
*     - The algorithm was originally adapted from the EPHSLA program of
*       D.H.P.Jones (private communication, 1996).  The method is based
*       on Stumpff's Universal Variables.

*  See Also:
*     Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-12 (TIMJ):
*        Initial version taken directly from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2002 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palPlanel(double date,
               int jform,
               double epoch,
               double orbinc,
               double anode,
               double perih,
               double aorq,
               double e,
               double aorl,
               double dm,
               double pv[6],
               int *jstat) {

    int j;
    double u[13];

    /*  Validate elements and convert to "universal variables" parameters. */
    palEl2ue(
        date, jform, epoch, orbinc, anode, perih, aorq, e, aorl, dm, u, &j);

    /* Determine the position and velocity */
    if (j == 0) {
        palUe2pv(date, u, pv, &j);
        if (j != 0)
            j = -5;
    }

    /* Wrap up */
    *jstat = j;
}
/*
*+
*  Name:
*     palPlanet

*  Purpose:
*     Approximate heliocentric position and velocity of major planet

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPlanet ( double date, int np, double pv[6], int *j );

*  Arguments:
*     date = double (Given)
*        TDB Modified Julian Date (JD-2400000.5).
*     np = int (Given)
*        planet (1=Mercury, 2=Venus, 3=EMB, 4=Mars,
*                5=Jupiter, 6=Saturn, 7=Uranus, 8=Neptune)
*     pv = double [6] (Returned)
*        heliocentric x,y,z,xdot,ydot,zdot, J2000, equatorial triad
*        in units AU and AU/s.
*     j = int * (Returned)
*        - -2 = solution didn't converge.
*        - -1 = illegal np (1-8)
*        -  0 = OK
*        - +1 = warning: year outside 1000-3000

*  Description:
*     Calculates the approximate heliocentric position and velocity of
*     the specified major planet.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - See SOFA eraPlan94 for details
*     - Note that Pluto is supported in SLA/F but not in this routine
*     - Status -2 is equivalent to eraPlan94 status +2.
*     - Note that velocity units here match the SLA/F documentation.

*  History:
*     2012-03-07 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palPlanet(double date, int np, double pv[6], int *j) {
    double erapv[2][3];

    *j = eraPlan94(PAL__MJD0, date, np, erapv);

    /* Convert the outputs to the correct form and also correct AU/d
       to AU/s */
    pv[0] = erapv[0][0];
    pv[1] = erapv[0][1];
    pv[2] = erapv[0][2];
    pv[3] = erapv[1][0] / PAL__SPD;
    pv[4] = erapv[1][1] / PAL__SPD;
    pv[5] = erapv[1][2] / PAL__SPD;

    /* SLA compatibility for status */
    if (*j == 2)
        *j = -2;
}
/*
*+
*  Name:
*     palPlante

*  Purpose:
*     Topocentric RA,Dec of a Solar-System object from heliocentric orbital
elements

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPlante ( double date, double elong, double phi, int jform,
*                      double epoch, double orbinc, double anode, double perih,
*                      double aorq, double e, double aorl, double dm,
*                      double *ra, double *dec, double *r, int *jstat );


*  Description:
*     Topocentric apparent RA,Dec of a Solar-System object whose
*     heliocentric orbital elements are known.

*  Arguments:
*     date = double (Given)
*        TT MJD of observation (JD-2400000.5)
*     elong = double (Given)
*        Observer's east longitude (radians)
*     phi = double (Given)
*        Observer's geodetic latitude (radians)
*     jform = int (Given)
*        Element set actually returned (1-3; Note 6)
*     epoch = double (Given)
*        Epoch of elements (TT MJD)
*     orbinc = double (Given)
*        inclination (radians)
*     anode = double (Given)
*        longitude of the ascending node (radians)
*     perih = double (Given)
*        longitude or argument of perihelion (radians)
*     aorq = double (Given)
*        mean distance or perihelion distance (AU)
*     e = double (Given)
*        eccentricity
*     aorl = double (Given)
*        mean anomaly or longitude (radians, JFORM=1,2 only)
*     dm = double (Given)
*        daily motion (radians, JFORM=1 only)
*     ra = double * (Returned)
*        Topocentric apparent RA (radians)
*     dec = double * (Returned)
*        Topocentric apparent Dec (radians)
*     r = double * (Returned)
*        Distance from observer (AU)
*     jstat = int * (Returned)
*        status: 0 = OK
*             - -1 = illegal jform
*             - -2 = illegal e
*             - -3 = illegal aorq
*             - -4 = illegal dm
*             - -5 = numerical error

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - DATE is the instant for which the prediction is required.  It is
*       in the TT timescale (formerly Ephemeris Time, ET) and is a
*       Modified Julian Date (JD-2400000.5).
*     - The longitude and latitude allow correction for geocentric
*       parallax.  This is usually a small effect, but can become
*       important for near-Earth asteroids.  Geocentric positions can be
*       generated by appropriate use of routines palEpv (or palEvp) and
*       palUe2pv.
*     - The elements are with respect to the J2000 ecliptic and equinox.
*     - A choice of three different element-set options is available:
*
*       Option JFORM = 1, suitable for the major planets:
*
*         EPOCH  = epoch of elements (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = longitude of perihelion, curly pi (radians)
*         AORQ   = mean distance, a (AU)
*         E      = eccentricity, e (range 0 to <1)
*         AORL   = mean longitude L (radians)
*         DM     = daily motion (radians)
*
*       Option JFORM = 2, suitable for minor planets:
*
*         EPOCH  = epoch of elements (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = argument of perihelion, little omega (radians)
*         AORQ   = mean distance, a (AU)
*         E      = eccentricity, e (range 0 to <1)
*         AORL   = mean anomaly M (radians)
*
*       Option JFORM = 3, suitable for comets:
*
*         EPOCH  = epoch of elements and perihelion (TT MJD)
*         ORBINC = inclination i (radians)
*         ANODE  = longitude of the ascending node, big omega (radians)
*         PERIH  = argument of perihelion, little omega (radians)
*         AORQ   = perihelion distance, q (AU)
*         E      = eccentricity, e (range 0 to 10)
*
*       Unused arguments (DM for JFORM=2, AORL and DM for JFORM=3) are not
*       accessed.
*     - Each of the three element sets defines an unperturbed heliocentric
*       orbit.  For a given epoch of observation, the position of the body
*       in its orbit can be predicted from these elements, which are
*       called "osculating elements", using standard two-body analytical
*       solutions.  However, due to planetary perturbations, a given set
*       of osculating elements remains usable for only as long as the
*       unperturbed orbit that it describes is an adequate approximation
*       to reality.  Attached to such a set of elements is a date called
*       the "osculating epoch", at which the elements are, momentarily,
*       a perfect representation of the instantaneous position and
*       velocity of the body.
*
*       Therefore, for any given problem there are up to three different
*       epochs in play, and it is vital to distinguish clearly between
*       them:
*
*       . The epoch of observation:  the moment in time for which the
*         position of the body is to be predicted.
*
*       . The epoch defining the position of the body:  the moment in time
*         at which, in the absence of purturbations, the specified
*         position (mean longitude, mean anomaly, or perihelion) is
*         reached.
*
*       . The osculating epoch:  the moment in time at which the given
*         elements are correct.
*
*       For the major-planet and minor-planet cases it is usual to make
*       the epoch that defines the position of the body the same as the
*       epoch of osculation.  Thus, only two different epochs are
*       involved:  the epoch of the elements and the epoch of observation.
*
*       For comets, the epoch of perihelion fixes the position in the
*       orbit and in general a different epoch of osculation will be
*       chosen.  Thus, all three types of epoch are involved.
*
*       For the present routine:
*
*       . The epoch of observation is the argument DATE.
*
*       . The epoch defining the position of the body is the argument
*         EPOCH.
*
*       . The osculating epoch is not used and is assumed to be close
*         enough to the epoch of observation to deliver adequate accuracy.
*         If not, a preliminary call to sla_PERTEL may be used to update
*         the element-set (and its associated osculating epoch) by
*         applying planetary perturbations.
*     - Two important sources for orbital elements are Horizons, operated
*       by the Jet Propulsion Laboratory, Pasadena, and the Minor Planet
*       Center, operated by the Center for Astrophysics, Harvard.
*
*       The JPL Horizons elements (heliocentric, J2000 ecliptic and
*       equinox) correspond to SLALIB arguments as follows.
*
*        Major planets:
*
*         JFORM  = 1
*         EPOCH  = JDCT-2400000.5
*         ORBINC = IN (in radians)
*         ANODE  = OM (in radians)
*         PERIH  = OM+W (in radians)
*         AORQ   = A
*         E      = EC
*         AORL   = MA+OM+W (in radians)
*         DM     = N (in radians)
*
*         Epoch of osculation = JDCT-2400000.5
*
*        Minor planets:
*
*         JFORM  = 2
*         EPOCH  = JDCT-2400000.5
*         ORBINC = IN (in radians)
*         ANODE  = OM (in radians)
*         PERIH  = W (in radians)
*         AORQ   = A
*         E      = EC
*         AORL   = MA (in radians)
*
*         Epoch of osculation = JDCT-2400000.5
*
*        Comets:
*
*         JFORM  = 3
*         EPOCH  = Tp-2400000.5
*         ORBINC = IN (in radians)
*         ANODE  = OM (in radians)
*         PERIH  = W (in radians)
*         AORQ   = QR
*         E      = EC
*
*         Epoch of osculation = JDCT-2400000.5
*
*      The MPC elements correspond to SLALIB arguments as follows.
*
*        Minor planets:
*
*         JFORM  = 2
*         EPOCH  = Epoch-2400000.5
*         ORBINC = Incl. (in radians)
*         ANODE  = Node (in radians)
*         PERIH  = Perih. (in radians)
*         AORQ   = a
*         E      = e
*         AORL   = M (in radians)
*
*         Epoch of osculation = Epoch-2400000.5
*
*       Comets:
*
*         JFORM  = 3
*         EPOCH  = T-2400000.5
*         ORBINC = Incl. (in radians)
*         ANODE  = Node. (in radians)
*         PERIH  = Perih. (in radians)
*         AORQ   = q
*         E      = e
*
*         Epoch of osculation = Epoch-2400000.5

*  History:
*     2012-03-12 (TIMJ):
*        Initial version direct conversion of SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palPlante(double date,
               double elong,
               double phi,
               int jform,
               double epoch,
               double orbinc,
               double anode,
               double perih,
               double aorq,
               double e,
               double aorl,
               double dm,
               double *ra,
               double *dec,
               double *r,
               int *jstat) {

    double u[13];

    /* Transform conventional elements to universal elements */
    palEl2ue(
        date, jform, epoch, orbinc, anode, perih, aorq, e, aorl, dm, u, jstat);

    /* If succcessful, make the prediction */
    if (*jstat == 0)
        palPlantu(date, elong, phi, u, ra, dec, r, jstat);
}

/*
*+
*  Name:
*     palPlantu

*  Purpose:
*     Topocentric RA,Dec of a Solar-System object from universal elements

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPlantu ( double date, double elong, double phi, const double
u[13],
*                      double *ra, double *dec, double *r, int *jstat ) {

*  Description:
*     Topocentric apparent RA,Dec of a Solar-System object whose
*     heliocentric universal elements are known.

*  Arguments:
*     date = double (Given)
*        TT MJD of observation (JD-2400000.5)
*     elong = double (Given)
*        Observer's east longitude (radians)
*     phi = double (Given)
*        Observer's geodetic latitude (radians)
*     u = const double [13] (Given)
*        Universal orbital elements
*          -   (0)  combined mass (M+m)
*          -   (1)  total energy of the orbit (alpha)
*          -   (2)  reference (osculating) epoch (t0)
*          - (3-5)  position at reference epoch (r0)
*          - (6-8)  velocity at reference epoch (v0)
*          -   (9)  heliocentric distance at reference epoch
*          -  (10)  r0.v0
*          -  (11)  date (t)
*          -  (12)  universal eccentric anomaly (psi) of date, approx
*     ra = double * (Returned)
*        Topocentric apparent RA (radians)
*     dec = double * (Returned)
*        Topocentric apparent Dec (radians)
*     r = double * (Returned)
*        Distance from observer (AU)
*     jstat = int * (Returned)
*        status: 0 = OK
*             - -1 = radius vector zero
*             - -2 = failed to converge

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - DATE is the instant for which the prediction is required.  It is
*       in the TT timescale (formerly Ephemeris Time, ET) and is a
*       Modified Julian Date (JD-2400000.5).
*     - The longitude and latitude allow correction for geocentric
*       parallax.  This is usually a small effect, but can become
*       important for near-Earth asteroids.  Geocentric positions can be
*       generated by appropriate use of routines palEpv (or palEvp) and
*       palUe2pv.
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference 2).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - The universal elements are with respect to the J2000 equator and
*       equinox.

*  See Also:
*     - Sterne, Theodore E., "An Introduction to Celestial Mechanics",
*       Interscience Publishers Inc., 1960.  Section 6.7, p199.
*     - Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-12 (TIMJ):
*        Initial version direct conversion of SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

void palPlantu(double date,
               double elong,
               double phi,
               const double u[13],
               double *ra,
               double *dec,
               double *r,
               int *jstat) {

    int i;
    double dvb[3], dpb[3], vsg[6], vsp[6], v[6], rmat[3][3], vgp[6], stl,
        vgo[6], dx, dy, dz, d, tl;

    double ucp[13];

    /* To retain the stated const API and conform to the documentation
       we must copy the contents of the u array as palUe2pv updates
       the final two elements */
    for (i = 0; i < 13; i++) {
        ucp[i] = u[i];
    }

    /* Sun to geocentre (J2000, velocity in AU/s) */
    palEpv(date, vsg, &(vsg[3]), dpb, dvb);
    for (i = 3; i < 6; i++) {
        vsg[i] /= PAL__SPD;
    }

    /* Sun to planet (J2000) */
    palUe2pv(date, ucp, vsp, jstat);

    /* Geocentre to planet (J2000) */
    for (i = 0; i < 6; i++) {
        v[i] = vsp[i] - vsg[i];
    }

    /* Precession and nutation to date */
    palPrenut(2000.0, date, rmat);
    eraRxp(rmat, v, vgp);
    eraRxp(rmat, &(v[3]), &(vgp[3]));

    /* Geocentre to observer (date) */
    stl = palGmst(date - palDt(palEpj(date)) / PAL__SPD) + elong;
    palPvobs(phi, 0.0, stl, vgo);

    /* Observer to planet (date) */
    for (i = 0; i < 6; i++) {
        v[i] = vgp[i] - vgo[i];
    }

    /* Geometric distance (AU) */
    dx = v[0];
    dy = v[1];
    dz = v[2];
    d = sqrt(dx * dx + dy * dy + dz * dz);

    /* Light time (sec) */
    tl = PAL__CR * d;

    /* Correct position for planetary aberration */
    for (i = 0; i < 3; i++) {
        v[i] -= tl * v[i + 3];
    }

    /* To RA,Dec */
    eraC2s(v, ra, dec);
    *ra = eraAnp(*ra);
    *r = d;
}

/*
*+
*  Name:
*     palPm

*  Purpose:
*     Apply corrections for proper motion a star RA,Dec

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPm ( double r0, double d0, double pr, double pd,
*                  double px, double rv, double ep0, double ep1,
*                  double *r1, double *d1 );

*  Arguments:
*     r0 = double (Given)
*        RA at epoch ep0 (radians)
*     d0 = double (Given)
*        Dec at epoch ep0 (radians)
*     pr = double (Given)
*        RA proper motion in radians per year.
*     pd = double (Given)
*        Dec proper motion in radians per year.
*     px = double (Given)
*        Parallax (arcsec)
*     rv = double (Given)
*        Radial velocity (km/sec +ve if receding)
*     ep0 = double (Given)
*        Start epoch in years, assumed to be Julian.
*     ep1 = double (Given)
*        End epoch in years, assumed to be Julian.
*     r1 = double * (Returned)
*        RA at epoch ep1 (radians)
*     d1 = double * (Returned)
*        Dec at epoch ep1 (radians)

*  Description:
*     Apply corrections for proper motion to a star RA,Dec using the
*     SOFA routine eraStarpm.

*  Authors:
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Uses eraStarpm but ignores the status returns from that routine.
*       In particular note that parallax should not be zero when the
*       proper motions are non-zero. SLA/F allows parallax to be zero.
*     - Assumes all epochs are Julian epochs.

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palPm(double r0,
           double d0,
           double pr,
           double pd,
           double px,
           double rv,
           double ep0,
           double ep1,
           double *r1,
           double *d1) {

    // int status;
    double ep1a, ep1b, ep2a, ep2b;
    double pmr2, pmd2, px2, rv2;

    /* SOFA requires the epochs in TDB MJD so we have to
       assume that the supplied epochs are Julian years */
    eraEpj2jd(ep0, &ep1a, &ep1b);
    eraEpj2jd(ep1, &ep2a, &ep2b);

    // status = eraStarpm( r0, d0, pr, pd, px, rv,
    eraStarpm(r0,
              d0,
              pr,
              pd,
              px,
              rv,
              ep1a,
              ep1b,
              ep2a,
              ep2b,
              r1,
              d1,
              &pmr2,
              &pmd2,
              &px2,
              &rv2);
}
/*
*+
*  Name:
*     palPrebn

*  Purpose:
*     Generate the matrix of precession between two objects (old)

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPrebn ( double bep0, double bep1, double rmatp[3][3] );

*  Arguments:
*     bep0 = double (Given)
*        Beginning Besselian epoch.
*     bep1 = double (Given)
*        Ending Besselian epoch
*     rmatp = double[3][3] (Returned)
*        precession matrix in the sense V(BEP1) = RMATP * V(BEP0)

*  Description:
*     Generate the matrix of precession between two epochs,
*     using the old, pre-IAU1976, Bessel-Newcomb model, using
*     Kinoshita's formulation

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  See Also:
*     Kinoshita, H. (1975) 'Formulas for precession', SAO Special
*     Report No. 364, Smithsonian Institution Astrophysical
*     Observatory, Cambridge, Massachusetts.

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

void palPrebn(double bep0, double bep1, double rmatp[3][3]) {

    double t, bigt, zeta, theta, z, tas2r, w;

    /* Interval between basic epoch B1850.0 and beginning epoch in TC */
    bigt = (bep0 - 1850) / 100.;

    /*  Interval over which precession required, in tropical centuries */
    t = (bep1 - bep0) / 100.;

    /* Euler angles */
    tas2r = t * PAL__DAS2R;
    w = 2303.5548 + (1.39720 + 0.000059 * bigt) * bigt;

    zeta = (w + (0.30242 - 0.000269 * bigt + 0.017996 * t) * t) * tas2r;
    z = (w + (1.09478 + 0.000387 * bigt + 0.018324 * t) * t) * tas2r;
    theta = (2005.1125 + (-0.85294 - 0.000365 * bigt) * bigt +
             (-0.42647 - 0.000365 * bigt - 0.041802 * t) * t) *
            tas2r;

    /*  Rotation matrix */
    palDeuler("ZYZ", -zeta, theta, -z, rmatp);
}
/*
*+
*  Name:
*     palPrec

*  Purpose:
*     Form the matrix of precession between two epochs (IAU 2006)

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palPrec( double ep0, double ep1, double rmatp[3][3] )

*  Arguments:
*     ep0 = double (Given)
*        Beginning epoch
*     ep1 = double (Given)
*        Ending epoch
*     rmatp = double[3][3] (Returned)
*        Precession matrix

*  Description:
*     The IAU 2006 precession matrix from ep0 to ep1 is found and
*     returned. The matrix is in the sense  V(EP1)  =  RMATP * V(EP0).
*     The epochs are TDB (loosely TT) Julian epochs.
*
*     Though the matrix method itself is rigorous, the precession
*     angles are expressed through canonical polynomials which are
*     valid only for a limited time span of a few hundred years around
*     the current epoch.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-10 (DSB):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1996 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palPrec(double ep0, double ep1, double rmatp[3][3]) {

    /* Local Variables: */
    double rmatq[3][3];
    double ep0_days;
    double ep1_days;

    /* Convert supplied dates to days since J2000 */
    ep0_days = (ep0 - 2000.0) * ERFA_DJY;
    ep1_days = (ep1 - 2000.0) * ERFA_DJY;

    /* If beginning epoch is J2000, just return the rotation matrix from
       J2000 to EP1. */
    if (ep0 == 2000.0) {
        eraPmat06(ERFA_DJ00, ep1_days, rmatp);

        /* If end epoch is J2000, get the rotation matrix from J2000 to EP0 and
           then transpose it to get the rotation matrix from EP0 to J2000. */
    } else if (ep1 == 2000.0) {
        eraPmat06(ERFA_DJ00, ep0_days, rmatp);
        eraTr(rmatp, rmatp);

        /* Otherwise. get the two matrices used above and multiply them
           together. */
    } else {
        eraPmat06(ERFA_DJ00, ep0_days, rmatp);
        eraTr(rmatp, rmatp);
        eraPmat06(ERFA_DJ00, ep1_days, rmatq);
        eraRxr(rmatp, rmatq, rmatp);
    }
}
/*
*+
*  Name:
*     palPreces

*  Purpose:
*     Precession - either FK4 or FK5 as required.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPreces ( const char sys[3], double ep0, double ep1,
*                      double *ra, double *dc );

*  Arguments:
*     sys = const char [3] (Given)
*        Precession to be applied: FK4 or FK5. Case insensitive.
*     ep0 = double (Given)
*        Starting epoch.
*     ep1 = double (Given)
*        Ending epoch
*     ra = double * (Given & Returned)
*        On input the RA mean equator & equinox at epoch ep0. On exit
*        the RA mean equator & equinox of epoch ep1.
*     dec = double * (Given & Returned)
*        On input the dec mean equator & equinox at epoch ep0. On exit
*        the dec mean equator & equinox of epoch ep1.

*  Description:
*     Precess coordinates using the appropriate system and epochs.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Uses palPrec for FK5 data and palPrebn for FK4 data.
*     - The epochs are Besselian if SYSTEM='FK4' and Julian if 'FK5'.
*        For example, to precess coordinates in the old system from
*        equinox 1900.0 to 1950.0 the call would be:
*             palPreces( "FK4", 1900.0, 1950.0, &ra, &dc );
*     - This routine will NOT correctly convert between the old and
*       the new systems - for example conversion from B1950 to J2000.
*       For these purposes see palFk425, palFk524, palFk45z and
*       palFk54z.
*     - If an invalid SYSTEM is supplied, values of -99D0,-99D0 will
*       be returned for both RA and DC.

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

#include <string.h>

void palPreces(
    const char sys[3], double ep0, double ep1, double *ra, double *dc) {

    double pm[3][3];
    double v1[3];
    double v2[3];

    /* Generate appropriate precession matrix */
    if (strncasecmp("FK4", sys, 3) == 0) {
        palPrebn(ep0, ep1, pm);
    } else if (strncasecmp("FK5", sys, 3) == 0) {
        palPrec(ep0, ep1, pm);
    } else {
        *ra = -99.0;
        *dc = -99.0;
        return;
    }

    /* Convert RA,Dec to x,y,z */
    eraS2c(*ra, *dc, v1);

    /* Precess */
    eraRxp(pm, v1, v2);

    /* Back to RA,Dec */
    eraC2s(v2, ra, dc);
    *ra = eraAnp(*ra);
}
/*
*+
*  Name:
*     palPrenut

*  Purpose:
*     Form the matrix of bias-precession-nutation (IAU 2006/2000A)

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPrenut( double epoch, double date, double rmatpn[3][3] )

*  Arguments:
*     epoch = double (Returned)
*        Julian epoch for mean coordinates.
*     date = double (Returned)
*        Modified Julian Date (JD-2400000.5) for true coordinates.
*     rmatpn = double[3][3] (Returned)
*        combined NPB matrix

*  Description:
*     Form the matrix of bias-precession-nutation (IAU 2006/2000A).
*     The epoch and date are TT (but TDB is usually close enough).
*     The matrix is in the sense   v(true)  =  rmatpn * v(mean).

*  Authors:
*     PTW: Pat Wallace (STFC)
*     {enter_new_authors_here}

*  History:
*     2012-02-10 (PTW):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palPrenut(double epoch, double date, double rmatpn[3][3]) {

    /* Local Variables: */
    double bpa;
    double bpia;
    double bqa;
    double chia;
    double d1;
    double d2;
    double eps0;
    double epsa;
    double gam;
    double oma;
    double pa;
    double phi;
    double pia;
    double psi;
    double psia;
    double r1[3][3];
    double r2[3][3];
    double thetaa;
    double za;
    double zetaa;

    /* Specified Julian epoch as a 2-part JD. */
    eraEpj2jd(epoch, &d1, &d2);

    /* P matrix, from specified epoch to J2000.0. */
    eraP06e(d1,
            d2,
            &eps0,
            &psia,
            &oma,
            &bpa,
            &bqa,
            &pia,
            &bpia,
            &epsa,
            &chia,
            &za,
            &zetaa,
            &thetaa,
            &pa,
            &gam,
            &phi,
            &psi);
    eraIr(r1);
    eraRz(-chia, r1);
    eraRx(oma, r1);
    eraRz(psia, r1);
    eraRx(-eps0, r1);

    /* NPB matrix, from J2000.0 to date. */
    eraPnm06a(PAL__MJD0, date, r2);

    /* NPB matrix, from specified epoch to date. */
    eraRxr(r2, r1, rmatpn);
}
/*
*+
*  Name:
*     palPv2el

*  Purpose:
*     Position velocity to heliocentirc osculating elements

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPv2el ( const double pv[6], double date, double pmass, int jformr,
*                     int *jform, double *epoch, double *orbinc,
*                     double *anode, double *perih, double *aorq, double *e,
*                     double *aorl, double *dm, int *jstat );

*  Arguments:
*     pv = const double [6] (Given)
*        Heliocentric x,y,z,xdot,ydot,zdot of date,
*        J2000 equatorial triad (AU,AU/s; Note 1)
*     date = double (Given)
*        Date (TT Modified Julian Date = JD-2400000.5)
*     pmass = double (Given)
*        Mass of the planet (Sun=1; Note 2)
*     jformr = int (Given)
*        Requested element set (1-3; Note 3)
*     jform = int * (Returned)
*        Element set actually returned (1-3; Note 4)
*     epoch = double * (Returned)
*        Epoch of elements (TT MJD)
*     orbinc = double * (Returned)
*        inclination (radians)
*     anode = double * (Returned)
*        longitude of the ascending node (radians)
*     perih = double * (Returned)
*        longitude or argument of perihelion (radians)
*     aorq = double * (Returned)
*        mean distance or perihelion distance (AU)
*     e = double * (Returned)
*        eccentricity
*     aorl = double * (Returned)
*        mean anomaly or longitude (radians, JFORM=1,2 only)
*     dm = double * (Returned)
*        daily motion (radians, JFORM=1 only)
*     jstat = int * (Returned)
*        status:  0 = OK
*               - -1 = illegal PMASS
*               - -2 = illegal JFORMR
*               - -3 = position/velocity out of range

*  Description:
*     Heliocentric osculating elements obtained from instantaneous position
*     and velocity.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The PV 6-vector is with respect to the mean equator and equinox of
*       epoch J2000.  The orbital elements produced are with respect to
*       the J2000 ecliptic and mean equinox.
*     - The mass, PMASS, is important only for the larger planets.  For
*       most purposes (e.g. asteroids) use 0D0.  Values less than zero
*       are illegal.
*     - Three different element-format options are supported:
*
*       Option JFORM=1, suitable for the major planets:
*
*       EPOCH  = epoch of elements (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = longitude of perihelion, curly pi (radians)
*       AORQ   = mean distance, a (AU)
*       E      = eccentricity, e
*       AORL   = mean longitude L (radians)
*       DM     = daily motion (radians)
*
*       Option JFORM=2, suitable for minor planets:
*
*       EPOCH  = epoch of elements (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = argument of perihelion, little omega (radians)
*       AORQ   = mean distance, a (AU)
*       E      = eccentricity, e
*       AORL   = mean anomaly M (radians)
*
*       Option JFORM=3, suitable for comets:
*
*       EPOCH  = epoch of perihelion (TT MJD)
*       ORBINC = inclination i (radians)
*       ANODE  = longitude of the ascending node, big omega (radians)
*       PERIH  = argument of perihelion, little omega (radians)
*       AORQ   = perihelion distance, q (AU)
*       E      = eccentricity, e
*
*     - It may not be possible to generate elements in the form
*       requested through JFORMR.  The caller is notified of the form
*       of elements actually returned by means of the JFORM argument:

*        JFORMR   JFORM     meaning
*
*          1        1       OK - elements are in the requested format
*          1        2       never happens
*          1        3       orbit not elliptical
*
*          2        1       never happens
*          2        2       OK - elements are in the requested format
*          2        3       orbit not elliptical
*
*          3        1       never happens
*          3        2       never happens
*          3        3       OK - elements are in the requested format
*
*     - The arguments returned for each value of JFORM (cf Note 5: JFORM
*       may not be the same as JFORMR) are as follows:
*
*         JFORM         1              2              3
*         EPOCH         t0             t0             T
*         ORBINC        i              i              i
*         ANODE         Omega          Omega          Omega
*         PERIH         curly pi       omega          omega
*         AORQ          a              a              q
*         E             e              e              e
*         AORL          L              M              -
*         DM            n              -              -
*
*       where:
*
*         t0           is the epoch of the elements (MJD, TT)
*         T              "    epoch of perihelion (MJD, TT)
*         i              "    inclination (radians)
*         Omega          "    longitude of the ascending node (radians)
*         curly pi       "    longitude of perihelion (radians)
*         omega          "    argument of perihelion (radians)
*         a              "    mean distance (AU)
*         q              "    perihelion distance (AU)
*         e              "    eccentricity
*         L              "    longitude (radians, 0-2pi)
*         M              "    mean anomaly (radians, 0-2pi)
*         n              "    daily motion (radians)
*         -             means no value is set
*
*     - At very small inclinations, the longitude of the ascending node
*       ANODE becomes indeterminate and under some circumstances may be
*       set arbitrarily to zero.  Similarly, if the orbit is close to
*       circular, the true anomaly becomes indeterminate and under some
*       circumstances may be set arbitrarily to zero.  In such cases,
*       the other elements are automatically adjusted to compensate,
*       and so the elements remain a valid description of the orbit.
*     - The osculating epoch for the returned elements is the argument
*       DATE.
*
*     - Reference:  Sterne, Theodore E., "An Introduction to Celestial
*                   Mechanics", Interscience Publishers, 1960

*  History:
*     2012-03-09 (TIMJ):
*        Initial version converted from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

void palPv2el(const double pv[6],
              double date,
              double pmass,
              int jformr,
              int *jform,
              double *epoch,
              double *orbinc,
              double *anode,
              double *perih,
              double *aorq,
              double *e,
              double *aorl,
              double *dm,
              int *jstat) {

    /*  Sin and cos of J2000 mean obliquity (IAU 1976) */
    const double SE = 0.3977771559319137;
    const double CE = 0.9174820620691818;

    /*  Minimum allowed distance (AU) and speed (AU/day) */
    const double RMIN = 1e-3;
    const double VMIN = 1e-8;

    /*  How close to unity the eccentricity has to be to call it a parabola */
    const double PARAB = 1.0e-8;

    double X, Y, Z, XD, YD, ZD, R, V2, V, RDV, GMU, HX, HY, HZ, HX2PY2, H2, H,
        OI, BIGOM, AR, ECC, S, C, AT, U, OM, GAR3, EM1, EP1, HAT, SHAT, CHAT,
        AE, AM, DN, PL, EL, Q, TP, THAT, THHF, F;

    int JF;

    /*  Validate arguments PMASS and JFORMR.*/
    if (pmass < 0.0) {
        *jstat = -1;
        return;
    }
    if (jformr < 1 || jformr > 3) {
        *jstat = -2;
        return;
    }

    /*  Provisionally assume the elements will be in the chosen form. */
    JF = jformr;

    /*  Rotate the position from equatorial to ecliptic coordinates. */
    X = pv[0];
    Y = pv[1] * CE + pv[2] * SE;
    Z = -pv[1] * SE + pv[2] * CE;

    /*  Rotate the velocity similarly, scaling to AU/day. */
    XD = PAL__SPD * pv[3];
    YD = PAL__SPD * (pv[4] * CE + pv[5] * SE);
    ZD = PAL__SPD * (-pv[4] * SE + pv[5] * CE);

    /*  Distance and speed. */
    R = sqrt(X * X + Y * Y + Z * Z);
    V2 = XD * XD + YD * YD + ZD * ZD;
    V = sqrt(V2);

    /*  Reject unreasonably small values. */
    if (R < RMIN || V < VMIN) {
        *jstat = -3;
        return;
    }

    /*  R dot V. */
    RDV = X * XD + Y * YD + Z * ZD;

    /*  Mu. */
    GMU = (1.0 + pmass) * PAL__GCON * PAL__GCON;

    /*  Vector angular momentum per unit reduced mass. */
    HX = Y * ZD - Z * YD;
    HY = Z * XD - X * ZD;
    HZ = X * YD - Y * XD;

    /*  Areal constant. */
    HX2PY2 = HX * HX + HY * HY;
    H2 = HX2PY2 + HZ * HZ;
    H = sqrt(H2);

    /*  Inclination. */
    OI = atan2(sqrt(HX2PY2), HZ);

    /*  Longitude of ascending node. */
    if (HX != 0.0 || HY != 0.0) {
        BIGOM = atan2(HX, -HY);
    } else {
        BIGOM = 0.0;
    }

    /*  Reciprocal of mean distance etc. */
    AR = 2.0 / R - V2 / GMU;

    /*  Eccentricity. */
    ECC = sqrt(DMAX(1.0 - AR * H2 / GMU, 0.0));

    /*  True anomaly. */
    S = H * RDV;
    C = H2 - R * GMU;
    if (S != 0.0 || C != 0.0) {
        AT = atan2(S, C);
    } else {
        AT = 0.0;
    }

    /*  Argument of the latitude. */
    S = sin(BIGOM);
    C = cos(BIGOM);
    U = atan2((-X * S + Y * C) * cos(OI) + Z * sin(OI), X * C + Y * S);

    /*  Argument of perihelion. */
    OM = U - AT;

    /*  Capture near-parabolic cases. */
    if (fabs(ECC - 1.0) < PARAB)
        ECC = 1.0;

    /*  Comply with JFORMR = 1 or 2 only if orbit is elliptical. */
    if (ECC > 1.0)
        JF = 3;

    /*  Functions. */
    GAR3 = GMU * AR * AR * AR;
    EM1 = ECC - 1.0;
    EP1 = ECC + 1.0;
    HAT = AT / 2.0;
    SHAT = sin(HAT);
    CHAT = cos(HAT);

    /*  Variable initializations to avoid compiler warnings. */
    AM = 0.0;
    DN = 0.0;
    PL = 0.0;
    EL = 0.0;
    Q = 0.0;
    TP = 0.0;

    /*  Ellipse? */
    if (ECC < 1.0) {

        /*     Eccentric anomaly. */
        AE = 2.0 * atan2(sqrt(-EM1) * SHAT, sqrt(EP1) * CHAT);

        /*     Mean anomaly. */
        AM = AE - ECC * sin(AE);

        /*     Daily motion. */
        DN = sqrt(GAR3);
    }

    /*  "Major planet" element set? */
    if (JF == 1) {

        /*     Longitude of perihelion. */
        PL = BIGOM + OM;

        /*     Longitude at epoch. */
        EL = PL + AM;
    }

    /*  "Comet" element set? */
    if (JF == 3) {

        /*     Perihelion distance. */
        Q = H2 / (GMU * EP1);

        /*     Ellipse, parabola, hyperbola? */
        if (ECC < 1.0) {

            /*        Ellipse: epoch of perihelion. */
            TP = date - AM / DN;

        } else {

            /*        Parabola or hyperbola: evaluate tan ( ( true anomaly ) / 2
             * ) */
            THAT = SHAT / CHAT;
            if (ECC == 1.0) {

                /*           Parabola: epoch of perihelion. */
                TP = date - THAT * (1.0 + THAT * THAT / 3.0) * H * H2 /
                                (2.0 * GMU * GMU);

            } else {

                /*           Hyperbola: epoch of perihelion. */
                THHF = sqrt(EM1 / EP1) * THAT;
                F = log(1.0 + THHF) - log(1.0 - THHF);
                TP = date - (ECC * sinh(F) - F) / sqrt(-GAR3);
            }
        }
    }

    /*  Return the appropriate set of elements. */
    *jform = JF;
    *orbinc = OI;
    *anode = eraAnp(BIGOM);
    *e = ECC;
    if (JF == 1) {
        *perih = eraAnp(PL);
        *aorl = eraAnp(EL);
        *dm = DN;
    } else {
        *perih = eraAnp(OM);
        if (JF == 2)
            *aorl = eraAnp(AM);
    }
    if (JF != 3) {
        *epoch = date;
        *aorq = 1.0 / AR;
    } else {
        *epoch = TP;
        *aorq = Q;
    }
    *jstat = 0;
}
/*
*+
*  Name:
*     palPv2ue

*  Purpose:
*     Universal elements to position and velocity.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palPv2ue( const double pv[6], double date, double pmass,
*                    double u[13], int * jstat );

*  Arguments:
*     pv = double [6] (Given)
*        Heliocentric x,y,z,xdot,ydot,zdot of date, (AU,AU/s; Note 1)
*     date = double (Given)
*        Date (TT modified Julian Date = JD-2400000.5)
*     pmass = double (Given)
*        Mass of the planet (Sun=1; note 2)
*     u = double [13] (Returned)
*        Universal orbital elements (Note 3)
*
*          -  (0)  combined mass (M+m)
*          -   (1)  total energy of the orbit (alpha)
*          -   (2)  reference (osculating) epoch (t0)
*          - (3-5)  position at reference epoch (r0)
*          - (6-8)  velocity at reference epoch (v0)
*          -   (9)  heliocentric distance at reference epoch
*          -  (10)  r0.v0
*          -  (11)  date (t)
*          -  (12)  universal eccentric anomaly (psi) of date, approx
*     jstat = int * (Returned)
*        status: 0 = OK
*               - -1 = illegal PMASS
*               - -2 = too close to Sun
*               - -3 = too slow

*  Description:
*     Construct a universal element set based on an instantaneous position
*     and velocity.


*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The PV 6-vector can be with respect to any chosen inertial frame,
*       and the resulting universal-element set will be with respect to
*       the same frame.  A common choice will be mean equator and ecliptic
*       of epoch J2000.
*     - The mass, PMASS, is important only for the larger planets.  For
*       most purposes (e.g. asteroids) use 0D0.  Values less than zero
*       are illegal.
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - Reference:  Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-09 (TIMJ):
*        Initial version from the SLA/F implementation.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palPv2ue(
    const double pv[6], double date, double pmass, double u[13], int *jstat) {

    /*  Canonical days to seconds */
    const double CD2S = PAL__GCON / PAL__SPD;

    /*  Minimum allowed distance (AU) and speed (AU per canonical day) */
    const double RMIN = 1e-3;
    const double VMIN = 1e-3;

    double T0, CM, X, Y, Z, XD, YD, ZD, R, V2, V, ALPHA, RDV;

    /*  Reference epoch. */
    T0 = date;

    /*  Combined mass (mu=M+m). */
    if (pmass < 0.0) { /* Negative planet mass */
        *jstat = -1;
        return;
    }
    CM = 1.0 + pmass;

    /*  Unpack the state vector, expressing velocity in AU per canonical day. */
    X = pv[0];
    Y = pv[1];
    Z = pv[2];
    XD = pv[3] / CD2S;
    YD = pv[4] / CD2S;
    ZD = pv[5] / CD2S;

    /*  Heliocentric distance, and speed. */
    R = sqrt(X * X + Y * Y + Z * Z);
    V2 = XD * XD + YD * YD + ZD * ZD;
    V = sqrt(V2);

    /*  Reject unreasonably small values. */
    if (R < RMIN) { /* Too close */
        *jstat = -2;
        return;
    }
    if (V < VMIN) { /* Too slow */
        *jstat = -3;
        return;
    }

    /*  Total energy of the orbit. */
    ALPHA = V2 - 2.0 * CM / R;

    /*  Outward component of velocity. */
    RDV = X * XD + Y * YD + Z * ZD;

    /*  Construct the universal-element set. */
    u[0] = CM;
    u[1] = ALPHA;
    u[2] = T0;
    u[3] = X;
    u[4] = Y;
    u[5] = Z;
    u[6] = XD;
    u[7] = YD;
    u[8] = ZD;
    u[9] = R;
    u[10] = RDV;
    u[11] = T0;
    u[12] = 0.0;

    *jstat = 0;
    return;
}
/*
*+
*  Name:
*     palPvobs

*  Purpose:
*     Position and velocity of an observing station.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     palPvobs( double p, double h, double stl, double pv[6] )

*  Arguments:
*     p = double (Given)
*        Latitude (geodetic, radians).
*     h = double (Given)
*        Height above reference spheroid (geodetic, metres).
*     stl = double (Given)
*        Local apparent sidereal time (radians).
*     pv = double[ 6 ] (Returned)
*        position/velocity 6-vector (AU, AU/s, true equator
*                                    and equinox of date).

*  Description:
*     Returns the position and velocity of an observing station.

*  Notes:
*     - The WGS84 reference ellipsoid is used.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-16 (DSB):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

void palPvobs(double p, double h, double stl, double pv[6]) {

    /* Local Variables: */
    double xyz[3], z, r, s, c, v;

    /* Geodetic to geocentric conversion (WGS84 reference ellipsoid). */
    eraGd2gc(ERFA_WGS84, 0.0, p, h, xyz);

    /* Convert from metres to AU */
    r = xyz[0] / ERFA_DAU;
    z = xyz[2] / ERFA_DAU;

    /* Functions of ST. */
    s = sin(stl);
    c = cos(stl);

    /* Speed. */
    v = PAL__SR * r;

    /* Position. */
    pv[0] = r * c;
    pv[1] = r * s;
    pv[2] = z;

    /* Velocity. */
    pv[3] = -v * s;
    pv[4] = v * c;
    pv[5] = 0.0;
}

/*
*+
*  Name:
*     palRdplan

*  Purpose:
*     Approximate topocentric apparent RA,Dec of a planet

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palRdplan( double date, int np, double elong, double phi,
*                     double * ra, double * dec, double * diam );

*  Arguments:
*     date = double (Given)
*        MJD of observation (JD-2400000.5) in TDB. For all practical
*        purposes TT can be used instead of TDB, and for many applications
*        UT will do (except for the Moon).
*     np = int (Given)
*        Planet: 1 = Mercury
*                2 = Venus
*                3 = Moon
*                4 = Mars
*                5 = Jupiter
*                6 = Saturn
*                7 = Uranus
*                8 = Neptune
*             else = Sun
*     elong = double (Given)
*        Observer's east longitude (radians)
*     phi = double (Given)
*        Observer's geodetic latitude (radians)
*     ra = double * (Returned)
*        RA (topocentric apparent, radians)
*     dec = double * (Returned)
*        Dec (topocentric apparent, radians)
*     diam = double * (Returned)
*        Angular diameter (equatorial, radians)

*  Description:
*     Approximate topocentric apparent RA,Dec of a planet, and its
*     angular diameter.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Unlike with slaRdplan, Pluto is not supported.
*     - The longitude and latitude allow correction for geocentric
*       parallax.  This is a major effect for the Moon, but in the
*       context of the limited accuracy of the present routine its
*       effect on planetary positions is small (negligible for the
*       outer planets).  Geocentric positions can be generated by
*       appropriate use of the routines palDmoon and eraPlan94.

*  History:
*     2012-03-07 (TIMJ):
*        Initial version, with some documentation from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1997 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"
#include "palmac.h"

#include <math.h>

void palRdplan(double date,
               int np,
               double elong,
               double phi,
               double *ra,
               double *dec,
               double *diam) {

    /* AU in km */
    const double AUKM = 1.49597870e8;

    /* Equatorial radii (km) */
    const double EQRAU[] = {696000.0, /* Sun */
                            2439.7,
                            6051.9,
                            1738,
                            3397,
                            71492,
                            60268,
                            25559,
                            24764};

    /* Local variables */
    int i, j;
    double stl;
    double vgm[6];
    double v[6];
    double rmat[3][3];
    double vse[6];
    double vsg[6];
    double vsp[6];
    double vgo[6];
    double dx, dy, dz, r, tl;

    /* Classify np */
    if (np < 0 || np > 8)
        np = 0; /* Sun */

    /* Approximate local sidereal time */
    stl = palGmst(date - palDt(palEpj(date)) / 86400.0) + elong;

    /* Geocentre to Moon (mean of date) */
    palDmoon(date, v);

    /* Nutation to true of date */
    palNut(date, rmat);
    eraRxp(rmat, v, vgm);
    eraRxp(rmat, &(v[3]), &(vgm[3]));

    /* Moon? */
    if (np == 3) {

        /* geocentre to Moon (true of date) */
        for (i = 0; i < 6; i++) {
            v[i] = vgm[i];
        }

    } else {

        /* Not moon: precession/nutation matrix J2000 to date */
        palPrenut(2000.0, date, rmat);

        /* Sun to Earth-Moon Barycentre (J2000) */
        palPlanet(date, 3, v, &j);

        /* Precession and nutation to date */
        eraRxp(rmat, v, vse);
        eraRxp(rmat, &(v[3]), &(vse[3]));

        /* Sun to geocentre (true of date) */
        for (i = 0; i < 6; i++) {
            vsg[i] = vse[i] - 0.012150581 * vgm[i];
        }

        /* Sun ? */
        if (np == 0) {

            /* Geocentre to Sun */
            for (i = 0; i < 6; i++) {
                v[i] = -vsg[i];
            }

        } else {

            /* Sun to Planet (J2000) */
            palPlanet(date, np, v, &j);

            /* Precession and nutation to date */
            eraRxp(rmat, v, vsp);
            eraRxp(rmat, &(v[3]), &(vsp[3]));

            /* Geocentre to planet */
            for (i = 0; i < 6; i++) {
                v[i] = vsp[i] - vsg[i];
            }
        }
    }

    /* Refer to origina at the observer */
    palPvobs(phi, 0.0, stl, vgo);
    for (i = 0; i < 6; i++) {
        v[i] -= vgo[i];
    }

    /* Geometric distance (AU) */
    dx = v[0];
    dy = v[1];
    dz = v[2];
    r = sqrt(dx * dx + dy * dy + dz * dz);

    /* Light time */
    tl = PAL__CR * r;

    /* Correct position for planetary aberration */
    for (i = 0; i < 3; i++) {
        v[i] -= tl * v[i + 3];
    }

    /* To RA,Dec */
    eraC2s(v, ra, dec);
    *ra = eraAnp(*ra);

    /* Angular diameter (radians) */
    *diam = 2.0 * asin(EQRAU[np] / (r * AUKM));
}
/*
*+
*  Name:
*     palRefco

*  Purpose:
*     Determine constants in atmospheric refraction model

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palRefco ( double hm, double tdk, double pmb, double rh,
*                     double wl, double phi, double tlr, double eps,
*                     double *refa, double *refb );

*  Arguments:
*     hm = double (Given)
*        Height of the observer above sea level (metre)
*     tdk = double (Given)
*        Ambient temperature at the observer (K)
*     pmb = double (Given)
*        Pressure at the observer (millibar)
*     rh = double (Given)
*        Relative humidity at the observer (range 0-1)
*     wl = double (Given)
*        Effective wavelength of the source (micrometre)
*     phi = double (Given)
*        Latitude of the observer (radian, astronomical)
*     tlr = double (Given)
*        Temperature lapse rate in the troposphere (K/metre)
*     eps = double (Given)
*        Precision required to terminate iteration (radian)
*     refa = double * (Returned)
*        tan Z coefficient (radian)
*     refb = double * (Returned)
*        tan**3 Z coefficient (radian)

*  Description:
*     Determine the constants A and B in the atmospheric refraction
*     model dZ = A tan Z + B tan**3 Z.
*
*     Z is the "observed" zenith distance (i.e. affected by refraction)
*     and dZ is what to add to Z to give the "topocentric" (i.e. in vacuo)
*     zenith distance.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - Typical values for the TLR and EPS arguments might be 0.0065 and
*     1E-10 respectively.
*
*     - The radio refraction is chosen by specifying WL > 100 micrometres.
*
*     - The routine is a slower but more accurate alternative to the
*     palRefcoq routine.  The constants it produces give perfect
*     agreement with palRefro at zenith distances arctan(1) (45 deg)
*     and arctan(4) (about 76 deg).  It achieves 0.5 arcsec accuracy
*     for ZD < 80 deg, 0.01 arcsec accuracy for ZD < 60 deg, and
*     0.001 arcsec accuracy for ZD < 45 deg.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version. A direct copy of the Fortran SLA implementation.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

void palRefco(double hm,
              double tdk,
              double pmb,
              double rh,
              double wl,
              double phi,
              double tlr,
              double eps,
              double *refa,
              double *refb) {

    double r1, r2;

    /*  Sample zenith distances: arctan(1) and arctan(4) */
    const double ATN1 = 0.7853981633974483;
    const double ATN4 = 1.325817663668033;

    /*  Determine refraction for the two sample zenith distances */
    palRefro(ATN1, hm, tdk, pmb, rh, wl, phi, tlr, eps, &r1);
    palRefro(ATN4, hm, tdk, pmb, rh, wl, phi, tlr, eps, &r2);

    /*  Solve for refraction constants */
    *refa = (64.0 * r1 - r2) / 60.0;
    *refb = (r2 - 4.0 * r1) / 60.0;
}
/*
*+
*  Name:
*     palRefro

*  Purpose:
*     Atmospheric refraction for radio and optical/IR wavelengths

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palRefro( double zobs, double hm, double tdk, double pmb,
                     double rh, double wl, double phi, double tlr,
                     double eps, double * ref ) {

*  Arguments:
*     zobs = double (Given)
*        Observed zenith distance of the source (radian)
*     hm = double (Given)
*        Height of the observer above sea level (metre)
*     tdk = double (Given)
*        Ambient temperature at the observer (K)
*     pmb = double (Given)
*        Pressure at the observer (millibar)
*     rh = double (Given)
*        Relative humidity at the observer (range 0-1)
*     wl = double (Given)
*        Effective wavelength of the source (micrometre)
*     phi = double (Given)
*        Latitude of the observer (radian, astronomical)
*     tlr = double (Given)
*        Temperature lapse rate in the troposphere (K/metre)
*     eps = double (Given)
*        Precision required to terminate iteration (radian)
*     ref = double * (Returned)
*        Refraction: in vacuao ZD minus observed ZD (radian)

*  Description:
*     Calculates the atmospheric refraction for radio and optical/IR
*     wavelengths.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - A suggested value for the TLR argument is 0.0065.  The
*     refraction is significantly affected by TLR, and if studies
*     of the local atmosphere have been carried out a better TLR
*     value may be available.  The sign of the supplied TLR value
*     is ignored.
*
*     - A suggested value for the EPS argument is 1E-8.  The result is
*     usually at least two orders of magnitude more computationally
*     precise than the supplied EPS value.
*
*     - The routine computes the refraction for zenith distances up
*     to and a little beyond 90 deg using the method of Hohenkerk
*     and Sinclair (NAO Technical Notes 59 and 63, subsequently adopted
*     in the Explanatory Supplement, 1992 edition - see section 3.281).
*
*     - The code is a development of the optical/IR refraction subroutine
*     AREF of C.Hohenkerk (HMNAO, September 1984), with extensions to
*     support the radio case.  Apart from merely cosmetic changes, the
*     following modifications to the original HMNAO optical/IR refraction
*     code have been made:
*
*     .  The angle arguments have been changed to radians.
*
*     .  Any value of ZOBS is allowed (see note 6, below).
*
*     .  Other argument values have been limited to safe values.
*
*     .  Murray's values for the gas constants have been used
*        (Vectorial Astrometry, Adam Hilger, 1983).
*
*     .  The numerical integration phase has been rearranged for
*        extra clarity.
*
*     .  A better model for Ps(T) has been adopted (taken from
*        Gill, Atmosphere-Ocean Dynamics, Academic Press, 1982).
*
*     .  More accurate expressions for Pwo have been adopted
*        (again from Gill 1982).
*
*     .  The formula for the water vapour pressure, given the
*        saturation pressure and the relative humidity, is from
*        Crane (1976), expression 2.5.5.

*     .  Provision for radio wavelengths has been added using
*        expressions devised by A.T.Sinclair, RGO (private
*        communication 1989).  The refractivity model currently
*        used is from J.M.Rueger, "Refractive Index Formulae for
*        Electronic Distance Measurement with Radio and Millimetre
*        Waves", in Unisurv Report S-68 (2002), School of Surveying
*        and Spatial Information Systems, University of New South
*        Wales, Sydney, Australia.
*
*     .  The optical refractivity for dry air is from Resolution 3 of
*        the International Association of Geodesy adopted at the XXIIth
*        General Assembly in Birmingham, UK, 1999.
*
*     .  Various small changes have been made to gain speed.
*
*     - The radio refraction is chosen by specifying WL > 100 micrometres.
*     Because the algorithm takes no account of the ionosphere, the
*     accuracy deteriorates at low frequencies, below about 30 MHz.
*
*     - Before use, the value of ZOBS is expressed in the range +/- pi.
*     If this ranged ZOBS is -ve, the result REF is computed from its
*     absolute value before being made -ve to match.  In addition, if
*     it has an absolute value greater than 93 deg, a fixed REF value
*     equal to the result for ZOBS = 93 deg is returned, appropriately
*     signed.
*
*     - As in the original Hohenkerk and Sinclair algorithm, fixed values
*     of the water vapour polytrope exponent, the height of the
*     tropopause, and the height at which refraction is negligible are
*     used.
*
*     - The radio refraction has been tested against work done by
*     Iain Coulson, JACH, (private communication 1995) for the
*     James Clerk Maxwell Telescope, Mauna Kea.  For typical conditions,
*     agreement at the 0.1 arcsec level is achieved for moderate ZD,
*     worsening to perhaps 0.5-1.0 arcsec at ZD 80 deg.  At hot and
*     humid sea-level sites the accuracy will not be as good.
*
*     - It should be noted that the relative humidity RH is formally
*     defined in terms of "mixing ratio" rather than pressures or
*     densities as is often stated.  It is the mass of water per unit
*     mass of dry air divided by that for saturated air at the same
*     temperature and pressure (see Gill 1982).

*     - The algorithm is designed for observers in the troposphere. The
*     supplied temperature, pressure and lapse rate are assumed to be
*     for a point in the troposphere and are used to define a model
*     atmosphere with the tropopause at 11km altitude and a constant
*     temperature above that.  However, in practice, the refraction
*     values returned for stratospheric observers, at altitudes up to
*     25km, are quite usable.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version, direct port of SLA Fortran source.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Patrick T. Wallace
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1.h"
#include "palmac.h"

#include <math.h>

void palRefro(double zobs,
              double hm,
              double tdk,
              double pmb,
              double rh,
              double wl,
              double phi,
              double tlr,
              double eps,
              double *ref) {

    /*
     *  Fixed parameters
     */

    /*  93 degrees in radians */
    const double D93 = 1.623156204;
    /*  Universal gas constant */
    const double GCR = 8314.32;
    /*  Molecular weight of dry air */
    const double DMD = 28.9644;
    /*  Molecular weight of water vapour */
    const double DMW = 18.0152;
    /*  Mean Earth radius (metre) */
    const double S = 6378120.;
    /*  Exponent of temperature dependence of water vapour pressure */
    const double DELTA = 18.36;
    /*  Height of tropopause (metre) */
    const double HT = 11000.;
    /*  Upper limit for refractive effects (metre) */
    const double HS = 80000.;
    /*  Numerical integration: maximum number of strips. */
    const int ISMAX = 16384l;

    /* Local variables */
    int is, k, n, i, j;

    int optic, loop; /* booleans */

    double zobs1, zobs2, hmok, tdkok, pmbok, rhok, wlok, alpha, tol, wlsq, gb,
        a, gamal, gamma, gamm2, delm2, tdc, psat, pwo, w, c1, c2, c3, c4, c5,
        c6, r0, tempo, dn0, rdndr0, sk0, f0, rt, tt, dnt, rdndrt, sine, zt, ft,
        dnts, rdndrp, zts, fts, rs, dns, rdndrs, zs, fs, refold, z0, zrange, fb,
        ff, fo, fe, h, r, sz, rg, dr, tg, dn, rdndr, t, f, refp, reft;

    /*  The refraction integrand */
#define refi(DN, RDNDR) RDNDR / (DN + RDNDR)

    /*  Transform ZOBS into the normal range. */
    zobs1 = palDrange(zobs);
    zobs2 = DMIN(fabs(zobs1), D93);

    /*  keep other arguments within safe bounds. */
    hmok = DMIN(DMAX(hm, -1e3), HS);
    tdkok = DMIN(DMAX(tdk, 100.0), 500.0);
    pmbok = DMIN(DMAX(pmb, 0.0), 10000.0);
    rhok = DMIN(DMAX(rh, 0.0), 1.0);
    wlok = DMAX(wl, 0.1);
    alpha = DMIN(DMAX(fabs(tlr), 0.001), 0.01);

    /*  tolerance for iteration. */
    tol = DMIN(DMAX(fabs(eps), 1e-12), 0.1) / 2.0;

    /*  decide whether optical/ir or radio case - switch at 100 microns. */
    optic = wlok < 100.0;

    /*  set up model atmosphere parameters defined at the observer. */
    wlsq = wlok * wlok;
    gb = 9.784 * (1.0 - 0.0026 * cos(phi + phi) - 0.00000028 * hmok);
    if (optic) {
        a = (287.6155 + (1.62887 + 0.01360 / wlsq) / wlsq) * 273.15e-6 /
            1013.25;
    } else {
        a = 77.6890e-6;
    }
    gamal = (gb * DMD) / GCR;
    gamma = gamal / alpha;
    gamm2 = gamma - 2.0;
    delm2 = DELTA - 2.0;
    tdc = tdkok - 273.15;
    psat = pow(10.0, (0.7859 + 0.03477 * tdc) / (1.0 + 0.00412 * tdc)) *
           (1.0 + pmbok * (4.5e-6 + 6.0e-10 * tdc * tdc));
    if (pmbok > 0.0) {
        pwo = rhok * psat / (1.0 - (1.0 - rhok) * psat / pmbok);
    } else {
        pwo = 0.0;
    }
    w = pwo * (1.0 - DMW / DMD) * gamma / (DELTA - gamma);
    c1 = a * (pmbok + w) / tdkok;
    if (optic) {
        c2 = (a * w + 11.2684e-6 * pwo) / tdkok;
    } else {
        c2 = (a * w + 6.3938e-6 * pwo) / tdkok;
    }
    c3 = (gamma - 1.0) * alpha * c1 / tdkok;
    c4 = (DELTA - 1.0) * alpha * c2 / tdkok;
    if (optic) {
        c5 = 0.0;
        c6 = 0.0;
    } else {
        c5 = 375463e-6 * pwo / tdkok;
        c6 = c5 * delm2 * alpha / (tdkok * tdkok);
    }

    /*  conditions at the observer. */
    r0 = S + hmok;
    pal1Atmt(r0,
             tdkok,
             alpha,
             gamm2,
             delm2,
             c1,
             c2,
             c3,
             c4,
             c5,
             c6,
             r0,
             &tempo,
             &dn0,
             &rdndr0);
    sk0 = dn0 * r0 * sin(zobs2);
    f0 = refi(dn0, rdndr0);

    /*  conditions in the troposphere at the tropopause. */
    rt = S + DMAX(HT, hmok);
    pal1Atmt(r0,
             tdkok,
             alpha,
             gamm2,
             delm2,
             c1,
             c2,
             c3,
             c4,
             c5,
             c6,
             rt,
             &tt,
             &dnt,
             &rdndrt);
    sine = sk0 / (rt * dnt);
    zt = atan2(sine, sqrt(DMAX(1.0 - sine * sine, 0.0)));
    ft = refi(dnt, rdndrt);

    /*  conditions in the stratosphere at the tropopause. */
    pal1Atms(rt, tt, dnt, gamal, rt, &dnts, &rdndrp);
    sine = sk0 / (rt * dnts);
    zts = atan2(sine, sqrt(DMAX(1.0 - sine * sine, 0.0)));
    fts = refi(dnts, rdndrp);

    /*  conditions at the stratosphere limit. */
    rs = S + HS;
    pal1Atms(rt, tt, dnt, gamal, rs, &dns, &rdndrs);
    sine = sk0 / (rs * dns);
    zs = atan2(sine, sqrt(DMAX(1.0 - sine * sine, 0.0)));
    fs = refi(dns, rdndrs);

    /*  variable initialization to avoid compiler warning. */
    reft = 0.0;

    /*  integrate the refraction integral in two parts;  first in the
     *  troposphere (k=1), then in the stratosphere (k=2). */

    for (k = 1; k <= 2; k++) {

        /*     initialize previous refraction to ensure at least two iterations.
         */
        refold = 1.0;

        /*     start off with 8 strips. */
        is = 8;

        /*     start z, z range, and start and end values. */
        if (k == 1) {
            z0 = zobs2;
            zrange = zt - z0;
            fb = f0;
            ff = ft;
        } else {
            z0 = zts;
            zrange = zs - z0;
            fb = fts;
            ff = fs;
        }

        /*     sums of odd and even values. */
        fo = 0.0;
        fe = 0.0;

        /*     first time through the loop we have to do every point. */
        n = 1;

        /*     start of iteration loop (terminates at specified precision). */
        loop = 1;
        while (loop) {

            /*        strip width. */
            h = zrange / ((double)is);

            /*        initialize distance from earth centre for quadrature pass.
             */
            if (k == 1) {
                r = r0;
            } else {
                r = rt;
            }

            /*        one pass (no need to compute evens after first time). */
            for (i = 1; i < is; i += n) {

                /*           sine of observed zenith distance. */
                sz = sin(z0 + h * (double)(i));

                /*           find r (to the nearest metre, maximum four
                 * iterations). */
                if (sz > 1e-20) {
                    w = sk0 / sz;
                    rg = r;
                    dr = 1.0e6;
                    j = 0;
                    while (fabs(dr) > 1.0 && j < 4) {
                        j++;
                        if (k == 1) {
                            pal1Atmt(r0,
                                     tdkok,
                                     alpha,
                                     gamm2,
                                     delm2,
                                     c1,
                                     c2,
                                     c3,
                                     c4,
                                     c5,
                                     c6,
                                     rg,
                                     &tg,
                                     &dn,
                                     &rdndr);
                        } else {
                            pal1Atms(rt, tt, dnt, gamal, rg, &dn, &rdndr);
                        }
                        dr = (rg * dn - w) / (dn + rdndr);
                        rg = rg - dr;
                    }
                    r = rg;
                }

                /*           find the refractive index and integrand at r. */
                if (k == 1) {
                    pal1Atmt(r0,
                             tdkok,
                             alpha,
                             gamm2,
                             delm2,
                             c1,
                             c2,
                             c3,
                             c4,
                             c5,
                             c6,
                             r,
                             &t,
                             &dn,
                             &rdndr);
                } else {
                    pal1Atms(rt, tt, dnt, gamal, r, &dn, &rdndr);
                }
                f = refi(dn, rdndr);

                /*           accumulate odd and (first time only) even values.
                 */
                if (n == 1 && i % 2 == 0) {
                    fe += f;
                } else {
                    fo += f;
                }
            }

            /*        evaluate the integrand using simpson's rule. */
            refp = h * (fb + 4.0 * fo + 2.0 * fe + ff) / 3.0;

            /*        has the required precision been achieved (or can't be)? */
            if (fabs(refp - refold) > tol && is < ISMAX) {

                /*           no: prepare for next iteration.*/

                /*           save current value for convergence test. */
                refold = refp;

                /*           double the number of strips. */
                is += is;

                /*           sum of all current values = sum of next pass's even
                 * values. */
                fe += fo;

                /*           prepare for new odd values. */
                fo = 0.0;

                /*           skip even values next time. */
                n = 2;
            } else {

                /*           yes: save troposphere component and terminate the
                 * loop. */
                if (k == 1)
                    reft = refp;
                loop = 0;
            }
        }
    }

    /*  result. */
    *ref = reft + refp;
    if (zobs1 < 0.0)
        *ref = -(*ref);
}
/*
*+
*  Name:
*     palRefv

*  Purpose:
*     Adjust an unrefracted Cartesian vector to include the effect of
atmospheric refraction

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palRefv ( double vu[3], double refa, double refb, double vr[3] );

*  Arguments:
*     vu[3] = double (Given)
*        Unrefracted position of the source (Az/El 3-vector)
*     refa = double (Given)
*        tan Z coefficient (radian)
*     refb = double (Given)
*        tan**3 Z coefficient (radian)
*     vr[3] = double (Returned)
*        Refracted position of the source (Az/El 3-vector)

*  Description:
*     Adjust an unrefracted Cartesian vector to include the effect of
*     atmospheric refraction, using the simple A tan Z + B tan**3 Z
*     model.

*  Authors:
*     TIMJ: Tim Jenness
*     PTW: Patrick Wallace
*     {enter_new_authors_here}

*  Notes:
*     - This routine applies the adjustment for refraction in the
*     opposite sense to the usual one - it takes an unrefracted
*     (in vacuo) position and produces an observed (refracted)
*     position, whereas the A tan Z + B tan**3 Z model strictly
*     applies to the case where an observed position is to have the
*     refraction removed.  The unrefracted to refracted case is
*     harder, and requires an inverted form of the text-book
*     refraction models;  the algorithm used here is equivalent to
*     one iteration of the Newton-Raphson method applied to the above
*     formula.
*
*     - Though optimized for speed rather than precision, the present
*     routine achieves consistency with the refracted-to-unrefracted
*     A tan Z + B tan**3 Z model at better than 1 microarcsecond within
*     30 degrees of the zenith and remains within 1 milliarcsecond to
*     beyond ZD 70 degrees.  The inherent accuracy of the model is, of
*     course, far worse than this - see the documentation for sla_REFCO
*     for more information.
*
*     - At low elevations (below about 3 degrees) the refraction
*     correction is held back to prevent arithmetic problems and
*     wildly wrong results.  For optical/IR wavelengths, over a wide
*     range of observer heights and corresponding temperatures and
*     pressures, the following levels of accuracy (arcsec, worst case)
*     are achieved, relative to numerical integration through a model
*     atmosphere:
*
*              ZD    error
*
*              80      0.7
*              81      1.3
*              82      2.5
*              83      5
*              84     10
*              85     20
*              86     55
*              87    160
*              88    360
*              89    640
*              90   1100
*              91   1700         } relevant only to
*              92   2600         } high-elevation sites
*
*     The results for radio are slightly worse over most of the range,
*     becoming significantly worse below ZD=88 and unusable beyond
*     ZD=90.
*
*     - See also the routine palRefz, which performs the adjustment to
*     the zenith distance rather than in Cartesian Az/El coordinates.
*     The present routine is faster than palRefz and, except very low down,
*     is equally accurate for all practical purposes.  However, beyond
*     about ZD 84 degrees palRefz should be used, and for the utmost
*     accuracy iterative use of palRefro should be considered.

*  History:
*     2014-07-15 (TIMJ):
*        Initial version. A direct copy of the Fortran SLA implementation.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2014 Tim Jenness
*     Copyright (C) 2004 Patrick Wallace
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palRefv(double vu[3], double refa, double refb, double vr[3]) {

    double x, y, z1, z, zsq, rsq, r, wb, wt, d, cd, f;

    /*  Initial estimate = unrefracted vector */
    x = vu[0];
    y = vu[1];
    z1 = vu[2];

    /*  Keep correction approximately constant below about 3 deg elevation */
    z = DMAX(z1, 0.05);

    /*  One Newton-Raphson iteration */
    zsq = z * z;
    rsq = x * x + y * y;
    r = sqrt(rsq);
    wb = refb * rsq / zsq;
    wt = (refa + wb) / (1.0 + (refa + 3.0 * wb) * (zsq + rsq) / zsq);
    d = wt * r / z;
    cd = 1.0 - d * d / 2.0;
    f = cd * (1.0 - wt);

    /*  Post-refraction x,y,z */
    vr[0] = x * f;
    vr[1] = y * f;
    vr[2] = cd * (z + d * r) + (z1 - z);
}
/*
*+
*  Name:
*     palRefz

*  Purpose:
*     Adjust unrefracted zenith distance

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palRefz ( double zu, double refa, double refb, double *zr );

*  Arguments:
*     zu = double (Given)
*         Unrefracted zenith distance of the source (radians)
*     refa = double (Given)
*         tan Z coefficient (radians)
*     refb = double (Given)
*         tan**3 Z coefficient (radian)
*     zr = double * (Returned)
*         Refracted zenith distance (radians)

*  Description:
*     Adjust an unrefracted zenith distance to include the effect of
*     atmospheric refraction, using the simple A tan Z + B tan**3 Z
*     model (plus special handling for large ZDs).

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - This routine applies the adjustment for refraction in the
*     opposite sense to the usual one - it takes an unrefracted
*     (in vacuo) position and produces an observed (refracted)
*     position, whereas the A tan Z + B tan**3 Z model strictly
*     applies to the case where an observed position is to have the
*     refraction removed.  The unrefracted to refracted case is
*     harder, and requires an inverted form of the text-book
*     refraction models;  the formula used here is based on the
*     Newton-Raphson method.  For the utmost numerical consistency
*     with the refracted to unrefracted model, two iterations are
*     carried out, achieving agreement at the 1D-11 arcseconds level
*     for a ZD of 80 degrees.  The inherent accuracy of the model
*     is, of course, far worse than this - see the documentation for
*     palRefco for more information.
*
*     - At ZD 83 degrees, the rapidly-worsening A tan Z + B tan^3 Z
*     model is abandoned and an empirical formula takes over.  For
*     optical/IR wavelengths, over a wide range of observer heights and
*     corresponding temperatures and pressures, the following levels of
*     accuracy (arcsec, worst case) are achieved, relative to numerical
*     integration through a model atmosphere:
*
*              ZR    error
*
*              80      0.7
*              81      1.3
*              82      2.4
*              83      4.7
*              84      6.2
*              85      6.4
*              86      8
*              87     10
*              88     15
*              89     30
*              90     60
*              91    150         } relevant only to
*              92    400         } high-elevation sites
*
*     For radio wavelengths the errors are typically 50% larger than
*     the optical figures and by ZD 85 deg are twice as bad, worsening
*     rapidly below that.  To maintain 1 arcsec accuracy down to ZD=85
*     at the Green Bank site, Condon (2004) has suggested amplifying
*     the amount of refraction predicted by palRefz below 10.8 deg
*     elevation by the factor (1+0.00195*(10.8-E_t)), where E_t is the
*     unrefracted elevation in degrees.
*
*     The high-ZD model is scaled to match the normal model at the
*     transition point;  there is no glitch.
*
*     - Beyond 93 deg zenith distance, the refraction is held at its
*     93 deg value.
*
*     - See also the routine palRefv, which performs the adjustment in
*     Cartesian Az/El coordinates, and with the emphasis on speed
*     rather than numerical accuracy.

*  References:
*     Condon,J.J., Refraction Corrections for the GBT, PTCS/PN/35.2,
*     NRAO Green Bank, 2004.

*  History:
*     2012-08-24 (TIMJ):
*        Initial version, ported directly from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2004 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palRefz(double zu, double refa, double refb, double *zr) {

    /* Constants */

    /* Largest usable ZD (deg) */
    const double D93 = 93.0;

    /* ZD at which one model hands over to the other (radians) */
    const double Z83 = 83.0 * PAL__DD2R;

    /* coefficients for high ZD model (used beyond ZD 83 deg) */
    const double C1 = +0.55445;
    const double C2 = -0.01133;
    const double C3 = +0.00202;
    const double C4 = +0.28385;
    const double C5 = +0.02390;

    /* High-ZD-model prefiction (deg) for that point */
    const double REF83 =
        (C1 + C2 * 7.0 + C3 * 49.0) / (1.0 + C4 * 7.0 + C5 * 49.0);

    double zu1, zl, s, c, t, tsq, tcu, ref, e, e2;

    /*  perform calculations for zu or 83 deg, whichever is smaller */
    zu1 = DMIN(zu, Z83);

    /*  functions of ZD */
    zl = zu1;
    s = sin(zl);
    c = cos(zl);
    t = s / c;
    tsq = t * t;
    tcu = t * tsq;

    /*  refracted zd (mathematically to better than 1 mas at 70 deg) */
    zl = zl -
         (refa * t + refb * tcu) / (1.0 + (refa + 3.0 * refb * tsq) / (c * c));

    /*  further iteration */
    s = sin(zl);
    c = cos(zl);
    t = s / c;
    tsq = t * t;
    tcu = t * tsq;
    ref = zu1 - zl +
          (zl - zu1 + refa * t + refb * tcu) /
              (1.0 + (refa + 3.0 * refb * tsq) / (c * c));

    /*  special handling for large zu */
    if (zu > zu1) {
        e = 90.0 - DMIN(D93, zu * PAL__DR2D);
        e2 = e * e;
        ref =
            (ref / REF83) * (C1 + C2 * e + C3 * e2) / (1.0 + C4 * e + C5 * e2);
    }

    /*  return refracted zd */
    *zr = zu - ref;
}
/*
*+
*  Name:
*     palRverot

*  Purpose:
*     Velocity component in a given direction due to Earth rotation

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palRverot ( double phi, double ra, double da, double st );

*  Arguments:
*     phi = double (Given)
*        latitude of observing station (geodetic) (radians)
*     ra = double (Given)
*        apparent RA (radians)
*     da = double (Given)
*        apparent Dec (radians)
*     st = double (Given)
8        Local apparent sidereal time.

*  Returned Value:
*     palRverot = double
*        Component of Earth rotation in direction RA,DA (km/s).
*        The result is +ve when the observatory is receding from the
*        given point on the sky.

*  Description:
*     Calculate the velocity component in a given direction due to Earth
*     rotation.
*
*     The simple algorithm used assumes a spherical Earth, of
*     a radius chosen to give results accurate to about 0.0005 km/s
*     for observing stations at typical latitudes and heights.  For
*     applications requiring greater precision, use the routine
*     palPvobs.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-03-02 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"

#include <math.h>

double palRverot(double phi, double ra, double da, double st) {

    /*  Nominal mean sidereal speed of Earth equator in km/s (the actual
     *  value is about 0.4651) */
    const double espeed = 0.4655;
    return espeed * cos(phi) * sin(st - ra) * cos(da);
}
/*
*+
*  Name:
*     palRvgalc

*  Purpose:
*     Velocity component in a given direction due to the rotation
*     of the Galaxy.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palRvgalc( double r2000, double d2000 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 mean RA (radians)
*     d2000 = double (Given)
*        J2000.0 mean Dec (radians)

*  Returned Value:
*     Component of dynamical LSR motion in direction R2000,D2000 (km/s).

*  Description:
*     This function returns the Component of dynamical LSR motion in
*     the direction of R2000,D2000. The result is +ve when the dynamical
*     LSR is receding from the given point on the sky.
*
*  Notes:
*     - The Local Standard of Rest used here is a point in the
*     vicinity of the Sun which is in a circular orbit around
*     the Galactic centre.  Sometimes called the "dynamical" LSR,
*     it is not to be confused with a "kinematical" LSR, which
*     is the mean standard of rest of star catalogues or stellar
*     populations.
*
*  Reference:
*     - The orbital speed of 220 km/s used here comes from Kerr &
*     Lynden-Bell (1986), MNRAS, 221, p1023.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-16 (DSB):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

double palRvgalc(double r2000, double d2000) {

    /* Local Variables: */
    double vb[3];

    /*
     *  LSR velocity due to Galactic rotation
     *
     *  Speed = 220 km/s
     *  Apex  = L2,B2  90deg, 0deg
     *        = RA,Dec  21 12 01.1  +48 19 47  J2000.0
     *
     *  This is expressed in the form of a J2000.0 x,y,z vector:
     *
     *      VA(1) = X = -SPEED*COS(RA)*COS(DEC)
     *      VA(2) = Y = -SPEED*SIN(RA)*COS(DEC)
     *      VA(3) = Z = -SPEED*SIN(DEC)
     */

    double va[3] = {-108.70408, +97.86251, -164.33610};

    /* Convert given J2000 RA,Dec to x,y,z. */
    eraS2c(r2000, d2000, vb);

    /* Compute dot product with LSR motion vector. */
    return eraPdp(va, vb);
}
/*
*+
*  Name:
*     palRvlg

*  Purpose:
*     Velocity component in a given direction due to Galactic rotation
*     and motion of the local group.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palRvlg( double r2000, double d2000 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 mean RA (radians)
*     d2000 = double (Given)
*        J2000.0 mean Dec (radians)

*  Returned Value:
*     Component of SOLAR motion in direction R2000,D2000 (km/s).

*  Description:
*     This function returns the velocity component in a given
*     direction due to the combination of the rotation of the
*     Galaxy and the motion of the Galaxy relative to the mean
*     motion of the local group. The result is +ve when the Sun
*     is receding from the given point on the sky.
*
*  Reference:
*     - IAU Trans 1976, 168, p201.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-16 (DSB):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

double palRvlg(double r2000, double d2000) {

    /* Local Variables: */
    double vb[3];

    /*
     *
     *  Solar velocity due to Galactic rotation and translation
     *
     *  Speed = 300 km/s
     *
     *  Apex  = L2,B2  90deg, 0deg
     *        = RA,Dec  21 12 01.1  +48 19 47  J2000.0
     *
     *  This is expressed in the form of a J2000.0 x,y,z vector:
     *
     *      VA(1) = X = -SPEED*COS(RA)*COS(DEC)
     *      VA(2) = Y = -SPEED*SIN(RA)*COS(DEC)
     *      VA(3) = Z = -SPEED*SIN(DEC)
     */

    double va[3] = {-148.23284, +133.44888, -224.09467};

    /* Convert given J2000 RA,Dec to x,y,z. */
    eraS2c(r2000, d2000, vb);

    /* Compute dot product with Solar motion vector. */
    return eraPdp(va, vb);
}
/*
*+
*  Name:
*     palRvlsrd

*  Purpose:
*     Velocity component in a given direction due to the Sun's motion
*     with respect to the dynamical Local Standard of Rest.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palRvlsrd( double r2000, double d2000 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 mean RA (radians)
*     d2000 = double (Given)
*        J2000.0 mean Dec (radians)

*  Returned Value:
*     Component of "peculiar" solar motion in direction R2000,D2000 (km/s).

*  Description:
*     This function returns the velocity component in a given direction
*     due to the Sun's motion with respect to the dynamical Local Standard
*     of Rest. The result is +ve when the Sun is receding from the given
*     point on the sky.

*  Notes:
*     - The Local Standard of Rest used here is the "dynamical" LSR,
*     a point in the vicinity of the Sun which is in a circular orbit
*     around the Galactic centre.  The Sun's motion with respect to the
*     dynamical LSR is called the "peculiar" solar motion.
*     - There is another type of LSR, called a "kinematical" LSR.  A
*     kinematical LSR is the mean standard of rest of specified star
*     catalogues or stellar populations, and several slightly different
*     kinematical LSRs are in use.  The Sun's motion with respect to an
*     agreed kinematical LSR is known as the "standard" solar motion.
*     To obtain a radial velocity correction with respect to an adopted
*     kinematical LSR use the routine sla_RVLSRK.

*  Reference:
*     - Delhaye (1965), in "Stars and Stellar Systems", vol 5, p73.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-16 (DSB):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

double palRvlsrd(double r2000, double d2000) {

    /* Local Variables: */
    double vb[3];

    /*
     *  Peculiar solar motion from Delhaye 1965: in Galactic Cartesian
     *  coordinates (+9,+12,+7) km/s.  This corresponds to about 16.6 km/s
     *  towards Galactic coordinates L2 = 53 deg, B2 = +25 deg, or RA,Dec
     *  17 49 58.7 +28 07 04 J2000.
     *
     *  The solar motion is expressed here in the form of a J2000.0
     *  equatorial Cartesian vector:
     *
     *      VA(1) = X = -SPEED*COS(RA)*COS(DEC)
     *      VA(2) = Y = -SPEED*SIN(RA)*COS(DEC)
     *      VA(3) = Z = -SPEED*SIN(DEC)
     */

    double va[3] = {+0.63823, +14.58542, -7.80116};

    /* Convert given J2000 RA,Dec to x,y,z. */
    eraS2c(r2000, d2000, vb);

    /* Compute dot product with Solar motion vector. */
    return eraPdp(va, vb);
}
/*
*+
*  Name:
*     palRvlsrk

*  Purpose:
*     Velocity component in a given direction due to the Sun's motion
*     with respect to an adopted kinematic Local Standard of Rest.

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     double palRvlsrk( double r2000, double d2000 )

*  Arguments:
*     r2000 = double (Given)
*        J2000.0 mean RA (radians)
*     d2000 = double (Given)
*        J2000.0 mean Dec (radians)

*  Returned Value:
*     Component of "standard" solar motion in direction R2000,D2000 (km/s).

*  Description:
*     This function returns the velocity component in a given direction
*     due to the Sun's motion with respect to an adopted kinematic
*     Local Standard of Rest. The result is +ve when the Sun is receding
*     from the given point on the sky.

*  Notes:
*     - The Local Standard of Rest used here is one of several
*     "kinematical" LSRs in common use.  A kinematical LSR is the mean
*     standard of rest of specified star catalogues or stellar
*     populations.  The Sun's motion with respect to a kinematical LSR
*     is known as the "standard" solar motion.
*     - There is another sort of LSR, the "dynamical" LSR, which is a
*     point in the vicinity of the Sun which is in a circular orbit
*     around the Galactic centre.  The Sun's motion with respect to
*     the dynamical LSR is called the "peculiar" solar motion.  To
*     obtain a radial velocity correction with respect to the
*     dynamical LSR use the routine sla_RVLSRD.

*  Reference:
*     - Delhaye (1965), in "Stars and Stellar Systems", vol 5, p73.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     DSB: David Berry (JAC, Hawaii)
*     {enter_new_authors_here}

*  History:
*     2012-02-16 (DSB):
*        Initial version.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

double palRvlsrk(double r2000, double d2000) {

    /* Local Variables: */
    double vb[3];

    /*
     *  Standard solar motion (from Methods of Experimental Physics, ed Meeks,
     *  vol 12, part C, sec 6.1.5.2, p281):
     *
     *  20 km/s towards RA 18h Dec +30d (1900).
     *
     *  The solar motion is expressed here in the form of a J2000.0
     *  equatorial Cartesian vector:
     *
     *      VA(1) = X = -SPEED*COS(RA)*COS(DEC)
     *      VA(2) = Y = -SPEED*SIN(RA)*COS(DEC)
     *      VA(3) = Z = -SPEED*SIN(DEC)
     */

    double va[3] = {-0.29000, +17.31726, -10.00141};

    /* Convert given J2000 RA,Dec to x,y,z. */
    eraS2c(r2000, d2000, vb);

    /* Compute dot product with Solar motion vector. */
    return eraPdp(va, vb);
}
/*
*+
*  Name:
*     palSubet

*  Purpose:
*     Remove the E-terms from a pre IAU 1976 catalogue RA,Dec

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palSubet ( double rc, double dc, double eq,
*                     double *rm, double *dm );

*  Arguments:
*     rc = double (Given)
*        RA with E-terms included (radians)
*     dc = double (Given)
*        Dec with E-terms included (radians)
*     eq = double (Given)
*        Besselian epoch of mean equator and equinox
*     rm = double * (Returned)
*        RA without E-terms (radians)
*     dm = double * (Returned)
*        Dec without E-terms (radians)

*  Description:
*     Remove the E-terms (elliptic component of annual aberration)
*     from a pre IAU 1976 catalogue RA,Dec to give a mean place.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     Most star positions from pre-1984 optical catalogues (or
*     derived from astrometry using such stars) embody the
*     E-terms.  This routine converts such a position to a
*     formal mean place (allowing, for example, comparison with a
*     pulsar timing position).

*  See Also:
*     Explanatory Supplement to the Astronomical Ephemeris,
*     section 2D, page 48.

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palSubet(double rc, double dc, double eq, double *rm, double *dm) {
    double a[3]; /* The E-terms */
    double v[3];
    double f;
    int i;

    /* Note the preference for IAU routines */

    /* Retrieve the E-terms */
    palEtrms(eq, a);

    /* Spherical to Cartesian */
    eraS2c(rc, dc, v);

    /* Include the E-terms */
    f = 1.0 + eraPdp(v, a);
    for (i = 0; i < 3; i++) {
        v[i] = f * v[i] - a[i];
    }

    /* Cartesian to spherical */
    eraC2s(v, rm, dm);

    /* Bring RA into conventional range */
    *rm = eraAnp(*rm);
}
/*
*+
*  Name:
*     palSupgal

*  Purpose:
*     Convert from supergalactic to galactic coordinates

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palSupgal ( double dsl, double dsb, double *dl, double *db );

*  Arguments:
*     dsl = double (Given)
*       Supergalactic longitude.
*     dsb = double (Given)
*       Supergalactic latitude.
*     dl = double * (Returned)
*       Galactic longitude.
*     db = double * (Returned)
*       Galactic latitude.

*  Description:
*     Transformation from de Vaucouleurs supergalactic coordinates
*     to IAU 1958 galactic coordinates

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  See Also:
*     - de Vaucouleurs, de Vaucouleurs, & Corwin, Second Reference
*       Catalogue of Bright Galaxies, U. Texas, page 8.
*     - Systems & Applied Sciences Corp., Documentation for the
*       machine-readable version of the above catalogue,
*       Contract NAS 5-26490.
*
*    (These two references give different values for the galactic
*     longitude of the supergalactic origin.  Both are wrong;  the
*     correct value is L2=137.37.)

*  History:
*     2012-02-12(TIMJ):
*        Initial version with documentation taken from Fortran SLA
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1995 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software: you can redistribute it and/or
*     modify it under the terms of the GNU Lesser General Public
*     License as published by the Free Software Foundation, either
*     version 3 of the License, or (at your option) any later
*     version.
*
*     This program is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU Lesser General Public License for more details.
*
*     You should have received a copy of the GNU Lesser General
*     License along with this program.  If not, see
*     <http://www.gnu.org/licenses/>.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "pal1sofa.h"

void palSupgal(double dsl, double dsb, double *dl, double *db) {

    double v1[3];
    double v2[3];

    /*
     *  System of supergalactic coordinates:
     *
     *    SGL   SGB        L2     B2      (deg)
     *     -    +90      47.37  +6.32
     *     0     0         -      0
     *
     *  Galactic to supergalactic rotation matrix:
     */
    double rmat[3][3] = {{-0.735742574804, +0.677261296414, +0.000000000000},
                         {-0.074553778365, -0.080991471307, +0.993922590400},
                         {+0.673145302109, +0.731271165817, +0.110081262225}};

    /* Spherical to Cartesian */
    eraS2c(dsl, dsb, v1);

    /* Supergalactic to galactic */
    eraTrxp(rmat, v1, v2);

    /* Cartesian to spherical */
    eraC2s(v2, dl, db);

    /* Express in conventional ranges */
    *dl = eraAnp(*dl);
    *db = eraAnpm(*db);
}
/*
*+
*  Name:
*     palUe2el

*  Purpose:
*     Universal elements to heliocentric osculating elements

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palUe2el ( const double u[13], int jformr,
*                     int *jform, double *epoch, double *orbinc,
*                     double *anode, double *perih, double *aorq, double *e,
*                     double *aorl, double *dm, int *jstat );

*  Arguments:
*     u = const double [13] (Given)
*        Universal orbital elements (Note 1)
*            (0)  combined mass (M+m)
*            (1)  total energy of the orbit (alpha)
*            (2)  reference (osculating) epoch (t0)
*          (3-5)  position at reference epoch (r0)
*          (6-8)  velocity at reference epoch (v0)
*            (9)  heliocentric distance at reference epoch
*           (10)  r0.v0
*           (11)  date (t)
*           (12)  universal eccentric anomaly (psi) of date, approx
*     jformr = int (Given)
*        Requested element set (1-3; Note 3)
*     jform = int * (Returned)
*        Element set actually returned (1-3; Note 4)
*     epoch = double * (Returned)
*        Epoch of elements (TT MJD)
*     orbinc = double * (Returned)
*        inclination (radians)
*     anode = double * (Returned)
*        longitude of the ascending node (radians)
*     perih = double * (Returned)
*        longitude or argument of perihelion (radians)
*     aorq = double * (Returned)
*        mean distance or perihelion distance (AU)
*     e = double * (Returned)
*        eccentricity
*     aorl = double * (Returned)
*        mean anomaly or longitude (radians, JFORM=1,2 only)
*     dm = double * (Returned)
*        daily motion (radians, JFORM=1 only)
*     jstat = int * (Returned)
*        status:  0 = OK
*                -1 = illegal combined mass
*                -2 = illegal JFORMR
*                -3 = position/velocity out of range

*  Description:
*     Transform universal elements into conventional heliocentric
*     osculating elements.

*  Authors:
*     PTW: Patrick T. Wallace
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference 2).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - The universal elements are with respect to the mean equator and
*       equinox of epoch J2000.  The orbital elements produced are with
*       respect to the J2000 ecliptic and mean equinox.
*     - Three different element-format options are supported:
*
*        Option JFORM=1, suitable for the major planets:
*
*        EPOCH  = epoch of elements (TT MJD)
*        ORBINC = inclination i (radians)
*        ANODE  = longitude of the ascending node, big omega (radians)
*        PERIH  = longitude of perihelion, curly pi (radians)
*        AORQ   = mean distance, a (AU)
*        E      = eccentricity, e
*        AORL   = mean longitude L (radians)
*        DM     = daily motion (radians)
*
*        Option JFORM=2, suitable for minor planets:
*
*        EPOCH  = epoch of elements (TT MJD)
*        ORBINC = inclination i (radians)
*        ANODE  = longitude of the ascending node, big omega (radians)
*        PERIH  = argument of perihelion, little omega (radians)
*        AORQ   = mean distance, a (AU)
*        E      = eccentricity, e
*        AORL   = mean anomaly M (radians)
*
*        Option JFORM=3, suitable for comets:
*
*        EPOCH  = epoch of perihelion (TT MJD)
*        ORBINC = inclination i (radians)
*        ANODE  = longitude of the ascending node, big omega (radians)
*        PERIH  = argument of perihelion, little omega (radians)
*        AORQ   = perihelion distance, q (AU)
*        E      = eccentricity, e
*
*     - It may not be possible to generate elements in the form
*       requested through JFORMR.  The caller is notified of the form
*       of elements actually returned by means of the JFORM argument:
*
*        JFORMR   JFORM     meaning
*
*          1        1       OK - elements are in the requested format
*          1        2       never happens
*          1        3       orbit not elliptical
*
*          2        1       never happens
*          2        2       OK - elements are in the requested format
*          2        3       orbit not elliptical
*
*          3        1       never happens
*          3        2       never happens
*          3        3       OK - elements are in the requested format
*
*     - The arguments returned for each value of JFORM (cf Note 6: JFORM
*       may not be the same as JFORMR) are as follows:
*
*         JFORM         1              2              3
*         EPOCH         t0             t0             T
*         ORBINC        i              i              i
*         ANODE         Omega          Omega          Omega
*         PERIH         curly pi       omega          omega
*         AORQ          a              a              q
*         E             e              e              e
*         AORL          L              M              -
*         DM            n              -              -
*
*     where:
*
*         t0           is the epoch of the elements (MJD, TT)
*         T              "    epoch of perihelion (MJD, TT)
*         i              "    inclination (radians)
*         Omega          "    longitude of the ascending node (radians)
*         curly pi       "    longitude of perihelion (radians)
*         omega          "    argument of perihelion (radians)
*         a              "    mean distance (AU)
*         q              "    perihelion distance (AU)
*         e              "    eccentricity
*         L              "    longitude (radians, 0-2pi)
*         M              "    mean anomaly (radians, 0-2pi)
*         n              "    daily motion (radians)
*         -             means no value is set
*
*     - At very small inclinations, the longitude of the ascending node
*       ANODE becomes indeterminate and under some circumstances may be
*       set arbitrarily to zero.  Similarly, if the orbit is close to
*       circular, the true anomaly becomes indeterminate and under some
*       circumstances may be set arbitrarily to zero.  In such cases,
*       the other elements are automatically adjusted to compensate,
*       and so the elements remain a valid description of the orbit.

*   See Also:
*     - Sterne, Theodore E., "An Introduction to Celestial Mechanics",
*       Interscience Publishers Inc., 1960.  Section 6.7, p199.
*     - Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-09 (TIMJ):
*        Initial version
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 1999 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

void palUe2el(const double u[],
              int jformr,
              int *jform,
              double *epoch,
              double *orbinc,
              double *anode,
              double *perih,
              double *aorq,
              double *e,
              double *aorl,
              double *dm,
              int *jstat) {

    /*  Canonical days to seconds */
    const double CD2S = PAL__GCON / PAL__SPD;

    int i;
    double pmass, date, pv[6];

    /* Unpack the universal elements */
    pmass = u[0] - 1.0;
    date = u[2];
    for (i = 0; i < 3; i++) {
        pv[i] = u[i + 3];
        pv[i + 3] = u[i + 6] * CD2S;
    }

    /* Convert the position and velocity etc into conventional elements */
    palPv2el(pv,
             date,
             pmass,
             jformr,
             jform,
             epoch,
             orbinc,
             anode,
             perih,
             aorq,
             e,
             aorl,
             dm,
             jstat);
}
/*
*+
*  Name:
*     palUe2pv

*  Purpose:
*     Heliocentric position and velocity of a planet, asteroid or comet, from
universal elements

*  Language:
*     Starlink ANSI C

*  Type of Module:
*     Library routine

*  Invocation:
*     void palUe2pv( double date, double u[13], double pv[6], int *jstat );

*  Arguments:
*     date = double (Given)
*        TT Modified Julian date (JD-2400000.5).
*     u = double [13] (Given & Returned)
*        Universal orbital elements (updated, see note 1)
*        given    (0)   combined mass (M+m)
*          "      (1)   total energy of the orbit (alpha)
*          "      (2)   reference (osculating) epoch (t0)
*          "    (3-5)   position at reference epoch (r0)
*          "    (6-8)   velocity at reference epoch (v0)
*          "      (9)   heliocentric distance at reference epoch
*          "     (10)   r0.v0
*       returned (11)   date (t)
*          "     (12)   universal eccentric anomaly (psi) of date
*     jstat = int * (Returned)
*       status:  0 = OK
*               -1 = radius vector zero
*               -2 = failed to converge

*  Description:
*     Heliocentric position and velocity of a planet, asteroid or comet,
*     starting from orbital elements in the "universal variables" form.

*  Authors:
*     PTW: Pat Wallace (STFC)
*     TIMJ: Tim Jenness (JAC, Hawaii)
*     {enter_new_authors_here}

*  Notes:
*     - The "universal" elements are those which define the orbit for the
*       purposes of the method of universal variables (see reference).
*       They consist of the combined mass of the two bodies, an epoch,
*       and the position and velocity vectors (arbitrary reference frame)
*       at that epoch.  The parameter set used here includes also various
*       quantities that can, in fact, be derived from the other
*       information.  This approach is taken to avoiding unnecessary
*       computation and loss of accuracy.  The supplementary quantities
*       are (i) alpha, which is proportional to the total energy of the
*       orbit, (ii) the heliocentric distance at epoch, (iii) the
*       outwards component of the velocity at the given epoch, (iv) an
*       estimate of psi, the "universal eccentric anomaly" at a given
*       date and (v) that date.
*     - The companion routine is palEl2ue.  This takes the conventional
*       orbital elements and transforms them into the set of numbers
*       needed by the present routine.  A single prediction requires one
*       one call to palEl2ue followed by one call to the present routine;
*       for convenience, the two calls are packaged as the routine
*       sla_PLANEL.  Multiple predictions may be made by again
*       calling palEl2ue once, but then calling the present routine
*       multiple times, which is faster than multiple calls to palPlanel.
*     - It is not obligatory to use palEl2ue to obtain the parameters.
*       However, it should be noted that because palEl2ue performs its
*       own validation, no checks on the contents of the array U are made
*       by the present routine.
      - DATE is the instant for which the prediction is required.  It is
*       in the TT timescale (formerly Ephemeris Time, ET) and is a
*       Modified Julian Date (JD-2400000.5).
      - The universal elements supplied in the array U are in canonical
*       units (solar masses, AU and canonical days).  The position and
*       velocity are not sensitive to the choice of reference frame.  The
*       palEl2ue routine in fact produces coordinates with respect to the
*       J2000 equator and equinox.
*     - The algorithm was originally adapted from the EPHSLA program of
*       D.H.P.Jones (private communication, 1996).  The method is based
*       on Stumpff's Universal Variables.
*     - Reference:  Everhart, E. & Pitkin, E.T., Am.J.Phys. 51, 712, 1983.

*  History:
*     2012-03-09 (TIMJ):
*        Initial version cloned from SLA/F.
*        Adapted with permission from the Fortran SLALIB library.
*     {enter_further_changes_here}

*  Copyright:
*     Copyright (C) 2005 Rutherford Appleton Laboratory
*     Copyright (C) 2012 Science and Technology Facilities Council.
*     All Rights Reserved.

*  Licence:
*     This program is free software; you can redistribute it and/or
*     modify it under the terms of the GNU General Public License as
*     published by the Free Software Foundation; either version 3 of
*     the License, or (at your option) any later version.
*
*     This program is distributed in the hope that it will be
*     useful, but WITHOUT ANY WARRANTY; without even the implied
*     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
*     PURPOSE. See the GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with this program; if not, write to the Free Software
*     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
*     MA 02110-1301, USA.

*  Bugs:
*     {note_any_bugs_here}
*-
*/

#include "pal.h"
#include "palmac.h"

#include <math.h>

void palUe2pv(double date, double u[13], double pv[6], int *jstat) {

    /*  Canonical days to seconds */
    const double CD2S = PAL__GCON / PAL__SPD;

    /*  Test value for solution and maximum number of iterations */
    const double TEST = 1e-13;
    const int NITMAX = 25;

    int I, NIT, N;
    double CM, ALPHA, T0, P0[3], V0[3], R0, SIGMA0, T, PSI, DT, W, TOL, PSJ,
        PSJ2, BETA, S0, S1, S2, S3, FF, R, F, G, FD, GD;

    double PLAST = 0.0;
    double FLAST = 0.0;

    /*  Unpack the parameters. */
    CM = u[0];
    ALPHA = u[1];
    T0 = u[2];
    for (I = 0; I < 3; I++) {
        P0[I] = u[I + 3];
        V0[I] = u[I + 6];
    }
    R0 = u[9];
    SIGMA0 = u[10];
    T = u[11];
    PSI = u[12];

    /*  Approximately update the universal eccentric anomaly. */
    PSI = PSI + (date - T) * PAL__GCON / R0;

    /*  Time from reference epoch to date (in Canonical Days: a canonical
     *  day is 58.1324409... days, defined as 1/PAL__GCON). */
    DT = (date - T0) * PAL__GCON;

    /*  Refine the universal eccentric anomaly, psi. */
    NIT = 1;
    W = 1.0;
    TOL = 0.0;
    while (fabs(W) >= TOL) {

        /*     Form half angles until BETA small enough. */
        N = 0;
        PSJ = PSI;
        PSJ2 = PSJ * PSJ;
        BETA = ALPHA * PSJ2;
        while (fabs(BETA) > 0.7) {
            N = N + 1;
            BETA = BETA / 4.0;
            PSJ = PSJ / 2.0;
            PSJ2 = PSJ2 / 4.0;
        }

        /*     Calculate Universal Variables S0,S1,S2,S3 by nested series. */
        S3 = PSJ * PSJ2 *
             ((((((BETA / 210.0 + 1.0) * BETA / 156.0 + 1.0) * BETA / 110.0 +
                 1.0) *
                    BETA / 72.0 +
                1.0) *
                   BETA / 42.0 +
               1.0) *
                  BETA / 20.0 +
              1.0) /
             6.0;
        S2 = PSJ2 *
             ((((((BETA / 182.0 + 1.0) * BETA / 132.0 + 1.0) * BETA / 90.0 +
                 1.0) *
                    BETA / 56.0 +
                1.0) *
                   BETA / 30.0 +
               1.0) *
                  BETA / 12.0 +
              1.0) /
             2.0;
        S1 = PSJ + ALPHA * S3;
        S0 = 1.0 + ALPHA * S2;

        /*     Undo the angle-halving. */
        TOL = TEST;
        while (N > 0) {
            S3 = 2.0 * (S0 * S3 + PSJ * S2);
            S2 = 2.0 * S1 * S1;
            S1 = 2.0 * S0 * S1;
            S0 = 2.0 * S0 * S0 - 1.0;
            PSJ = PSJ + PSJ;
            TOL += TOL;
            N--;
        }

        /*     Values of F and F' corresponding to the current value of psi. */
        FF = R0 * S1 + SIGMA0 * S2 + CM * S3 - DT;
        R = R0 * S0 + SIGMA0 * S1 + CM * S2;

        /*     If first iteration, create dummy "last F". */
        if (NIT == 1)
            FLAST = FF;

        /*     Check for sign change. */
        if (FF * FLAST < 0.0) {

            /*        Sign change:  get psi adjustment using secant method. */
            W = FF * (PLAST - PSI) / (FLAST - FF);
        } else {

            /*        No sign change:  use Newton-Raphson method instead. */
            if (R == 0.0) {
                /* Null radius vector */
                *jstat = -1;
                return;
            }
            W = FF / R;
        }

        /*     Save the last psi and F values. */
        PLAST = PSI;
        FLAST = FF;

        /*     Apply the Newton-Raphson or secant adjustment to psi. */
        PSI = PSI - W;

        /*     Next iteration, unless too many already. */
        if (NIT > NITMAX) {
            *jstat = -2; /* Failed to converge */
            return;
        }
        NIT++;
    }

    /*  Project the position and velocity vectors (scaling velocity to AU/s). */
    W = CM * S2;
    F = 1.0 - W / R0;
    G = DT - CM * S3;
    FD = -CM * S1 / (R0 * R);
    GD = 1.0 - W / R;
    for (I = 0; I < 3; I++) {
        pv[I] = P0[I] * F + V0[I] * G;
        pv[I + 3] = CD2S * (P0[I] * FD + V0[I] * GD);
    }

    /*  Update the parameters to allow speedy prediction of PSI next time. */
    u[11] = date;
    u[12] = PSI;

    /*  OK exit. */
    *jstat = 0;
    return;
}
