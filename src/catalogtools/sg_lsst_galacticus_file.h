///
/// @package phosim
/// @file sg_lsst_galacticus_file.h
/// @brief Class to read/write/access the records of the an entry of the LSST
///         Galacticus galaxy catalog
///         ref: https://sites.google.com/site/galacticusmodel////
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
#ifndef SGLSSTGALACTICUSFILE_H
#define SGLSSTGALACTICUSFILE_H

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// *************************************************************************
//  List of named variables in the Galacticus galaxy file
// *************************************************************************
enum SGGalacticusElements {
GE_NODEINDEX,		   GE_REDSHIFT,		     GE_RA,
GE_DEC,	        	   GE_V_PEC,		     GE_LOG_STELLARMASS,
GE_METALLICITY,	           GE_POSITIONX,     	     GE_POSITIONY,
GE_POSITIONZ,		   GE_VELOCITYX,	     GE_VELOCITYY,
GE_VELOCITYZ,		   GE_DISK_RA,       	     GE_DISK_DEC,
GE_DISK_SIGMA0, 	   GE_DISK_RE,         	     GE_DISK_INDEX,
GE_DISK_A,		   GE_DISK_B,		     GE_DISK_THETA_LOS,
GE_DISK_PHI,		   GE_LOG_DISK_STELLARMASS,  GE_DISK_AGE_,
GE_DISK_SFR,       	   GE_DISK_METALLICITY,	     GE_DISK_ELLIPTICITY,
GE_BULGE_RA,		   GE_BULGE_DEC,	     GE_BULGE_SIGMA0,
GE_BULGE_RE,		   GE_BULGE_INDEX,	     GE_BULGE_A,
GE_BULGE_B,		   GE_BULGE_THETA_LOS,	     GE_BULGE_PHI,
GE_LOG_BULGE_STELLARMASS, GE_BULGE_AGE_, 	     GE_BULGE_SFR,
GE_BULGE_METALLICITY, 	   GE_AGN_RA,		     GE_AGN_DEC,
GE_AGN_MASS, 		   GE_AGN_ACCRETNRATE, 	     GE_LSST_U_REST,
GE_LSST_G_REST,            GE_LSST_R_REST,     	     GE_LSST_I_REST,
GE_LSST_Z_REST,	           GE_LSST_Y4_REST, 	     GE_LSST_U_OBSERVED,
GE_LSST_G_OBSERVED,	   GE_LSST_R_OBSERVED,	     GE_LSST_I_OBSERVED,
GE_LSST_Z_OBSERVED,	   GE_LSST_Y4_OBSERVED,	     GE_DISK_LSST_U_REST,
GE_DISK_LSST_G_REST,	   GE_DISK_LSST_R_REST,      GE_DISK_LSST_I_REST,
GE_DISK_LSST_Z_REST,	   GE_DISK_LSST_Y4_REST,     GE_DISK_LSST_U_OBSERVED,
GE_DISK_LSST_G_OBSERVED,   GE_DISK_LSST_R_OBSERVED,  GE_DISK_LSST_I_OBSERVED,
GE_DISK_LSST_Z_OBSERVED,   GE_DISK_LSST_Y4_OBSERVED, GE_BULGE_LSST_U_REST,
GE_BULGE_LSST_G_REST,	   GE_BULGE_LSST_R_REST,     GE_BULGE_LSST_I_REST,
GE_BULGE_LSST_Z_REST,	   GE_BULGE_LSST_Y4_REST,    GE_BULGE_LSST_U_OBSERVED,
GE_BULGE_LSST_G_OBSERVED,  GE_BULGE_LSST_R_OBSERVED, GE_BULGE_LSST_I_OBSERVED,
GE_BULGE_LSST_Z_OBSERVED,  GE_BULGE_LSST_Y4_OBSERVED,GE_BULGE_SED_FILE_NAME,
GE_DISK_SED_FILE_NAME};
// *********************************************************************
// Above enum assums that each enumerted name is assiggned a value in
// sequence starting at 0. There are 82 names so GE_BULGE_SED_FILE_NAME == 80
// *********************************************************************

class SGGalacticusLSSTFile
// ***********************************************************************
// This class creates/opens/closes/reads/writes various versions
// (text and binaary)  of the LSST Galacticus catalog
// ref: https://sites.google.com/site/galacticusmodel/
// Name of the LSST text file version:
// planck_p10_mergertrees_root100_250k_nbody_0.9.4Planck_lcd4.5z3_BH_LSSTugrizy4ROref4_mock.ascii
// Name of the Galacticus + M.Weisner SED file names: GalacticusWithSED.txt
// ***********************************************************************
{
 public:
  SGGalacticusLSSTFile();
  virtual ~SGGalacticusLSSTFile();

  void OpenGalacticusTextFile(std::string inputTxtFileName);
  bool ReadGalacticusTextFile(std::vector <double>&  dataRecord,
			      std::string& BulgeSEDFileName,
			      std::string& DiskSEDFileName);
  void CloseGalacticusTextFile();

  double getRecordIndex(){return fRecordIndex;};
  bool hasSED(){return fHasSED;};
  bool IsEntryLineGood(std::string galaxyEntry,int recordIndex);

  void AddElementToCheckList(SGGalacticusElements element)
                    {checkElementList.push_back( (int)element ); };

  std::vector< int > checkElementList;

 private:

  std::string fGalacticusTextFileName;
  std::ifstream fGalacticusTextFile;
  std::ofstream fOutputBinaryFile;
  double fRecordIndex;
  bool fHasSED;
};
#endif
