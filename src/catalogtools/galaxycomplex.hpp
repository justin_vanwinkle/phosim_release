#ifndef PHOSIM_GALAXYCOMPLEX_HPP
#define PHOSIM_GALAXYCOMPLEX_HPP

const double kPI = 3.1415926535897932384626433832795028841971693993751;

#include "rng_mwc.hpp"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace RandomNumbers;

class GalacticComplexObject {
    // **********************************************************************
    // Structure to hold sersicComplex values for phosimcatgen GalaxyComplex
    // class
    // **********************************************************************
  public:
    std::string ID;
    double RADeg;
    double DecDeg;
    double M; // magnitude
    std::string SEDFileName;
    double z; // Red Shift

    // sersicComplex parameters
    double a;
    double b;
    double c;
    double alpha;
    double beta;
    int index;
    double clumpFrac;
    double clump;
    double clumpWidth;
    double spiralFrac;
    double alphaSpiral;
    double bar;
    double spiralWidth;
    double phi0;
};
// *****************************************************************

class GalaxyComplex
// ***************************************************************
// This class uses IDL code from John Peterson (2018-02-15), converted to C++
// by Glenn Sembroski. (Not sure of definitions or souces of any of this)
// It produces a random galaxy, both  the disk and bulge magnitudes,
// the red shift z
// and it fills the sersicComplex class for each (disk and bulge) in order for
// the calling program to generate a galaxy object(2 objects, disk and bulge
//  actually) for a phosim catalog.
// ****************************************************************
{
  private:
    /* double fFOV; */
    int fNumGalaxies;
    int fNumValues;

  public:
    std::vector<double> fMagnitudeValues;
    std::vector<double> fCumulativeDensityProb;
    std::vector<double> fZValues;
    std::vector<double> fCumulativeZProb;

    int GetNumberOfGalaxies() {
        return fNumGalaxies;
    };

    GalaxyComplex(double FOVDeg) {
        Init(FOVDeg);
    }

    void Init(double fovDeg)
    // **********************************************************************
    // fovDeg is in units of degrees and is diameter requested.
    // **********************************************************************
    {
        double densityTotal = 0;

        // This code wantsa to us fov as radius with units of arcmin.
        double fovRadiusArcMin = (fovDeg / 2.) * 60.;
        fNumValues = 10000; //

        // MagnitueValues and zValues are lookup tables for more efficency.
        fMagnitudeValues.resize(fNumValues, 0.0);
        fCumulativeDensityProb.resize(fNumValues, 0.0);

        std::vector<double> density;
        density.resize(fNumValues, 0.0);

        std::vector<double> densityProb;
        densityProb.resize(fNumValues, 0.0);

        double mmin = -17.0; // This is an absolute magnitude

        for (int i = 0; i < fNumValues; i++) {

            // Magnitude look up table, sort of
            fMagnitudeValues.at(i) =
                -25.0 + ((double)i / (double)fNumValues) * (mmin + 25.0);

            double ppower = 0.4 * (-20.3 - fMagnitudeValues.at(i));
            density.at(i) = 0.4 * log(10.0) * 0.005 *
                            pow(10., (ppower * (-1.2 + 1.0))) *
                            exp(-pow(10., ppower));
            densityTotal += density.at(i);
        }

        // Normalize density. Makes a probability.
        for (int i = 1; i < fNumValues; i++) {
            densityProb.at(i) = density.at(i) / densityTotal;
            fCumulativeDensityProb.at(i) =
                fCumulativeDensityProb.at(i - 1) + densityProb.at(i);
        }

        // Number of galaxies in FOVMINUTES
        // Breaking up Nf calculation into terms for computation clarity.
        // But where the breaks should be physics wise (ie what terms mean what)
        // is unnown to me

        double term1 = (mmin + 25.0) / fNumValues;
        double term2 = 4. * kPI * pow((3e5 / 70.0), 3);
        double term3 = ((4. * kPI) / pow((kPI / 180.), 2)) * 60.0 * 60.0;

        int Nf = densityTotal * term1 * (term2 / term3) * kPI *
                 (fovRadiusArcMin * fovRadiusArcMin);

        // Redshift Distribution,
        fZValues.resize(fNumValues, 0.0);
        fCumulativeZProb.resize(fNumValues, 0.0);

        std::vector<double> zProb;
        zProb.resize(fNumValues, 0.0);

        double zc = 1.0; // Some type of z scale?

        double totalZProb = 0.0;
        for (int i = 0; i < fNumValues; i++) {
            // z=5 is max z we are interested in.
            fZValues.at(i) = ((double)i / (double)fNumValues) * 5.0;
            zProb.at(i) = pow(fZValues.at(i), 2) * exp(-fZValues.at(i) / zc);
            totalZProb += zProb.at(i);
        }

        // I think  this is number of galaxies in fov.
        fNumGalaxies = (int)(Nf * (totalZProb / fNumValues) * 5.0);

        for (int i = 1; i < fNumValues; i++) {
            zProb.at(i) = zProb.at(i) / totalZProb;
            fCumulativeZProb.at(i) = fCumulativeZProb.at(i - 1) + zProb.at(i);
        }

        return;
    }
    // ***************************************************************************

    double GenerateGalaxy(GalacticComplexObject &Bulge,
                          GalacticComplexObject &Disk)
    // ****************************************************************************
    // This method claculates Magnitudes, sizes and sersicComplex  paramteres
    // for a random galaxy disk and bulge following simple distributions of
    // such.
    // ****************************************************************************
    // GalacticComplex object defined in GalaxyObject class
    // ****************************************************************************
    {
        // double maxflux = 30.0;  //What units? Do I need this?

        // Find z. pick a z and find its index in the z arrays. Find index where
        // the cumulatrive probability exceeds the random selection

        double u = RngDouble();
        int g = 0;
        for (int i = 1; i < fNumValues; i++) {
            if (u <= fCumulativeZProb.at(i)) {
                g = i;
                break;
            }
        }

        double redShift = fZValues.at(g); // z in range: 0 -> 5.0
        double distance = 3e5 / (70.0) * redShift; // Units?

        // Determine total flux flux and then convert to disk and bulge
        // magnitude
        u = RngDouble();
        for (int i = 1; i < fNumValues; i++) {
            if (u <= fCumulativeDensityProb.at(i)) {
                g = i;
                break;
            }
        }

        // Get absolute magnitude of galaxy
        double MAbsolute = fMagnitudeValues.at(g);

        // Correct for distance. Generate total magnitude
        double MApparent = MAbsolute + (5.0 * log10(distance * 1e6 / 10.0));

        // Now the size of the galaxy
        double ssize = pow((pow(10.0, (-0.4 * (MAbsolute + 21.0)))), .333) *
                       0.01 / (distance * (kPI / (180. * 3600.0))); // Units?

        // Divide up MApparent between disk and bulge randomly
        double bulgefrac = RngDouble();

        Bulge.M = -2.5 * log10(bulgefrac * pow(10., (-0.4 * MApparent)));
        Disk.M = -2.5 * log10((1. - bulgefrac) * pow(10., (-0.4 * MApparent)));
        Bulge.z = redShift;
        Disk.z = redShift;

        // Now fill in the sersic values for sersicComplex.
        Bulge.a = ssize * (0.8 + 0.2 * RngDouble()) * 0.75;
        Bulge.b = ssize * (0.8 + 0.2 * RngDouble()) * 0.75;
        Bulge.c = ssize * (0.8 + 0.2 * RngDouble()) * 0.75;
        Bulge.alpha = 0.0;
        Bulge.beta = 0.0;
        Bulge.index = 4;
        Bulge.clumpFrac = RngDouble() * 0.5;
        Bulge.clump = 300;
        Bulge.clumpWidth = ssize * 0.1;
        Bulge.spiralFrac = 0;
        Bulge.alphaSpiral = 0;
        Bulge.bar = 0;
        Bulge.spiralWidth = 0;
        Bulge.phi0 = 0;

        Disk.a = ssize * (0.8 + 0.2 * RngDouble()) * 1.25;
        Disk.b = ssize * (0.8 + 0.2 * RngDouble()) * 1.25;
        Disk.c = ssize * (0.2 * RngDouble()) * 1.25;
        Disk.alpha = RngDouble() * 360.0;
        Disk.beta = (acos(2.0 * RngDouble() - 1.0) - (kPI / 2.0)) * 180. / kPI;
        Disk.index = 1;
        Disk.clumpFrac = RngDouble() * 0.5;
        Disk.clump = 300;
        Disk.clumpWidth = ssize * 0.1;
        Disk.spiralFrac = RngDouble();
        Disk.alphaSpiral = RngDouble() * 60.0 - 30.0;
        Disk.bar = (ssize * 0.5);
        Disk.spiralWidth = (ssize * 0.1);
        Disk.phi0 = RngDouble() * 360.0;

        return MApparent; // return total Magnitude of galaxy. Used to slect M
                          // range
    }
    // ************************************************************************

    std::string
    GenerateSersicComplexObjectString(GalacticComplexObject &Object) {
        // **********************************************************************
        // Generate string that goes into phosim object catalog for the
        // sersicComplex part
        // **********************************************************************
        std::ostringstream os;
        os << "sersicComplex" << std::fixed << std::setw(6)
           << std::setprecision(2) << Object.a << " " << Object.b << " "
           << Object.c << " " << Object.alpha << " " << Object.beta << " "
           << Object.index << " " << Object.clumpFrac << " " << Object.clump
           << " " << Object.clumpWidth << " " << Object.spiralFrac << " "
           << Object.alphaSpiral << " " << Object.bar << " "
           << Object.spiralWidth << " " << Object.phi0 << " ";

        return os.str();
    }
    // ************************************************************************

    void WriteObject(GalacticComplexObject &Object, std::ofstream *ofs)
    // ************************************************************************
    // Write the object line to the catalog file
    // ************************************************************************
    {
        // *********************************
        // Get the sersic part of the line
        // *********************************
        std::string sersicCmplxStr = GenerateSersicComplexObjectString(Object);

        *ofs << "object " << Object.ID << " " << std::fixed
             << std::setprecision(8) << Object.RADeg << " " << Object.DecDeg
             << " " << std::setprecision(2) << Object.M << " "
             << Object.SEDFileName << " " << std::setprecision(3) << Object.z
             << " 0.0 0.0 0.0 0.0 0.0 " << sersicCmplxStr << "none none"
             << std::endl;
        return;
    }
    // *************************************************************************
};
// *********************************************************************

#endif
