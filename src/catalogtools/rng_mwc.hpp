#ifndef PHOSIM_RNG_MWC_HPP
#define PHOSIM_RNG_MWC_HPP

#include <cassert>
#include <cmath>
#include <cstdint>
#include <ctime>

namespace RandomNumbers {

// ========= uniform distribution ==========
// returns number from interval <0,1>
float RngFloat();
double RngDouble();

// returns a random number from
// ======== normal distribution ==========
// with mean=0 and variance=1
double random_gaussian();

// returns a random number from
// ========= exponential distribution ==========
// with rate=1
double random_exp();

long long poisson(long long lambda);

void set_seed_rng32Mwc(uint32_t z, uint32_t w);

// set seed from 64-bit value
void RngSetSeed64(uint64_t seed);

// set seed from 32-bit value
// mixes up bits from the seed to produce a 64-bit seed
void RngSetSeed32(uint32_t seed);

// set seed by using time and clock functions for entrpy
void RngSetSeedFromTime();

// Draw 'count' numbers from rng
void RngUnwind(int count);

// ========= uniform distribution ==========
// returns number from interval <0,1>
float RngFloat_reseed();
double RngDouble_reseed();

// returns a random number from
// ======== normal distribution ==========
// with mean=0 and variance=1
double random_gaussian_reseed();

// returns a random number from
// ========= exponential distribution ==========
// with rate=1
double random_exp_reseed();

// set seed from 64-bit value
void RngSetSeed64_reseed(uint64_t seed);

// set seed from 32-bit value
// mixes up bits from the seed to produce a 64-bit seed
void RngSetSeed32_reseed(uint32_t seed);

// set seed by using time and clock functions for entrpy
void RngSetSeedFromTime_reseed();

// Draw 'count' numbers from rng
void RngUnwind_reseed(int count);

// return the current seed
void RngGetSeed(uint32_t *z, uint32_t *w);

// returns a random number from
// ======== normal distribution ==========
// with mean=0 and variance=1
double random_gaussian() {

    double u1 = 0.0, u2 = 0.0, v1 = 0.0, v2 = 0.0, s = 2.0;
    while (s >= 1) {
        u1 = RngDouble();
        u2 = RngDouble();
        v1 = 2.0 * u1 - 1.0;
        v2 = 2.0 * u2 - 1.0;
        s = pow(v1, 2) + pow(v2, 2);
    }
    double x1 = v1 * sqrt((-2.0 * log(s)) / s);
    return x1;
}

// returns a random number from
// ========= exponential distribution ==========
// with rate=1
double random_exp() {
    return -log(RngDouble());
}

// Poisson distributor (JRP)
long long poisson(long long lambda) {
    long long count;
    double f, g;
    long long d;

    if (lambda < 10) {
        f = RngDouble();
        d = 0;
        while (f >= exp(-(double)lambda)) {
            g = RngDouble();
            f = f * g;
            d++;
        }
        count = d;
    } else {
        count = (long long)((double)lambda +
                            sqrt((double)lambda) * random_gaussian());
        if (count < 0) {
            count = 0;
        }
    }
    return (count);
}

// double MWC rng state
static uint32_t m_z = 1234;
static uint32_t m_w = 42;

// Marsaglia's double MWC random number generator
uint32_t RngUint32() {
    m_z = 36969 * (m_z & 65535) + (m_z >> 16);
    m_w = 18000 * (m_w & 65535) + (m_w >> 16);
    return ((m_z << 16) + m_w); /* 32-bit result */
}

// derived
// ========= uniform distribution ==========
// returns number from interval <0,1>
// conversion bias removed
float RngFloat() {
    const uint32_t rng_floatMask = 0x007FFFFF;
    const auto intToFloat = float(1.0 / (rng_floatMask + 1.0));
    const float plusBiasFloat = intToFloat / 2;

    uint32_t rn = RngUint32() & rng_floatMask;
    return rn * intToFloat + plusBiasFloat;
}

// derived
// ========= uniform distribution ==========
// returns number from interval <0,1>
// conversion bias removed
double RngDouble() {
    const double intToDouble = 1.0 / (UINT32_MAX + 1.0);
    const double plusBiasDouble = intToDouble / 2;

    uint32_t rn = RngUint32();
    return rn * intToDouble + plusBiasDouble;
}

// Draw 'count' numbers from rng
void RngUnwind(int count) {
    for (int i = 0; i < count; i++) {
        RngUint32();
    }
}

time_t timeSec1970() {
    return time(nullptr);
}

// Note: values of m_z and m_w must not be zero (neither one)!!!!!
void set_seed_rng32Mwc(uint32_t z, uint32_t w) {

    assert(w != 0 && z != 0);
    m_z = z;
    m_w = w;
}

// set seed from 64-bit value
void RngSetSeed64(uint64_t seed) {

    auto z = uint32_t(seed >> 32);
    auto w = uint32_t(seed & 0xFFFFFFFF);
    if (z == 0) {
        z = 4232482;
    }
    if (w == 0) {
        w = 1234628;
    }

    set_seed_rng32Mwc(z, w);
}

// set seed from 32-bit value
// mixes up bits from the seed to produce a 64-bit seed
void RngSetSeed32(uint32_t seed) {

    uint32_t lobits = seed;
    uint32_t hibits = seed * 91 * 53;
    uint64_t seed64 = (uint64_t(hibits) << 32) + lobits;

    RngSetSeed64(seed64);
}

// set seed from current time and clock()
void RngSetSeedFromTime() {

    uint32_t lobits = uint32_t(clock()) + uint32_t(timeSec1970());
    uint32_t hibits = uint32_t(clock()) * 53 + uint32_t(timeSec1970()) * 91;
    uint64_t seed64 = ((uint64_t)hibits << 32) + lobits;

    RngSetSeed64(seed64);
}

// SECOND SET OF RANDOM NUMBER FUNCTIONS WHERE WE MESS WITH SEED

// returns a random number from
// ======== normal distribution ==========
// with mean=0 and variance=1
double random_gaussian_reseed() {

    double u1 = 0.0, u2 = 0.0, v1 = 0.0, v2 = 0.0, s = 2.0;
    while (s >= 1) {
        u1 = RngDouble_reseed();
        u2 = RngDouble_reseed();
        v1 = 2.0 * u1 - 1.0;
        v2 = 2.0 * u2 - 1.0;
        s = pow(v1, 2) + pow(v2, 2);
    }
    double x1 = v1 * sqrt((-2.0 * log(s)) / s);
    return x1;
}

// returns a random number from
// ========= exponential distribution ==========
// with rate=1
double random_exp_reseed() {
    return -log(RngDouble_reseed());
}

// double MWC rng state
static uint32_t m_z_reseed = 1234;
static uint32_t m_w_reseed = 42;

// Marsaglia's double MWC random number generator
uint32_t RngUint32_reseed() {
    m_z_reseed = 36969 * (m_z_reseed & 65535) + (m_z_reseed >> 16);
    m_w_reseed = 18000 * (m_w_reseed & 65535) + (m_w_reseed >> 16);
    return ((m_z_reseed << 16) + m_w_reseed); /* 32-bit result */
}

// derived
// ========= uniform distribution ==========
// returns number from interval <0,1>
// conversion bias removed
float RngFloat_reseed() {
    const uint32_t rng_floatMask = 0x007FFFFF;
    const auto intToFloat = float(1.0 / (rng_floatMask + 1.0));
    const float plusBiasFloat = intToFloat / 2;

    uint32_t rn = RngUint32_reseed() & rng_floatMask;
    return rn * intToFloat + plusBiasFloat;
}

// derived
// ========= uniform distribution ==========
// returns number from interval <0,1>
// conversion bias removed
double RngDouble_reseed() {
    const double intToDouble = 1.0 / (UINT32_MAX + 1.0);
    const double plusBiasDouble = intToDouble / 2;

    uint32_t rn = RngUint32_reseed();
    return rn * intToDouble + plusBiasDouble;
}

// Draw 'count' numbers from rng
void RngUnwind_reseed(int count) {

    for (int i = 0; i < count; i++) {
        RngUint32_reseed();
    }
}

// Note: values of m_z and m_w must not be zero (neither one)!!!!!
void set_seed_rng32Mwc_reseed(uint32_t z, uint32_t w) {
    assert(w != 0 && z != 0);
    m_z_reseed = z;
    m_w_reseed = w;
}

// set seed from 64-bit value
void RngSetSeed64_reseed(uint64_t seed) {

    auto z = uint32_t(seed >> 32);
    auto w = uint32_t(seed & 0xFFFFFFFF);
    if (z == 0) {
        z = 4232482;
    }
    if (w == 0) {
        w = 1234628;
    }
    set_seed_rng32Mwc_reseed(z, w);
}

// set seed from 32-bit value
// mixes up bits from the seed to produce a 64-bit seed
void RngSetSeed32_reseed(uint32_t seed) {

    uint32_t lobits = seed;
    uint32_t hibits = seed * 91 * 53;
    uint64_t seed64 = (uint64_t(hibits) << 32) + lobits;

    RngSetSeed64_reseed(seed64);
}

// set seed from current time and clock()
void RngSetSeedFromTime_reseed() {

    uint32_t lobits = uint32_t(clock()) + uint32_t(timeSec1970());
    uint32_t hibits = uint32_t(clock()) * 53 + uint32_t(timeSec1970()) * 91;
    uint64_t seed64 = ((uint64_t)hibits << 32) + lobits;

    RngSetSeed64_reseed(seed64);
}

// return the current seed
void RngGetSeed(uint32_t *z, uint32_t *w) {
    *z = m_z;
    *w = m_w;
}

}; // namespace RandomNumbers

#endif
