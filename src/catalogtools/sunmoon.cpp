///-*-mode:c++; mode:font-lock;-*-
///
/// @package phosim
/// @file sunmoon.cpp
/// @brief  Functions related to the sun and moon
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///
// *************************************************************************
// Most of this follows algorithms using pallib derived from moon.c by
// Robert Jedicke.
// see ifa.hawaii.edu/users/jedicke/cutility/html/moon_8h-source.html
// *************************************************************************
#include "sunmoon.h"

SunMoon::SunMoon() {
    // Nothing yet
}
// ***********************************************************************

SunMoon::~SunMoon() {
    // Nothing Yet
}
// ***********************************************************************

void SunMoon::Sun2000(double MJD, double *pSunRA2000Rad, double *pSunDec2000Rad)
// ************************************************************************
// Determines the geocentric equatorial J2000 coordinates of the Sun
// MJD time of the observation as a modified julian date
// pSunRA2000Rad,pSunDec2000Rad Geocentric equatorial J2000 coordinates of
//  the sun.
// ************************************************************************
{
    double pBarycentricVelocity[3];
    double pBarycentricPosition[3];
    double pHeliocentricVelocity[3];
    double pHeliocentricPosition[3];

    // *******************************************************
    // Get helio and barycentric position and velocity of the Earth
    // *******************************************************
    palEvp(MJD,
           2000.0,
           pBarycentricVelocity,
           pBarycentricPosition,
           pHeliocentricVelocity,
           pHeliocentricPosition);

    // *******************************************************
    // We actually want the geocentric position of the sun...
    // *******************************************************
    for (int i = 0; i < 3; i++) {
        pHeliocentricPosition[i] = -pHeliocentricPosition[i];
    }

    // *******************************************************
    // Convert heliocentric cartesian equatorial coordinate to spherical coords
    // *******************************************************
    double sunRARad;
    double sunDecRad;
    palDcc2s(pHeliocentricPosition, &sunRARad, &sunDecRad);

    // *******************************************************
    // RA is backwards...
    // *******************************************************
    sunRARad = palDranrm(PAL__D2PI + sunRARad);

    *pSunRA2000Rad = sunRARad;
    *pSunDec2000Rad = sunDecRad;
    return;

    // *******************************************************
    // Following is not used. Left here for reference
    // Calculate the heliocentric distance
    // peqSun->dr_au = v3Length ( &v3HeliocentricPosition );
    // *******************************************************
}
// ***********************************************************************

void SunMoon::MoonOfDate(double MJD, double *pMoonRARad, double *pMoonDecRad)
// ************************************************************************
// Determines the geocentric equatorial coordinates of the moon using pallib
// routines palDmoon & palDcc2s.
// MJD observation time is a modified julian date
// returns RA/Dec of the moon (in radians)
// ************************************************************************
{
    double pMoonCoordsVel[6]; // (x,y,z,dxdt,dydt,dzdt)

    // ******************************************************************
    // Get the of-date geocentric equatorial cartesian coords and velocity
    // of the moon.
    // ******************************************************************
    palDmoon(MJD, pMoonCoordsVel);

    // Pick up just the coords
    double pMoonCoords[3];
    for (int i = 0; i < 3; i++) {
        pMoonCoords[i] = pMoonCoordsVel[i];
    }

    // ******************************************************************
    // Convert cartesian to of-date equatorial coordinates
    // ******************************************************************
    double moonRARad;
    double moonDecRad;
    palDcc2s(pMoonCoords, &moonRARad, &moonDecRad);

    // RA is backwards... */
    moonRARad = palDranrm(PAL__D2PI + moonRARad);

    *pMoonRARad = moonRARad;
    *pMoonDecRad = moonDecRad;

    // *****************************************************
    // Following not used. Just left here for reference
    // Save the lunar distance as well */
    // peqMoonOfDate->dr_au = xyzSize ( (CARTESIAN*) v6 );
    // *****************************************************
    return;
}
// ***************************************************************************

void SunMoon::Moon2000(double MJD,
                       double *pMoonRA2000Rad,
                       double *pMoonDec2000Rad)
// ************************************************************************
// Determines the geocentric equatorial J2000 coordinates of the moon
// using pallib routine palDmoon, palPrenut, palDc62s, and .
// ************************************************************************
// MJD time of the observation as a modified julian date
// moonRA2000Rad,moonDec2000Rad Geocentric equatorial J2000 coordinates of
//  the moon.
// ************************************************************************
{
    double pMoonCoordsVelOfDate[6]; // (x,y,z,dxdt,dydt,dzdt)
    double pRotMatrix[3][3]; // Rotation matrix
    double pXYZMoon2000[3]; // J2000 cartesian co-ords of moon */

    // *********************************************************************
    // Get the of-date geocentric equatorial cartesian coords and velocity
    //   of the moon. */
    // *********************************************************************
    palDmoon(MJD, pMoonCoordsVelOfDate);

    // *********************************************************************
    // Get the precession & nutation matrix from mean equator & equinox
    // of-date to J2000.0 */
    // *********************************************************************
    palPrenut(2000.0, MJD, pRotMatrix);

    // *******************************************************************
    // Rotate the moon's of-date coords to J2000
    // *******************************************************************
    // Pick up just the coords
    double pMoonCoordsOfDate[3];
    for (int i = 0; i < 3; i++) {
        pMoonCoordsOfDate[i] = pMoonCoordsVelOfDate[i];
    }

    palDmxv(pRotMatrix, pMoonCoordsOfDate, pXYZMoon2000);

    // *******************************************************************
    // Convert cartesian to J2000 equatorial coordinates
    // *******************************************************************
    double moonRA2000Rad;
    double moonDec2000Rad;
    palDcc2s(pXYZMoon2000, &moonRA2000Rad, &moonDec2000Rad);

    // RA is backwards...
    moonRA2000Rad = palDranrm(PAL__D2PI + moonRA2000Rad);

    *pMoonRA2000Rad = moonRA2000Rad;
    *pMoonDec2000Rad = moonDec2000Rad;

    // 8***********************************************
    // Not used but keep as reference
    // Save the lunar distance as well */
    // peqMoon2000->dr_au = xyzSize ( &xyzMoon2000 );
    // ************************************************
    return;
}
// **************************************************************************

double SunMoon::MoonPhase(double MJD,
                          double *pSunRA2000Rad,
                          double *pSunDec2000Rad,
                          double *pMoonRA2000Rad,
                          double *pMoonDec2000Rad)
// ***************************************************************************
//  Determines the phase of the moon expressed as a percentage of
//  illumination. See RJ Logbook #4 p.84 for simple derivation.
//  returns: Phase of the moon expressed as a percentage illumination
// **************************************************************************
//  MJD of the observation as a modified julian date
//  pSunRA2000Rad,pSunDec2000Rad:   Pointers to Geocentric equatorial J2000
//                                  coordinates of the Sun(may be nullptr)
//  moonRA2000Rad,moonDec2000Rad:  Pointers to Geocentric equatorial J2000
//                                 coordinates of the moon(may be nullptr)
// **************************************************************************
{
    double dSeparation_rad;

    // ************************************************************
    // Get the J2000 equatorial coordinates of the Sun(if nullptr)
    // ************************************************************
    if (*pSunRA2000Rad <= 0.0 || *pSunDec2000Rad <= 0.0) {
        Sun2000(MJD, pSunRA2000Rad, pSunDec2000Rad);
    }
    // ************************************************************
    // Get the J2000 equatorial coordinates of the Moon
    // ************************************************************
    if (*pMoonRA2000Rad <= 0.0 || *pMoonDec2000Rad <= 0.0) {
        Moon2000(MJD, pMoonRA2000Rad, pMoonDec2000Rad);
    }
    // ************************************************************
    // Angular separation between the Sun and Moon
    // ************************************************************
    dSeparation_rad = palDsep(
        *pSunRA2000Rad, *pSunDec2000Rad, *pMoonRA2000Rad, *pMoonDec2000Rad);

    // ************************************************** *********
    // Convert angular separation into percentage illumination
    // ************************************************************
    return (1.0 - cos(dSeparation_rad)) / 2.0;
}
// ***********************************************************************
