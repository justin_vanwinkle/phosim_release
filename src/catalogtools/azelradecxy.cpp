//-*-mode:c++; mode:font-lock;-*-
/// @package phosim
/// @file stars_and_galaxies_generator.cpp
/// @brief General conversion class between camera coords (X,Y) to Az,El to RA
/// Dec
///         stars and/or galaxies
///
/// @brief Created by:
/// @author Glenn Sembroski (Purdue)
///
/// @brief Modified by:
/// @author
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "azelradecxy.h"

// Note: we use PAL__DPI from the palmac.h for pi!

using namespace std;

AzElRADecXY::AzElRADecXY(double EastLongitude, double Latitude) {
    SetLocation(EastLongitude, Latitude);
}
// **********************************************************************

AzElRADecXY::AzElRADecXY(double EastLongitude,
                         double Latitude,
                         double RASource2000,
                         double DecSource2000) {
    SetLocation(EastLongitude, Latitude);
    fRASource2000 = palDranrm(RASource2000);
    fDecSource2000 = palDrange(DecSource2000);
}
// **********************************************************************

AzElRADecXY::~AzElRADecXY() {
    // Nothing here
}
// **********************************************************************

void AzElRADecXY::SetLocation(double EastLongitude, double Latitude) {
    fEastLongitude = EastLongitude;
    fLatitude = Latitude;
    fCosLatitude = cos(fLatitude);
    fSinLatitude = sin(fLatitude);
}

void AzElRADecXY::XY2RADec2000(double fX,
                               double fY,
                               double MJD,
                               double RASource2000,
                               double DecSource2000,
                               double &fRA2000,
                               double &fDec2000) {
    fRASource2000 = palDranrm(RASource2000);
    fDecSource2000 = palDrange(DecSource2000);
    XY2RADec2000(fX, fY, MJD, fRA2000, fDec2000);
}

void AzElRADecXY::XY2RADec2000(
    double fX, double fY, double MJD, double &fRA2000, double &fDec2000)
// **************************************************************************
// This routone converts non-derotated camera plane coordinates(in deg)
// to RA,Dec 2000
// **************************************************************************
// Notes: VATime is by refereence so we don't construct and destruct a copy
// each time this is called. We don't change it. This is for speed reasons.
{
    // convert RA and Dec of the tangent point (always in epoch 2000) to Az Elev
    // Since the focal plane coordinates x,y are parallel and perpendicular to
    // the altitude direction (verticle from azimuth position on horizen to
    // zenith) respectivly, we first need to convert ra_src,dec_src coords to
    // azimuth,altitude coords: calculate our position and convert that back to
    // ra,dec.
    double fElSrc;
    double fAzSrc;
    RADec2000ToAzEl(fRASource2000, fDecSource2000, MJD, fAzSrc, fElSrc);

    // Now determine projection of x,y from focal plane tangent at az_trk and
    // alt_trk. Assume y axis is verticle with +y corresponding to increasing
    // altitude. Assume x axis is horizontal with +x coresponding to increasing
    // azimuth.These assumptions must be checked!~!!!!!!!!!!
    // *********************************************************************
    // 23/06/06 Michael Danial and others say both axis need to be reversed
    // +y corresponds to decreasing altitude
    // +x corresponds to decreasing azimuth
    // So done.
    // **********************************************************************
    //  double xi=(fX*PAL__DPI/180.);
    //  double eta=(fY*PAL__DPI/180.);

    double xi = -(fX * PAL__DPI / 180.);
    double eta = -(fY * PAL__DPI / 180.);

    // First the most probable source location

    double fAzXY, fElXY;

    palDtp2s(xi, eta, fAzSrc, fElSrc, &fAzXY, &fElXY);

    // now convert this az and el to RA2000 Dec2000 coords.

    AzEl2RADec2000(fAzXY, fElXY, MJD, fRA2000, fDec2000);
    return;
}
// ************************************************************************

void AzElRADecXY::AzEl2RADec2000(
    double fAz, double fEl, double MJD, double &fRA2000, double &fDec2000)
// ************************************************************************
// Convert Az and El at MJD to RaDec epoch 2000
// ************************************************************************
{
    double fHourangle;
    double fDecApparent;
    palDh2e(fAz, fEl, fLatitude, &fHourangle, &fDecApparent);

    // convert hour angle back to ra
    // double fLocalSidereal = palGmst(MJD) + fEastLongitude;
    // radians
    double fLocalSidereal = palGmst(MJD) + fEastLongitude + palEqeqx(MJD);
    double fRAApparent = (fLocalSidereal - fHourangle);

    // Convert the apparent RA and Dec to the RA Dec for epoch 2000
    palAmp(fRAApparent, fDecApparent, MJD, 2000, &fRA2000, &fDec2000);
    fRA2000 = palDranrm(fRA2000);
    fDec2000 = palDrange(fDec2000);
    return;
}
// ************************************************************************

void AzElRADecXY::RADec2000ToAzEl(double fRA2000,
                                  double fDec2000,
                                  double MJD,
                                  double &fSourceAz,
                                  double &fSourceElev)
// ************************************************************************
// Convert RA2000,Dec2000 at MJD to Az/Elev
// ************************************************************************
{
    // convert RA2000 and Dec2000 (always in epoch 2000) to apparent
    // Ra Dec
    double fRAApparent;
    double fDecApparent;
    palMap(
        fRA2000, fDec2000, 0, 0, 0, 0, 2000, MJD, &fRAApparent, &fDecApparent);
    // Get Hourangle.First we need to get the time into greenich mean solar
    // time. local siderial time: double fLocalSidereal = palGmst(MJD) +
    // fEastLongitude; radians
    double fLocalSidereal = palGmst(MJD) + fEastLongitude + palEqeqx(MJD);
    double fHourangle = palDranrm(fLocalSidereal) - fRAApparent;

    // convert fHourangle,fDecApparent coords to
    // azimuth,altitude coords
    palDe2h(fHourangle, fDecApparent, fLatitude, &fSourceAz, &fSourceElev);
    fSourceAz = palDranrm(fSourceAz);
    fSourceElev = palDrange(fSourceElev);
    return;
}
// **************************************************************************

//
void AzElRADecXY::RADec2000ToXY(double RA2000,
                                double Dec2000,
                                double MJD,
                                double RASource2000,
                                double DecSource2000,
                                double &X,
                                double &Y) {
    fRASource2000 = palDranrm(RASource2000);
    fDecSource2000 = palDrange(DecSource2000);
    RADec2000ToXY(RA2000, Dec2000, MJD, X, Y);
    return;
}

void AzElRADecXY::RADec2000ToXY(
    double fRA2000, double fDec2000, double MJD, double &fX, double &fY)
// **************************************************************************
// Find camera plane coordinates(in deg) at MJD for equitorial location
// RA2000,Dec2000.  It is assumed for this call that the
// RASource2000,DecSource2000 of the center of the camera has already been
// loaded.
// **************************************************************************
// Method is to convert to fRA2000,fDec2000 to  Az,El and then convert
// AZ,El to X/Y
// **************************************************************************
{
    // *******************************************************************
    // convert RA and Dec of the star point (always in epoch 2000) to Az Elev
    // Focal plane coordinates x,y are parallel and perpendicular to
    // the altitude direction (vertical from azimuth position on horizen to
    // zenith) respectivly
    // *********************************************************************
    double fAz;
    double fElev;
    RADec2000ToAzEl(fRA2000, fDec2000, MJD, fAz, fElev);
    AzElToXY(fAz, fElev, MJD, fX, fY);
    return;
}
// **************************************************************************

void AzElRADecXY::AzElToXY(double fAz,
                           double fElev,
                           double MJD,
                           double RASource2000,
                           double DecSource2000,
                           double &fX,
                           double &fY) {
    fRASource2000 = palDranrm(RASource2000);
    fDecSource2000 = palDrange(DecSource2000);
    AzElToXY(fAz, fElev, MJD, fX, fY);
    return;
}
// **************************************************************************

void AzElRADecXY::AzElToXY(
    double fAz, double fElev, double MJD, double &fX, double &fY)
/// **************************************************************************
// Find camera plane coordinates(in deg) at MJD for location
// Az,El.  It is assumed for this call that the
// RASource2000,DecSource2000 of the center of the camera has already been
// loaded.
// **************************************************************************
{
    // *******************************************************************
    // convert RA and Dec of the tangent point (always in epoch 2000) to Az Elev
    // Since the focal plane coordinates x,y are parallel and perpendicular to
    // the altitude direction (vertical from azimuth position on horizen to
    // zenith) respectivly, we first need to convert ra_src,dec_src coords to
    // azimuth,altitude coord
    // *********************************************************************
    double fElSrc;
    double fAzSrc;
    RADec2000ToAzEl(fRASource2000, fDecSource2000, MJD, fAzSrc, fElSrc);

    // **********************************************************************
    // Get xi,eta of 'star' in tangent plane.
    // **********************************************************************
    int j;
    double xi;
    double eta;
    palDs2tp(fAz, fElev, fAzSrc, fElSrc, &xi, &eta, &j);

    // ************************************************************************
    // Convert back to our X,Y which is in degrees.
    // ************************************************************************

    fX = -(xi * 180. / PAL__DPI);
    fY = -(eta * 180. / PAL__DPI);
    return;
}
// ********************************************************************

// *******************************************************************************
// Find the coordinates in the camera plane of a source with some Az/El
// values given some tracking coordinates in the Az/El system.
//
// Az_Rad Azimuth of the source (in radians) for which we want to know the
// coordinates in the camera plane
// El_Rad Elevation of the source (in radians) for which we want to know the
// coordinates in the camera plane
// TrackingAz_Rad Azimuth (in radians) of the tracking position (where the
// telescope is pointing)
// TrackingEl_Rad Elevation (in radians) of the tracking position (where the
// telescope is pointing)
//  X_Deg Camera plane coordinate of the source in the X direction (in degrees)
//  Y_Deg Camera plane coordinate of the source in the Y direction (in degrees)
// ******************************************************************************

void AzElRADecXY::AzElToXY(double Az_Rad,
                           double El_Rad,
                           double TrackingAz_Rad,
                           double TrackingEl_Rad,
                           double &X_Deg,
                           double &Y_Deg) {
    // Get xi,eta in tangent plane.
    int j = 0;
    double xi = 0;
    double eta = 0;
    palDs2tp(Az_Rad, El_Rad, TrackingAz_Rad, TrackingEl_Rad, &xi, &eta, &j);

    // Convert X,Y which must be in degrees.
    X_Deg = -(xi * 180 / PAL__DPI);
    Y_Deg = -(eta * 180 / PAL__DPI);
}
// *****************************************************************************

void AzElRADecXY::XYToAzEl(double X_Deg,
                           double Y_Deg,
                           double TrackingAz_Rad,
                           double TrackingEl_Rad,
                           double &Az_Rad,
                           double &El_Rad)
// ********************************************************************
// This method converts the X Y coordinates (given in degrees) to Az El
// values (in radians). The Az El tracking coordinates (in radins) must
// be provided.
// ********************************************************************
{

    // Now determine projection of x,y from focal plane tangent at the
    // tracking azimuth and elevation

    double xi = -(X_Deg * PAL__DPI / 180.);
    double eta = -(Y_Deg * PAL__DPI / 180.);

    double fAz = 0;
    double fEl = 0;

    palDtp2s(xi, eta, TrackingAz_Rad, TrackingEl_Rad, &fAz, &fEl);

    Az_Rad = fAz;
    El_Rad = fEl;
}
// ********************************************************************

void AzElRADecXY::XYToDlDmDn(double fX,
                             double fY,
                             double fAzSrc,
                             double fElSrc,
                             double &fDl,
                             double &fDm,
                             double &fDn)
// *********************************************************************
//  The fX,fY are in camera coordinates(degrees). Get the unit vector in ground
//  plane coords of the direction.
// Kascade definition X + east, y + south.
// Vegas definition X + east, y+ north and z + up
// Using VEGAS  Definition here
// ********************************************************************
{
    // Get the az,elev of this X,Y posistion.
    double xi = -(fX * PAL__DPI / 180.);
    double eta = -(fY * PAL__DPI / 180.);

    // First the az/elev of the  source location
    double fAzXY, fElXY;
    palDtp2s(xi, eta, fAzSrc, fElSrc, &fAzXY, &fElXY);

    fDn = cos(PAL__DPI / 2 - fElXY); // dn
    fDm = sin(PAL__DPI / 2 - fElXY) * cos(fAzXY); // dm
    fDl = sin(PAL__DPI / 2 - fElXY) * sin(fAzXY); // dl
    return;
}
// ********************************************************************

void AzElRADecXY::DlDmDnToAzEl(
    double Dl, double Dm, double Dn, double &fAz, double &fEl)
// **************************************************************************
//   Get the Az and Elevation(radians) of a vector X
// **************************************************************************
// Kascade definition X + east, y + south.
// Vegas definition X + east, y+ north and z + up
// USING VEGAS Definition here
//***************************************************************************
{
    double fLatitude;
    double fLongitude;
    double pfX[3];
    pfX[0] = Dl;
    pfX[1] = Dm;
    pfX[2] = Dn;
    palDcc2s(pfX, &fLongitude, &fLatitude);

    fEl = fLatitude;
    fAz = (PAL__DPI / 2) - fLongitude; // From 0 on X CCW axis to 0 On y axis CW
    fAz = palDranrm(fAz);
    fEl = palDrange(fEl);
    return;
}
// *************************************************************************

void AzElRADecXY::DlDmDnToXY(double Dl,
                             double Dm,
                             double Dn,
                             double fAzSrc,
                             double fElSrc,
                             double &fX,
                             double &fY)
// *********************************************************************
//  The fX,fY are in camera coordinates(degrees).
// ********************************************************************
{
    double fAz;
    double fElev;
    DlDmDnToAzEl(Dl, Dm, Dn, fAz, fElev);

    // **********************************************************************
    // Get xi,eta of 'star' in tangent plane.
    // **********************************************************************
    int j;
    double xi;
    double eta;
    palDs2tp(fAz, fElev, fAzSrc, fElSrc, &xi, &eta, &j);

    // ************************************************************************
    // Convert back to our X,Y which is in degrees.
    // ************************************************************************

    fX = -(xi * 180. / PAL__DPI);
    fY = -(eta * 180. / PAL__DPI);
    return;
}
// **************************************************************************

void AzElRADecXY::Derotate(double MJD,
                           double fX,
                           double fY,
                           double RASource2000,
                           double DecSource2000,
                           double &fXDerotated,
                           double &fYDerotated)
// ********************************************************************
//  The fX,fY are in camera coordinates(degrees). In order to combine data
//  from different runs (i.e. different sidereal times) they must be
//  de-rotated to the same hour angle.
// ********************************************************************
{
    fRASource2000 = palDranrm(RASource2000);
    fDecSource2000 = palDrange(DecSource2000);
    Derotate(MJD, fX, fY, fXDerotated, fYDerotated);
    return;
}
// ********************************************************************

void AzElRADecXY::Derotate(
    double MJD, double fX, double fY, double &fXDerotated, double &fYDerotated)
// *********************************************************************
//  The fX,fY are in camera coordinates(degrees). In order to combine data
//  from different runs (i.e. different sidereal times) they must be
//  de-rotated to the same hour angle.
// ********************************************************************
{
    // convert RA and Dec of the tangent point (always in epoch 2000) to Az Elev
    // Since the focal plane coordinates x,y are parallel and perpendicular to
    // the altitude direction (verticle from azimuth position on horizen to
    // zenith) respectivly, we first need to convert ra_src,dec_src coords to
    // azimuth,altitude coords:
    double fElSrc;
    double fAzSrc;
    RADec2000ToAzEl(fRASource2000, fDecSource2000, MJD, fAzSrc, fElSrc);

    // Now get a derotation angle.

    double fDerotangle =
        atan2(-1.0 * fCosLatitude * sin(fAzSrc),
              (cos(fElSrc) * fSinLatitude - sin(fElSrc) * cos(fAzSrc)));
    // Now derotate X and Y
    fXDerotated = fX * cos(fDerotangle) - fY * sin(fDerotangle);
    fYDerotated = fX * sin(fDerotangle) + fY * cos(fDerotangle);
    fXDerotated = -fXDerotated; // flip coordinate
    fYDerotated = -fYDerotated; // flip coordinate
    return;
}
// ***************************************************************************

std::string AzElRADecXY::RAToString(double fRA)
// ***************************************************************************
// Convert RA(radians) to a string. Used in print out.
// ***************************************************************************
{
    int fHourMinSec[4];
    char fSign[1];
    fRA = palDranrm(fRA);
    palDr2tf(1, fRA, fSign, fHourMinSec);
    ostringstream os;
    os << fHourMinSec[0] << ":" << fHourMinSec[1] << ":" << fHourMinSec[2]
       << "." << fHourMinSec[3] << endl;
    return os.str();
}
// ***************************************************************************

std::string AzElRADecXY::DecToString(double fDec)
// ***************************************************************************
// Convert Dec(radians) to a string. Used in print out.
// ***************************************************************************
{
    int fDegMinSec[4];
    char fSign[1];
    fDec = palDrange(fDec);
    palDr2af(1, fDec, fSign, fDegMinSec);
    ostringstream os;
    os << fDegMinSec[0] << ":" << fDegMinSec[1] << ":" << fDegMinSec[2] << "."
       << fDegMinSec[3] << endl;
    return os.str();
}
