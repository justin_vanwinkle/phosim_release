#include "constants.hpp"
#include "atmosphere.hpp"
#include "operator.hpp"

#include <ctime>

int main() {
    // Default values.
    std::string obshistid = "9999";
    double outerx = 500000.0;
    double pix = 256.0; // pixel size in cm
    int telconfig = 0;
    float constrainseeing = -1;

    double seeing;
    double constrainairglow = -1.0;
    char tempstring[4096];
    FILE *fptr;
    char outputfilename[4096];
    double zenith, altitude;
    long monthnum;
    long seed = 0;
    double tai = 0.0;

    int atmospheremode = 1;
    int opticsonlymode = 0;
    int turbulencemode = 1;
    int opacitymode = 1;
    long atmosphericDispersion = 1;
    int skipAtmosphere = 0;
    int spaceMode = -1;

    int haveMonth = 0;
    int haveTai = 0;
    int haveSunAlt = 0;
    double sunalt = -90.0;

    int haveMoonRa = 0;
    double moonra = 0.0;
    int haveMoonDec = 0;
    double moondec = 0.0;
    int haveMoonAlt = 0;
    double moonalt = 0.0;
    int haveMoonPhase = 0;
    double moonphase = 0.0;

    int havePointingRA = 0;
    double pointingra = 0.0;
    int havePointingDec = 0;
    double pointingdec = 0.0;
    int haveAltitude = 0;
    int haveAzimuth = 0;
    double azimuth = 0.0;
    int haveDistMoon = 0;
    double dist2moon = 0.0;

    int haveRotTelPos = 0;
    int haveRotSkyPos = 0;
    double rotTelPos = 0.0;
    double rotSkyPos = 0.0;

    int haveFilter = 0;
    int filter = 0;

    Atmosphere atmosphere;
    std::string mjdstring;
    int haveMjdString = 0;

    atmosphere.numlevel = 7;
    atmosphere.constrainclouds = -1.0;
    static int maxlevel = 100;
    std::vector<int> cloudscreen(maxlevel, 0);
    cloudscreen[1] = 1;
    cloudscreen[2] = 1;
    std::vector<float> outerscale(maxlevel, -1);

    // Set some default values.
    atmosphere.datadir = "../data";
    atmosphere.instrdir = "../data/lsst";
    std::string workdir(".");

    // Read parameters from stdin.
    readText pars(std::cin);
    for (size_t t(0); t < pars.getSize(); t++) {
        std::string line(pars[t]);
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        readText::get(line, "workdir", workdir);
        readText::get(line, "instrdir", atmosphere.instrdir);
        readText::get(line, "datadir", atmosphere.datadir);
        readText::get(line, "numlevel", atmosphere.numlevel);
        readText::get(line, "telconfig", telconfig);
        readText::get(line, "obshistid", obshistid);
        readText::get(line, "outerx", outerx);
        readText::get(line, "cloudscreen", cloudscreen);
        readText::get(line, "outerscale", outerscale);
        readText::get(line, "obsseed", seed);
        if (readText::getKey(line, "monthnum", monthnum)) {
            haveMonth = 1;
        }
        if (readText::getKey(line, "solaralt", sunalt)) {
            haveSunAlt = 1;
        }
        if (readText::getKey(line, "moonra", moonra)) {
            haveMoonRa = 1;
        }
        if (readText::getKey(line, "moondec", moondec)) {
            haveMoonDec = 1;
        }
        if (readText::getKey(line, "moonalt", moonalt)) {
            haveMoonAlt = 1;
        }
        if (readText::getKey(line, "phaseang", moonphase)) {
            haveMoonPhase = 1;
        }
        if (readText::getKey(line, "tai", tai)) {
            haveTai = 1;
        }
        if (readText::getKey(line, "pointingra", pointingra)) {
            havePointingRA = 1;
        }
        if (readText::getKey(line, "pointingdec", pointingdec)) {
            havePointingDec = 1;
        }
        if (readText::getKey(line, "azimuth", azimuth)) {
            haveAzimuth = 1;
        }
        if (readText::getKey(line, "moondist", dist2moon)) {
            haveDistMoon = 1;
        }
        if (readText::getKey(line, "tai", mjdstring)) {
            haveMjdString = 1;
        }
        if (readText::getKey(line, "rotationangle", rotSkyPos)) {
            haveRotSkyPos = 1;
        }
        if (readText::getKey(line, "spiderangle", rotTelPos)) {
            haveRotTelPos = 1;
        }
        if (readText::getKey(line, "filter", filter)) {
            haveFilter = 1;
        }
        readText::get(line, "atmospheremode", atmospheremode);
        readText::get(line, "opticsonlymode", opticsonlymode);
        readText::get(line, "atmosphericdispersion", atmosphericDispersion);
        readText::get(line, "spacemode", spaceMode);

        // check to see if atmosphere is off
        if (keyName == "cleareverything" || opticsonlymode == 1 ||
            atmospheremode == 0) {
            skipAtmosphere = 1;
        }
        if (keyName == "clearturbulence") {
            turbulencemode = 0;
        }
        if (keyName == "clearopacity") {
            opacitymode = 0;
        }
        if (turbulencemode == 0 && atmosphericDispersion == 0.0 &&
            opacitymode == 0) {
            skipAtmosphere = 1;
        }

        if (readText::getKey(line, "constrainseeing", seeing)) {
            constrainseeing = seeing / 2.35482;
        }
        if (readText::getKey(line, "altitude", altitude)) {
            haveAltitude = 1;
            zenith = 90 - altitude;
        }
        readText::get(line, "zenith", zenith);
        readText::getKey(line, "constrainclouds", atmosphere.constrainclouds);
        readText::getKey(line, "constrainairglow", constrainairglow);
    }

    std::cout << "-------------------------------------------------------------"
                 "-----------------------------"
              << std::endl
              << "Operator" << std::endl
              << "-------------------------------------------------------------"
                 "-----------------------------"
              << std::endl;

    // Get site location
    double latitude = 0.0, longitude = 0.0;
    std::string ss;
    ss = atmosphere.instrdir + "/location.txt";
    std::ifstream newStream(ss.c_str());
    if (newStream) {
        readText locationPars(atmosphere.instrdir + "/location.txt");
        for (size_t t(0); t < locationPars.getSize(); t++) {
            std::string line(locationPars[t]);
            readText::getKey(line, "latitude", latitude);
            readText::getKey(line, "longitude", longitude);
            readText::get(line, "groundlevel", atmosphere.groundlevel);
            if (spaceMode == -1) {
                readText::getKey(line, "spacemode", spaceMode);
            }
        }
    }
    if (spaceMode == -1) {
        spaceMode = 0;
    }

    if (spaceMode > 0) {
        latitude = 0;
        longitude = 0;
        skipAtmosphere = 1;
        // say spaceMode 1 = low eath orbit
        // 2 = LagrangePt. L1
        // 3=L2, (JWST) ...etc
    }

    // Get time
    if (haveTai == 0) {
    redo2:
        tai = 51544.5 + atmosphere.random.uniform() * 36525;
        double lstt = localSiderealTime(tai, longitude);
        double tsunAlt, tsunAz, tsunRa, tsunDec;
        sunPos(tai, &tsunRa, &tsunDec);
        raDectoAltAz(lstt, latitude, tsunRa, tsunDec, &tsunAlt, &tsunAz);
        if ((tsunAlt > -18) && (spaceMode == 0)) {
            goto redo2;
        }
    }
    if (mjdstring == "now" || mjdstring == "tonight") {
        time_t now = time(nullptr);
        struct tm tstruct {};
        tstruct = *gmtime(&now);
        long curryr = tstruct.tm_year + 1900;
        long currmo = tstruct.tm_mon + 1;
        long currdy = tstruct.tm_mday;
        long currhr = tstruct.tm_hour;
        long currmn = tstruct.tm_min;
        long currsc = tstruct.tm_sec;
        if ((currmo == 1) || (currmo == 2)) {
            curryr = curryr - 1;
            currmo = currmo + 12;
        }
        long curra = floor(curryr / 100);
        long currb = 2 - curra + floor(curra / 4);
        double currjd = floor(365.25 * (curryr + 4716)) +
                        floor(30.6001 * (currmo + 1)) + currdy + currb - 1524.5;
        double currmjd = currjd - 2400000.5;
        currmjd +=
            currhr / 24.0 + currmn / 24.0 / 60.0 + currsc / 24.0 / 60.0 / 60.0;
        tai = currmjd;
    redo:
        if (mjdstring == "tonight") {
            tai = currmjd + atmosphere.random.uniform();
        }
        double lstt = localSiderealTime(tai, longitude);
        double tsunAlt, tsunAz, tsunRa, tsunDec;
        sunPos(tai, &tsunRa, &tsunDec);
        raDectoAltAz(lstt, latitude, tsunRa, tsunDec, &tsunAlt, &tsunAz);
        if ((mjdstring == "tonight") && (tsunAlt > -18) && (spaceMode == 0)) {
            goto redo;
        }
        if ((mjdstring == "now") && (tsunAlt > -18) && (spaceMode == 0)) {
            std::cout << "Warning:  This observation is in daytime or twilight."
                      << std::endl;
        }
    }

    // Calendar Date
    long year, month, day;
    double frac;
    jdToCalendar(tai, &month, &day, &year, &frac);
    if (haveMonth == 1) {
        std::cout << "Warning Redundant Setting: Using the Month input as "
                  << month << " instead of " << monthnum << "." << std::endl;
    } else {
        monthnum = month;
    }

    // Day of year
    double dayOfYear = calculateDayOfYear(month, day, year, frac);

    // Local Sidereal Time
    double lst = localSiderealTime(tai, longitude);

    // Sun position
    double sunAlt, sunAz, sunRa, sunDec;
    sunPos(tai, &sunRa, &sunDec);
    raDectoAltAz(lst, latitude, sunRa, sunDec, &sunAlt, &sunAz);
    if (haveSunAlt == 1) {
        std::cout
            << "Warning Redundant Setting: Using the Sun Altitude input as "
            << sunalt << " instead of " << sunAlt << "." << std::endl;
    } else {
        sunalt = sunAlt;
    }

    atmosphere.random.setSeed32(seed);
    atmosphere.random.unwind(10000);
    double newra = 0.0, newdec = 0.0;

    if ((havePointingRA == 0) && (havePointingDec == 0) &&
        (haveAltitude == 0) && (haveAzimuth == 0)) {
        pointingra = atmosphere.random.uniform() * 360.0;
        pointingdec =
            (acos(2.0 * atmosphere.random.uniform() - 1.0) - PH_PI / 2.0) /
            DEGREE;
        raDectoAltAz(
            lst, latitude, pointingra, pointingdec, &altitude, &azimuth);
    } else {
        if (haveAltitude == 0) {
            altitude =
                (acos(0.5 * atmosphere.random.uniform() - 1.0) - PH_PI / 2.0) /
                DEGREE;
        }
        if (haveAzimuth == 0) {
            azimuth = atmosphere.random.uniform() * 360.;
        }
        altAztoRaDec(lst, latitude, altitude, azimuth, &newra, &newdec);
        if (havePointingRA == 0) {
            pointingra = newra;
        } else {
            std::cout << "Warning Redundant Setting: Using the Right Ascension "
                         "input as "
                      << pointingra << " instead of " << newra << "."
                      << std::endl;
        }
        if (havePointingDec == 0) {
            pointingdec = newdec;
        } else {
            std::cout
                << "Warning Redundant Setting: Using the Declination input as "
                << pointingdec << " instead of " << newdec << "." << std::endl;
        }
    }

    // Moon position
    double moonAlt, moonAz, moonRa, moonDec, newMoonPhase;
    moonPos(tai, &moonRa, &moonDec);
    raDectoAltAz(lst, latitude, moonRa, moonDec, &moonAlt, &moonAz);
    newMoonPhase = moonPhase(sunRa, sunDec, moonRa, moonDec);
    double distMoon = distSphere(pointingra, pointingdec, moonRa, moonDec);
    if (haveMoonRa == 1) {
        std::cout << "Warning Redundant Setting: Using the Moon RA input as "
                  << moonra << " instead of " << moonRa << "." << std::endl;
    } else {
        moonra = moonRa;
    }
    if (haveMoonDec == 1) {
        std::cout << "Warning Redundant Setting: Using the Moon Dec input as "
                  << moondec << " instead of " << moonDec << "." << std::endl;
    } else {
        moondec = moonDec;
    }
    if (haveMoonAlt == 1) {
        std::cout << "Warning Redundant Setting: Using the Moon Alt input as "
                  << moonalt << " instead of " << moonAlt << "." << std::endl;
    } else {
        moonalt = moonAlt;
    }
    if (haveMoonPhase == 1) {
        std::cout << "Warning Redundant Setting: Using the Moon Phase input as "
                  << moonphase << " instead of " << newMoonPhase << "."
                  << std::endl;
    } else {
        moonphase = newMoonPhase;
    }
    if (haveDistMoon == 1) {
        std::cout
            << "Warning Redundant Setting: Using the Moon Distance input as "
            << dist2moon << " instead of " << distMoon << "." << std::endl;
    } else {
        dist2moon = distMoon;
    }

    double ha = lst * 15.0 - pointingra;

    // rottelpos = rotator angle
    // rotskypos + rotator = parallactic angle
    double newRotSkyPos = 0.0;
    if (haveRotTelPos == 1) {
        newRotSkyPos = calculateRotSkyPos(ha, latitude, pointingdec, rotTelPos);
        if (haveRotSkyPos == 1) {
            std::cout << "Warning Redundant Setting: Using the Parallactic - "
                         "Rotator input as "
                      << rotSkyPos << " instead of " << newRotSkyPos
                      << std::endl;
        } else {
            rotSkyPos = newRotSkyPos;
        }
    } else {
        if (haveRotSkyPos == 1) {
            rotTelPos =
                calculateRotTelPos(ha, latitude, pointingdec, rotSkyPos);
        } else {
            rotTelPos = atmosphere.random.uniform() * 360.0;
            rotSkyPos =
                calculateRotSkyPos(ha, latitude, pointingdec, rotTelPos);
        }
    }

    // Filter
    int numberFilter = 0;
    char opticsFile[4096];
    int goodFilter = 1;
    while (goodFilter != 0) {
        sprintf(opticsFile,
                "%s/optics_%d.txt",
                atmosphere.instrdir.c_str(),
                numberFilter);
        std::ifstream f(opticsFile);
        goodFilter = static_cast<int>(f.good());
        if (goodFilter != 0) {
            numberFilter++;
        }
    }
    if (haveFilter == 1) {
        if ((filter < 0) || (filter > (numberFilter - 1))) {
            std::cout << "Filter/Optics Configuration is out of range."
                      << std::endl;
            exit(1);
        }
    } else {
        filter = floor(atmosphere.random.uniform() * numberFilter);
    }

    // Calculated inputs
    sprintf(
        outputfilename, "%s/obs_%s.pars", workdir.c_str(), obshistid.c_str());
    fptr = fopen(outputfilename, "a+");
    fprintf(fptr, "pointingra %lf\n", pointingra);
    fprintf(fptr, "pointingdec %lf\n", pointingdec);
    fprintf(fptr, "altitude %lf\n", altitude);
    fprintf(fptr, "azimuth %lf\n", azimuth);
    fprintf(fptr, "tai %lf\n", tai);
    fprintf(fptr, "moonra %lf\n", moonra);
    fprintf(fptr, "moondec %lf\n", moondec);
    fprintf(fptr, "phaseang %lf\n", moonphase);
    fprintf(fptr, "moondist %lf\n", dist2moon);
    fprintf(fptr, "moonalt %lf\n", moonalt);
    fprintf(fptr, "monthnum %ld\n", monthnum);
    fprintf(fptr, "solaralt %lf\n", sunalt);
    fprintf(fptr, "spiderangle %lf\n", rotTelPos);
    fprintf(fptr, "rotationangle %lf\n", rotSkyPos);
    fprintf(fptr, "filter %d\n", filter);
    fprintf(fptr, "dayofyear %lf\n", dayOfYear);

    fclose(fptr);

    std::cout << "-------------------------------------------------------------"
                 "-----------------------------"
              << std::endl;

    printf("[rightascension] Pointing Right Ascension (degrees)        %lf\n",
           pointingra);
    printf("[declination] Pointing Declination (degrees)               %lf\n",
           pointingdec);
    if (spaceMode == 0) {
        printf(
            "[altitude] Pointing Altitude (degrees)                     %lf\n",
            altitude);
        printf(
            "[azimuth] Pointing Azimuth (degrees)                       %lf\n",
            azimuth);
    }
    printf("[mjd] Modified Julian Date (days)                          %lf\n",
           tai);
    printf(
        "[date] Date (month/day/year/day-fraction)                  "
        "%02ld/%02ld/%2ld/%lf\n",
        monthnum,
        day,
        year,
        frac);
    printf("[rottelpos] Rotator Angle (degrees)                        %lf\n",
           rotTelPos);
    printf("[moonra] Moon Right Ascension (degrees)                    %lf\n",
           moonra);
    printf("[moondec] Moon Declination (degrees)                       %lf\n",
           moondec);
    printf("[moonphase] Moon Phase (percent)                           %lf\n",
           moonphase);
    printf("[moondist] Distance to Moon (degrees)                      %lf\n",
           dist2moon);
    printf("[moonalt] Altitude of Moon (degrees)                       %lf\n",
           moonalt);
    printf("[sunalt] Altitude of Sun (degrees)                         %lf\n",
           sunalt);
    printf("[rotskypos] Parallactic - Rotator Angle (degrees)          %lf\n",
           rotSkyPos);

    if (skipAtmosphere == 0) {

        std::cout << "---------------------------------------------------------"
                     "---------------------------------"
                  << std::endl
                  << "Atmosphere Creator" << std::endl
                  << "---------------------------------------------------------"
                     "---------------------------------"
                  << std::endl;

        atmosphere.osests.reserve(atmosphere.numlevel);
        atmosphere.altitudes.reserve(atmosphere.numlevel);
        atmosphere.jests.reserve(atmosphere.numlevel);

        sprintf(outputfilename,
                "%s/atmosphere_%s.pars",
                workdir.c_str(),
                obshistid.c_str());

        atmosphere.createAtmosphere(
            monthnum, constrainseeing, outputfilename, cloudscreen, seed, tai);

        // overwrite outer scales if they have been provided.
        for (int m(0); m < atmosphere.numlevel; m++) {
            if (outerscale[m] >= 0) {
                atmosphere.osests[m] = outerscale[m];
            }
        }

        fptr = fopen(outputfilename, "a+");
        for (int i = 0; i < atmosphere.numlevel; i++) {
            printf("Creating layer %d.\n", i);
            double outers = atmosphere.osests[i] * 100.0;
            sprintf(tempstring,
                    "%s/atmospherescreen_%s_%d",
                    workdir.c_str(),
                    obshistid.c_str(),
                    i);
            atmosphere.turb2d(
                seed * 10 + i, seeing, outerx, outers, zenith, 0.5, tempstring);
            fprintf(fptr,
                    "atmospherefile %d atmospherescreen_%s_%d\n",
                    i,
                    obshistid.c_str(),
                    i);
        }
        for (int i = 0; i < atmosphere.numlevel; i++) {
            if (cloudscreen[i] != 0) {
                double height =
                    (atmosphere.altitudes[i] - atmosphere.groundlevel) / 1000.0;
                sprintf(tempstring,
                        "%s/cloudscreen_%s_%d",
                        workdir.c_str(),
                        obshistid.c_str(),
                        i);
                atmosphere.cloud(seed * 10 + i, height, pix, tempstring);
                fprintf(fptr,
                        "cloudfile %d cloudscreen_%s_%d\n",
                        i,
                        obshistid.c_str(),
                        i);
            }
        }

        sprintf(tempstring,
                "%s/airglowscreen_%s",
                workdir.c_str(),
                obshistid.c_str());
        atmosphere.airglow(seed * 10, tempstring);

        atmosphere.random.setSeed32(seed);
        atmosphere.random.unwind(10000);
        if (constrainairglow > 0.0) {
            fprintf(fptr, "airglowpintensity %.3f\n", constrainairglow);
            fprintf(fptr, "airglowcintensity %.3f\n", constrainairglow);
        } else {
            fprintf(fptr,
                    "airglowpintensity %.3f\n",
                    (21.7 +
                     0.2 * atmosphere.random.normalCorrel(
                               tai, 500.0 / (24.0 * 3600.0)) +
                     2.5 * log10(cos(zenith * PH_PI / 180.0)) +
                     (0.50 +
                      0.50 * sin(2 * PH_PI * (tai - 54466.0) / (365.24 * 11.0) -
                                 PH_PI / 2.0))));
            fprintf(fptr,
                    "airglowcintensity %.3f\n",
                    (21.7 +
                     0.3 * atmosphere.random.normalCorrel(
                               tai + 10000, 500.0 / (24.0 * 3600.0)) +
                     2.5 * log10(cos(zenith * PH_PI / 180.0))));
        }
        fclose(fptr);
    }

    return (0);
}
