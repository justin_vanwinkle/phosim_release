#ifndef PHOSIM_INSTRUMENT_HPP
#define PHOSIM_INSTRUMENT_HPP

#include "constants.hpp"
#include "dlsm.hpp"
#include "instrumentfiles.hpp"
#include "random.hpp"
#include "readtext.hpp"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

InstrumentFiles instrumentFiles;

#endif
