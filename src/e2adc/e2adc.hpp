#ifndef PHOSIM_E2ADC_HPP
#define PHOSIM_E2ADC_HPP

#include "fits.hpp"
#include "random.hpp"

#include <sstream>
#include <string>
#include <vector>

class E2adc {

  public:
    void setup();
    void setHotpixels();
    void convertADC();
    void writeFitsImage();

    std::string instrdir;
    std::string focalplanefile;
    std::string instr;
    std::ostringstream infile;

    double nonlinear{};
    long wellDepth{};
    double parallelcte{};
    double serialcte{};
    int readorder{};
    std::vector<double> adcerr;
    long minx{}, miny{};
    long namp{};
    std::string obshistid;
    long exposureid{};
    std::string chipid;
    long obsseed{};
    std::string devmaterial;
    std::string devtype;
    std::string devmode;
    double devvalue{};
    double vistime{};
    double exptime{};
    long nsnap{};
    int ngroups{};
    int nframes{};
    int nskip{};
    int filter{};
    long sequenceMode{};
    Random random;

    // individual amp
    std::vector<std::string> outchipid;
    std::vector<long> outminx;
    std::vector<long> outmaxx;
    std::vector<long> outminy;
    std::vector<long> outmaxy;
    std::vector<long> nx;
    std::vector<long> ny;
    std::vector<int> parallelread;
    std::vector<int> serialread;
    std::vector<int> parallelPrescan;
    std::vector<int> serialOverscan;
    std::vector<int> serialPrescan;
    std::vector<int> parallelOverscan;
    std::vector<double> bias;
    std::vector<double> gain;
    std::vector<double> readnoise;
    std::vector<double> darkcurrent;
    std::vector<double> hotpixelrate;
    std::vector<double> hotcolumnrate;
    std::vector<std::vector<std::vector<unsigned short> > > fullReadoutMap;
    std::vector<std::vector<std::vector<unsigned long> > > fullReadoutMapL;
    std::vector<std::vector<float> > crosstalk;
    std::vector<int> dataBit;

    long onaxes[2]{};
    fitsfile *foptr{};
    std::vector<std::vector<float> > emap;
    std::vector<float> adcmap;
    std::vector<unsigned int> hotpixelmap;
    int flatdir{};
    int tarfile{};
};

#endif
