#ifndef PHOSIM_CONSTANTS_HPP
#define PHOSIM_CONSTANTS_HPP

const double PH_PI = 3.141592653589793238462643;
const double ARCSEC = 0.0000048413681109536;
const double DEGREE = 0.017453292519943295;
const double HALFSQ5M1 = 0.618034;
const double HALF3MSQ5 = 0.381966;

// PHYSICS

const double EPSILON_0 = 8.85419e-14; // Farads cm^-1
const double E_CHARGE = 1.6022e-19; // Coloumbs
const double K_BOLTZMANN = 1.38066e-23; // Joules K^-1
const double EPSILON_SI = 11.7; // Relative permittivity for Si
const double E_RADIUS = 2.81794e-13; // cm
const double H_CGS = 6.6260755e-27; // ergs s
const double C_CGS = 2.99792458e10; // cm s^-1

// ASTRONOMY

const double RADIUS_EARTH = 6371.0; // km
const double EARTH_SUN = 149597870.700; // km
const double EARTH_MOON = 384403.0; // km

#endif
