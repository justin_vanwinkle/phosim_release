///
/// @package phosim
/// @file readtext.h
/// @brief class to extract parameters from text files
///
/// @brief Created by
/// @author En-Hsing Peng (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#ifndef PHOSIM_READTEXT_HPP
#define PHOSIM_READTEXT_HPP

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>

class readText {
  public:
    readText(std::istream &inStream) {
        readStream(inStream);
    }

    readText(const std::string &txtFile) {
        std::ifstream inStream(txtFile.c_str());
        if (!inStream) {
            std::cout << "Error reading " << txtFile << std::endl;
            exit(1);
        }
        readStream(inStream);
    }

    void readStream(std::istream &inStream) {
        std::string line;
        while (getLine(inStream, line)) {
            m_data.push_back(line);
        }
    }

    const std::string &operator[](const size_t index) const {
        return m_data[index];
    }

    bool static goodLine(std::string &line) {
        // ignore empty lines, lines starting with #, and all chars following #
        std::string::size_type idx = line.find('#');
        if (idx == 0) {
            return false;
        }
        if (idx != std::string::npos) {
            line = line.substr(0, idx - 1);
        }
        if (line.empty()) {
            return false;
        }
        return true;
    }

    static std::istream &getLine(std::istream &inStream, std::string &line) {
        if (std::getline(inStream, line)) {
            while (!goodLine(line)) {
                if (!std::getline(inStream, line)) {
                    break;
                }
            }
        }
        return inStream;
    }

    static bool findKey(const std::string &line, const std::string &key) {
        // *********************************************************************
        // See if first string in line is the key we are looking for
        // *********************************************************************
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        return keyName == key;
    }
    // ************************************************************************

    static void readCol(const std::string &txtFile,
                        std::vector<double> &col1,
                        std::vector<double> &col2) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line;
            while (getLine(inStream, line)) {
                std::istringstream iss(line);
                double d1, d2;
                iss >> d1 >> d2;
                col1.push_back(d1);
                col2.push_back(d2);
            }
        } else {
            std::cout << "Error reading " + txtFile << std::endl;
            exit(1);
        }
    }

    static void
    readColArr(const std::string &txtFile, double *&col1, double *&col2) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line;
            size_t m(0);
            while (getLine(inStream, line)) {
                m++;
            }
            col1 = new double[m];
            col2 = new double[m];
            inStream.clear();
            inStream.seekg(0);
            size_t i(0);
            while (getLine(inStream, line)) {
                std::istringstream iss(line);
                iss >> col1[i] >> col2[i];
                i++;
            }
        } else {
            std::cout << "Error reading " + txtFile << std::endl;
            exit(1);
        }
    }

    static void readMultiCol(const std::string &txtFile,
                             std::vector<double> &col1,
                             std::vector<std::vector<double>> &col2) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line;
            while (getLine(inStream, line)) {
                std::istringstream iss(line);
                double d1, v;
                std::vector<double> d2;
                iss >> d1;
                col1.push_back(d1);
                while (iss >> v) {
                    d2.push_back(v);
                }
                col2.push_back(d2);
            }
        } else {
            std::cout << "Error reading " + txtFile << std::endl;
            exit(1);
        }
    }

    static void
    readMultiColArr(const std::string &txtFile, double *&col1, double **&col2) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line;
            size_t m(0);
            size_t n(0);
            while (getLine(inStream, line)) {
                if (m == 0) {
                    double v;
                    std::istringstream iss(line);
                    while (iss >> v) {
                        n++;
                    }
                }
                m++;
            }
            col1 = new double[m];
            col2 = new double *[m];
            for (size_t i(0); i < m; i++) {
                col2[i] = new double[n];
            }
            inStream.clear();
            inStream.seekg(0);
            size_t i(0);
            while (getLine(inStream, line)) {
                std::istringstream iss(line);
                iss >> col1[i];
                for (size_t j(0); j < n; j++) {
                    iss >> col2[i][j];
                }
                i++;
            }
        } else {
            std::cout << "Error reading " + txtFile << std::endl;
            exit(1);
        }
    }

    static void readSegmentation(const std::string &txtFile,
                                 const std::string &key,
                                 std::vector<std::string> &value) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line;
            while (getLine(inStream, line)) {
                std::istringstream iss(line);
                std::string keyName;
                iss >> keyName;
                if (keyName == key) {
                    int i(0);
                    int numAmplifiers;
                    iss >> numAmplifiers;
                    value.resize(numAmplifiers);
                    while (getLine(inStream, value[i])) {
                        i++;
                        if (i == numAmplifiers) {
                            break;
                        }
                    }
                    break;
                }
            }
        } else {
            std::cout << "Error reading " + txtFile << std::endl;
            exit(1);
        }
    }

    void add(const std::string &line) {
        m_data.push_back(line);
    }
    size_t getSize() {
        return m_data.size();
    }

    template <class T>
    static bool
    getkey(const std::string &line, const T &key, std::string &value) {
        std::istringstream iss(line);
        T keyName;
        iss >> keyName;
        if (keyName == key) {
            std::getline(iss, value);
            return true;
        }
        return false;
    }

    template <class T>
    static bool
    getKey(const std::string &line, const std::string &key, T &value) {
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        if (keyName == key) {
            iss >> value;
            return true;
        }
        return false;
    }

    template <class T>
    static bool getKeyVector(const std::string &line,
                             const std::string &key,
                             std::vector<T> &value) {
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        if (keyName == key) {
            size_t i;
            iss >> i;
            if (i >= value.size()) {
                value.resize(i + 1);
            }
            iss >> value[i];
            return true;
        }
        return false;
    }

    template <class T>
    static bool getKeyMatrix(const std::string &line,
                             const std::string &key,
                             std::vector<std::vector<T>> &value) {
        std::istringstream iss(line);
        std::string keyName;
        iss >> keyName;
        if (keyName == key) {
            int i, j;
            iss >> i >> j;
            iss >> value[i][j];
            return true;
        }
        return false;
    }

    template <class T>
    static void get(const std::string &line, const std::string &key, T &value) {
        (void)getKey<T>(line, key, value);
    };

    template <class T>
    static void get(const std::string &line,
                    const std::string &key,
                    std::vector<T> &value) {
        (void)getKeyVector<T>(line, key, value);
    };

    template <class T>
    static void get(const std::string &line,
                    const std::string &key,
                    std::vector<std::vector<T>> &value) {
        (void)getKeyMatrix<T>(line, key, value);
    };

    template <class T>
    static std::string get(const std::string &txtFile, const T &key) {
        std::ifstream inStream(txtFile.c_str());
        if (inStream.is_open()) {
            std::string line, value;
            while (getLine(inStream, line)) {
                if (getkey(line, key, value)) {
                    break;
                }
            }
            return value;
        }
        std::cout << "Error reading " + txtFile << std::endl;
        exit(1);
    }

    void setLine(int index, std::string &line) {
        m_data.at(index) = line;
    }

    void eraseLine(int index) {
        m_data.erase(m_data.begin() + index);
    }

  private:
    std::vector<std::string> m_data;
};

#endif
