#include "raytrace/image.hpp"

int main() {
    Image im;

    im.parser();
    im.background();
    im.atmSetup();
    im.telSetup();
    im.sourceLoop();
    im.cleanup();

    return (0);
}
